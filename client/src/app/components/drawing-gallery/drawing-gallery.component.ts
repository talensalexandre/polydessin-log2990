import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, EventEmitter,
    OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { MatChipInputEvent, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { map } from 'rxjs/operators';
import { DrawingStored } from 'src/app/interfaces-enums/drawing';
import { States } from 'src/app/interfaces-enums/states';
import { DrawingGalleryService } from 'src/app/services/drawing-gallery/drawing-gallery.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

const WAIT_3SEC = 3000;

@Component({
    selector: 'app-drawing-gallery',
    templateUrl: './drawing-gallery.component.html',
    styleUrls: ['./drawing-gallery.component.scss']
})
export class DrawingGalleryComponent implements AfterViewInit, OnDestroy {
    readonly openEntryPoint: EventEmitter<undefined>;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
    labels: string[];
    drawings: DrawingStored[];
    drawingsToDisplay: DrawingStored[];
    loading: boolean;
    viewActive: boolean;

    @ViewChildren('preview') preview: QueryList<ElementRef<SVGElement>>;

    constructor(public dialogRef: MatDialogRef<DrawingGalleryComponent>,
                private dialog: MatDialog,
                private galleryService: DrawingGalleryService,
                private svgHandler: SvgHandlerService,
                private snackBar: MatSnackBar,
                private saveManager: SaveManagerService,
                private stateSelector: StateSelectorService,
                private cdRef: ChangeDetectorRef) {
        this.loading = true;
        this.openEntryPoint = new EventEmitter();
        this.drawings = new Array<DrawingStored>();
        this.drawingsToDisplay = new Array<DrawingStored>();
        this.labels = new Array<string>();
        this.viewActive = true;
    }

    ngAfterViewInit(): void {
        this.getAllDrawings();
    }

    ngOnDestroy(): void {
        this.viewActive = false;
    }

    private getAllDrawings(): void {
        this.saveManager.getDrawings().pipe(map((canvas: DrawingStored[] | number) => {
            if (canvas === 0) {
                this.openSnackBar('Désolé, le serveur est innaccessible.', 'red-snackbar');
                this.dialogRef.close();
                this.openEntryPoint.emit(undefined);
                return;
            }
            this.drawings = canvas as DrawingStored[];
            this.drawingsToDisplay = canvas as DrawingStored[];
            this.displayDrawings(this.drawingsToDisplay);
        })).subscribe();
    }

    private displayDrawings(drawings: DrawingStored[]): void {
        if (drawings.length === 0) {
            this.openSnackBar('Pas de dessin trouvé !', 'red-snackbar');
            this.loading = false;
            return;
        }
        if (this.viewActive) {
            this.cdRef.detectChanges();
        }
        const svgArray = this.galleryService.createElementFromString(drawings);
        this.preview.forEach((element, index) => {
            this.galleryService.displayElement(element.nativeElement, svgArray, index);
        });
        this.loading = false;
    }

    private updateDrawings(labels: string[]): void {
        this.drawingsToDisplay = new Array<DrawingStored>();
        labels.forEach((labelsValue) => {
            this.drawings.forEach((drawingValue) => {
                if (drawingValue.metaDrawing.labels.includes(labelsValue) &&
                    !this.drawingsToDisplay.includes(drawingValue)) {
                    this.drawingsToDisplay.push(drawingValue);
                }
            });
        });
    }

    addLabel(event: MatChipInputEvent): void {
        this.loading = true;
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            this.labels.push(value.trim());
            this.updateDrawings(this.labels);
            this.displayDrawings(this.drawingsToDisplay);
        } else {
            this.loading = false;
        }
        input.value = '';
    }

    removeLabel(label: string): void {
        this.loading = true;
        const index = this.labels.indexOf(label);
        if (index >= 0) {
            this.labels.splice(index, 1);
        }
        this.updateDrawings(this.labels);
        this.labels.length === 0 ? this.getAllDrawings() : this.displayDrawings(this.drawingsToDisplay);
    }

    openDrawing(drawing: DrawingStored): void {
        if (!this.svgHandler.svgCanvas || this.svgHandler.svgCanvas.childNodes.length === 0) {
            this.createDrawing(drawing);
            return;
        }
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            disableClose: true,
            autoFocus: false,
            data: {
                message: 'Toutes les modifications du dessin en cours seront perdues. Voulez-vous continuer ?'
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.createDrawing(drawing);
            }
        });
    }

    private createDrawing(drawing: DrawingStored): void {
        this.galleryService.createDrawing(drawing);
        this.dialogRef.close();
        this.stateSelector.changeState(States.selection, false);
        this.openSnackBar('Dessin ouvert avec succès !', 'snackbar');
    }

    private openSnackBar(message: string, style: string): void {
        this.snackBar.open(message, 'OK', {
            duration: WAIT_3SEC,
            panelClass: [style]
        });
    }

    removeDrawing(drawing: DrawingStored): void {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            disableClose: true,
            autoFocus: false,
            data: {
                message: 'Voulez vous vraiment supprimer ce dessin ?'
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (!confirmed) { return; }
            this.saveManager.deleteDrawing(String(drawing.metaDrawing.timestamp)).pipe(map((response: DrawingStored | number) => {
                response === 0 ?
                    this.openSnackBar('Ce dessin à deja été supprimé ou le serveur est inaccessible.', 'red-snackbar') :
                    this.openSnackBar('Dessin supprimé avec succès !', 'snackbar');
                this.drawings.splice(this.drawings.indexOf(drawing), 1);
                this.removeLabel('');
            })).subscribe();
        });
    }

    closeDialog(): void {
        this.dialogRef.close();
        this.openEntryPoint.emit(undefined);
    }
}
