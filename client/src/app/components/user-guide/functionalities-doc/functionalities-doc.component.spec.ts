import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatIconModule } from '@angular/material';
import { FunctionalitiesDocComponent } from './functionalities-doc.component';

describe('FunctionalitiesDocComponent', () => {
    let component: FunctionalitiesDocComponent;
    let fixture: ComponentFixture<FunctionalitiesDocComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule, MatIconModule],
            declarations: [ FunctionalitiesDocComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FunctionalitiesDocComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
