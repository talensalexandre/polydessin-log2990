export interface IEnvironment {
    production: boolean;
    SEND_URL: string;
    GET_URL: string;
    GET_ALL_URL: string;
    DELETE_URL: string;
    SEND_EMAIL_URL: string;
}
