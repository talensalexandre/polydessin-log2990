import { TestBed } from '@angular/core/testing';

import { States } from '../../interfaces-enums/states';
import { StateSelectorService } from './state-selector.service';

describe('StateSelectorService', () => {
    let service: StateSelectorService;
    let sidenavOpen: boolean;

    beforeEach(() => {TestBed.configureTestingModule({}); });

    beforeEach(() => {
        service = TestBed.get(StateSelectorService);
        sidenavOpen = false;
      });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should change state', () => {
        sidenavOpen = service.changeState(States.brush, sidenavOpen);
        expect(service.stateSelected).toBe(States.brush);

        sidenavOpen = service.changeState(States.line, sidenavOpen);
        expect(service.stateSelected).toBe(States.line);
    });

    it('should open/close side nav', () => {
        sidenavOpen = service.changeState(States.brush, sidenavOpen);
        expect(sidenavOpen).toBeTruthy();

        sidenavOpen = service.changeState(States.brush, sidenavOpen);
        expect(sidenavOpen).toBeFalsy();
    });

    it('should open documentation', () => {
        sidenavOpen = service.changeState(States.documentation, sidenavOpen);
        expect(service.stateSelected).toBe(States.documentation);
    });
});
