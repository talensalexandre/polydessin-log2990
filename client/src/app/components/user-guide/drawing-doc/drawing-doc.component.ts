import { Component } from '@angular/core';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';

@Component({
    selector: 'app-drawing-doc',
    templateUrl: './drawing-doc.component.html',
    styleUrls: ['./drawing-doc.component.scss']
})
export class DrawingDocComponent {

    constructor(public userGuideService: UserGuideService) { }
}
