import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ElementType } from '../../interfaces-enums/element-type';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { PolygonHandlerService } from './polygon-handler.service';

// tslint:disable:no-magic-numbers

const EXPECTED_SQUARE_COORDS = [
    {
        x: 15,
        y: 12.5
    },
    {
        x: 12.5,
        y: 15
    },
    {
        x: 15,
        y: 17.5
    },
    {
        x: 17.5,
        y: 15
    },
    {
        x: 15,
        y: 12.5
    }
];

describe('PolygonHandlerService', () => {
    let service: PolygonHandlerService;
    let polygonMock: jasmine.SpyObj<SVGElement>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    const colorManager = new ColorManagerService();
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'clickOnObject',
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        polygonMock = jasmine.createSpyObj('HTMLElement', ['getAttribute']);
        service = new PolygonHandlerService(svgHandlerServiceSpy, colorManager,
            clickerHandlerSpy, undoRedoSpy);
    });

    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should compute coords for a square with drag and drop from up', () => {
        const SIDES = 4;
        const THICKNESS = 5;
        const startCoords = {
            x: 10, y: 10
        };
        const endCoords = {
            x: 20, y: 20
        };
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(EXPECTED_SQUARE_COORDS);
    });

    it('should compute coords for a square with drag and drop from down', () => {
        const SIDES = 4;
        const THICKNESS = 5;
        const endCoords = {
            x: 10, y: 10
        };
        const startCoords = {
            x: 20, y: 20
        };
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(EXPECTED_SQUARE_COORDS);
    });

    it('should compute coords for a square with height limit', () => {
        const SIDES = 4;
        const THICKNESS = 5;
        const startCoords = {
            x: 10, y: 10
        };
        const endCoords = {
            x: 30, y: 20
        };
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(EXPECTED_SQUARE_COORDS);
    });

    it('should compute coords for a square with width limit', () => {
        const SIDES = 4;
        const THICKNESS = 5;
        const startCoords = {
            x: 10, y: 10
        };
        const endCoords = {
            x: 20, y: 30
        };
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(EXPECTED_SQUARE_COORDS);
    });

    it('should compute coords for a polygon with 6 sides', () => {
        const SIDES = 6;
        const THICKNESS = 5;
        const startCoords = {
            x: 10, y: 10
        };
        const endCoords = {
            x: 20, y: 20
        };
        const expectedPolygonCords = [
            {
                x: 14.665063509461097,
                y: 12.5
            },
            {
                x: 12.500000000000002,
                y: 13.75
            },
            {
                x: 12.5,
                y: 16.25
            },
            {
                x: 14.665063509461097,
                y: 17.5
            },
            {
                x: 16.830127018922195,
                y: 16.25
            },
            {
                x: 16.830127018922195,
                y: 13.750000000000002
            },
            {
                x: 14.665063509461097,
                y: 12.5
            }
        ];
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(expectedPolygonCords);
    });

    it('should compute coords for a triangle with width limit', () => {
        const SIDES = 3;
        const THICKNESS = 5;
        const startCoords = {
            x: 10, y: 10
        };
        const endCoords = {
            x: 100, y: 95
        };
        const expectedPolygonCords = [
            {
                x: 54.99999999999999,
                y: 12.5
            },
            {
                x: 12.499999999999993,
                y: 86.11215932167728
            },
            {
                x: 97.5,
                y: 86.1121593216773
            },
            {
                x: 54.99999999999999,
                y: 12.5
            }
        ];
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(expectedPolygonCords);
    });

    it('should not compute coords if sides is beyound limits', () => {
        let SIDES = 2;
        const THICKNESS = 5;
        const endCoords = {
            x: 10, y: 10
        };
        const startCoords = {
            x: 20, y: 20
        };
        const expectedPolygonCords = new Array();
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(expectedPolygonCords);
        SIDES = 13;
        expect(service.getCoordinatesForPolygon(startCoords, endCoords, SIDES, THICKNESS))
            .toEqual(expectedPolygonCords);
    });

    it('should remove all coordinates from polygon', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polygon');
        fakeElement.setAttribute('points', '100,110 120,130 150,160');
        service.removeAllCoordinatesFromPolygon(fakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
    });

    it('should add coordinates to polygon', () => {
        const newCoordinates = { x: 150, y: 120 };
        service.addCoordinatesToPolygon(polygonMock, newCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(polygonMock.getAttribute).toHaveBeenCalled();
    });

    it('should create a polygon with fill true', () => {
        const fill = true;
        const stroke = true;
        const size = 10;
        service.createElement(fill, stroke, size, ElementType.polygon);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(5);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a polygon with fill false', () => {
        const fill = false;
        const stroke = false;
        const size = 10;
        service.createElement(fill, stroke, size, ElementType.polygon);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(5);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill false and no oldchild', () => {
        const fill = false;
        const stroke = true;
        const size = 10;
        service.elementPrevisualization(fill, stroke, size, ElementType.polygon, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill true and no oldchild', () => {
        const fill = true;
        const stroke = false;
        const size = 10;
        service.elementPrevisualization(fill, stroke, size, ElementType.polygon, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill true and oldchild', () => {
        const fill = true;
        const stroke = true;
        const size = 10;
        const oldChildMock: SVGElement = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.polygon, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should create a rectangle previsualization with fill false and oldchild', () => {
        const fill = false;
        const stroke = false;
        const size = 10;
        const oldChildMock: SVGElement = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.polygon, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should compute correctly isShapeTooBigComparedToSize ', () => {
        let isTooBig = service.isShapeTooBigComparedToSize({x: 10, y: 10}, {x: 20, y: 20}, 2);
        expect(isTooBig).toBeFalsy();
        isTooBig = service.isShapeTooBigComparedToSize({x: 10, y: 10}, {x: 20, y: 20}, 6);
        expect(isTooBig).toBeTruthy();
    });
});
