import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatIconModule } from '@angular/material';
import { DrawingDocComponent } from './drawing-doc.component';

describe('DrawingDocComponent', () => {
    let component: DrawingDocComponent;
    let fixture: ComponentFixture<DrawingDocComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule, MatIconModule],
            declarations: [ DrawingDocComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DrawingDocComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
