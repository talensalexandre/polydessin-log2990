import { Injectable } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { NavigationArrows } from 'src/app/interfaces-enums/navigation-arrows';
import { Coordinates } from '../../../interfaces-enums/coordinates';
import { MouseHandlerService } from '../../mouse-handler/mouse-handler.service';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';
import { SelectionSvgHandlerService } from '../selection-svg-handler/selection-svg-handler.service';
import { SelectionTransformService } from '../selection-transform/selection-transform.service';

const INITIAL_COORDS = -1;
const NEGATIVE_3_PX = -3;
const POSITIVE_3_PX = 3;
const SHORT_WAIT = 100;
const LONG_WAIT = 400;
const NUMBER_OF_ELEMENTS = 6;
const FIRST_INDEX = 0;
const SECOND_INDEX = 1;
const THIRD_INDEX = 2;
const FORTH_INDEX = 3;
const FIFTH_INDEX = 4;
const SIXTH_INDEX = 5;

@Injectable({
    providedIn: 'root',
})
export class SelectionMoveService {
    private oldCoordinates: Coordinates;
    private keysPressed: Map<NavigationArrows, boolean>;
    private keysUnsubscribe: Map<NavigationArrows, Subscription>;

    constructor(
        private selectionCalculator: SelectionCalculatorService,
        private selectionSvgHandler: SelectionSvgHandlerService,
        private mouseHandler: MouseHandlerService,
        private selectionTransform: SelectionTransformService,
        private svgHandler: SvgHandlerService,
        private undoRedo: UndoRedoService,
        private saveManager: SaveManagerService
    ) {
        this.oldCoordinates = { x: INITIAL_COORDS, y: INITIAL_COORDS };
        this.keysPressed = new Map<NavigationArrows, boolean>();
        this.keysPressed
            .set(NavigationArrows.up, false)
            .set(NavigationArrows.down, false)
            .set(NavigationArrows.left, false)
            .set(NavigationArrows.right, false);
        this.keysUnsubscribe = new Map<NavigationArrows, Subscription>();
    }

    translate(element: SVGElement, coordinates: Coordinates): void {
        const newMatrix = new Array<number>();
        const newTranslate = [1, 0, 0, 1, coordinates.x, coordinates.y];
        const oldMatrix = this.selectionTransform.getOldMatrix(element);
        const tempMatrix = new Array<number>();
        for (let i = 0 ; i < NUMBER_OF_ELEMENTS; i++) {
            tempMatrix[i] = +oldMatrix[i + 1];
        }
        newMatrix[FIRST_INDEX] = tempMatrix[FIRST_INDEX];
        newMatrix[SECOND_INDEX] = tempMatrix[SECOND_INDEX];
        newMatrix[THIRD_INDEX] = tempMatrix[THIRD_INDEX];
        newMatrix[FORTH_INDEX] = tempMatrix[FORTH_INDEX];
        newMatrix[FIFTH_INDEX] = tempMatrix[FIFTH_INDEX] + newTranslate[FIFTH_INDEX];
        newMatrix[SIXTH_INDEX] = tempMatrix[SIXTH_INDEX] + newTranslate[SIXTH_INDEX];
        this.selectionTransform.appendTransformationToElements(element, newMatrix);
    }

    private checkLimits(coordinates: Coordinates): Coordinates {
        const boundaryElement = this.selectionSvgHandler.selectionRectangles.get('boundary');
        const clientRectTop = boundaryElement ? boundaryElement.getBoundingClientRect().top : INITIAL_COORDS;
        const clientRectLeft = boundaryElement ? boundaryElement.getBoundingClientRect().left : INITIAL_COORDS;
        const clientRectBottom = boundaryElement ? boundaryElement.getBoundingClientRect().bottom : INITIAL_COORDS;
        const clientRectRight = boundaryElement ? boundaryElement.getBoundingClientRect().right : INITIAL_COORDS;
        const canvasBoundingRect = this.svgHandler.svgCanvas.getBoundingClientRect();
        const touchingTop = clientRectTop < POSITIVE_3_PX + canvasBoundingRect.top && coordinates.y < 0;
        const touchingBottom =
            (clientRectBottom > this.svgHandler.canvasHeight - POSITIVE_3_PX + canvasBoundingRect.top) && (coordinates.y > 0);
        const touchingLeft = (clientRectLeft < canvasBoundingRect.left + POSITIVE_3_PX) && (coordinates.x < 0);
        const touchingRight =
            (clientRectRight > this.svgHandler.canvasWidth + canvasBoundingRect.left - POSITIVE_3_PX) && (coordinates.x > 0);
        coordinates.y = (touchingTop || touchingBottom) ? 0 : coordinates.y;
        coordinates.x = (touchingLeft || touchingRight) ? 0 : coordinates.x;
        return coordinates;
    }

    private moveWithKeys(keyType: NavigationArrows): void {
        switch (keyType) {
            case NavigationArrows.up :
                this.checkLimitsAndTranslate({ x: 0, y: NEGATIVE_3_PX });
                break;
            case NavigationArrows.left :
                this.checkLimitsAndTranslate({ x: NEGATIVE_3_PX, y: 0 });
                break;
            case NavigationArrows.right :
                this.checkLimitsAndTranslate({ x: POSITIVE_3_PX, y: 0 });
                break;
            case NavigationArrows.down :
                this.checkLimitsAndTranslate({ x: 0, y: POSITIVE_3_PX });
                break;
        }
    }

    moveWithMouse(): void {
        this.oldCoordinates.x = (this.oldCoordinates.x === INITIAL_COORDS) ? this.mouseHandler.startCoordinates.x : this.oldCoordinates.x;
        this.oldCoordinates.y = (this.oldCoordinates.y === INITIAL_COORDS) ? this.mouseHandler.startCoordinates.y : this.oldCoordinates.y;
        const vectorX = this.mouseHandler.actualCoordinates.x - this.oldCoordinates.x;
        const vectorY = this.mouseHandler.actualCoordinates.y - this.oldCoordinates.y;
        this.oldCoordinates.x = this.mouseHandler.actualCoordinates.x;
        this.oldCoordinates.y = this.mouseHandler.actualCoordinates.y;
        this.checkLimitsAndTranslate({ x: vectorX, y: vectorY });
    }

    private checkLimitsAndTranslate(coordinates: Coordinates): void {
        const checkedCoords = this.checkLimits(coordinates);
        this.selectionCalculator.selectedObjects.forEach((value) => {
            this.saveManager.removeSvgElement(value);
            this.translate(value, checkedCoords);
            this.saveManager.addSvgElement(value);
        });
        this.selectionSvgHandler.selectionRectangles.forEach((value) => {
            this.translate(value, checkedCoords);
        });
        try {
            this.selectionCalculator.computeCenters();
        } catch (error) {
            this.keysPressed.forEach((value, key) => {
                this.destroyInterval(key);
            });
        }
    }

    resetMouseCoordinates(): void {
        this.oldCoordinates.x = INITIAL_COORDS;
        this.oldCoordinates.y = INITIAL_COORDS;
    }

    keyboardDelay(keyType: NavigationArrows): void {
        if (this.keysPressed.get(keyType)) {
            return;
        }
        let canAddToUndoRedo = true;
        this.keysPressed.forEach((value, key) => {
            if (this.keysPressed.get(key)) {
                canAddToUndoRedo = false;
            }
        });
        if (canAddToUndoRedo) {
            this.undoRedo.addElementBeforeTransform(this.selectionCalculator.selectedObjects);
        }
        this.keysPressed.set(keyType, true);
        this.moveWithKeys(keyType);
        this.keysUnsubscribe.set(
            keyType,
            interval(SHORT_WAIT)
                .pipe(delay(LONG_WAIT))
                .subscribe(() => this.moveWithKeys(keyType)),
        );
    }

    destroyInterval(keyType: NavigationArrows): void {
        this.keysPressed.set(keyType, false);
        const checkKeyUnsubscribe = this.keysUnsubscribe.get(keyType);
        if (checkKeyUnsubscribe) {
            checkKeyUnsubscribe.unsubscribe();
        }
        let canAddToUndoRedo = true;
        this.keysPressed.forEach((value, key) => {
            if (this.keysPressed.get(key)) {
                canAddToUndoRedo = false;
            }
        });
        if (canAddToUndoRedo) {
            this.undoRedo.transformCurrent();
        }
    }
}
