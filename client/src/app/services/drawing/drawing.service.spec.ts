import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ClickerHandlerService } from 'src/app/services/clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { DrawingService } from './drawing.service';

// tslint:disable:no-magic-numbers

describe('DrawingService', () => {
    let service: DrawingService;
    let polyLineMock: jasmine.SpyObj<SVGElement>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let colorManagerSpy: jasmine.SpyObj<ColorManagerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    const coordinates = {x: 100, y: 100};
    const size = 20;
    const colorManager = new ColorManagerService();
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
            {provide: ColorManagerService, useValue: colorManagerSpy}
        ]
    }));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        colorManagerSpy = jasmine.createSpyObj(
            'ColorManagerService',
            [
                'updateColorWithPixel'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'clickOnObject',
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        polyLineMock = jasmine.createSpyObj('HTMLElement', ['getAttribute']);
        service = new DrawingService(svgHandlerServiceSpy, colorManager,
            clickerHandlerSpy, undoRedoSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create a polyLine', () => {
        rendererSpy.createElement.and.returnValue(polyLineMock);
        const polyLine = service.createPolyLine(coordinates, String(size));
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(3);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(6);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(clickerHandlerSpy.addSelectionStyles).toHaveBeenCalled();
        expect(polyLine).toBe(polyLineMock);
    });

    it('should create a polyLine but not add the selection styles', () => {
        rendererSpy.createElement.and.returnValue(polyLineMock);
        const polyLine = service.createPolyLine(coordinates, String(size), false);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(3);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(clickerHandlerSpy.addSelectionStyles).not.toHaveBeenCalled();
        expect(polyLine).toBe(polyLineMock);
    });

    it('should create a polyLine for previzualisation', () => {
        rendererSpy.createElement.and.returnValue(polyLineMock);
        const polyLine = service.createPolyLine(coordinates, String(size), false);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(3);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(clickerHandlerSpy.addSelectionStyles).not.toHaveBeenCalled();
        expect(polyLine).toBe(polyLineMock);
    });

    it('should add coordinates to polyLine', () => {
        const newCoordinates = { x: 150, y: 120 };
        service.addCoordinatesToPolyline(polyLineMock, newCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(polyLineMock.getAttribute).toHaveBeenCalled();
    });

    it('should apply filter to polyLine', () => {
        service.applyFilterToPolyLine(polyLineMock, 'blur');
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
    });

    it('should get Last coordinates', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        fakeElement.setAttribute('points', '100,110 120,130 150,160');
        const coords = service.getLastCoordinatesOfPolyline(fakeElement);
        expect(coords.x).toEqual(150);
        expect(coords.y).toEqual(160);
    });

    it('getLastCoordinates should return {0,0}', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        const coords = service.getLastCoordinatesOfPolyline(fakeElement);
        expect(coords.x).toEqual(0);
        expect(coords.y).toEqual(0);
    });

    it('should delete last segment', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        fakeElement.setAttribute('points', '100,110 120,130 150,160');
        service.deleteLastSegmentFromPolyline(fakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
    });

    it('should not delete last segment', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        service.deleteLastSegmentFromPolyline(fakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(0);
    });

    it('should delete Element from canvas', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        service.removeElement(fakeElement);
        expect(rendererSpy.removeChild).toHaveBeenCalled();
    });

    it('should not delete Element from canvas', () => {
      service.removeElement();
      expect(rendererSpy.removeChild).toHaveBeenCalledTimes(0);
    });
});
