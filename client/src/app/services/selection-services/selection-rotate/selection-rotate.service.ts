import { Injectable } from '@angular/core';
import { Coordinates } from '../../../interfaces-enums/coordinates';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';
import { SelectionSvgHandlerService } from '../selection-svg-handler/selection-svg-handler.service';
import { SelectionTransformService } from '../selection-transform/selection-transform.service';

const NEGATIVE = -1;
const BIG_DEGRES_CHANGE = 15;
const FIRST_INDEX = 0;
const SECOND_INDEX = 1;
const THIRD_INDEX = 2;
const FORTH_INDEX = 3;
const FIFTH_INDEX = 4;
const SIXTH_INDEX = 5;
const HALF_CIRCLE = 180;
const NUMBER_OF_ELEMENTS = 6;

@Injectable({
    providedIn: 'root',
})
export class SelectionRotateService {
    constructor(
        private selectionTransform: SelectionTransformService,
        private selectionCalculator: SelectionCalculatorService,
        private selectionSvgHandler: SelectionSvgHandlerService,
        private undoRedo: UndoRedoService,
        private saveManager: SaveManagerService
    ) {}

    private rotate(element: SVGElement, angle: number, centerCoords: Coordinates): void {
        const newMatrix = new Array<number>();
        const oldMatrix = this.selectionTransform.getOldMatrix(element);
        const radianAngle = angle * (Math.PI / HALF_CIRCLE);
        const cosAngle = Math.cos(radianAngle);
        const sinAngle = Math.sin(radianAngle);
        const longFirstCalculation = ((1 - cosAngle) * centerCoords.x) + (sinAngle * centerCoords.y);
        const longSecondCalculation = ((1 - cosAngle) * centerCoords.y) - (sinAngle * centerCoords.x);
        const tempMatrix = new Array<number>();
        for (let i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            tempMatrix[i] = +oldMatrix[i + 1];
        }
        newMatrix[FIRST_INDEX] = tempMatrix[FIRST_INDEX] * cosAngle + tempMatrix[SECOND_INDEX] * -sinAngle;
        newMatrix[SECOND_INDEX] = tempMatrix[FIRST_INDEX] * sinAngle + tempMatrix[SECOND_INDEX] * cosAngle;
        newMatrix[THIRD_INDEX] = tempMatrix[THIRD_INDEX] * cosAngle + tempMatrix[FORTH_INDEX] * -sinAngle;
        newMatrix[FORTH_INDEX] = tempMatrix[THIRD_INDEX] * sinAngle + tempMatrix[FORTH_INDEX] * cosAngle;
        newMatrix[FIFTH_INDEX] = tempMatrix[FIFTH_INDEX] * cosAngle + tempMatrix[SIXTH_INDEX] * -sinAngle + longFirstCalculation;
        newMatrix[SIXTH_INDEX] = tempMatrix[FIFTH_INDEX] * sinAngle + tempMatrix[SIXTH_INDEX] * cosAngle + longSecondCalculation;
        this.selectionTransform.appendTransformationToElements(element, newMatrix);
    }

    private getCenterAndRotateAllElements(angle: number): void {
        this.selectionCalculator.selectedObjects.forEach((value, index) => {
            this.rotate(value, angle, this.selectionCalculator.centerOfSelection);
            this.selectionSvgHandler.createSelectionRectangles(value, false, true);
        });
    }

    private getCenterAndRotateEachElement(angle: number): void {
        this.selectionCalculator.selectedObjects.forEach((value, index) => {
            this.rotate(value, angle, this.selectionCalculator.centersOfSelectedObjects[index]);
            this.selectionSvgHandler.createSelectionRectangles(value, false, true);
        });
    }

    wheelMoved(direction: boolean, shiftDown: boolean, altDown: boolean): void {
        if (this.selectionCalculator.selectedObjects.length !== 0) {
            this.undoRedo.addElementBeforeTransform(this.selectionCalculator.selectedObjects);
            this.selectionCalculator.selectedObjects.forEach((value) => {
                this.saveManager.removeSvgElement(value);
            });
            const multiplier = (direction) ? 1 : NEGATIVE;
            if (altDown && !shiftDown) {
                this.getCenterAndRotateAllElements(1 * multiplier);
            } else if (altDown && shiftDown) {
                this.getCenterAndRotateEachElement(1 * multiplier);
            } else if (shiftDown && !altDown) {
                this.getCenterAndRotateEachElement(BIG_DEGRES_CHANGE * multiplier);
            } else {
                this.getCenterAndRotateAllElements(BIG_DEGRES_CHANGE * multiplier);
            }
            this.undoRedo.transformCurrent();
            this.selectionCalculator.selectedObjects.forEach((value) => {
                this.saveManager.addSvgElement(value);
            });
        }
    }
}
