import { Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { ElementType } from '../../interfaces-enums/element-type';
import { RectangleDimensions } from '../../interfaces-enums/rectangle-dimensions';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const HALF_OPACITY = 0.5;

export abstract class AbstractShapeService {
    renderer: Renderer2;
    dimensions: RectangleDimensions;

    constructor(protected svgHandler: SvgHandlerService,
                protected colorManager: ColorManagerService,
                protected clickerHandler: ClickerHandlerService,
                protected undoRedoService: UndoRedoService) {
        this.renderer = this.svgHandler.renderer;
        this.dimensions = {coordinates: {x: 0, y: 0}, width: 0, height: 0};
    }

    createElement(fill: boolean,
                  stroke: boolean,
                  size: number,
                  type: ElementType): SVGElement {
        const element = this.renderer.createElement('svg:' + type.toString(), this.svgHandler.SVG_LINK);
        const fillStyle = fill ? this.colorManager.colorSelected[ColorChoice.mainColor].inString : 'none';
        const strokeStyle = stroke ? size : 'none';
        const strokeColor = stroke ? this.colorManager.colorSelected[ColorChoice.secondaryColor].inString : 'none';
        this.renderer.setAttribute(element, 'stroke-width', String(strokeStyle));
        this.renderer.setAttribute(element, 'stroke', strokeColor);
        this.renderer.setStyle(element, 'fill', fillStyle);
        this.renderer.setAttribute(element, 'size', String(size));
        this.setElementAttributesCreation(element);
        this.renderer.setAttribute(element, 'typeElement', 'shapeElement');
        this.renderer.appendChild(this.svgHandler.svgCanvas, element);
        this.undoRedoService.updateCurrent(new Array(element), ChangeType.addToCanvas);
        this.clickerHandler.addSelectionStyles(element);
        this.svgHandler.drawingOnCanvas = true;
        return element;
    }

    elementPrevisualization(fill: boolean,
                            stroke: boolean,
                            size: number,
                            type: ElementType,
                            oldChild: (SVGElement | undefined)): SVGElement {
        if (! oldChild) {
            oldChild = this.renderer.createElement('svg:' + type.toString(), this.svgHandler.SVG_LINK);
            this.renderer.appendChild(this.svgHandler.svgCanvas, oldChild);
        }
        const fillStyle = fill ? this.colorManager.colorSelected[ColorChoice.mainColor].inString : 'none';
        const strokeStyle = stroke ? size : 'none';
        const strokeColor = stroke ? this.colorManager.colorSelected[ColorChoice.secondaryColor].inString : 'none';
        this.renderer.setStyle(oldChild, 'stroke-width', strokeStyle);
        this.renderer.setStyle(oldChild, 'fill-opacity', HALF_OPACITY);
        this.renderer.setStyle(oldChild, 'fill', fillStyle);
        this.renderer.setStyle(oldChild, 'stroke-opacity', HALF_OPACITY);
        this.renderer.setStyle(oldChild, 'stroke', strokeColor);
        this.setElementAttributesPrevisualization(oldChild);
        return oldChild as SVGElement;
    }

    isShapeTooBigComparedToSize(startCoordinates: Coordinates,
                                endCoordinates: Coordinates,
                                size: number): boolean {
        const deltaX = Math.abs(startCoordinates.x - endCoordinates.x);
        const deltaY = Math.abs(startCoordinates.y - endCoordinates.y);
        if (deltaX < size * 2 || deltaY < size * 2) {
            this.dimensions.width = 0;
            this.dimensions.height = 0;
            return true;
        }
        return false;
    }

    protected abstract setElementAttributesPrevisualization(element: SVGElement | undefined): void;

    protected abstract setElementAttributesCreation(element: SVGElement | undefined): void;
}
