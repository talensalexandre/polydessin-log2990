import { TestBed } from '@angular/core/testing';
import { ShortcutHandlerService } from './shortcut-handler.service';

// tslint:disable:no-string-literal

describe('ShortcutHandlerService', () => {
    let service: ShortcutHandlerService;
    const callback = () => {return; };

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        service = new ShortcutHandlerService();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add to shortcuts', () => {
        const mapSpy = spyOn(service['shortcuts'], 'set');
        service.addShortcut('a', callback);
        expect(mapSpy).toHaveBeenCalledWith('a', callback);
    });

    it('should add to ctrlShortcuts', () => {
        const mapSpy = spyOn(service['ctrlShortcuts'], 'set');
        service.addCtrlShortcut('a', callback);
        expect(mapSpy).toHaveBeenCalledWith('a', callback);
    });

    it('should not execute Shortcut', () => {
        const mapSpy = spyOn(service['shortcuts'], 'get');
        const fakeKeyboard = new KeyboardEvent('keydown', { key: 'a'});
        const eventSpy = spyOn(fakeKeyboard, 'preventDefault');
        service.executeShortcut(fakeKeyboard);
        expect(mapSpy).toHaveBeenCalledWith('a');
        expect(eventSpy).not.toHaveBeenCalled();
    });

    it('should not execute CtrlShortcut', () => {
        const mapSpy = spyOn(service['ctrlShortcuts'], 'get');
        const fakeKeyboard = new KeyboardEvent('keydown', { key: 'a', ctrlKey: true});
        const eventSpy = spyOn(fakeKeyboard, 'preventDefault');
        service.executeCtrlShortcut(fakeKeyboard);
        expect(mapSpy).toHaveBeenCalledWith('a');
        expect(eventSpy).not.toHaveBeenCalled();
    });
});
