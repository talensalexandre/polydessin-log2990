import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { DrawingService } from '../drawing/drawing.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { LineHandlerService } from './line-handler.service';

// tslint:disable:no-magic-numbers

describe('LineHandlerService', () => {
    let service: LineHandlerService;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let lineMock: jasmine.SpyObj<SVGElement>;
    let fakeElement: SVGElement;
    const coordinates = { x: 100, y: 100 };
    const size = 20;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        drawingServiceSpy = jasmine.createSpyObj(
            'DrawingService',
            [
                'createPolyLine'
            ]
        );
        lineMock = jasmine.createSpyObj(
            'SVGElement',
            [
                'getAttribute',
                'setAttribute',
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new LineHandlerService(
            svgHandlerServiceSpy,
            drawingServiceSpy,
            new ColorManagerService(),
            clickerHandlerSpy,
            undoRedoSpy
        );
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should empty children', () => {
        service.junctionPoints = [];
        service.junctionPoints.push(fakeElement);
        service.emptyChildren();
        expect(service.junctionPoints.length).toBe(0);
    });

    it('linePrevisualization should create child if does not exists', () => {
        rendererSpy.createElement.and.returnValue(lineMock);
        const child = service.linePrevisualization(coordinates, coordinates, String(size), undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(2);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(6);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(child).toBe(lineMock);
    });

    it('linePrevisualization should not create child if already exists', () => {
        rendererSpy.createElement.and.returnValue(lineMock);
        const child = service.linePrevisualization(coordinates, coordinates, String(size), lineMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(6);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
        expect(child).toBe(lineMock);
    });

    it('connectFirstAndLastPoint should set right attributes (normal juction)', () => {
        rendererSpy.createElement.and.returnValue(lineMock);
        fakeElement.setAttribute('points', '100,100');
        service.connectFirstAndLastPoint(coordinates, lineMock, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(0);
    });

    it('connectFirstAndLastPoint should set right attributes (point juction)', () => {
        rendererSpy.createElement.and.returnValue(lineMock);
        fakeElement.setAttribute('points', '100,100');
        service.connectFirstAndLastPoint(coordinates, lineMock, fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(2);
    });

    it('connectFirstAndLastPoint should not do anything of points is null', () => {
        rendererSpy.createElement.and.returnValue(lineMock);
        service.connectFirstAndLastPoint(coordinates, lineMock, fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(2);
    });

    it('createJunction should create a junction', () => {
        service.junctionPoints = [];
        service.junctionPoints.push(fakeElement);
        service.createJunction(coordinates, String(size));
        expect(drawingServiceSpy.createPolyLine).toHaveBeenCalled();
        expect(service.junctionPoints.length).toEqual(2);
    });

    it('removeJunction should remove junction', () => {
        service.junctionPoints = [];
        service.junctionPoints.push(fakeElement);
        service.removeJunction();
        expect(service.junctionPoints.length).toEqual(0);
        expect(rendererSpy.removeChild).toHaveBeenCalled();
    });

    it('deleteLine should call removeChild 1 time if arrays are empty', () => {
        service.junctionPoints = [];
        service.deleteLine(fakeElement);
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(1);
    });

    it('deleteLine should call removeChild for each objet in arrays', () => {
        service.junctionPoints = [fakeElement];
        service.deleteLine(fakeElement);
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(2);
    });

    it('should update shiftPrevisualization with angle of 315 degrees', () => {
        spyOn(service, 'linePrevisualization').and.callThrough();
        const endCoords = { x: 200, y: 200 };
        fakeElement.setAttribute('x1', '100');
        fakeElement.setAttribute('y1', '100');
        fakeElement.setAttribute('x2', '110');
        fakeElement.setAttribute('y2', '110');
        fakeElement.setAttribute('style', 'stroke: black; stroke-opacity: 0.5; stroke-linecap: round; stroke-width: 0;');
        fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
        expect(service.linePrevisualization).toHaveBeenCalled();
    });

    it('should update shiftPrevisualization with angle of 0 degree', () => {
      spyOn(service, 'linePrevisualization').and.callThrough();
      const endCoords = { x: 110, y: 99 };
      fakeElement.setAttribute('x1', '100');
      fakeElement.setAttribute('y1', '100');
      fakeElement.setAttribute('x2', '110');
      fakeElement.setAttribute('y2', '100');
      fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
      expect(service.linePrevisualization).toHaveBeenCalled();
  });

    it('should update shiftPrevisualization with angle of 180 degrees', () => {
        spyOn(service, 'linePrevisualization').and.callThrough();
        const endCoords = { x: 0, y: 100 };
        fakeElement.setAttribute('x1', '100');
        fakeElement.setAttribute('y1', '100');
        fakeElement.setAttribute('x2', '110');
        fakeElement.setAttribute('y2', '110');
        fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
        expect(service.linePrevisualization).toHaveBeenCalled();
    });

    it('should update shiftPrevisualization with angle of 90 degrees', () => {
        spyOn(service, 'linePrevisualization').and.callThrough();
        const endCoords = { x: 100, y: 0 };
        fakeElement.setAttribute('x1', '100');
        fakeElement.setAttribute('y1', '100');
        fakeElement.setAttribute('x2', '110');
        fakeElement.setAttribute('y2', '110');
        fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
        expect(service.linePrevisualization).toHaveBeenCalled();

    });

    it('should update shiftPrevisualization with angle of 270 degrees', () => {
        spyOn(service, 'linePrevisualization').and.callThrough();
        const endCoords = { x: 100, y: 200 };
        fakeElement.setAttribute('x1', '100');
        fakeElement.setAttribute('y1', '100');
        fakeElement.setAttribute('x2', '110');
        fakeElement.setAttribute('y2', '110');
        fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
        expect(service.linePrevisualization).toHaveBeenCalled();
    });

    it('should not update shiftPrevisualization if coords outside canva (<0)', () => {
        spyOn(service, 'linePrevisualization').and.callThrough();
        const endCoords = { x: 700, y: -500 };
        fakeElement.setAttribute('x1', '100');
        fakeElement.setAttribute('y1', '100');
        fakeElement.setAttribute('x2', '110');
        fakeElement.setAttribute('y2', '110');
        fakeElement = service.shiftPrevisualization(coordinates, endCoords, String(size), fakeElement);
        expect(service.linePrevisualization).toHaveBeenCalled();
    });

    it('should add an element to the svg g element', () => {
        service.addElementToG(fakeElement, '10');
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
    });

    it('should create the svg g element', () => {
        service.createGElement();
        expect(rendererSpy.createElement).toHaveBeenCalledWith('svg:g', svgHandlerServiceSpy.SVG_LINK);
        expect(clickerHandlerSpy.addSelectionStyles).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(2);
    });
});
