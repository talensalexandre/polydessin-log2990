import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { SprayHandlerService } from './spray-handler.service';

// tslint:disable:no-magic-numbers

describe('SprayHandlerService', () => {
    let service: SprayHandlerService;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    const sprayMock = document.createElementNS('http://www.w3.org/2000/svg', 'svg:path');
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    const colorManager = new ColorManagerService();
    let spraySpy: jasmine.Spy;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'clickOnObject',
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        rendererSpy.createElement.and.returnValue(sprayMock);
        spraySpy = spyOn(sprayMock, 'getAttribute').and.returnValue('M 10 10 Z');
        svgHandlerSpy.renderer = rendererSpy;
        service = new SprayHandlerService(svgHandlerSpy, colorManager, clickerHandlerSpy, undoRedoSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create and svg:g element to be the spray object', () => {
        service.createSprayObject();
        expect(rendererSpy.createElement).toHaveBeenCalledWith('svg:path', svgHandlerSpy.SVG_LINK);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(6);
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, sprayMock);
        expect(clickerHandlerSpy.addSelectionStyles).toHaveBeenCalledWith(sprayMock);
    });

    it('should add a random circle to the canvas', () => {
        spyOn(Math, 'random').and.returnValue(0.5);
        const coords = {x: 100, y: 100};
        const diameter = 20;
        const expectedNewPath = 'M 10 10 ZM 100 100 Z';
        service.addRandomCircle(sprayMock, coords, diameter);
        expect(spraySpy).toHaveBeenCalledWith('d');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(sprayMock, 'd', expectedNewPath);
    });
});
