import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorPopupComponent } from '../color-popup/color-popup.component';

const COLOR_DIALOG_WIDTH = '500px';

@Component({
    selector: 'app-color',
    templateUrl: './color.component.html',
    styleUrls: ['./color.component.scss']
})
export class ColorComponent {
    mainColorSelection: RGBA;
    secondaryColorSelection: RGBA;
    backgroundColor: RGBA;
    ColorChoice: typeof ColorChoice = ColorChoice;
    readonly subscribeFunction = () => {
        this.svgHandler.updateBackgroundColor(this.backgroundColor.inString);
        this.saveManager.autosaveBackground(this.backgroundColor.inString, this.svgHandler.canvasWidth, this.svgHandler.canvasHeight);
    }

    constructor(
        public dialog: MatDialog,
        private colorManager: ColorManagerService,
        private svgHandler: SvgHandlerService,
        private saveManager: SaveManagerService
    ) {
        this.mainColorSelection = this.colorManager.colorSelected[ColorChoice.mainColor];
        this.secondaryColorSelection = this.colorManager.colorSelected[ColorChoice.secondaryColor];
        this.backgroundColor = this.colorManager.colorSelected[ColorChoice.backgroundColor];
     }

    openColorPicker(colorChoice: ColorChoice): void {
        const window = this.dialog.open(ColorPopupComponent, {
            width: COLOR_DIALOG_WIDTH,
            data: colorChoice
        });
        window.afterClosed().subscribe(this.subscribeFunction);
    }

    switchColor(): void {
        const mainColor = this.colorManager.colorSelected[ColorChoice.mainColor];
        const secondaryColor = this.colorManager.colorSelected[ColorChoice.secondaryColor];
        const tempColor = JSON.parse(JSON.stringify(mainColor));
        mainColor.Dec = secondaryColor.Dec;
        mainColor.Hex = secondaryColor.Hex;
        mainColor.inString = secondaryColor.inString;
        secondaryColor.Dec = tempColor.Dec;
        secondaryColor.Hex = tempColor.Hex;
        secondaryColor.inString = tempColor.inString;
    }
}
