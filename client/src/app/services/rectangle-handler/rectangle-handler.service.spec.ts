import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ColorComponent } from 'src/app/components/color-components/color/color.component';
import { RectangleComponent } from '../../components/rectangle/rectangle.component';
import { ElementType } from '../../interfaces-enums/element-type';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { RectangleHandlerService } from './rectangle-handler.service';

// tslint:disable:no-magic-numbers

describe('RectangleHandlerService', () => {
    let fill: boolean;
    let stroke: boolean;
    let oldChildMock: jasmine.SpyObj<SVGElement>;
    const size = 20;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let service: RectangleHandlerService;
    const colorManager = new ColorManagerService();
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;

    beforeEach(() => TestBed.configureTestingModule({
        declarations: [RectangleComponent, ColorComponent]
    }));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'setStyle',
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'clickOnObject',
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new RectangleHandlerService(svgHandlerServiceSpy, colorManager,
            clickerHandlerSpy, undoRedoSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create a rectangle with fill true', () => {
        fill = true;
        stroke = true;
        service.createElement(fill, stroke, size, ElementType.rect);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(8);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle with fill false', () => {
        fill = false;
        stroke = false;
        service.createElement(fill, stroke, size, ElementType.rect);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(8);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill false and no oldchild', () => {
        fill = false;
        stroke = true;
        service.elementPrevisualization(fill, stroke, size, ElementType.rect, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill true and no oldchild', () => {
        fill = true;
        stroke = false;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.rect, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a rectangle previsualization with fill true and oldchild', () => {
        fill = true;
        stroke = true;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.rect, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should create a rectangle previsualization with fill false and oldchild', () => {
        fill = false;
        stroke = false;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.rect, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should calculate square startCoordinates < endCoordinates', () => {
        const startCoordinates = {x: 100, y: 100};
        const endCoordinates = {x: 200, y: 250};
        service.calculateSquare(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(110);
        expect(service.dimensions.coordinates.y).toEqual(110);
        expect(service.dimensions.width).toEqual(90);
        expect(service.dimensions.height).toEqual(90);
    });

    it('should calculate square startCoordinates > endCoordinates', () => {
        const startCoordinates = {x: 100, y: 100};
        const endCoordinates = {x: 50, y: 30};
        service.calculateSquare(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(30);
        expect(service.dimensions.coordinates.y).toEqual(30);
        expect(service.dimensions.width).toEqual(60);
        expect(service.dimensions.height).toEqual(60);
    });

    it('should calculate rectangle', () => {
        const startCoordinates = {x: 100, y: 100};
        const endCoordinates = {x: 200, y: 250};
        service.calculateRectangle(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(110);
        expect(service.dimensions.coordinates.y).toEqual(110);
        expect(service.dimensions.width).toEqual(90);
        expect(service.dimensions.height).toEqual(140);
    });

    it('should calculate rectangle', () => {
        const startCoordinates = {x: 300, y: 300};
        const endCoordinates = {x: 200, y: 250};
        service.calculateRectangle(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(180);
        expect(service.dimensions.coordinates.y).toEqual(230);
        expect(service.dimensions.width).toEqual(110);
        expect(service.dimensions.height).toEqual(60);
    });

    it('should create a rectangle previsualization with fill false and oldchild', () => {
        fill = false;
        stroke = false;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.ellipse, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });
});
