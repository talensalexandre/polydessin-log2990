import { ElementRef, Injectable, Renderer2 } from '@angular/core';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

const MAX_COLOR_VALUE = 255;
const VIEWBOX_WIDTH = 400;
const VIEWBOX_HEIGHT = 300;
const VIEWBOX_SIZE: number[] = [VIEWBOX_WIDTH, VIEWBOX_HEIGHT];
// These specific values are the values for sepia tone that are recommended by Microsoft.
const FILTER_1 = 0.393;
const FILTER_2 = 0.769;
const FILTER_3 = 0.189;
const FILTER_4 = 0.349;
const FILTER_5 = 0.686;
const FILTER_6 = 0.168;
const FILTER_7 = 0.272;
const FILTER_8 = 0.534;
const FILTER_9 = 0.131;
const GRAY_DIVISOR = 4;
const BLACK_WHITE_FILTER = 1;
const OLD_FILTER = 2;
const INVERT_FILTER = 3;
const SATURATE_FILTER = 4;
const BLUR_FILTER = 5;

@Injectable({
    providedIn: 'root'
})
export class ExportDrawingService {
    private renderer: Renderer2;

    constructor(private svgHandler: SvgHandlerService,
                private colorManager: ColorManagerService) {
        this.renderer = this.svgHandler.renderer;
    }

    changeBackGroundColor(svg: Node, red: number, green: number, blue: number, opacity: number): void {
        this.renderer.setStyle(svg, 'background-color', 'rgba(' +
            red + ', ' + green + ', ' + blue + ', ' + opacity + ')');
    }

    applySvgFilter(filter: number, svg: Node, isprevisu: boolean): void {
        switch (filter) {
            case BLACK_WHITE_FILTER:
                this.renderer.setAttribute(svg, 'filter', 'sepia(100%) grayscale(100%)');
                if (!isprevisu) {
                    this.grayscaleBackGroundColor(svg);
                }
                break;
            case OLD_FILTER:
                this.renderer.setAttribute(svg, 'filter', ' grayscale(100%) sepia(100%) ');
                if (!isprevisu) {
                    this.sepiaBackgroundColor(svg);
                }
                break;
            case INVERT_FILTER:
                this.renderer.setAttribute(svg, 'filter', 'invert(100%)');
                if (!isprevisu) {
                    this.invertBackgroundColor(svg);
                }
                break;
            case SATURATE_FILTER:
                this.renderer.setAttribute(svg, 'filter', 'saturate(50)');
                break;
            case BLUR_FILTER:
                this.renderer.setAttribute(svg, 'filter', 'blur(2px)');
                break;
            default:
                this.renderer.removeAttribute(svg, 'filter');
        }
    }

    private sepiaBackgroundColor(svg: Node): void {
        const oldRed = this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Red;
        const oldGreen = this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Green;
        const oldBlue = this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Blue;
        const newRed = FILTER_1 * oldRed +
            FILTER_2 * oldGreen +
            FILTER_3 * oldBlue;
        const newGreen = FILTER_4 * oldRed +
            FILTER_5 * oldGreen +
            FILTER_6 * oldBlue;
        const newBlue = FILTER_7 * oldRed +
            FILTER_8 * oldGreen +
            FILTER_9 * oldBlue;
        const opacity = this.colorManager.colorHistory[ColorChoice.backgroundColor].Dec.Alpha;
        this.changeBackGroundColor(svg, newRed, newGreen, newBlue, opacity);
    }

    private grayscaleBackGroundColor(svg: Node): void {
        const newColor = (this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Red +
            this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Green +
            this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Blue) / GRAY_DIVISOR;
        const opacity = this.colorManager.colorHistory[ColorChoice.backgroundColor].Dec.Alpha;
        this.changeBackGroundColor(svg, newColor, newColor, newColor, opacity);
    }

    private invertBackgroundColor(svg: Node): void {
        const newRed = MAX_COLOR_VALUE - this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Red;
        const newGreen = MAX_COLOR_VALUE - this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Green;
        const newBlue = MAX_COLOR_VALUE - this.colorManager.colorSelected[ColorChoice.backgroundColor].Dec.Blue;
        const opacity = this.colorManager.colorHistory[ColorChoice.backgroundColor].Dec.Alpha;
        this.changeBackGroundColor(svg, newRed, newGreen, newBlue, opacity);
    }

    initialisePrevisualization(element: Node, previsu: ElementRef): void {
        this.renderer.removeStyle(element, 'width');
        this.renderer.removeStyle(element, 'height');
        this.renderer.setAttribute(element, 'viewBox', '0 0 ' + this.svgHandler.canvasWidth + ' ' + this.svgHandler.canvasHeight);
        const ratio = Math.max(this.svgHandler.canvasWidth / VIEWBOX_SIZE[0],
            this.svgHandler.canvasHeight / VIEWBOX_SIZE[1]);
        this.renderer.setAttribute(element, 'witdh', (this.svgHandler.canvasWidth / ratio).toString());
        this.renderer.setAttribute(element, 'height', (this.svgHandler.canvasHeight / ratio).toString());
        this.renderer.removeStyle(element, 'border-style');
        this.renderer.appendChild(previsu.nativeElement, element);
    }

    initialiseDownloadFile(svgCanvas: Node): void {
        this.renderer.removeStyle(svgCanvas, 'height');
        this.renderer.removeStyle(svgCanvas, 'width');
        this.renderer.removeStyle(svgCanvas, 'border-style');
        this.renderer.setAttribute(svgCanvas, 'witdh', String(this.svgHandler.canvasWidth));
        this.renderer.setAttribute(svgCanvas, 'height', String(this.svgHandler.canvasHeight));
    }
}
