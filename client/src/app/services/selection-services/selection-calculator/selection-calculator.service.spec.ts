import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Coordinates } from '../../../interfaces-enums/coordinates';
import { RectangleDimensions } from '../../../interfaces-enums/rectangle-dimensions';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { SelectionCalculatorService } from './selection-calculator.service';

// tslint:disable:no-magic-numbers no-string-literal

describe('SelectionCalculatorService', () => {
    let service: SelectionCalculatorService;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeBoundingClientRectValues: DOMRect = new DOMRect(200, 200, 600, 600);
    const secondFakeBoundingValues: DOMRect = new DOMRect(190, 190, 620, 620);
    const secondFakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const thirdFakeBoundingValues: DOMRect = new DOMRect(300, 300, 300, 300);
    const thirdFakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    const fakeCanvasRectValues: DOMRect = new DOMRect(100, 0, 1000, 1000);
    let canvasSpy: jasmine.SpyObj<SVGElement>;

    beforeEach(() => {
        fakeSvgElement.setAttribute('size', '10');
        secondFakeElement.setAttribute('size', '10');
        thirdFakeElement.setAttribute('size', '10');
        spyOn(fakeSvgElement, 'getBoundingClientRect').and.returnValue(fakeBoundingClientRectValues);
        spyOn(secondFakeElement, 'getBoundingClientRect').and.returnValue(secondFakeBoundingValues);
        spyOn(thirdFakeElement, 'getBoundingClientRect').and.returnValue(thirdFakeBoundingValues);
    });

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            { provide: SvgHandlerService, useValue: svgHandlerSpy },
            { provide: Renderer2, useValue: rendererSpy },
            { provide: SVGElement, useValue: canvasSpy }
        ]
    }));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        canvasSpy = jasmine.createSpyObj(
            'SVGElement',
            [
                'getBoundingClientRect'
            ]
        );
        canvasSpy.getBoundingClientRect.and.returnValue(fakeCanvasRectValues);
        svgHandlerSpy.svgCanvas = canvasSpy;
        svgHandlerSpy.renderer = rendererSpy;
        service = new SelectionCalculatorService(svgHandlerSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return boundary minimum coordinates and boundary sizes when adding first element', () => {
        service.selectedObjects.push(fakeSvgElement);
        const result: RectangleDimensions = service.getRectangleDimensions(fakeSvgElement, true, false);
        expect(result.coordinates.x).toBe(200 - 100 - 5);
        expect(result.coordinates.y).toBe(200 - 5 - 1);
        expect(result.width).toBe(800 - 100 + 5 + 2 - 200 + 100 + 5);
        expect(result.height).toBe(800 + 5 + 1 - 200 + 5 + 1);
    });

    it('should return boundary minimum coordinates and boundary sizes when removing only element', () => {
        service.selectedObjects.push(fakeSvgElement);
        const result: RectangleDimensions = service.getRectangleDimensions(secondFakeElement, false, false);
        expect(result.coordinates.x).toBe(200 - 100 - 5);
        expect(result.coordinates.y).toBe(200 - 5 - 1);
        expect(result.width).toBe(800 - 100 + 5 + 2 - 200 + 100 + 5);
        expect(result.height).toBe(800 + 5 + 1 - 200 + 5 + 1);
    });

    it('should return boundary minimum coordinates and boundary sizes when adding a second element', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        service.selectedObjects.push(secondFakeElement);
        const result: RectangleDimensions = service.getRectangleDimensions(secondFakeElement, true, false);
        expect(result.coordinates.x).toBe(190 - 100 - 5);
        expect(result.coordinates.y).toBe(190 - 5 - 1);
        expect(result.width).toBe(810 - 100 + 5 + 2 - 190 + 100 + 5);
        expect(result.height).toBe(810 + 5 + 1 - 190 + 5 + 1);
    });

    it('should return boundary minimum coordinates and boundary sizes when removing an element', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        service.selectedObjects.push(secondFakeElement);
        service.getRectangleDimensions(secondFakeElement, true, false);
        const result: RectangleDimensions = service.getRectangleDimensions(thirdFakeElement, false, false);
        expect(result.coordinates.x).toBe(190 - 100 - 5);
        expect(result.coordinates.y).toBe(190 - 5 - 1);
        expect(result.width).toBe(810 - 100 + 5 + 2 - 190 + 100 + 5);
        expect(result.height).toBe(810 + 5 + 1 - 190 + 5 + 1);
    });
    it('should return boundary minimum coordinates and boundary sizes when removing the biggest element', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        service.selectedObjects.push(secondFakeElement);
        service.getRectangleDimensions(secondFakeElement, true, false);
        const result: RectangleDimensions = service.getRectangleDimensions(secondFakeElement, false, false);
        expect(result.coordinates.x).toBe(190 - 100 - 5);
        expect(result.coordinates.y).toBe(190 - 5 - 1);
        expect(result.width).toBe(820 - 100 + 5 + 2 - 200 + 100 + 5);
        expect(result.height).toBe(820 + 5 + 1 - 200 + 5 + 1);
    });

    it('should return boundary minimum coordinates and boundary sizes when removing a non-selected element', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        service.selectedObjects.push(thirdFakeElement);
        service.getRectangleDimensions(thirdFakeElement, true, false);
        service.getRectangleDimensions(secondFakeElement, true, false);
        const result: RectangleDimensions = service.getRectangleDimensions(secondFakeElement, false, false);
        expect(result.coordinates.x).toBe(200 - 100 - 5);
        expect(result.coordinates.y).toBe(200 - 5 - 1);
        expect(result.width).toBe(800 - 100 + 5 + 2 - 200 + 100 + 5);
        expect(result.height).toBe(800 + 5 + 1 - 200 + 5 + 1);
    });

    it('should return coordinates for top selection rectangle', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        const result: Coordinates = service.calculateRectangleCoordinates('top');
        expect(result.x).toBe(200 + 300 - 100 + 1 - 5);
        expect(result.y).toBe(200 - 5 - 1 - 5);
    });

    it('should return coordinates for bottom selection rectangle', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        const result: Coordinates = service.calculateRectangleCoordinates('bottom');
        expect(result.x).toBe(200 + 300 - 100 + 1 - 5);
        expect(result.y).toBe(200 + 600 + 5 + 1 - 5);
    });

    it('should return coordinates for left selection rectangle', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        const result: Coordinates = service.calculateRectangleCoordinates('left');
        expect(result.x).toBe(200 - 100 - 5 - 5);
        expect(result.y).toBe(200 + 300 - 5);
    });

    it('should return coordinates for right selection rectangle', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        const result: Coordinates = service.calculateRectangleCoordinates('right');
        expect(result.x).toBe(200 + 600 - 100 + 2 + 5 - 5);
        expect(result.y).toBe(200 + 300 - 5);
    });

    it('should return coordinates for boundary selection rectangle', () => {
        service.selectedObjects.push(fakeSvgElement);
        service.getRectangleDimensions(fakeSvgElement, true, false);
        const result: Coordinates = service.calculateRectangleCoordinates('boundary');
        expect(result.x).toBe(200 - 100 - 5);
        expect(result.y).toBe(200 - 5 - 1);
    });

    it('should return false if object not in selection', () => {
        const minCoordinates = {x: 10, y: 10};
        const maxCoordinates = {x: 20, y: 20};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeFalsy();
    });

    it('should return true if object in selection V1', () => {
        const minCoordinates = {x: 10, y: 10};
        const maxCoordinates = {x: 200, y: 200};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V2', () => {
        const minCoordinates = {x: 10, y: 200};
        const maxCoordinates = {x: 200, y: 800};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V3', () => {
        const minCoordinates = {x: 200, y: 200};
        const maxCoordinates = {x: 800, y: 800};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V4', () => {
        const minCoordinates = {x: 200, y: 10};
        const maxCoordinates = {x: 800, y: 200};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V5', () => {
        const minCoordinates = {x: 200, y: 200};
        const maxCoordinates = {x: 300, y: 300};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V6', () => {
        const minCoordinates = {x: 200, y: 10};
        const maxCoordinates = {x: 300, y: 500};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should return true if object in selection V7', () => {
        const minCoordinates = {x: 10, y: 200};
        const maxCoordinates = {x: 300, y: 300};
        const result = service.isObjectInSelection(fakeSvgElement, minCoordinates, maxCoordinates);
        expect(result).toBeTruthy();
    });

    it('should compute the center', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'getRectangleDimensions').and.callThrough();
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'objectBoundaryCoordinates').and.callThrough();
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'objectMaxCoordinates').and.callThrough();
        service.selectedObjects = new Array<SVGElement>();
        service.selectedObjects.push(fakeSvgElement);
        service.selectedObjects.push(secondFakeElement);
        service.computeCenters();
        expect(service.getRectangleDimensions).toHaveBeenCalledWith(fakeSvgElement, false, true);
        expect(service['objectBoundaryCoordinates']).toHaveBeenCalledTimes(5);
        expect(service['objectMaxCoordinates']).toHaveBeenCalledTimes(5);
    });
});
