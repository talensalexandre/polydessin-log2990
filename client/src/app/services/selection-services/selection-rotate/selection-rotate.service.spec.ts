import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';
import { SelectionSvgHandlerService } from '../selection-svg-handler/selection-svg-handler.service';
import { SelectionTransformService } from '../selection-transform/selection-transform.service';
import { SelectionRotateService } from './selection-rotate.service';

// tslint:disable:no-magic-numbers no-string-literal no-any max-file-line-count

describe('SelectionRotateService', () => {
    let service: SelectionRotateService;
    let selectionSvgHandlerSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let selectionTransformSpy: jasmine.SpyObj<SelectionTransformService>;
    let selectionCalculatorSpy: jasmine.SpyObj<SelectionCalculatorService>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let canvasSpy: jasmine.SpyObj<SVGElement>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    const fakeCanvasRectValues: DOMRect = new DOMRect(100, 0, 1000, 1000);
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeBoundingClientRectValues: DOMRect = new DOMRect(200, 200, 700, 700);
    let fakeRegArray: RegExpExecArray;

    beforeEach(() => {
        fakeRegArray = /matrix\(\s*([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)/
            .exec('matrix(1,0,0,1,0,0)') as RegExpExecArray;
        selectionSvgHandlerSpy = jasmine.createSpyObj(
            'SelectionSvgHandlerService',
            [
                'svgHandler',
                'createSelectionRectangles',
                'selectionRectangles'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer',
                'setAttribute'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        canvasSpy = jasmine.createSpyObj(
            'SVGElement',
            [
                'getBoundingClientRect'
            ]
        );
        selectionTransformSpy = jasmine.createSpyObj(
            'SelectionTransformService',
            [
                'appendTransformationToElements',
                'getOldMatrix'
            ]
        );
        selectionTransformSpy.getOldMatrix.and.returnValue(fakeRegArray);
        undoRedoSpy = jasmine.createSpyObj(
            'UndoRedoService',
            [
                'addElementBeforeTransform',
                'transformCurrent'
            ]
        );
        selectionCalculatorSpy = jasmine.createSpyObj(
            'SelectionCalculatorService',
            [
                'centerOfSelection',
                'selectedObjects',
                'centerOfSelection',
                'translationOfSelectedObjects',
                'centersOfSelectedObjects'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'removeSvgElement',
                'addSvgElement'
            ]
        );
        fakeSvgElement.setAttribute('size', '10');
        fakeSvgElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        spyOn(fakeSvgElement, 'getBoundingClientRect').and.returnValue(fakeBoundingClientRectValues);
        canvasSpy.getBoundingClientRect.and.returnValue(fakeCanvasRectValues);
        svgHandlerSpy.svgCanvas = canvasSpy;
        selectionCalculatorSpy.centerOfSelection = {x: 0, y: 0};
        selectionCalculatorSpy.centersOfSelectedObjects = new Array<Coordinates>();
        selectionCalculatorSpy.centersOfSelectedObjects[0] = {x: 0, y: 0};
        selectionSvgHandlerSpy.svgHandler = svgHandlerSpy;
        selectionSvgHandlerSpy.svgHandler.renderer = rendererSpy;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
    });

    beforeEach(() =>
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
            ],
            providers: [
                { provide: SelectionTransformService, useValue: selectionTransformSpy },
                { provide: SelectionCalculatorService, useValue: selectionCalculatorSpy },
                { provide: SelectionSvgHandlerService, useValue: selectionSvgHandlerSpy },
                { provide: UndoRedoService, useValue: undoRedoSpy },
                { provide: SaveManagerService, useValue: saveManagerSpy }
            ],
        }),
    );

    beforeEach(() => {
        service = TestBed.get(SelectionRotateService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should do nothing ', () => {
        spyOn<any>(service, 'getCenterAndRotateAllElements').and.callThrough();
        service.wheelMoved(true, false, false);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(0);
        expect(service['getCenterAndRotateAllElements']).toHaveBeenCalledTimes(0);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledTimes(0);
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(0);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledTimes(0);
    });

    it('should rotate all elements by 15 degrees', () => {
        spyOn<any>(service, 'getCenterAndRotateAllElements').and.callThrough();
        service['selectionCalculator'].selectedObjects.push(fakeSvgElement);
        service.wheelMoved(true, false, false);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(service['getCenterAndRotateAllElements']).toHaveBeenCalledWith(15);
        expect(selectionSvgHandlerSpy.createSelectionRectangles).toHaveBeenCalled();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalled();
    });

    it('should rotate one element by 15 degrees', () => {
        spyOn<any>(service, 'getCenterAndRotateEachElement').and.callThrough();
        service['selectionCalculator'].selectedObjects.push(fakeSvgElement);
        service.wheelMoved(true, true, false);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(service['getCenterAndRotateEachElement']).toHaveBeenCalledWith(15);
        expect(selectionSvgHandlerSpy.createSelectionRectangles).toHaveBeenCalled();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalled();
    });

    it('should rotate all elements by 1 degrees', () => {
        spyOn<any>(service, 'getCenterAndRotateAllElements').and.callThrough();
        service['selectionCalculator'].selectedObjects.push(fakeSvgElement);
        service.wheelMoved(true, false, true);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(service['getCenterAndRotateAllElements']).toHaveBeenCalledWith(1);
        expect(selectionSvgHandlerSpy.createSelectionRectangles).toHaveBeenCalled();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalled();
    });

    it('should rotate all elements by -15 degrees', () => {
        spyOn<any>(service, 'getCenterAndRotateAllElements').and.callThrough();
        service['selectionCalculator'].selectedObjects.push(fakeSvgElement);
        service.wheelMoved(false, false, false);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(service['getCenterAndRotateAllElements']).toHaveBeenCalledWith(-15);
        expect(selectionSvgHandlerSpy.createSelectionRectangles).toHaveBeenCalled();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalled();
    });

    it('should rotate all elements by -1 degrees', () => {
        spyOn<any>(service, 'getCenterAndRotateEachElement').and.callThrough();
        service['selectionCalculator'].selectedObjects.push(fakeSvgElement);
        service.wheelMoved(false, true, true);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(service['getCenterAndRotateEachElement']).toHaveBeenCalledWith(-1);
        expect(selectionSvgHandlerSpy.createSelectionRectangles).toHaveBeenCalled();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalled();
    });
});
