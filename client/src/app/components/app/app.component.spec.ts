import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonModule, MatButtonToggleModule, MatCardModule,
    MatChipsModule, MatDialogModule, MatFormFieldModule, MatIconModule,
    MatInputModule, MatListModule, MatOptionModule, MatProgressSpinnerModule,
    MatSelectModule, MatSidenavModule, MatSliderModule, MatSlideToggleModule, MatSnackBarModule
} from '@angular/material';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { States } from 'src/app/interfaces-enums/states';
import { ClipboardService } from 'src/app/services/clipboard/clipboard.service';
import { GridService } from 'src/app/services/grid/grid.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { UndoRedoService } from 'src/app/services/undo-redo/undo-redo.service';
import { BrushComponent } from '../brush/brush.component';
import { ColorApplicatorComponent } from '../color-components/color-applicator/color-applicator.component';
import { ColorPickerComponent } from '../color-components/color-picker/color-picker.component';
import { ColorComponent } from '../color-components/color/color.component';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { CreateNewDrawingComponent } from '../create-new-drawing/create-new-drawing.component';
import { DrawingGalleryComponent } from '../drawing-gallery/drawing-gallery.component';
import { EllipseComponent } from '../ellipse/ellipse.component';
import { EntryPointComponent } from '../entry-point/entry-point.component';
import { EraserComponent } from '../eraser/eraser.component';
import { ExportDrawingComponent } from '../export-drawing/export-drawing.component';
import { GridComponent } from '../grid/grid.component';
import { LineComponent } from '../line/line.component';
import { PaintBucketComponent } from '../paint-bucket/paint-bucket.component';
import { PencilComponent } from '../pencil/pencil.component';
import { PolygonComponent } from '../polygon/polygon.component';
import { RectangleComponent } from '../rectangle/rectangle.component';
import { SaveManagerComponent } from '../save-manager/save-manager.component';
import { SelectionComponent } from '../selection/selection.component';
import { SprayComponent } from '../spray/spray.component';
import { ColorDocComponent } from '../user-guide/color-doc/color-doc.component';
import { DrawingDocComponent } from '../user-guide/drawing-doc/drawing-doc.component';
import { FunctionalitiesDocComponent } from '../user-guide/functionalities-doc/functionalities-doc.component';
import { SelectionDocComponent } from '../user-guide/selection-doc/selection-doc.component';
import { ShapeDocComponent } from '../user-guide/shape-doc/shape-doc.component';
import { UserGuideComponent } from '../user-guide/user-guide.component';
import { AppComponent } from './app.component';

// tslint:disable:no-magic-numbers max-file-line-count no-string-literal

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let gridServiceSpy: jasmine.SpyObj<GridService>;
    let clipboardServiceSpy: jasmine.SpyObj<ClipboardService>;

    beforeEach(() => {
        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        mouseHandlerServiceSpy.isMouseDown = false;
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'svgHandlerServiceSpy',
            [
                'isLineBeingCreated',
                'addListeners',
                'deleteListeners'
            ]
        );
        svgHandlerServiceSpy.isLineBeingCreated = false;
        gridServiceSpy = jasmine.createSpyObj(
            'gridServiceSpy',
            [
                'handleKey'
            ]
        );
        clipboardServiceSpy = jasmine.createSpyObj(
            'clipboardServiceSpy',
            [
                'handleKey'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoSpy',
            [
                'undo',
                'redo'
            ]
        );
        clipboardServiceSpy['selectedObjects'] = new Array<SVGElement>();
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    RouterTestingModule, HttpClientModule, MatIconModule,
                    MatListModule, MatSidenavModule, BrowserAnimationsModule,
                    MatSliderModule, MatFormFieldModule, MatInputModule, MatButtonModule,
                    MatDialogModule, FormsModule, MatSelectModule, MatSlideToggleModule,
                    ReactiveFormsModule, MatButtonToggleModule, MatOptionModule,
                    MatCardModule, MatProgressSpinnerModule, MatChipsModule, MatSnackBarModule
                ],
            declarations:
                [
                    AppComponent, ColorComponent, BrushComponent,
                    LineComponent, PencilComponent, RectangleComponent,
                    EntryPointComponent, UserGuideComponent, CreateNewDrawingComponent,
                    ColorApplicatorComponent, SelectionComponent, SprayComponent,
                    GridComponent, EraserComponent, ExportDrawingComponent, EllipseComponent,
                    ConfirmationDialogComponent, DrawingGalleryComponent, SaveManagerComponent,
                    ColorPickerComponent, PolygonComponent, ColorDocComponent, DrawingDocComponent,
                    FunctionalitiesDocComponent, SelectionDocComponent, ShapeDocComponent, PaintBucketComponent
                ],
            providers:
                [
                    { provide: MouseHandlerService, useValue: mouseHandlerServiceSpy },
                    { provide: SvgHandlerService, useValue: svgHandlerServiceSpy },
                    { provide: GridService, useValue: gridServiceSpy },
                    { provide: UndoRedoService, useValue: undoRedoSpy },
                    { provide: ClipboardService, useValue: clipboardServiceSpy }
                ]
        })
            .overrideModule(
                BrowserDynamicTestingModule,
                {
                    set: {
                        entryComponents: [EntryPointComponent, CreateNewDrawingComponent,
                            ExportDrawingComponent, DrawingGalleryComponent, SaveManagerComponent]
                    }
                });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should open Dialog when page load', () => {
        expect(EntryPointComponent).toBeTruthy();
    });

    it('should return if right state is selected', () => {
        component.changeState(States.brush);
        expect(component.isCorrectState(States.brush)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should open CreateNewDrawingComponent dialog when createNewDrawing() function is called', () => {
        component.createNewDrawing();
        expect(CreateNewDrawingComponent).toBeTruthy();
        expect(component.isCorrectState(States.newDrawing)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should open drawingGalleryComponent dialog when openDrawingGallery() function is called', () => {
        component.openDrawingGallery();
        expect(DrawingGalleryComponent).toBeTruthy();
        expect(component.isCorrectState(States.drawingGallery)).toBeTruthy();
        expect(component.isCorrectState(States.eraser)).toBeFalsy();
    });

    it('should change state to pencil when c is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const cKeyMock = new KeyboardEvent('keydown', { key: 'c' });
        component.changeState(States.brush);
        svgHandlerServiceSpy.isLineBeingCreated = false;
        mouseHandlerServiceSpy.isMouseDown = false;
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(cKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should not change state because button down', () => {
        const cKeyMock = new KeyboardEvent('keydown', { key: 'c' });
        svgHandlerServiceSpy.isLineBeingCreated = false;
        component['newDrawingService'].blockNavigation = false;
        mouseHandlerServiceSpy.isMouseDown = true;
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(cKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should not change state because line is being drawn', () => {
        const cKeyMock = new KeyboardEvent('keydown', { key: 'c' });
        component['newDrawingService'].blockNavigation = false;
        svgHandlerServiceSpy.isLineBeingCreated = true;
        mouseHandlerServiceSpy.isMouseDown = false;
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(cKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should not change state when x is pressed', () => {
        const xKeyMock = new KeyboardEvent('keydown', { key: 'x' });
        component['newDrawingService'].blockNavigation = false;
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(xKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
        expect(component.isCorrectState(States.brush)).toBeTruthy();
    });

    it('should change state to newDrawing when CTRL+o is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'o', ctrlKey: true });
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.newDrawing)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should change state to save when CTRL+s is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', {key: 's', ctrlKey: true});
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.save)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should not change state to newDrawing or save when CTRL+p is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlPKeyMock = new KeyboardEvent('keydown', { key: 'p', ctrlKey: true });
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlPKeyMock);
        expect(component.isCorrectState(States.newDrawing)).toBeFalsy();
        expect(component.isCorrectState(States.brush)).toBeTruthy();
    });

    it('should change state to exportDrawing when CTRL+e is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlEKeyMock = new KeyboardEvent('keydown', { key: 'e', ctrlKey: true });
        component.changeState(States.drawingExport);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlEKeyMock);
        expect(component.isCorrectState(States.drawingExport)).toBeTruthy();
        expect(component.isCorrectState(States.brush)).toBeFalsy();
    });

    it('should change state to drawingGallery when CTRL+g is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlGKeyMock = new KeyboardEvent('keydown', { key: 'g', ctrlKey: true });
        component.changeState(States.drawingGallery);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlGKeyMock);
        expect(component.isCorrectState(States.drawingGallery)).toBeTruthy();
        expect(component.isCorrectState(States.eraser)).toBeFalsy();
    });

    it('should not change state when dialog is open', () => {
        component['newDrawingService'].blockNavigation = false;
        const cKeyMock = new KeyboardEvent('keydown', { key: 'c' });
        component.changeState(States.brush);
        component['dialog'].openDialogs.length = 1;
        component.handleKeyDown(cKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
        expect(component.isCorrectState(States.brush)).toBeTruthy();
    });

    it('should handle grid key event', () => {
        component['newDrawingService'].blockNavigation = false;
        const plusKeyMock = new KeyboardEvent('keydown', { key: '+' });
        const minusKeyMock = new KeyboardEvent('keydown', { key: '-' });
        const gKeyMock = new KeyboardEvent('keydown', { key: 'g' });
        svgHandlerServiceSpy.isLineBeingCreated = false;
        mouseHandlerServiceSpy.isMouseDown = false;
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(plusKeyMock);
        expect(gridServiceSpy.handleKey).toHaveBeenCalledTimes(1);
        component.handleKeyDown(minusKeyMock);
        expect(gridServiceSpy.handleKey).toHaveBeenCalledTimes(2);
        component.handleKeyDown(gKeyMock);
        expect(gridServiceSpy.handleKey).toHaveBeenCalledTimes(3);
    });

    it('should change state to brush when w is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'w' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.brush)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to line when l is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'l' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.line)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to rectangle when 1 is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: '1' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.rectangle)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to selection when s is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 's' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.selection)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to spray when a is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'a' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.spray)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to colorPicker when i is pressed', () => {
        component.svgHandlerService.svgCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'i' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.colorPicker)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to colorApplicator when r is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: 'r' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.colorApplicator)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change state to ellipse when 2 is pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: '2' });
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.ellipse)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should not change state to ellipse when 2 is pressed but stateService isModifyingInput', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlOKeyMock = new KeyboardEvent('keydown', { key: '2' });
        component['stateService'].isModifyingInput = true;
        component.changeState(States.pencil);
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlOKeyMock);
        expect(component.isCorrectState(States.pencil)).toBeTruthy();
        expect(component.isCorrectState(States.ellipse)).toBeFalsy();
    });

    it('should call undo when ctrl+z pressed', () => {
        component['newDrawingService'].blockNavigation = false;
        const ctrlZKeyMock = new KeyboardEvent('keydown', {key: 'z', ctrlKey: true});
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlZKeyMock);
        expect(undoRedoSpy.undo).toHaveBeenCalled();
    });

    it('should call redo when ctrl+shift+Z pressed', () => {
        const ctrlShiftZKeyMock = new KeyboardEvent('keydown', {key: 'Z', ctrlKey: true, shiftKey: true});
        component['newDrawingService'].blockNavigation = false;
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(ctrlShiftZKeyMock);
        expect(undoRedoSpy.redo).toHaveBeenCalled();
    });

    it('should open SavaManager dialog when state is set to save', () => {
        component.changeState(States.save);
        expect(SaveManagerComponent).toBeTruthy();
        expect(component.isCorrectState(States.save)).toBeTruthy();
        expect(component.isCorrectState(States.pencil)).toBeFalsy();
    });

    it('should change css with different screen sizes', () => {
        spyOnProperty(window, 'innerHeight').and.returnValue(1000);
        window.dispatchEvent(new Event('resize'));
        expect(component.iconFooterClass).toBe('bottomStick');
        expect(component.sidenavClass).toBe('leftSidenavNormal');
    });

    it('should handle clipboard key event', () => {
        component['newDrawingService'].blockNavigation = false;
        const copyKeyMock = new KeyboardEvent('keydown', { key: 'c', ctrlKey: true });
        const duplicateKeyMock = new KeyboardEvent('keydown', { key: 'd', ctrlKey: true });
        const cutAllKeyMock = new KeyboardEvent('keydown', { key: 'x', ctrlKey: true });
        const deleteKeyMock = new KeyboardEvent('keydown', { key: 'Delete' });
        const pasteKeyMock = new KeyboardEvent('keydown', { key: 'v', ctrlKey: true });
        const ctrlZKeyMock = new KeyboardEvent('keydown', {key: 'z', ctrlKey: true});
        svgHandlerServiceSpy.isLineBeingCreated = false;
        mouseHandlerServiceSpy.isMouseDown = false;
        component['dialog'].openDialogs.length = 0;
        component.handleKeyDown(copyKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(1);
        component.handleKeyDown(duplicateKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(2);
        component.handleKeyDown(cutAllKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(3);
        component.handleKeyDown(deleteKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(4);
        component.handleKeyDown(pasteKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(5);
        component.handleKeyDown(ctrlZKeyMock);
        expect(clipboardServiceSpy.handleKey).toHaveBeenCalledTimes(5);
    });
});
