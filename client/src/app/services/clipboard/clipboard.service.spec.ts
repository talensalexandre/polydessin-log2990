import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Renderer2 } from '@angular/core';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionCalculatorService } from '../selection-services/selection-calculator/selection-calculator.service';
import { SelectionMoveService } from '../selection-services/selection-move/selection-move.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { ClipboardService } from './clipboard.service';

// tslint:disable:no-magic-numbers no-any no-string-literal

describe('ClipboardService', () => {
    let service: ClipboardService;
    let selectionSvgHandlerServiceSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let selectionCalculatorServiceSpy: jasmine.SpyObj<SelectionCalculatorService>;
    let saveManagerServiceSpy: jasmine.SpyObj<SaveManagerService>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let selectionMoveSpy: jasmine.SpyObj<SelectionMoveService>;
    const fakeSvgElementRect = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeSvgElementPolygon = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polygon');

    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, HttpClientTestingModule],
        providers: [{ provide: SelectionSvgHandlerService, useValue: selectionSvgHandlerServiceSpy }],
    }));

    beforeEach(() => {
        selectionSvgHandlerServiceSpy = jasmine.createSpyObj(
            'SelectionSvgHandlerService',
            [
                'selectAll',
                'unselectAll',
                'selectObject'
            ]
        );
        selectionCalculatorServiceSpy = jasmine.createSpyObj(
            'SelectionCalculatorService',
            [
                'selectedObjects'
            ]
        );
        saveManagerServiceSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'svgContainer',
                'removeSvgElement',
                'addSvgElement'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'elements'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'UndoRedoService',
            [
                'current',
                'updateCurrent'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        selectionMoveSpy = jasmine.createSpyObj(
            'SelectionMoveService',
            [
                'translate'
            ]
        );
        svgHandlerSpy.renderer = rendererSpy;
        svgHandlerSpy.elements = new Array();
    });

    beforeEach(() => {
        service = new ClipboardService(selectionCalculatorServiceSpy,
            selectionSvgHandlerServiceSpy,
            saveManagerServiceSpy,
            svgHandlerSpy,
            undoRedoSpy,
            selectionMoveSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should handle selectAll', () => {
        const selectAll = 'a';
        service.handleKey(selectAll);
        expect(selectionSvgHandlerServiceSpy.selectAll).toHaveBeenCalled();
    });

    it('should handle delete', () => {
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const deleteKey = 'Delete';
        service.handleKey(deleteKey);
        expect(undoRedoSpy.updateCurrent).toHaveBeenCalledTimes(1);
        expect(svgHandlerSpy.renderer.removeChild).toHaveBeenCalledTimes(2);
        expect(saveManagerServiceSpy.removeSvgElement).toHaveBeenCalledTimes(2);
        expect(selectionSvgHandlerServiceSpy.unselectAll).toHaveBeenCalledTimes(1);
    });

    it('should reset clipboard when copy without selection', () => {
        spyOn<any>(service, 'copy').and.callThrough();
        service['clipboard'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const copyKey = 'c';
        service.handleKey(copyKey);
        expect(service['copy']).toHaveBeenCalledWith(true);
        expect(service['clipboard'].length).toBe(0);
    });

    it('should copy to clipboard', () => {
        spyOn<any>(service, 'copy').and.callThrough();
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const copyKey = 'c';
        service.handleKey(copyKey);
        expect(service['copy']).toHaveBeenCalledWith(true);
        expect(service['clipboard'].length).toBe(2);
    });

    it('should handle cut', () => {
        spyOn<any>(service, 'copy').and.callThrough();
        spyOn<any>(service, 'delete');
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const cutKey = 'x';
        service.handleKey(cutKey);
        expect(service['clipboard'].length).toBe(2);
        expect(service['copy']).toHaveBeenCalledWith(true);
        expect(service['delete']).toHaveBeenCalledTimes(1);
    });

    it('should handle paste', () => {
        spyOn<any>(service, 'copy').and.callThrough();
        service['clipboard'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const pasteKey = 'v';
        const fakeBoundingClientRectValues = { coordinates: { x: 10, y: 10 }, height: 50, width: 50 };
        selectionCalculatorServiceSpy.boundaryDimensions = fakeBoundingClientRectValues;
        service.handleKey(pasteKey);
        expect(service['clipboard'].length).toBe(2);
        expect(service['copy']).toHaveBeenCalledWith(false);
    });

    it('should not paste outside of canvans', () => {
        spyOn<any>(service, 'copy').and.callThrough();
        service['clipboard'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        svgHandlerSpy.canvasHeight = 100;
        svgHandlerSpy.canvasWidth = 100;
        const pasteKey = 'v';
        const fakeBoundingClientRectValues = { coordinates: { x: 90, y: 90 }, height: 50, width: 50 };
        selectionCalculatorServiceSpy.boundaryDimensions = fakeBoundingClientRectValues;
        service.handleKey(pasteKey);
        expect(service['clipboard'].length).toBe(2);
        expect(service['copy']).toHaveBeenCalledWith(false);
    });

    it('should handle duplicate', () => {
        spyOn<any>(service, 'duplicate').and.callThrough();
        spyOn<any>(service, 'appendSVGElement').and.callThrough();
        service['clipboard'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        service['selectedObjects'] = [fakeSvgElementPolygon];
        const duplicateKey = 'd';
        const fakeBoundingClientRectValues = { coordinates: { x: 10, y: 10 }, height: 50, width: 50 };
        selectionCalculatorServiceSpy.boundaryDimensions = fakeBoundingClientRectValues;
        service.handleKey(duplicateKey);
        expect(service['duplicate']).toHaveBeenCalled();
        expect(service['appendSVGElement']).toHaveBeenCalled();
        expect(service['firstSelectedObjectsToDuplicate'].length).toBe(1);
        expect(selectionSvgHandlerServiceSpy.unselectAll).toHaveBeenCalled();
        expect(service['clipboard'].length).toBe(2);
    });

    it('should handle delete', () => {
        spyOn<any>(service, 'delete').and.callThrough();
        service['selectedObjects'] = [fakeSvgElementPolygon];
        const deleteKey = 'Delete';
        service.handleKey(deleteKey);
        expect(selectionSvgHandlerServiceSpy.unselectAll).toHaveBeenCalled();
        expect(saveManagerServiceSpy.removeSvgElement).toHaveBeenCalled();
    });

    it('should handle select All', () => {
        spyOn<any>(service, 'selectAll').and.callThrough();
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const selectAllKey = 'a';
        service.handleKey(selectAllKey);
        expect(selectionSvgHandlerServiceSpy.selectAll).toHaveBeenCalled();
    });

    it('should not handle key', () => {
        spyOn<any>(service, 'selectAll');
        spyOn<any>(service, 'delete');
        spyOn<any>(service, 'duplicate');
        spyOn<any>(service, 'copy');
        spyOn<any>(service, 'cut');
        spyOn<any>(service, 'paste');
        service['selectedObjects'] = [fakeSvgElementPolygon, fakeSvgElementRect];
        const randomKey = 'h';
        service.handleKey(randomKey);
        expect(service['selectAll']).not.toHaveBeenCalled();
        expect(service['delete']).not.toHaveBeenCalled();
        expect(service['duplicate']).not.toHaveBeenCalled();
        expect(service['copy']).not.toHaveBeenCalled();
        expect(service['cut']).not.toHaveBeenCalled();
        expect(service['paste']).not.toHaveBeenCalled();
    });
});
