import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectionType } from 'src/app/interfaces-enums/selection-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { RectangleHandlerService } from 'src/app/services/rectangle-handler/rectangle-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { RectangleComponent } from './rectangle.component';

// tslint:disable:no-string-literal

describe('RectangleComponent', () => {
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let mouseHandler: MouseHandlerService;
    let rectangleHandlerSpy: jasmine.SpyObj<RectangleHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let saveManagerServiceSpy: jasmine.SpyObj<SaveManagerService>;
    let component: RectangleComponent;
    let fixture: ComponentFixture<RectangleComponent>;

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]);
        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        rectangleHandlerSpy = jasmine.createSpyObj('RectangleHandlerService',
            [
                'createRectangle',
                'calculateSquare',
                'calculateRectangle',
                'rectanglePrevisualization',
                'elementPrevisualization',
                'createElement',
                'isShapeTooBigComparedToSize'
            ]
        );
        saveManagerServiceSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement'
        ]);
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['removeElement']);
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports:
            [
                MatFormFieldModule, MatSelectModule, BrowserAnimationsModule,
                FormsModule, MatInputModule, MatIconModule, MatDialogModule,
                HttpClientModule
            ],
        declarations: [ RectangleComponent, ColorComponent ],
        providers: [{provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
                    {provide: MouseHandlerService, useValue: mouseHandlerServiceSpy},
                    {provide: RectangleHandlerService, useValue: rectangleHandlerSpy},
                    {provide: DrawingService, useValue: drawingServiceSpy},
                    {provide: SaveManagerService, useValue: saveManagerServiceSpy}]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RectangleComponent);
        mouseHandler = TestBed.get(MouseHandlerService);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        component.ngOnInit();
        expect(svgHandlerServiceSpy.addListeners).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', { key: 'Shift' });
        mouseHandler.isMouseDown = true;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeTruthy();
        expect(rectangleHandlerSpy.calculateSquare).toHaveBeenCalled();
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', { key: 'Shift' });
        mouseHandler.isMouseDown = false;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeTruthy();
        expect(rectangleHandlerSpy.calculateSquare).toHaveBeenCalledTimes(0);
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', { key: 'alt' });
        mouseHandler.isMouseDown = false;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(rectangleHandlerSpy.calculateSquare).toHaveBeenCalledTimes(0);
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', { key: 'Shift' });
        mouseHandler.isMouseDown = true;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(rectangleHandlerSpy.calculateRectangle).toHaveBeenCalled();
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', { key: 'alt' });
        mouseHandler.isMouseDown = false;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(rectangleHandlerSpy.calculateRectangle).toHaveBeenCalledTimes(0);
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', { key: 'Shift' });
        mouseHandler.isMouseDown = false;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(rectangleHandlerSpy.calculateRectangle).toHaveBeenCalledTimes(0);
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should call onMouseMovement when mouse moves with shiftKeyDown=true and mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        const shiftKeyDown = true;
        component.selectedType = SelectionType.outline;
        component['shiftKeyDown'] = shiftKeyDown;
        mouseHandler.isMouseDown = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(rectangleHandlerSpy.calculateSquare).toHaveBeenCalled();
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should change fill and stroke when contour', () => {
        component.selectedType = SelectionType.outline;
        component.layoutType();
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
    });

    it('should change fill and stroke when plein', () => {
        component.selectedType = SelectionType.fill;
        component.layoutType();
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeFalsy();
    });

    it('should change fill and stroke when plein avec contour', () => {
        component.selectedType = SelectionType.outlineAndFill;
        component.layoutType();
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
    });

    it('should call onMouseUp when eventListeners.mouseUp is called', () => {
        const mockMouseUp = new MouseEvent('mouseup');
        component['mouseMoved'] = true;
        component.layoutType();
        component.selectedType = SelectionType.outline;
        component['eventListeners'].mouseUp(mockMouseUp);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(rectangleHandlerSpy.createElement).toHaveBeenCalledTimes(1);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should call onMouseDown when eventListeners.mouseDown is called', () => {
        const mockMouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouseDown);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should call mouseMouvementExtend when mouse moves with shiftKeyDown=false and mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        const shiftKeyDown = false;
        component.selectedType = SelectionType.outline;
        component['shiftKeyDown'] = shiftKeyDown;
        mouseHandler.isMouseDown = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(rectangleHandlerSpy.calculateRectangle).toHaveBeenCalled();
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should should call eventListeners.mouseLeave when mouse moves out of canvas', () => {
        component['isDrawing'] = true;
        const mouseEvent = new MouseEvent('click');
        component['mouseHandler'].isMouseDown = true;
        component['eventListeners'].mouseLeave(mouseEvent);
        expect(component['isDrawing']).toEqual(false);
        expect(component['mouseHandler'].isMouseDown).toEqual(false);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
    });
});
