import { Component } from '@angular/core';
import { ElementType } from 'src/app/interfaces-enums/element-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { RectangleHandlerService } from 'src/app/services/rectangle-handler/rectangle-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { StateSelectorService } from '../../services/state-selector/state-selector.service';
import { AbstractShapeComponent } from '../abstract-tool/abstract-shape.component';

@Component({
    selector: 'app-rectangle',
    templateUrl: './rectangle.component.html',
    styleUrls: ['./rectangle.component.scss'],
})
export class RectangleComponent extends AbstractShapeComponent {

    constructor(
        protected mouseHandler: MouseHandlerService,
        protected svgHandler: SvgHandlerService,
        public stateSelector: StateSelectorService,
        private rectangleHandler: RectangleHandlerService,
        protected drawingService: DrawingService,
        protected saveManager: SaveManagerService
    ) {
        super(mouseHandler, svgHandler, stateSelector, drawingService, saveManager);
    }

    protected handleKeyDownExtend(): void {
        this.rectangleHandler.calculateSquare(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.previsualisationElement = this.rectangleHandler.elementPrevisualization(
            Boolean(this.fill),
            Boolean(this.stroke),
            this.size,
            ElementType.rect,
            this.previsualisationElement
        );
    }

    protected handleKeyUpExtend(): void {
        this.rectangleHandler.calculateRectangle(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.previsualisationElement = this.rectangleHandler.elementPrevisualization(
            Boolean(this.fill),
            Boolean(this.stroke),
            this.size,
            ElementType.rect,
            this.previsualisationElement
        );
    }

    protected destroyExtend(): void {
        this.drawingService.removeElement(this.previsualisationElement);
    }

    protected mouseMouvementCalculateShiftShape(): void {
        this.rectangleHandler.calculateSquare(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.isSelectionTooSmall = this.rectangleHandler.isShapeTooBigComparedToSize(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size / 2
        );
    }

    protected mouseMouvementCalculateNormalShape(): void {
        this.rectangleHandler.calculateRectangle(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.isSelectionTooSmall = this.rectangleHandler.isShapeTooBigComparedToSize(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size / 2
        );
    }

    protected mouseMouvementPrevisualization(): void {
        this.previsualisationElement = this.rectangleHandler.elementPrevisualization(
            this.fill,
            this.stroke,
            this.size,
            ElementType.rect,
            this.previsualisationElement
        );
    }

    protected mouseUpCreateShape(): void {
        this.svgElement = this.rectangleHandler.createElement(
            this.fill,
            this.stroke,
            this.size,
            ElementType.rect
        );
    }

    protected mouseLeftExtend(): void {
        return;
    }

    protected mouseUpExtendExtra(): void {
        return;
    }
}
