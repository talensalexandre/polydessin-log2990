export interface EventListeners {
    mouseMove: (event: MouseEvent) => boolean | void;
    changedMouseMove: boolean;
    mouseDown: (event: MouseEvent) => boolean | void;
    changedMouseDown: boolean;
    mouseUp: (event: MouseEvent) => boolean | void;
    changedMouseUp: boolean;
    mouseLeave: (event: MouseEvent) => boolean | void;
    changedMouseLeave: boolean;
    mouseClick: (event: MouseEvent) => boolean | void;
    changedMouseClick: boolean;
    mouseDblClick: (event: MouseEvent) => boolean | void;
    changedMouseDblClick: boolean;
    mouseWheel: (event: WheelEvent) => boolean | void;
    changedWheelPosition: boolean;
    contextMenu: (event: MouseEvent) => boolean | void;
    changedContextMenu: boolean;
}
