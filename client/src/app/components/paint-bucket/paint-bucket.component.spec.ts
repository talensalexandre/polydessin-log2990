import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PaintBucketHandlerService } from 'src/app/services/paint-bucket-handler/paint-bucket-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { PaintBucketComponent } from './paint-bucket.component';

// tslint:disable:no-magic-numbers no-string-literal

describe('PaintBucketComponent', () => {
    let component: PaintBucketComponent;
    let fixture: ComponentFixture<PaintBucketComponent>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let paintBucketHandlerSpy: jasmine.SpyObj<PaintBucketHandlerService>;

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj('SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        mouseHandlerServiceSpy = jasmine.createSpyObj('MouseHandlerService',
            [
                'onMouseLeave',
                'onMouseClick',
                'makeActualCoordsEndCoords',
                'updateEndCoords',
                'onMouseMovement',
                'onMouseDown',
                'onMouseUp'
            ]
        );
        paintBucketHandlerSpy = jasmine.createSpyObj('PaintBucketHandlerService',
            [
                'convertSvgToCanvas',
                'tolerance',
                'getBaseColor',
                'createPath',
                'findAllAvailableSpace',
                'calculatePaintBucketArea',
                'killProcess'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                    FormsModule, MatSelectModule, MatIconModule, HttpClientModule,
                    MatDialogModule
                ],
            declarations:
                [
                    PaintBucketComponent, ColorComponent
                ],
            providers:
                [
                    { provide: SvgHandlerService, useValue: svgHandlerServiceSpy },
                    { provide: MouseHandlerService, useValue: mouseHandlerServiceSpy },
                    { provide: PaintBucketHandlerService, useValue: paintBucketHandlerSpy }
                ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaintBucketComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        component['mouseMouvementExtend']();
        component['mouseUpExtend']();
        expect(component).toBeTruthy();
    });

    it('should call convertSvgToCanvas when when afterView is called', () => {
        component.ngAfterViewInit();
        expect(paintBucketHandlerSpy.convertSvgToCanvas).toHaveBeenCalledWith(component.htmlCanvas);
    });

    it('should change size if above 100', () => {
        component['paintBucketHandler'].tolerance = 200;
        component.changeSize();
        expect(component['paintBucketHandler'].tolerance).toBe(100);
    });

    it('should change size if under 0', () => {
        component['paintBucketHandler'].tolerance = -200;
        component.changeSize();
        expect(component['paintBucketHandler'].tolerance).toBe(0);
    });

    it('should not change size', () => {
        component['paintBucketHandler'].tolerance = 50;
        component.changeSize();
        expect(component['paintBucketHandler'].tolerance).toBe(50);
    });

    it('should set isDrawing to false', () => {
        component['destroyExtend']();
        expect(component['isDrawing']).toBeFalsy();
    });

    it('should set extend mouseDown', () => {
        const fakeCoords = {x: 100, y: 100};
        component['paintBucketHandler'].filling = false;
        component['mouseDownExtend'](fakeCoords);
        expect(paintBucketHandlerSpy['calculatePaintBucketArea']).toHaveBeenCalledWith(fakeCoords, component.htmlCanvas);
    });

    it('should not set extend mouseDown', () => {
        const fakeCoords = {x: 100, y: 100};
        component['paintBucketHandler'].filling = true;
        component['mouseDownExtend'](fakeCoords);
        expect(paintBucketHandlerSpy['calculatePaintBucketArea']).not.toHaveBeenCalled();
    });

    it('should increase tolereance', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeSize').and.callThrough();
        component.increaseTolerance();
        expect(component.changeSize).toHaveBeenCalledTimes(1);
    });

    it('should decrease tolereance', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeSize').and.callThrough();
        component.decreaseTolerance();
        expect(component.changeSize).toHaveBeenCalledTimes(1);
    });

    it('should change variable to true when esc key is pressed', () => {
        component['dialog'].openDialogs.length = 1;
        const mockDownKey = new KeyboardEvent('keydown', { key: 'Escape' });
        component.handleKeyDown(mockDownKey);
        expect(component.paintBucketHandler.killProcess).toBeTruthy();
    });

    it('should not change variable to true when esc key is pressed because dialog is open', () => {
        component.paintBucketHandler.killProcess = false;
        component['dialog'].openDialogs.length = 0;
        const mockDownKey = new KeyboardEvent('keydown', { key: 'Escape' });
        component.handleKeyDown(mockDownKey);
        expect(component.paintBucketHandler.killProcess).toBeTruthy();
    });

    it('should change variable to true when esc key is pressed', () => {
        component.paintBucketHandler.killProcess = false;
        component['dialog'].openDialogs.length = 0;
        const mockDownKey = new KeyboardEvent('keydown', { key: 'x' });
        component.handleKeyDown(mockDownKey);
        expect(component.paintBucketHandler.killProcess).toBeFalsy();
    });
});
