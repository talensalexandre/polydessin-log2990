import { HttpClientModule } from '@angular/common/http';
import { ElementRef, Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ExportDrawingService } from './export-drawing.service';

const GRAYSCALE_COLOR = 191.25;
const SEPIA_COLOR_RED = 344.505;
const SEPIA_COLOR_GREEN = 306.765;
const SEPIA_COLOR_BLUE = 238.93500000000003;
const INVERT_COLOR = 0;

// tslint:disable:no-magic-numbers max-file-line-count no-any no-string-literal

describe('ExportDrawingService', () => {
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let service: ExportDrawingService;
    let fakeElement: SVGElement;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [Renderer2, HttpClientModule]
        }).compileComponents();
    });

    beforeEach(() => {
        service = TestBed.get(ExportDrawingService);
        rendererSpy = jasmine.createSpyObj('Renderer2',
            [
                'setAttribute',
                'removeAttribute',
                'removeStyle',
                'appendChild',
                'setStyle'
            ]);
        service['renderer'] = rendererSpy;
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should apply right filter', () => {
        spyOn<any>(service, 'grayscaleBackGroundColor');
        spyOn<any>(service, 'sepiaBackgroundColor');
        spyOn<any>(service, 'invertBackgroundColor');

        service.applySvgFilter(1, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['grayscaleBackGroundColor']).toHaveBeenCalledTimes(0);

        service.applySvgFilter(1, fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['grayscaleBackGroundColor']).toHaveBeenCalled();

        service.applySvgFilter(2, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['sepiaBackgroundColor']).toHaveBeenCalledTimes(0);

        service.applySvgFilter(2, fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['sepiaBackgroundColor']).toHaveBeenCalled();

        service.applySvgFilter(3, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['invertBackgroundColor']).toHaveBeenCalledTimes(0);

        service.applySvgFilter(3, fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
        expect(service['invertBackgroundColor']).toHaveBeenCalled();

        service.applySvgFilter(4, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();

        service.applySvgFilter(5, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalled();
    });

    it('should not apply filter', () => {
        service.applySvgFilter(12, fakeElement, true);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(0);
        expect(rendererSpy.removeAttribute).toHaveBeenCalled();
    });

    it('should initialize previsualization', () => {
        const elRef: ElementRef = new ElementRef(fakeElement);
        service.initialisePrevisualization(fakeElement, elRef);
        expect(rendererSpy.removeStyle).toHaveBeenCalledTimes(3);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(1);
    });

    it('should initialize download file', () => {
        service.initialiseDownloadFile(fakeElement);
        expect(rendererSpy.removeStyle).toHaveBeenCalledTimes(3);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(2);
    });

    it('sepiaBackgroundColor should change style', () => {
        spyOn<any>(service, 'changeBackGroundColor');
        service['sepiaBackgroundColor'](fakeElement);
        expect(service['changeBackGroundColor']).toHaveBeenCalledWith(fakeElement, SEPIA_COLOR_RED, SEPIA_COLOR_GREEN, SEPIA_COLOR_BLUE, 1);
    });

    it('grayscaleBackGroundColor should change style', () => {
        spyOn<any>(service, 'changeBackGroundColor');
        service['grayscaleBackGroundColor'](fakeElement);
        expect(service['changeBackGroundColor']).toHaveBeenCalledWith(fakeElement, GRAYSCALE_COLOR, GRAYSCALE_COLOR, GRAYSCALE_COLOR, 1);
    });

    it('invertBackgroundColor should change style', () => {
        spyOn<any>(service, 'changeBackGroundColor');
        service['invertBackgroundColor'](fakeElement);
        expect(service['changeBackGroundColor']).toHaveBeenCalledWith(fakeElement, INVERT_COLOR, INVERT_COLOR, INVERT_COLOR, 1);
    });

    it('Shoul change backGround color', () => {
        service['changeBackGroundColor'](fakeElement, 0, 1, 2, 1);
        expect(rendererSpy.setStyle).toHaveBeenCalledWith(fakeElement, 'background-color', 'rgba(' +
            0 + ', ' + 1 + ', ' + 2 + ', ' + 1 + ')');
    });
});
