import { SimpleChange } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { AlphaColorGradientComponent } from './alpha-color-gradient.component';

const FIRST_NUMBER = 75;
const SECOND_AND_THIRD_NUMBER = 25;
const LAST_NUMBER = 255;
const CLAMPED_ARRAY = [FIRST_NUMBER, SECOND_AND_THIRD_NUMBER, SECOND_AND_THIRD_NUMBER, LAST_NUMBER];

describe('AlphaColorGradientComponent', () => {
    let component: AlphaColorGradientComponent;
    let fixture: ComponentFixture<AlphaColorGradientComponent>;
    let colorManagerSpy: jasmine.SpyObj<ColorManagerService>;

    beforeEach(() => {
        colorManagerSpy = jasmine.createSpyObj(
            'ColorManagerService',
            [
                'getColorStringWithCustomAlpha',
                'updateColorWithPixel'
            ]
        );
        colorManagerSpy.getColorStringWithCustomAlpha.and.returnValue('rgba(255,255,255,1)');
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            declarations: [ AlphaColorGradientComponent ],
            providers: [{ provide: MAT_DIALOG_DATA, useValue: {}},
                        { provide: ColorManagerService, useValue: colorManagerSpy}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AlphaColorGradientComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should update gradient with color picked', () => {
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        const pixels: Uint8ClampedArray = new Uint8ClampedArray(CLAMPED_ARRAY);
        const colorPixel: ImageData = new ImageData(pixels, 1, 1);
        contextSpy.getImageData.and.returnValue(colorPixel);
        component.updateColorWithCoordinates({x: 1, y: 1});
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalled();
    });

    it('should update main gradient if color is updated', () => {
        spyOn(component, 'createAlphaGradiantDisplayer').and.callThrough();
        component.ngOnChanges({
            shouldUpdateGradient: new SimpleChange(undefined, 'rgba(0,0,0,1)', true)
        });
        fixture.detectChanges();
        expect(component.createAlphaGradiantDisplayer).toHaveBeenCalledTimes(0);
        component.ngOnChanges({
            shouldUpdateGradient: new SimpleChange(undefined, 'rgba(255,0,0,1)', false)
        });
        fixture.detectChanges();
        expect(component.createAlphaGradiantDisplayer).toHaveBeenCalledTimes(1);
        component.ngOnChanges({
            shouldUpdateForAlpha: new SimpleChange(undefined, 'rgba(255,0,0,0.5)', false)
        });
        fixture.detectChanges();
        expect(component.createAlphaGradiantDisplayer).toHaveBeenCalledTimes(2);
    });

    it('should update color if user clic on gradient', () => {
        spyOn(component, 'updateColorWithCoordinates').and.callThrough();
        const mockMouse = new MouseEvent('mousedown');
        component.mouseDownFromGradient(mockMouse);
        fixture.detectChanges();
        expect(component.updateColorWithCoordinates).toHaveBeenCalledTimes(1);
    });

    it('should not update gradient with color picked', () => {
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        contextSpy.getImageData.and.returnValue({data: undefined});
        component.allGradientContext = contextSpy;
        component.updateColorWithCoordinates({x: 1, y: 1});
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalledTimes(0);
    });
});
