import { HttpClientModule } from '@angular/common/http';
import { Renderer2 } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatSnackBarModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color/color.component';
import { ColorPickerComponent } from './color-picker.component';

// tslint:disable:no-string-literal no-any

const FIRST_NUMBER = 75;
const SECOND_AND_THIRD_NUMBER = 0;
const LAST_NUMBER = 1;
const CLAMPED_ARRAY = [FIRST_NUMBER, SECOND_AND_THIRD_NUMBER, SECOND_AND_THIRD_NUMBER, LAST_NUMBER];

describe('ColorPickerComponent', () => {
    let component: ColorPickerComponent;
    let fixture: ComponentFixture<ColorPickerComponent>;
    let colorManagerSpy: jasmine.SpyObj<ColorManagerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    const fakeCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const mainDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const secondaryDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const backgroundDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };

    beforeEach(() => {
        colorManagerSpy = jasmine.createSpyObj(
            'ColorManagerService',
                [
                    'updateColorWithPixel'
                ]
        );
        colorManagerSpy.colorSelected = new Array<RGBA>();
        colorManagerSpy.colorSelected[ColorChoice.mainColor] = mainDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.secondaryColor] = secondaryDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.backgroundColor] = backgroundDefaultColor;
        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'addListeners',
                'deleteListeners',
                'createCanvas',
                'svgCanvas'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement'
            ]
        );
        svgHandlerSpy.renderer = rendererSpy;
        svgHandlerSpy.svgCanvas = fakeCanvas;
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                    FormsModule, MatIconModule, MatDialogModule, HttpClientModule,
                    MatSnackBarModule
                ],
            declarations: [ ColorPickerComponent, ColorComponent ],
            providers: [
                { provide: ColorManagerService, useValue: colorManagerSpy },
                { provide: SvgHandlerService, useValue: svgHandlerSpy },
                { provide: MouseHandlerService, useValue: mouseHandlerServiceSpy },
                { provide: SaveManagerService, useValue: saveManagerSpy }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPickerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        const mouseEvent = new MouseEvent('mousedown');
        const mouseEventButton = new MouseEvent('click', { button: 2 });
        component['eventListeners'].mouseDown(mouseEvent);
        component['eventListeners'].contextMenu(mouseEventButton);
        component.ngOnInit();
        expect(component['eventListeners'].changedMouseDown).toBeTruthy();
    });

    it('should delete listeners when closed', () => {
        component.ngOnDestroy();
        expect(svgHandlerSpy.deleteListeners).toHaveBeenCalled();
    });

    it('should prevent context menu from opening', () => {
        const clickSpy = jasmine.createSpyObj('MouseEvent', ['preventDefault']);
        component['eventListeners'].contextMenu(clickSpy);
        expect(clickSpy.preventDefault).toHaveBeenCalledTimes(1);
    });

    it('should change main color when left button of mouse down', () => {
        spyOn<any>(component, 'openSnackBar');
        const mouseEvent = new MouseEvent('click', { button: 0 });
        const coordinates = {x: 1, y: 1};
        component.coordinates = coordinates;
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        component.context = contextSpy;
        const pixels: Uint8ClampedArray = new Uint8ClampedArray(CLAMPED_ARRAY);
        const colorPixel: ImageData = new ImageData(pixels, 1, 1);
        contextSpy.getImageData.and.returnValue(colorPixel);
        component.colorPixel = pixels;
        component['eventListeners'].mouseDown(mouseEvent);
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalled();
        expect(component['openSnackBar']).toHaveBeenCalled();
    });

    it('should change secondary color when right button of mouse down', () => {
        spyOn<any>(component, 'openSnackBar');
        const mouseEvent = new MouseEvent('click', { button: 2 });
        const coordinates = {x: 1, y: 1};
        component.coordinates = coordinates;
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        component.context = contextSpy;
        const pixels: Uint8ClampedArray = new Uint8ClampedArray(CLAMPED_ARRAY);
        const colorPixel: ImageData = new ImageData(pixels, 1, 1);
        contextSpy.getImageData.and.returnValue(colorPixel);
        component.colorPixel = pixels;
        component['eventListeners'].mouseDown(mouseEvent);
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalled();
        expect(component['openSnackBar']).toHaveBeenCalled();
    });

    it('should not change when wrong button of mouse down', () => {
        spyOn<any>(component, 'openSnackBar');
        const mouseEvent = new MouseEvent('click', { button: 1 });
        const coordinates = {x: 1, y: 1};
        component.coordinates = coordinates;
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        component.context = contextSpy;
        const pixels: Uint8ClampedArray = new Uint8ClampedArray(CLAMPED_ARRAY);
        const colorPixel: ImageData = new ImageData(pixels, 1, 1);
        contextSpy.getImageData.and.returnValue(colorPixel);
        component.colorPixel = pixels;
        component['eventListeners'].mouseDown(mouseEvent);
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalledTimes(0);
        expect(component['openSnackBar']).toHaveBeenCalledTimes(0);
    });

    it('should not change when colorPixels does not exist', () => {
        spyOn<any>(component, 'openSnackBar');
        const mouseEvent = new MouseEvent('click', { button: 2 });
        const coordinates = {x: 1, y: 1};
        component.coordinates = coordinates;
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        component.context = contextSpy;
        contextSpy.getImageData.and.returnValue({data: undefined});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalledTimes(0);
        expect(component['openSnackBar']).toHaveBeenCalledTimes(0);
    });

    it('should call updateMousePosition', () => {
        const mockMouse = new MouseEvent('mousemove');
        component['updateMousePosition'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalled();
    });

    it('should call updateMouseDown', () => {
        const mockMouse = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouse);
        component['updateMouseDown'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
    });

    it('should call updateMouseUp', () => {
        const mockMouse = new MouseEvent('mouseup');
        component.mouseHandler.isMouseDown = true;
        component['updateMouseUp'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalled();
    });

    it('should not call updateMouseUp', () => {
        component.mouseHandler.isMouseDown = false;
        const mockMouse = new MouseEvent('mouseup');
        component['updateMouseUp'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should not call updateMouseUp', () => {
        component.mouseHandler.isMouseDown = false;
        const mockMouse = new MouseEvent('mouseup');
        component['updateMouseUp'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });
});
