import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatButtonToggleModule, MatDialog, MatDialogModule, MatDialogRef,
    MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatSnackBarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { FileType } from 'src/app/interfaces-enums/file-type';
import { ExportDrawingService } from 'src/app/services/export-drawing/export-drawing.service';
import { SendByEmailManagerService } from 'src/app/services/send-by-email-manager/send-by-email-manager.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ExportDrawingComponent } from './export-drawing.component';

// tslint:disable:no-magic-numbers no-string-literal

const SUCCESS = 201;
const FAIL = 400;

export class MatDialogMock {
    static confirm: boolean;
    // tslint:disable-next-line: typedef
    open() {
        return {
            afterClosed: () => of(MatDialogMock.confirm)
        };
    }
}

describe('ExportDrawingComponent', () => {
    let component: ExportDrawingComponent;
    let sendByEmailManagerService: SendByEmailManagerService;
    let fixture: ComponentFixture<ExportDrawingComponent>;
    let exportServiceSpy: jasmine.SpyObj<ExportDrawingService>;
    let fakeCanvas: Node;
    let debugElement: DebugElement;
    let fakeSvgElement: SVGElement;

    const mockDialogRef = {
        close: jasmine.createSpy('close'),
        open: jasmine.createSpy('open')
    };

    beforeEach(() => {
        exportServiceSpy = jasmine.createSpyObj('ExportDrawingService',
            [
                'applySvgFilter',
                'initialisePrevisualization',
                'initialiseDownloadFile'
            ]);
        TestBed.configureTestingModule({
            imports: [MatDialogModule, MatFormFieldModule, MatOptionModule, MatInputModule,
                MatButtonToggleModule, MatSelectModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule,
                HttpClientModule, MatSnackBarModule],
            declarations: [ExportDrawingComponent, ConfirmationDialogComponent],
            providers: [
                { provide: MatDialogRef, useValue: mockDialogRef },
                { provide: ExportDrawingService, useValue: exportServiceSpy },
                { provide: MatDialog, useClass: MatDialogMock },
            ]
        })
            .compileComponents();

        fixture = TestBed.createComponent(ExportDrawingComponent);
        debugElement = fixture.debugElement;
        sendByEmailManagerService = debugElement.injector.get(SendByEmailManagerService);
        component = fixture.componentInstance;
        fakeCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should apply filter', () => {
        component.applyFilter();
        expect(exportServiceSpy.applySvgFilter).toHaveBeenCalledTimes(1);
    });

    it('should close on cancel', () => {
        component.closeDialog();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should convert svg to dataURL', () => {
        component.svgCanvas = fakeCanvas;
        component.svgCanvas.appendChild(fakeSvgElement);
        component.fileType = FileType.svg;
        const data = component['svgToDataURL']();
        expect(data).toEqual('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0Lz48L3N2Zz4=');

    });

    it('should initialize previsu', () => {
        fakeCanvas.appendChild(fakeSvgElement);
        component['svgService'].svgCanvas = fakeCanvas as SVGElement;
        component['initialisePrevisualization']();
        expect(exportServiceSpy.initialisePrevisualization).toHaveBeenCalled();
        expect(exportServiceSpy.initialiseDownloadFile).toHaveBeenCalled();
    });

    it('should not initialize previsu', () => {
        component['svgService'].svgCanvas = fakeSvgElement;
        component.svgCanvas = fakeCanvas;
        component['initialisePrevisualization']();
        expect(exportServiceSpy.initialisePrevisualization).not.toHaveBeenCalled();
        expect(exportServiceSpy.initialiseDownloadFile).not.toHaveBeenCalled();
    });

    it('should not accept email', () => {
        component.emailValidator.setValue('');
        expect(component.emailValidator.hasError('required')).toBeTruthy();
        let returnValue = component.getErrorMail();
        expect(returnValue).toEqual('Champs requis');

        component.emailValidator.setValue('Test');
        expect(component.emailValidator.hasError('email')).toBeTruthy();
        returnValue = component.getErrorMail();
        expect(returnValue).toEqual('Courriel invalide');

        component.emailValidator.setValue('Test@valid.com');
        returnValue = component.getErrorMail();
        expect(returnValue).toEqual('');
    });

    it('should confirm and download', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'downloadImg');
        MatDialogMock.confirm = true;
        component.confirmDialog();
        expect(component['downloadImg']).toHaveBeenCalled();
    });

    it('should not confirm and download', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'downloadImg');
        MatDialogMock.confirm = false;
        component.confirmDialog();
        expect(component['downloadImg']).toHaveBeenCalledTimes(0);
    });

    it('should download in svg', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'downloadSVG');
        spyOn(component, 'closeDialog');
        component.svgCanvas = fakeCanvas;
        component.fileType = FileType.svg;
        component['downloadImg']();
        expect(component['downloadSVG']).toHaveBeenCalled();
        expect(component.closeDialog).toHaveBeenCalled();
    });

    it('should download in jpeg/png', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'downloadNonSVG');
        component.fileType = FileType.jpeg;
        component['downloadImg']();
        expect(component['downloadNonSVG']).toHaveBeenCalled();

        component.fileType = FileType.png;
        component['downloadImg']();
        expect(component['downloadNonSVG']).toHaveBeenCalled();
    });

    it('should call right functions with download svg', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'svgToDataURL').and.returnValue('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0' +
        'cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0Lz48L3N2Zz4=');
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'sendToServer');
        spyOn(component.exportBtn.nativeElement, 'setAttribute');
        spyOn(component.exportBtn.nativeElement, 'click');
        component.svgCanvas = fakeCanvas;
        component.name = 'test';
        component.fileType = FileType.svg;
        component['downloadSVG']();
        expect(component.exportBtn.nativeElement.setAttribute).toHaveBeenCalledTimes(2);
        expect(component.exportBtn.nativeElement.click).toHaveBeenCalledTimes(1);
        expect(component['svgToDataURL']).toHaveBeenCalled();

        component.isDownload = false;
        component['downloadSVG']();
        expect(component.exportBtn.nativeElement.setAttribute).toHaveBeenCalledTimes(4);
        expect(component.exportBtn.nativeElement.click).toHaveBeenCalledTimes(1);
        expect(component['sendToServer']).toHaveBeenCalled();
    });

    it('should call right functions with download jpeg/png', () => {
        spyOn(component.exportBtn.nativeElement, 'setAttribute');
        spyOn(component.exportBtn.nativeElement, 'click');
        spyOn(component.htmlCanvas.nativeElement, 'setAttribute');
        spyOn(component.htmlCanvas.nativeElement, 'getContext').and.callThrough();
        spyOn(component.htmlCanvas.nativeElement, 'toDataURL');
        component.svgCanvas = fakeCanvas;
        component.name = 'test';
        component.fileType = FileType.png;
        component['downloadNonSVG']();
        expect(component.htmlCanvas.nativeElement.getContext).toHaveBeenCalled();
        expect(component.htmlCanvas.nativeElement.setAttribute).toHaveBeenCalledTimes(2);
        expect(component.htmlCanvas.nativeElement.toDataURL).not.toHaveBeenCalled();
        expect(component.exportBtn.nativeElement.click).not.toHaveBeenCalled();
    });

    it('should call right functions with sending jpeg/png', () => {
        spyOn(component.exportBtn.nativeElement, 'setAttribute');
        spyOn(component.exportBtn.nativeElement, 'click');
        spyOn(component.htmlCanvas.nativeElement, 'setAttribute');
        spyOn(component.htmlCanvas.nativeElement, 'getContext').and.callThrough();
        spyOn(component.htmlCanvas.nativeElement, 'toDataURL');
        component.svgCanvas = fakeCanvas;
        component.name = 'test';
        component.isDownload = false;
        component.fileType = FileType.png;
        component['downloadNonSVG']();
        expect(component.htmlCanvas.nativeElement.getContext).toHaveBeenCalled();
        expect(component.htmlCanvas.nativeElement.setAttribute).toHaveBeenCalledTimes(2);
        expect(component.htmlCanvas.nativeElement.toDataURL).not.toHaveBeenCalled();
        expect(component.exportBtn.nativeElement.click).not.toHaveBeenCalled();
    });

    it('should show success message when sending email', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'popMessage');
        const sendSpySuccess = spyOn(sendByEmailManagerService, 'sendDrawingbyEmail').and.callThrough().and.returnValue(of(SUCCESS));
        component['sendToServer']('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0Lz48L3N2Zz4=');
        fixture.detectChanges();
        expect(sendSpySuccess).toHaveBeenCalledTimes(1);
        expect(component['popMessage']).toHaveBeenCalledWith('Votre courriel a été envoyé', 'snackbar');

    });

    it('should handle error when sending email', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'popMessage');
        const sendSpyFail = spyOn(sendByEmailManagerService, 'sendDrawingbyEmail').and.callThrough().and.returnValue(of(FAIL));
        component['sendToServer']('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxyZWN0Lz48L3N2Zz4=');
        fixture.detectChanges();
        expect(sendSpyFail).toHaveBeenCalledTimes(1);
        expect(component['popMessage']).toHaveBeenCalledWith('Une erreur est survenue', 'snackbar');
    });
});
