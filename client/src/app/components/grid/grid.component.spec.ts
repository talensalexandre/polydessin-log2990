import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatIconModule, MatInputModule, MatSliderModule, MatSlideToggleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridService } from 'src/app/services/grid/grid.service';
import { GridComponent } from './grid.component';

describe('GridComponent', () => {
    let component: GridComponent;
    let fixture: ComponentFixture<GridComponent>;
    let gridServiceSpy: jasmine.SpyObj<GridService>;

    beforeEach(() => {
        gridServiceSpy = jasmine.createSpyObj(
            'gridServiceSpy',
            [
                'changeGridSize'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MatFormFieldModule, MatSlideToggleModule, FormsModule,
                MatSliderModule, MatInputModule, BrowserAnimationsModule, MatIconModule ],
            declarations: [ GridComponent ],
            providers: [{provide: GridService, useValue: gridServiceSpy}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call service', () => {
        component.changeSize();
        expect(gridServiceSpy.changeGridSize).toHaveBeenCalled();
    });
});
