import { Injectable, Renderer2 } from '@angular/core';
import { ElementType } from '../../interfaces-enums/element-type';
import { MouseHandlerService } from '../mouse-handler/mouse-handler.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

const COLOR_STRING = '#08428d';
const DASH_FORMAT = '4 2';
const SELECTION_RECTANGLE_SIZE = 2;

@Injectable({
    providedIn: 'root',
})
export class PrevisualisationRectangleService {
    previsualizationRectangle: SVGElement;
    private renderer: Renderer2;

    constructor(
        private rectangleHandler: RectangleHandlerService,
        private mouseHandler: MouseHandlerService,
        private svgHandler: SvgHandlerService
    ) {
        this.renderer = this.svgHandler.renderer;
    }

    createPrevisualizationRectangle(): SVGElement {
        this.rectangleHandler.calculateRectangle(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            SELECTION_RECTANGLE_SIZE
        );
        this.previsualizationRectangle = this.rectangleHandler.elementPrevisualization(
            false,
            true,
            SELECTION_RECTANGLE_SIZE,
            ElementType.rect,
            this.previsualizationRectangle,
        );
        this.changeRectangleStyles(this.previsualizationRectangle);
        return this.previsualizationRectangle;
    }

    private changeRectangleStyles(rectangle: SVGElement): void {
        this.renderer.setStyle(rectangle, 'stroke', COLOR_STRING);
        this.renderer.setStyle(rectangle, 'stroke-dasharray', DASH_FORMAT);
    }

    deletePrevisualization(): void {
        if (this.previsualizationRectangle) {
            this.renderer.removeChild(this.svgHandler.svgCanvas, this.previsualizationRectangle);
        }
        delete this.previsualizationRectangle;
    }
}
