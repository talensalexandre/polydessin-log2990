import { Component } from '@angular/core';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';

@Component({
    selector: 'app-color-doc',
    templateUrl: './color-doc.component.html',
    styleUrls: ['./color-doc.component.scss']
})
export class ColorDocComponent {

    constructor(public userGuideService: UserGuideService) { }
}
