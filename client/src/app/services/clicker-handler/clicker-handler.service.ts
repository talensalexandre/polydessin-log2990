import { Injectable } from '@angular/core';
import { AttributeType } from 'src/app/interfaces-enums/attribute-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { States } from '../../interfaces-enums/states';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { StateSelectorService } from '../state-selector/state-selector.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

@Injectable({
    providedIn: 'root'
})
export class ClickerHandlerService {
    constructor(
        private svgHandler: SvgHandlerService,
        private colorManager: ColorManagerService,
        private stateSelector: StateSelectorService,
        private undoRedo: UndoRedoService,
        private selectionSvgHandler: SelectionSvgHandlerService,
        private saveManager: SaveManagerService
    ) {}

    addSelectionStyles(svgElement: SVGElement, resetTranslate: boolean = true): void {
        this.svgHandler.renderer.setAttribute(svgElement, 'isSelected', 'false');
        if (resetTranslate) {
            this.svgHandler.renderer.setAttribute(svgElement, 'transform', 'matrix(1,0,0,1,0,0)');
        }
        this.svgHandler.elements.push(svgElement);
        this.svgHandler.renderer.listen(svgElement, 'mousedown', ($event) => this.clickOnObject(svgElement, $event));
        this.svgHandler.renderer.listen(svgElement, 'contextmenu', () => false);
    }

    private clickOnObject(svgElement: SVGElement, clickEvent: MouseEvent): void {
        this.colorApplicatorClick(svgElement, clickEvent);
        this.selectionClick(svgElement, clickEvent);
    }

    private colorApplicatorClick(svgElement: SVGElement, clickEvent: MouseEvent): void {
        if (this.stateSelector.stateSelected === States.colorApplicator) {
            clickEvent.preventDefault();
            switch (svgElement.getAttribute('typeElement')) {
                case 'strokeElement':
                    if (clickEvent.button === 0) {
                        this.saveManager.removeSvgElement(svgElement);
                        this.undoRedo.addFromColorApplicator(svgElement, AttributeType.stroke);
                        this.svgHandler.renderer.setAttribute(svgElement, 'stroke',
                            this.colorManager.colorSelected[ColorChoice.mainColor].inString);
                        this.saveManager.addSvgElement(svgElement);
                    }
                    break;
                case 'shapeElement':
                    if (clickEvent.button === 0 && svgElement.style.fill !== 'none') {
                        this.saveManager.removeSvgElement(svgElement);
                        this.undoRedo.addFromColorApplicator(svgElement, AttributeType.fill);
                        this.svgHandler.renderer.setStyle(svgElement, 'fill',
                            this.colorManager.colorSelected[ColorChoice.mainColor].inString);
                        this.saveManager.addSvgElement(svgElement);
                    } else if (clickEvent.button === 2 && svgElement.style.stroke !== 'none') {
                        this.saveManager.removeSvgElement(svgElement);
                        this.undoRedo.addFromColorApplicator(svgElement, AttributeType.stroke);
                        this.svgHandler.renderer.setAttribute(svgElement, 'stroke',
                            this.colorManager.colorSelected[ColorChoice.secondaryColor].inString);
                        this.saveManager.addSvgElement(svgElement);
                    }
                    break;
            }
        }
    }

    private selectionClick(svgElement: SVGElement, clickEvent: MouseEvent): void {
        clickEvent.preventDefault();
        if (this.stateSelector.stateSelected === States.selection) {
            if (clickEvent.button === 0) {
                this.selectionSvgHandler.clickedOnElement = true;
                this.selectionSvgHandler.unselectAll(svgElement);
                this.selectionSvgHandler.selectObject(svgElement);
            } else if (clickEvent.button === 2) {
                this.selectionSvgHandler.nonTogableItem = svgElement;
                this.selectionSvgHandler.toggleSelection(svgElement);
            }
        }
    }
}
