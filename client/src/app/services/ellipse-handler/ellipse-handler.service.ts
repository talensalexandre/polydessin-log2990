import { Injectable } from '@angular/core';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { AbstractShapeService } from '../abstract-shape-service/abstract-shape.service';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { PrevisualisationRectangleService } from '../previsualisation-rectangle/previsualisation-rectangle.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const OUTLINE_DIVISON = 4;

@Injectable({
    providedIn: 'root'
})
export class EllipseHandlerService extends AbstractShapeService {
    constructor(protected svgHandler: SvgHandlerService,
                protected colorManager: ColorManagerService,
                protected clickerHandler: ClickerHandlerService,
                protected rectangleHandler: RectangleHandlerService,
                protected undoRedoService: UndoRedoService,
                private previsualizationRectangle: PrevisualisationRectangleService) {
       super(svgHandler, colorManager, clickerHandler, undoRedoService);
    }

    calculateEllipse(startCoordinates: Coordinates,
                     endCoordinates: Coordinates,
                     size: number): void {
        this.rectangleHandler.calculateRectangle(startCoordinates, endCoordinates, size);
        this.dimensions.coordinates.x = (this.rectangleHandler.dimensions.coordinates.x + (this.rectangleHandler.dimensions.width / 2)
            - size / OUTLINE_DIVISON);
        this.dimensions.coordinates.y = (this.rectangleHandler.dimensions.coordinates.y + (this.rectangleHandler.dimensions.height / 2)
            - size / OUTLINE_DIVISON);
        this.dimensions.width = Math.abs(this.rectangleHandler.dimensions.width / 2 - size / OUTLINE_DIVISON);
        this.dimensions.height = Math.abs(this.rectangleHandler.dimensions.height / 2 - size / OUTLINE_DIVISON);
        if (startCoordinates.x > endCoordinates.x) {
            this.dimensions.width = Math.abs(this.dimensions.width - size / 2);
            this.dimensions.coordinates.x = startCoordinates.x - this.dimensions.width - size / 2;
        }
        if (startCoordinates.y > endCoordinates.y) {
            this.dimensions.height = Math.abs(this.dimensions.height - size / 2);
            this.dimensions.coordinates.y = startCoordinates.y - this.dimensions.height - size / 2;
        }
    }

    calculateCircle(size: number): void {
        this.dimensions.coordinates.x = this.rectangleHandler.dimensions.coordinates.x + (this.rectangleHandler.dimensions.width / 2);
        this.dimensions.coordinates.y = this.rectangleHandler.dimensions.coordinates.y + (this.rectangleHandler.dimensions.height / 2);
        this.dimensions.width = Math.abs(this.rectangleHandler.dimensions.width / 2 - size / 2);
        this.dimensions.height = Math.abs(this.rectangleHandler.dimensions.height / 2 - size / 2);
        this.dimensions.width = Math.min(this.dimensions.width, this.dimensions.height);
        this.dimensions.height = this.dimensions.width;
    }

    protected setElementAttributesPrevisualization(element: SVGElement | undefined): void {
        this.previsualizationRectangle.createPrevisualizationRectangle();
        this.renderer.setAttribute(element, 'cx', String(this.dimensions.coordinates.x));
        this.renderer.setAttribute(element, 'cy', String(this.dimensions.coordinates.y));
        this.renderer.setAttribute(element, 'rx', String(this.dimensions.width));
        this.renderer.setAttribute(element, 'ry', String(this.dimensions.height));
    }

    protected setElementAttributesCreation(element: SVGElement | undefined): void {
        this.renderer.setAttribute(element, 'cx', String(this.dimensions.coordinates.x));
        this.renderer.setAttribute(element, 'cy', String(this.dimensions.coordinates.y));
        this.renderer.setAttribute(element, 'rx', String(this.dimensions.width));
        this.renderer.setAttribute(element, 'ry', String(this.dimensions.height));
    }

    deletePrevisualization(): void {
        this.previsualizationRectangle.deletePrevisualization();
    }
}
