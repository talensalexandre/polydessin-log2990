/*
  Transformation from SVG to Canvas and image download inspired of :
  C Kelleher. "Download SVG from Data URL". Available : http://bl.ocks.org/curran/7cf9967028259ea032e8
*/

import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FileType } from 'src/app/interfaces-enums/file-type';
import { ExportDrawingService } from 'src/app/services/export-drawing/export-drawing.service';
import { SendByEmailManagerService } from 'src/app/services/send-by-email-manager/send-by-email-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';

const WAIT_3SEC = 3000;
const OK_MESSAGE = 'OK';
const SUCCESS = 201;

@Component({
    selector: 'app-export-drawing',
    templateUrl: './export-drawing.component.html',
    styleUrls: ['./export-drawing.component.scss']
})
export class ExportDrawingComponent implements AfterViewInit {
    @ViewChild('previsualization', { static: false }) previsualization: ElementRef;
    @ViewChild('export', { static: false }) exportBtn: ElementRef;
    @ViewChild('pngContainer', { static: false }) pngContainer: ElementRef;
    @ViewChild('htmlCanvas', { static: false }) htmlCanvas: ElementRef;
    name: string;
    email: string;
    author: string;
    isDownload: boolean;
    fileType: FileType;
    filter: number;
    svgCanvas: Node;
    private svgPrevisualisation: Node;
    readonly SVG_LINK: string = 'http://www.w3.org/2000/svg';
    emailValidator: FormControl = new FormControl('', [Validators.required, Validators.email]);

    constructor(
        private dialogRef: MatDialogRef<ExportDrawingComponent>,
        private svgService: SvgHandlerService,
        private sendByEmailManager: SendByEmailManagerService,
        private exportService: ExportDrawingService,
        private dialog: MatDialog,
        private changeDetectorRef: ChangeDetectorRef,
        private snackBar: MatSnackBar
    ) {
        this.isDownload = true;
        this.name = '';
        this.email = '';
        this.author = '';
        this.filter = 0;
        this.fileType = FileType.jpeg;
    }

    ngAfterViewInit(): void {
        this.initialisePrevisualization();
        this.changeDetectorRef.detectChanges();
    }

    private initialisePrevisualization(): void {
        if (this.svgService.svgCanvas) {
            this.svgCanvas = this.svgService.svgCanvas.cloneNode(true);
            if (this.svgCanvas.childNodes.length !== 0) {
                this.svgPrevisualisation = this.svgCanvas.cloneNode(true);
                this.exportService.initialisePrevisualization(this.svgPrevisualisation, this.previsualization);
                this.exportService.initialiseDownloadFile(this.svgCanvas);
            }
        }
    }

    applyFilter(): void {
        this.exportService.applySvgFilter(this.filter, this.svgPrevisualisation, true);
    }

    getErrorMail(): string {
        return this.emailValidator.hasError('required') ? 'Champs requis' :
             this.emailValidator.hasError('email') ? 'Courriel invalide' : '';
    }

    private downloadImg(): void {
        if (this.fileType === FileType.svg) {
            this.downloadSVG();
        } else {
            this.downloadNonSVG();
        }
        this.closeDialog();
    }

    private svgToDataURL(): string {
        const svgToXML = new XMLSerializer().serializeToString(this.svgCanvas);
        return 'data:image/svg+xml;base64,' + btoa(svgToXML);
    }

    private downloadSVG(): void {
        this.exportBtn.nativeElement.setAttribute('download', this.name + '.' + this.fileType.toString());
        this.exportBtn.nativeElement.setAttribute('href', this.svgToDataURL());
        this.isDownload ? this.exportBtn.nativeElement.click() : this.sendToServer(this.svgToDataURL());
    }

    downloadNonSVG(): void {
        const img64 = this.svgToDataURL();
        this.htmlCanvas.nativeElement.setAttribute('width', this.svgService.canvasWidth);
        this.htmlCanvas.nativeElement.setAttribute('height', this.svgService.canvasHeight);
        const ctx = this.htmlCanvas.nativeElement.getContext('2d');
        const img = new Image();
        img.onload = () => {
            ctx.drawImage(img, 0, 0);
            const ref = this.htmlCanvas.nativeElement.toDataURL('image/' + this.fileType);
            this.exportBtn.nativeElement.setAttribute('href', ref);
            this.exportBtn.nativeElement.setAttribute('download', this.name + '.' + this.fileType.toString());
            this.isDownload ? this.exportBtn.nativeElement.click() : this.sendToServer(ref);
        };
        img.src = img64;
    }

    private popMessage(message: string, style: string): void {
        this.snackBar.open(message, OK_MESSAGE, {
            duration: WAIT_3SEC,
            panelClass: [style]
        });
    }

    private sendToServer(base64: string): void {
        this.sendByEmailManager.sendDrawingbyEmail(this.email, base64, this.name, this.fileType.toString())
            .subscribe((response: number) => {
                switch (response) {
                    case SUCCESS:
                        this.popMessage('Votre courriel a été envoyé', 'snackbar');
                        break;
                    default:
                        this.popMessage('Une erreur est survenue', 'snackbar');
                }
        });
    }

    confirmDialog(): void {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            disableClose: true,
            autoFocus: false,
            data: {
                message: 'Enregistrer au format ' + this.fileType.toString().toUpperCase() + ' ?'
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.exportService.applySvgFilter(this.filter, this.svgCanvas, false);
                this.downloadImg();
            }
        });
    }

    closeDialog(): void {
        this.dialogRef.close();
    }
}
