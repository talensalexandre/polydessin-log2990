import { OnDestroy, OnInit } from '@angular/core';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { EventListeners } from 'src/app/interfaces-enums/event-listeners';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';

const MAXIMUM_SIZE = 200;
const MINIMUM_SIZE = 1;

export abstract class AbstractToolComponent implements OnInit, OnDestroy {
    protected buttonUsed: number;
    protected svgElement: SVGElement;
    size: number;
    protected eventListeners: EventListeners;
    isDrawing: boolean;

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        protected saveManager: SaveManagerService
    ) {
        this.eventListeners = {} as EventListeners;
    }

    ngOnInit(): void {
        this.initExtend();
    }

    protected initExtend(): void {
        this.eventListeners.mouseMove = ($event) => this.updateMousePosition($event);
        this.eventListeners.mouseDown = ($event) => this.updateMouseDown($event);
        this.eventListeners.mouseUp = ($event) => this.updateMouseUp($event);
        this.eventListeners.mouseLeave = () => this.mouseLeft();
        this.eventListeners.changedMouseDown = true;
        this.eventListeners.changedMouseLeave = true;
        this.eventListeners.changedMouseMove = true;
        this.eventListeners.changedMouseUp = true;
        this.setDefaultSize();
        this.svgHandler.addListeners(this.eventListeners);
    }

    protected abstract setDefaultSize(): void;

    ngOnDestroy(): void {
        this.destroyExtend();
        this.svgHandler.deleteListeners(this.eventListeners);
        this.mouseHandler.isMouseDown = false;
    }

    protected updateMousePosition(event: MouseEvent): void {
        const coordinates: Coordinates = { x: event.offsetX, y: event.offsetY };
        this.mouseHandler.onMouseMovement(coordinates);
        this.mouseMouvementExtend(coordinates);
    }

    protected updateMouseDown(event: MouseEvent, buttonWanted: number = 0): void {
        if (!(event.button === buttonWanted)) {
            return;
        }
        this.changeSize();
        this.isDrawing = true;
        const coordinates: Coordinates = { x: event.offsetX, y: event.offsetY };
        this.mouseHandler.onMouseDown(coordinates);
        this.buttonUsed = event.button;
        this.mouseDownExtend(coordinates);
    }

    protected updateMouseUp(event: MouseEvent): void {
        if (!this.mouseHandler.isMouseDown) {
            return;
        }
        this.isDrawing = false;
        const coordinates: Coordinates = { x: event.offsetX, y: event.offsetY };
        this.mouseHandler.onMouseUp(coordinates);
        this.mouseUpExtend(coordinates, event.button);
        if (this.svgElement) {
            this.saveManager.addSvgElement(this.svgElement);
        }
    }

    protected mouseLeft(): void {
        this.isDrawing = false;
        this.mouseHandler.onMouseLeave();
    }

    changeSize(): void {
        this.size = Math.round(this.size);
        this.size = (this.size < MINIMUM_SIZE) ? MINIMUM_SIZE : this.size;
        this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        this.stateSelector.isModifyingInput = false;
    }

    increaseSize(): void {
        this.size++;
        this.changeSize();
    }

    decreaseSize(): void {
        this.size--;
        this.changeSize();
    }

    protected abstract destroyExtend(): void;

    protected abstract mouseMouvementExtend(coordinates: Coordinates): void;

    protected abstract mouseDownExtend(coordinates: Coordinates): void;

    protected abstract mouseUpExtend(coordinates: Coordinates, button: number): void;
}
