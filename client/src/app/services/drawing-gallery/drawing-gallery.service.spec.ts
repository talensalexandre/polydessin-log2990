import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { DrawingStored, MetaDrawing } from '../../interfaces-enums/drawing';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { CreateNewDrawingService } from '../create-new-drawing/create-new-drawing.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { DrawingGalleryService } from './drawing-gallery.service';

// tslint:disable:no-magic-numbers max-file-line-count no-any no-string-literal

describe('DrawingGalleryService', () => {
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    let newDrawingServiceSpy: jasmine.SpyObj<CreateNewDrawingService>;
    let fakeElement: SVGElement;
    let fakeCanvas: SVGElement;
    let service: DrawingGalleryService;
    let drawingStore: DrawingStored;
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;

    beforeEach(async () => {
        TestBed.configureTestingModule({});
    });

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj('SvgHandlerService',
            [
                'createCanvas',
                'deleteCanvas',
                'addBlurFilter',
                'addRoundedFilter',
                'addFogFilter',
                'addChaosFilter',
                'addShadowFilter'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj('SaveManagerService',
            [
                'addSvgElement'
            ]
        );
        newDrawingServiceSpy = jasmine.createSpyObj('NewDrawingService',
            [
                'createNewDrawing'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'addSelectionStyles'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new DrawingGalleryService(svgHandlerServiceSpy, newDrawingServiceSpy, saveManagerSpy, clickerHandlerSpy);
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        fakeCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        const labels = ['label', 'oui', 'logiciel'];
        const metaDrawing: MetaDrawing = {
            timestamp: 1, name: 'testDessin', labels, width: 1292, height: 923,
            backgroundColor: 'rgb(255, 0, 255)'
        };
        const fakeElementString = ['<svg:polyline xmlns:svg="http://www.w3.org/2000/svg" _ngcontent-hnw-c0="" stroke-width="12"' +
            ' points="193,246 193,246" stroke="rgba(255,0,0,1)" typeElement="strokeElement" size="12" isSelected="false"' +
            ' transform="translate(0,0)" style="stroke-linecap: round; stroke-linejoin: round; fill: none;"></svg:polyline>'];
        drawingStore = { metaDrawing, svgContainer: fakeElementString };
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should display element', () => {
        const fakeArray: SVGElement[] = new Array<SVGElement>();
        fakeArray.push(fakeElement);
        service.displayElement(fakeCanvas, fakeArray, 0);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(1);
    });

    it('should not display element', () => {
        const fakeArray: SVGElement[] = new Array<SVGElement>();
        fakeCanvas.appendChild(fakeElement);
        service.displayElement(fakeCanvas, fakeArray, 0);
        expect(rendererSpy.appendChild).not.toHaveBeenCalled();
    });

    it('should create element from string', () => {
        spyOn<any>(service, 'createPrevisualization');
        const fakeDrawings: DrawingStored[] = new Array<DrawingStored>();
        fakeDrawings.push(drawingStore);
        service.createElementFromString(fakeDrawings);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(fakeDrawings.length);
        expect(service['createPrevisualization']).toHaveBeenCalledWith(drawingStore);
    });

    it('should create the previsualization ', () => {
        service['createPrevisualization'](drawingStore);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(3);
    });

    it('should create a drawing', () => {
        service.createDrawing(drawingStore);
        expect(svgHandlerServiceSpy.deleteCanvas).not.toHaveBeenCalled();
        expect(newDrawingServiceSpy.createNewDrawing).toHaveBeenCalled();
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(drawingStore.svgContainer.length);
        expect(clickerHandlerSpy.addSelectionStyles).toHaveBeenCalledTimes(drawingStore.svgContainer.length);
    });

    it('should create a drawing and delete the old one', () => {
        svgHandlerServiceSpy.svgCanvas = fakeCanvas;
        service.createDrawing(drawingStore);
        expect(svgHandlerServiceSpy.deleteCanvas).toHaveBeenCalled();
    });
});
