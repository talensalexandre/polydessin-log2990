import { Injectable } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { CreateNewDrawingComponent } from 'src/app/components/create-new-drawing/create-new-drawing.component';
import { DrawingStored } from 'src/app/interfaces-enums/drawing';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { States } from '../../interfaces-enums/states';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionTransformService } from '../selection-services/selection-transform/selection-transform.service';
import { StateSelectorService } from '../state-selector/state-selector.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

const MAXIMUM_DIMENSION = 5000;
const MINIMUM_DIMENSION = 200;
const WIDTH_OFFSET = 1;
const HEIGHT_OFFSET = 5;

@Injectable({
    providedIn: 'root'
})
export class CreateNewDrawingService {
    isFromEntryPoint: boolean;
    dimensionsChanged: boolean;
    private wantedWidth: number;
    private wantedHeight: number;
    private wantedColor: string;
    openConfirmation: boolean;
    createDrawingOnce: boolean;
    blockNavigation: boolean;
    sidenavWidth: number;

    constructor(
        private svgHandler: SvgHandlerService,
        private stateService: StateSelectorService,
        private selectionTransform: SelectionTransformService,
        private saveManager: SaveManagerService,
        private clickerHandler: ClickerHandlerService
    ) {
        this.isFromEntryPoint = true;
        this.dimensionsChanged = false;
        this.openConfirmation = false;
        this.createDrawingOnce = false;
        this.blockNavigation = true;
    }

    createNewDrawing(
        width: number,
        height: number,
        color: string,
        dialogRef?: MatDialogRef<CreateNewDrawingComponent>
    ): boolean {
        this.wantedWidth = width;
        this.wantedHeight = height;
        this.wantedColor = color;
        if (this.svgHandler.drawingOnCanvas && dialogRef) {
            this.openConfirmation = true;
            dialogRef.updateSize('auto', 'auto');
            return false;
        }
        this.createCanvas();
        return true;
    }

    selectSelection(): void {
        this.stateService.changeState(States.selection, false);
    }

    createCanvas(): void {
        this.blockNavigation = false;
        if (this.createDrawingOnce) {
            this.saveManager.initialiseElements();
            this.svgHandler.deleteCanvas();
        }
        this.saveManager.autosaveBackground(this.wantedColor, this.wantedWidth, this.wantedHeight);
        this.svgHandler.createCanvas(this.wantedWidth, this.wantedHeight, this.wantedColor);
        this.selectSelection();
        this.createDrawingOnce = true;
        this.selectionTransform.ngOnInit();
    }

    verifyDimensions(dimensions: Coordinates): Coordinates {
        dimensions.x = (dimensions.x < MINIMUM_DIMENSION) ? MINIMUM_DIMENSION : dimensions.x;
        dimensions.x = (dimensions.x > MAXIMUM_DIMENSION) ? MAXIMUM_DIMENSION : dimensions.x;
        dimensions.y = (dimensions.y < MINIMUM_DIMENSION) ? MINIMUM_DIMENSION : dimensions.y;
        dimensions.y = (dimensions.y > MAXIMUM_DIMENSION) ? MAXIMUM_DIMENSION : dimensions.y;
        this.dimensionsChanged = true;
        return dimensions;
    }

    updateDimensions(dimensions: Coordinates, innerDimensions: Coordinates): Coordinates {
        return (this.dimensionsChanged) ? dimensions : innerDimensions;
    }

    findInnerDimensions(innerWidth: number, innerHeight: number): Coordinates {
        return {
            x: innerWidth - this.sidenavWidth - WIDTH_OFFSET,
            y: innerHeight - HEIGHT_OFFSET
        };
    }

    continueDrawing(localSaveDrawing: DrawingStored): void {
        this.saveManager.svgContainer = localSaveDrawing.svgContainer;
        this.createNewDrawing(
            localSaveDrawing.metaDrawing.width,
            localSaveDrawing.metaDrawing.height,
            localSaveDrawing.metaDrawing.backgroundColor
        );
        this.createElementFromString(localSaveDrawing);
        this.svgHandler.drawingOnCanvas = true;
    }

    private createElementFromString(drawing: DrawingStored): void {
        drawing.svgContainer.forEach((element) => {
            this.createSvgFromSave(element);
        });
    }

    private createSvgFromSave(element: string): void {
        const parser = new DOMParser();
        const svgElement = parser.parseFromString(element, 'application/xml').firstChild as SVGElement;
        this.svgHandler.renderer.appendChild(this.svgHandler.svgCanvas, svgElement);
        this.clickerHandler.addSelectionStyles(svgElement, false);
    }
}
