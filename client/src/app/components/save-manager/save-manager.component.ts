import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { MatChipInputEvent, MatDialogRef, MatSnackBar } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { MetaDrawing } from 'src/app/interfaces-enums/drawing';
import { States } from 'src/app/interfaces-enums/states';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { ExportDrawingService } from 'src/app/services/export-drawing/export-drawing.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';

const WAIT_3SEC = 3000;
const MAX_LABELS = 4;
const SUCCESS = 201;
const OK_MESSAGE = 'OK';

@Component({
  selector: 'app-save-manager',
  templateUrl: './save-manager.component.html',
  styleUrls: ['./save-manager.component.scss']
})

export class SaveManagerComponent implements AfterViewInit {
    @ViewChild('previsualization', { static: false }) previsualization: ElementRef;
    metaDrawing: MetaDrawing;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
    openConfirmation: boolean;
    private svgPrevisu: Node;
    svgCanvas: Node;

    constructor(public dialogRef: MatDialogRef<SaveManagerComponent>,
                private saveManager: SaveManagerService,
                private snackBar: MatSnackBar,
                private stateSelector: StateSelectorService,
                private svgHandler: SvgHandlerService,
                private colorManager: ColorManagerService,
                private exportService: ExportDrawingService,
                private cdRef: ChangeDetectorRef) {
        this.metaDrawing = {
            timestamp: new Date().getTime(),
            name: '',
            labels: [],
            width: this.svgHandler.canvasWidth,
            height: this.svgHandler.canvasHeight,
            backgroundColor: this.colorManager.colorSelected[ColorChoice.backgroundColor].inString
        };
        this.openConfirmation = false;
    }

    ngAfterViewInit(): void {
        this.initialisePrevisualization();
        this.cdRef.detectChanges();
    }

    private initialisePrevisualization(): void {
        if (this.svgHandler.svgCanvas) {
            this.svgCanvas = this.svgHandler.svgCanvas.cloneNode(true);
            this.svgPrevisu = this.svgCanvas.cloneNode(true);
            this.exportService.initialisePrevisualization(this.svgPrevisu, this.previsualization);
        }
    }

    add(event: MatChipInputEvent): void {
        const value = event.value;
        if ((value || '').trim() && this.metaDrawing.labels.length < MAX_LABELS) {
            this.metaDrawing.labels.push(value.trim());
        }
        event.input.value = '';
    }

    remove(label: string): void {
        const index = this.metaDrawing.labels.indexOf(label);
        this.metaDrawing.labels.splice(index, 1);
    }

    private popMessage(message: string): void {
        this.snackBar.open(message, OK_MESSAGE, {
            duration: WAIT_3SEC,
        });
    }

    saveDrawing(): void {
        if (this.metaDrawing.name.length > 0) {
            this.openConfirmation = true;
        } else {
            this.popMessage('Un nom pour le dessin doit être renseigné');
        }
    }

    sendToServer(): void {
        this.saveManager.sendDrawing(this.metaDrawing).subscribe((response) => {
            response === SUCCESS ? this.popMessage('Dessin sauvegardé avec success') :
                                    this.popMessage('Impossible de sauvegarder - erreur serveur');
        });
        this.stateSelector.changeState(States.defaultState, false);
        this.dialogRef.close();
    }
}
