export enum ElementType {
    rect = 'rect',
    ellipse = 'ellipse',
    polygon = 'polygon'
}
