import { expect } from 'chai';
import { Collection, MongoClient, MongoClientOptions } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { DrawingStored, MetaDrawing } from '../drawing';
import { DatabaseService } from './database.service';

// tslint:disable:no-any
const DB_NAME = 'DrawingDataBase';
const DB_COLLECTION = 'DrawingCollection';

let mongoServer: any;
const options: MongoClientOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};
let collection: Collection<DrawingStored>;
let clientTest: MongoClient;

const metaDrawingTestOne: MetaDrawing = {
    timestamp: 12,
    name: 'test',
    labels: ['hello'],
    width: 200,
    height: 200,
    backgroundColor: 'rgba(255, 255, 255, 1)'
};

const metaDrawingTestTwo: MetaDrawing = {
    timestamp: 24,
    name: 'test2',
    labels: ['hello', 'bye'],
    width: 400,
    height: 600,
    backgroundColor: 'rgba(255, 255, 255, 1)'
};

const metaDrawingTestFalse: MetaDrawing = {
    timestamp: 1,
    name: '',
    labels: [],
    width: -200,
    height: -200,
    backgroundColor: 'rgba(255, 255, 255, 1)'
};

const validDrawing: DrawingStored = {
    metaDrawing: metaDrawingTestOne,
    svgContainer: ['thing', 'things']
};

const validDrawingTwo: DrawingStored = {
    metaDrawing: metaDrawingTestTwo,
    svgContainer: ['thingsss', 'thingggg']
};

const nonValidDrawing: DrawingStored = {
    metaDrawing: metaDrawingTestFalse,
    svgContainer: []
};
let databaseService: DatabaseService;

before(async () => {
    mongoServer = new MongoMemoryServer();
    const mongoUri = await mongoServer.getUri();
    await MongoClient.connect(mongoUri, options)
        .then((client: MongoClient) => {
            collection = client.db(DB_NAME).collection(DB_COLLECTION);
            clientTest = client;
        });
    databaseService = new DatabaseService();
    // tslint:disable-next-line:no-string-literal
    databaseService['collection'] = collection;
});

after(async () => {
    // tslint:disable-next-line:no-string-literal
    await databaseService['collection'].deleteMany({});
    await clientTest.close();
    await mongoServer.stop();
});

describe('database service', () => {

    it('should go to home page', () => {
        const expectedTitle = 'Home Page';
        const expectedBody = 'If this message appears, your request is invalid';
        databaseService.addDrawing(validDrawing);
        const message = databaseService.homePage();
        expect(message.title).to.equals(expectedTitle);
        expect(message.body).to.equals(expectedBody);
    });

    it('should get all drawings', async () => {
        const expectedDrawings: DrawingStored[] = new Array<DrawingStored>();
        expectedDrawings.push(validDrawing);
        return databaseService.getAllDrawings().then((result: DrawingStored[]) => {
            return expect(result[0].metaDrawing.timestamp).to.equals(expectedDrawings[0].metaDrawing.timestamp);
        });
    });

    it('should get one drawing', async () => {
        databaseService.addDrawing(validDrawingTwo);
        return databaseService.getDrawingWithTimestamp(String(validDrawing.metaDrawing.timestamp)).then((result: DrawingStored) => {
            return expect(result.metaDrawing.timestamp).to.equals(validDrawing.metaDrawing.timestamp);
        });
    });

    it('should not add one drawing', async () => {
        return databaseService.addDrawing(nonValidDrawing).catch( () => expect(true));
    });

    it('should modify one drawing', async () => {
        databaseService.modifyDrawing(validDrawingTwo);
        return databaseService.getDrawingWithTimestamp(String(validDrawingTwo.metaDrawing.timestamp)).then((result: DrawingStored) => {
            return expect(result.metaDrawing.timestamp).to.equals(validDrawingTwo.metaDrawing.timestamp);
        });
    });

    it('should delete one drawing', async () => {
        databaseService.deleteDrawing(String(validDrawingTwo.metaDrawing.timestamp));
        return databaseService.getDrawingWithTimestamp(String(validDrawingTwo.metaDrawing.timestamp)).then((result: DrawingStored) => {
            return expect(result).to.equals(null);
        });
    });
});
