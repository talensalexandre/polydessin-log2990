import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ColorApplicatorComponent } from '../../components/color-components/color-applicator/color-applicator.component';
import { ColorComponent } from '../../components/color-components/color/color.component';
import { AttributeType } from '../../interfaces-enums/attribute-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { RGBA } from '../../interfaces-enums/rgba';
import { States } from '../../interfaces-enums/states';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { StateSelectorService } from '../state-selector/state-selector.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { ClickerHandlerService } from './clicker-handler.service';

describe('ClickerHandlerService', () => {
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let stateSelectorSpy: jasmine.SpyObj<StateSelectorService>;
    let selectionSvgHandlerSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    const colorManager =  new ColorManagerService();
    let service: ClickerHandlerService;
    let fakeElement: SVGElement;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const secondfakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    let callbackClick: (event: MouseEvent) => boolean | void;
    let callbackMenu: (event: MouseEvent) => boolean | void;

    const rgbaColor: RGBA = {
        Dec: {
            Red: 255,
            Green: 255,
            Blue: 255,
            Alpha: 1
        },
        Hex: {
            Red: 'ff',
            Green: 'ff',
            Blue: 'ff'
        },
        inString: 'rgba(255, 255, 255, 1)'
    };

    beforeEach(() => TestBed.configureTestingModule({
        declarations: [ColorApplicatorComponent, ColorComponent]
    }));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer',
                'elements'
            ]
        );
        svgHandlerServiceSpy.elements = new Array<SVGElement>();
        stateSelectorSpy = jasmine.createSpyObj(
            'StateSelectorService',
            [
                'stateSelected'
            ]
        );
        selectionSvgHandlerSpy = jasmine.createSpyObj(
            'SelectionSvgHandlerService',
            [
                'unselectAll',
                'selectObject',
                'itemSelected',
                'toggleSelection'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'getColor',
                'current',
                'addFromColorApplicator'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new ClickerHandlerService(svgHandlerServiceSpy, colorManager, stateSelectorSpy, undoRedoSpy, selectionSvgHandlerSpy,
            saveManagerSpy);
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
    });

    beforeEach(() => {
        service.addSelectionStyles(fakeElement);
        callbackClick = rendererSpy.listen.calls.argsFor(0)[2];
        callbackMenu = rendererSpy.listen.calls.argsFor(1)[2];
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add listeners to object', () => {
        expect(rendererSpy.listen).toHaveBeenCalledTimes(2);
    });

    it('should add selection styles to object but not reset transform', () => {
        service.addSelectionStyles(fakeElement, false);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeElement, 'isSelected', 'false');
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.listen).toHaveBeenCalledTimes(4);
    });

    it('should not do anything when good state not selected', () => {
        stateSelectorSpy.stateSelected = States.pencil;
        service.addSelectionStyles(fakeElement);
        const clickButton = new MouseEvent('click', { button: 0 });
        callbackClick(clickButton);
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
    });

    it('should left click on object and change stroke color to main color', () => {
        stateSelectorSpy.stateSelected = States.colorApplicator;
        fakeElement.setAttribute('typeElement', 'strokeElement');
        service.addSelectionStyles(fakeElement);
        colorManager.updateColorWithRGBA(ColorChoice.mainColor, rgbaColor, true);
        const clickButton = new MouseEvent('click', { button: 0 });
        callbackClick(clickButton);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            fakeElement, 'stroke', colorManager.colorSelected[ColorChoice.mainColor].inString);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(undoRedoSpy.addFromColorApplicator).toHaveBeenCalledWith(fakeElement, AttributeType.stroke);
    });

    it('should not do anything if we are in strokeElement but not left click', () => {
        stateSelectorSpy.stateSelected = States.colorApplicator;
        fakeElement.setAttribute('typeElement', 'strokeElement');
        service.addSelectionStyles(fakeElement);
        colorManager.updateColorWithRGBA(ColorChoice.mainColor, rgbaColor, true);
        const clickButton = new MouseEvent('click', { button: 1 });
        callbackClick(clickButton);
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(saveManagerSpy.removeSvgElement).not.toHaveBeenCalledWith(fakeElement);
        expect(saveManagerSpy.addSvgElement).not.toHaveBeenCalledWith(fakeElement);
        expect(undoRedoSpy.addFromColorApplicator).not.toHaveBeenCalledWith(fakeElement, AttributeType.stroke);
    });

    it('should left click on object and change fill color to main color', () => {
        stateSelectorSpy.stateSelected = States.colorApplicator;
        fakeElement.setAttribute('typeElement', 'shapeElement');
        fakeElement.setAttribute('fill', 'notnull');
        colorManager.updateColorWithRGBA(ColorChoice.mainColor, rgbaColor, true);
        service.addSelectionStyles(fakeElement);
        const clickButton = new MouseEvent('click', { button: 0 });
        callbackClick(clickButton);
        expect(rendererSpy.setStyle).toHaveBeenCalledWith(
            fakeElement, 'fill', colorManager.colorSelected[ColorChoice.mainColor].inString);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(undoRedoSpy.addFromColorApplicator).toHaveBeenCalledWith(fakeElement, AttributeType.fill);
    });

    it('should right click on object and change stroke color to secondary color', () => {
        stateSelectorSpy.stateSelected = States.colorApplicator;
        colorManager.updateColorWithRGBA(ColorChoice.secondaryColor, rgbaColor, true);
        fakeElement.setAttribute('typeElement', 'shapeElement');
        fakeElement.setAttribute('stroke', 'notnull');
        service.addSelectionStyles(fakeElement);
        const clickButton = new MouseEvent('click', { button: 2 });
        callbackClick(clickButton);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(
            fakeElement, 'stroke', colorManager.colorSelected[ColorChoice.secondaryColor].inString);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElement);
        expect(undoRedoSpy.addFromColorApplicator).toHaveBeenCalledWith(fakeElement, AttributeType.stroke);
    });

    it('should not click on object and do nothing when in shapeElement but not left or right click', () => {
        stateSelectorSpy.stateSelected = States.colorApplicator;
        colorManager.updateColorWithRGBA(ColorChoice.secondaryColor, rgbaColor, true);
        fakeElement.setAttribute('typeElement', 'shapeElement');
        service.addSelectionStyles(fakeElement);
        const clickButton = new MouseEvent('click', { button: 1 });
        callbackClick(clickButton);
        // tslint:disable-next-line: no-magic-numbers
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
    });

    it('should not open context menu', () => {
        fakeElement.setAttribute('contextmenu', 'click');
        service.addSelectionStyles(fakeElement);
        const clickButton = new MouseEvent('click', { button: 14 });
        const result = callbackMenu(clickButton);
        expect(result).toBeFalsy();
    });

    it('should not do anything when clicking on object if not in selection tool', () => {
        svgHandlerServiceSpy.elements.push(fakeSvgElement);
        stateSelectorSpy.stateSelected = States.colorApplicator;
        const mouseEvent = new MouseEvent('click', { button: 0 });
        service.addSelectionStyles(fakeSvgElement);
        callbackClick = rendererSpy.listen.calls.argsFor(2)[2];
        callbackClick(mouseEvent);
        expect(selectionSvgHandlerSpy.unselectAll).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.selectObject).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.toggleSelection).not.toHaveBeenCalled();
    });

    it('should unselect all then select clicked object with button 0', () => {
        svgHandlerServiceSpy.elements.push(fakeSvgElement);
        secondfakeElement.setAttribute('isSelected', 'true');
        fakeSvgElement.setAttribute('isSelected', 'true');
        stateSelectorSpy.stateSelected = States.selection;
        const mouseEvent = new MouseEvent('click', { button: 0 });
        service.addSelectionStyles(secondfakeElement);
        callbackClick = rendererSpy.listen.calls.argsFor(2)[2];
        callbackClick(mouseEvent);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledWith(secondfakeElement);
        expect(selectionSvgHandlerSpy.selectObject).toHaveBeenCalledWith(secondfakeElement);
        expect(selectionSvgHandlerSpy.toggleSelection).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.clickedOnElement).toBeTruthy();
    });

    it('should toggle selection when object is clicked with button 2', () => {
        svgHandlerServiceSpy.elements.push(fakeSvgElement);
        stateSelectorSpy.stateSelected = States.selection;
        const mouseEvent = new MouseEvent('click', { button: 2 });
        service.addSelectionStyles(secondfakeElement);
        callbackClick = rendererSpy.listen.calls.argsFor(2)[2];
        callbackClick(mouseEvent);
        expect(selectionSvgHandlerSpy.unselectAll).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.selectObject).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.toggleSelection).toHaveBeenCalledWith(secondfakeElement);
    });

    it('should do nothing if button is 1', () => {
        svgHandlerServiceSpy.elements.push(fakeSvgElement);
        stateSelectorSpy.stateSelected = States.selection;
        const mouseEvent = new MouseEvent('click', { button: 1 });
        service.addSelectionStyles(secondfakeElement);
        callbackClick = rendererSpy.listen.calls.argsFor(2)[2];
        callbackClick(mouseEvent);
        expect(selectionSvgHandlerSpy.unselectAll).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.selectObject).not.toHaveBeenCalled();
        expect(selectionSvgHandlerSpy.toggleSelection).not.toHaveBeenCalled();
    });
});
