import { Renderer2 } from '@angular/core';
import { TempElement } from '../../../interfaces-enums/temp-element';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';
import { SelectionSvgHandlerService } from './selection-svg-handler.service';

// tslint:disable:max-file-line-count no-magic-numbers

describe('SelectionSvgHandlerService', () => {
    let service: SelectionSvgHandlerService;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let selectionCalculatorSpy: jasmine.SpyObj<SelectionCalculatorService>;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const MIN_DOM = 100;
    const MAX_DOM = 200;
    const fakeBoundingClientRectValues: DOMRect = new DOMRect(MIN_DOM, MIN_DOM, MAX_DOM, MAX_DOM);
    const secondfakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeCanvasRectValues: DOMRect = new DOMRect(100, 0, 1000, 1000);
    let canvasSpy: jasmine.SpyObj<SVGElement>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;

    beforeEach(() => {
        spyOn(fakeSvgElement, 'getBoundingClientRect').and.returnValue(fakeBoundingClientRectValues);
    });

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'setAttribute',
                'listen',
                'appendChild',
                'removeChild',
                'setStyle',
                'getAttribute'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'svgCanvas',
                'renderer',
                'elements'
            ]
        );
        svgHandlerSpy.elements = new Array<SVGElement>();
        selectionCalculatorSpy = jasmine.createSpyObj(
            'SelectionCalculatorService',
            [
                'isObjectInSelection',
                'selectedObjects',
                'getRectangleDimensions',
                'calculateRectangleCoordinates',
                'computeCenters',
                'boundaryDimensions'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'getColor',
                'getTransform',
                'current'
            ]
        );
        canvasSpy = jasmine.createSpyObj(
            'SVGElement',
            [
                'getBoundingClientRect'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        canvasSpy.getBoundingClientRect.and.returnValue(fakeCanvasRectValues);
        svgHandlerSpy.svgCanvas = canvasSpy;
        undoRedoSpy.current = new Array<TempElement>();
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        svgHandlerSpy.renderer = rendererSpy;
        selectionCalculatorSpy.calculateRectangleCoordinates.and.returnValue({x: 0, y: 0});
        selectionCalculatorSpy.getRectangleDimensions.and.returnValue({coordinates: {x: 0, y: 0}, width: 10, height: 10});
        service = new SelectionSvgHandlerService(selectionCalculatorSpy, svgHandlerSpy, saveManagerSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add element to selection rectangles map', () => {
        service.addNewSelectionRectangle('top', fakeSvgElement);
        expect(service.selectionRectangles.get('top')).toBe(fakeSvgElement);
    });

    it('should unselect object if toggle is called and object is selected', () => {
        fakeSvgElement.setAttribute('isSelected', 'true');
        service.toggleSelection(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'false');
    });

    it('should do nothing if object is not selected and not in selection rectangle', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 80, y: 80 };
        service.svgHandler.elements.push(fakeSvgElement);
        service.findAllObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(0);
    });

    it('should select object if in selection and not selected', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 800, y: 800 };
        secondfakeElement.setAttribute('transform', 'matrix(0,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        service.svgHandler.elements.push(fakeSvgElement);
        service.findAllObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
    });

    it('should not select object if in selection but already selected', () => {
        const minCoordinates = { x: 0, y: 0 };
        const maxCoordinates = { x: 800, y: 800 };
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        service.svgHandler.elements.push(fakeSvgElement);
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.findAllObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).not.toHaveBeenCalled();
    });

    it('should unselect object if not in selection and selected', () => {
        const minCoordinates = { x: 10, y: 10 };
        let maxCoordinates = { x: 800, y: 800 };
        secondfakeElement.setAttribute('transform', 'matrix(0,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        service.svgHandler.elements.push(fakeSvgElement);
        service.findAllObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        maxCoordinates = { x: 80, y: 80 };
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(false);
        service.findAllObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'false');
    });

    it('should do nothing if object was not toggled and not in selection rectangle', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 80, y: 80 };
        service.svgHandler.elements.push(fakeSvgElement);
        service.toggleObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(0);
    });

    it('should toggle objects selection if in selection rectangle and not toggled', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 800, y: 800 };
        secondfakeElement.setAttribute('transform', 'matrix(0,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        fakeSvgElement.setAttribute('isSelected', 'false');
        service.svgHandler.elements.push(fakeSvgElement);
        service.toggleObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(service.toggledItems.length).toBe(1);
        expect(service.toggledItems[0]).toBe(fakeSvgElement);
    });

    it('should not toggle objects selection if in selection but already toggled', () => {
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 800, y: 800 };
        secondfakeElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        fakeSvgElement.setAttribute('isSelected', 'false');
        service.svgHandler.elements.push(fakeSvgElement);
        service.toggledItems.push(fakeSvgElement);
        service.toggleObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).not.toHaveBeenCalled();
        expect(service.toggledItems.length).toBe(1);
        expect(service.toggledItems[0]).toBe(fakeSvgElement);
    });

    it('should not toggle object which is non togable', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 800, y: 800 };
        fakeSvgElement.setAttribute('isSelected', 'false');
        service.svgHandler.elements.push(fakeSvgElement);
        service.nonTogableItem = fakeSvgElement;
        service.toggleObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).not.toHaveBeenCalled();
        expect(service.toggledItems.length).toBe(0);
        expect(service.toggledItems[0]).not.toBe(fakeSvgElement);
    });

    it('should toggle objects selection if not in selection rectangle and already toggled', () => {
        const minCoordinates = { x: 10, y: 10 };
        const maxCoordinates = { x: 800, y: 800 };
        secondfakeElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(true);
        fakeSvgElement.setAttribute('isSelected', 'false');
        service.svgHandler.elements.push(fakeSvgElement);
        service.toggleObjectsInLimits(minCoordinates, maxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(service.toggledItems.length).toBe(1);
        expect(service.toggledItems[0]).toBe(fakeSvgElement);
        const newMaxCoordinates = { x: 80, y: 80 };
        selectionCalculatorSpy.isObjectInSelection.and.returnValue(false);
        fakeSvgElement.setAttribute('isSelected', 'true');
        service.toggleObjectsInLimits(minCoordinates, newMaxCoordinates);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'false');
        expect(service.toggledItems.length).toBe(0);
    });

    it('should select svgElement and create rectangles if not already selected', () => {
        secondfakeElement.setAttribute('transform', 'matrix(0,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.selectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'transform', 'matrix(1,0,0,1,0,0)');
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, secondfakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'x', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'y', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'width', '10');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'height', '10');
        expect(service.selectionCalculator.selectedObjects.length).toBe(1);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should select svgElement and create rectangles if not already selected', () => {
        secondfakeElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.selectionRectangles.set('top', secondfakeElement);
        service.selectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, secondfakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'x', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'y', '0');
        expect(service.selectionCalculator.selectedObjects.length).toBe(1);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should select svgElement and create rectangles if not already selected and not reset transform', () => {
        secondfakeElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.selectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, secondfakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'x', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'y', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'width', '10');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'height', '10');
        expect(service.selectionCalculator.selectedObjects.length).toBe(1);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should select svgElement and not create rectangles if already selected', () => {
        service.selectionRectangles.set('top', secondfakeElement);
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.selectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'true');
        expect(rendererSpy.appendChild).not.toHaveBeenCalled();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(service.selectionCalculator.selectedObjects.length).toBe(1);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should unselect object an recreate selection rectangles', () => {
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.selectionCalculator.selectedObjects.push(secondfakeElement);
        service.unselectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'false');
        expect(rendererSpy.appendChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, secondfakeElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'x', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'y', '0');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'width', '10');
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'height', '10');
        expect(service.selectionCalculator.selectedObjects.length).toBe(1);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should unselect object an remove selection rectangles', () => {
        const serviceRemoveSelectionSpy = spyOn(service, 'removeSelectionRectangles');
        const serviceResetTranslateSpy = spyOn(service, 'resetTransformSelectionRectangles');
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.unselectObject(fakeSvgElement);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'isSelected', 'false');
        expect(serviceRemoveSelectionSpy).toHaveBeenCalled();
        expect(serviceResetTranslateSpy).toHaveBeenCalled();
        expect(service.selectionCalculator.selectedObjects.length).toBe(0);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should unselect all objects', () => {
        const serviceSpy = spyOn(service, 'unselectObject');
        fakeSvgElement.setAttribute('isSelected', 'true');
        secondfakeElement.setAttribute('isSelected', 'true');
        service.svgHandler.elements.push(fakeSvgElement);
        service.svgHandler.elements.push(secondfakeElement);
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.selectionCalculator.selectedObjects.push(secondfakeElement);
        service.unselectAll();
        expect(serviceSpy).toHaveBeenCalledWith(fakeSvgElement);
        expect(serviceSpy).toHaveBeenCalledWith(secondfakeElement);
    });

    it('should unselect all objects exept specified one', () => {
        const serviceSpy = spyOn(service, 'unselectObject');
        fakeSvgElement.setAttribute('isSelected', 'true');
        secondfakeElement.setAttribute('isSelected', 'true');
        service.svgHandler.elements.push(fakeSvgElement);
        service.svgHandler.elements.push(secondfakeElement);
        service.selectionCalculator.selectedObjects.push(fakeSvgElement);
        service.selectionCalculator.selectedObjects.push(secondfakeElement);
        service.unselectAll(fakeSvgElement);
        expect(serviceSpy).toHaveBeenCalledWith(secondfakeElement);
    });

    it('should select all objects', () => {
        const serviceSpy = spyOn(service, 'selectObject');
        service.svgHandler.elements.push(fakeSvgElement);
        service.svgHandler.elements.push(secondfakeElement);
        service.selectAll();
        expect(serviceSpy).toHaveBeenCalledWith(fakeSvgElement);
        expect(serviceSpy).toHaveBeenCalledWith(secondfakeElement);
    });

    it('should listen to boundary rectangles', () => {
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.listenToBoundaryRectangles();
        expect(rendererSpy.listen).toHaveBeenCalledTimes(2);
    });

    it('should set clickedOnElement to true when selection rectangle is clicked on', () => {
        const clickButton = new MouseEvent('click', { button: 0 });
        service.selectionRectangles.set('top', secondfakeElement);
        service.listenToBoundaryRectangles();
        const callbackClick = rendererSpy.listen.calls.argsFor(0)[2];
        callbackClick(clickButton);
        expect(service.clickedOnElement).toBeTruthy();
    });

    it('should not set clickedOnElement to true if button is not 0', () => {
        const clickButton = new MouseEvent('click', { button: 1 });
        service.selectionRectangles.set('top', secondfakeElement);
        service.clickedOnElement = false;
        service.listenToBoundaryRectangles();
        const callbackClick = rendererSpy.listen.calls.argsFor(0)[2];
        callbackClick(clickButton);
        expect(service.clickedOnElement).toBeFalsy();
    });

    it('should remove selection rectangles', () => {
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.removeSelectionRectangles();
        expect(rendererSpy.removeChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, secondfakeElement);
    });

    it('should set clickedOnBoundary to true when boundary rectangle is clicked in', () => {
        const clickButton = new MouseEvent('click', { buttons: 0 });
        service.selectionRectangles.set('boundary', secondfakeElement);
        service.listenToBoundaryRectangles();
        const callbackClick = rendererSpy.listen.calls.argsFor(1)[2];
        callbackClick(clickButton);
        expect(service.clickedOnBoundary).toBeTruthy();
    });

    it('should tell if mouse is in the selection rectangle', () => {
        const coordinates = { x: 100, y: 100 };
        service.mouseInSelection(coordinates, fakeSvgElement);
        expect(selectionCalculatorSpy.isObjectInSelection).toHaveBeenCalledWith(fakeSvgElement, coordinates, coordinates);
    });

    it('should reset transform selection attribute', () => {
        service.selectionRectangles.set('transform', secondfakeElement);
        service.resetTransformSelectionRectangles();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(secondfakeElement, 'transform', 'matrix(1,0,0,1,0,0)');
    });
});
