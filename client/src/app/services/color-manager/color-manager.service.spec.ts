import { TestBed } from '@angular/core/testing';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { RGBA } from '../../interfaces-enums/rgba';
import { ColorManagerService } from './color-manager.service';

// tslint:disable:no-magic-numbers

describe('ColorManagerService', () => {
    let colorManagerService: ColorManagerService;
    const rgbaColor: RGBA = {
        Dec: {
            Red: 255,
            Green: 255,
            Blue: 255,
            Alpha: 1
        },
        Hex : {
            Red: 'ff',
            Green: 'ff',
            Blue: 'ff'
        },
        inString: 'rgba(255, 255, 255, 1)'
    };
    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        colorManagerService = TestBed.get(ColorManagerService);
    });

    it('should be created', () => {
        colorManagerService = TestBed.get(ColorManagerService);
        expect(colorManagerService).toBeTruthy();
    });

    it('should update history', () => {
        colorManagerService.colorHistory.length = 5;
        colorManagerService.updateColorWithRGBA(ColorChoice.mainColor, rgbaColor, true);
        expect(colorManagerService.colorHistory.length).toBe(5);
    });

    it('should not update history', () => {
        colorManagerService.colorHistory.length = 5;
        colorManagerService.updateColorWithRGBA(ColorChoice.mainColor, rgbaColor, false);
        expect(colorManagerService.colorHistory.length).toBe(6);
    });

    it('should update color with hex', () => {
        colorManagerService.updateColorWithHex(ColorChoice.mainColor, '32', 'ff', 'ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Red).toBe(50);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Green).toBe(255);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Blue).toBe(255);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Red).toBe('32');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Green).toBe('ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Blue).toBe('ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].inString).toBe('rgba(50,255,255,1)');
    });

    it('should not update color with incorrect hex', () => {
        colorManagerService.updateColorWithHex(ColorChoice.mainColor, 'QQ', 'ts', 'PP');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Red).toBe(255);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Green).toBe(0);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Blue).toBe(0);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Red).toBe('ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Green).toBe('0');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Blue).toBe('0');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].inString).toBe('rgba(255,0,0,1)');
    });

    it('should update color with hex and update history', () => {
        const colorPixel = new Uint8ClampedArray(4);
        colorPixel[0] = 17;
        colorPixel[1] = 93;
        colorPixel[2] = 255;
        colorPixel[3] = 255;
        colorManagerService.updateColorWithPixel(ColorChoice.mainColor, colorPixel);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Red).toBe(17);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Green).toBe(93);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Blue).toBe(255);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Alpha).toBe(1);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Red).toBe('11');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Green).toBe('5d');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Blue).toBe('ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].inString).toBe('rgba(17,93,255,1)');
    });

    it('should update color with hex and not update history', () => {
        const colorPixel = new Uint8ClampedArray(4);
        colorPixel[0] = 17;
        colorPixel[1] = 93;
        colorPixel[2] = 255;
        colorPixel[3] = 25;
        colorManagerService.updateColorWithPixel(ColorChoice.mainColor, colorPixel);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Red).toBe(17);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Green).toBe(93);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Blue).toBe(255);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Dec.Alpha).toBe(0.09803921568627451);
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Red).toBe('11');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Green).toBe('5d');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].Hex.Blue).toBe('ff');
        expect(colorManagerService.colorSelected[ColorChoice.mainColor].inString).toBe('rgba(17,93,255,0.09803921568627451)');
    });

    it('should get custom string with max alpha', () => {
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Red = 255;
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Green = 100;
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Blue = 0;
        const result = colorManagerService.getColorStringWithCustomAlpha(ColorChoice.backgroundColor, true);
        expect(result).toBe('rgba(255,100,0,1)');
    });

    it('should get custom string with 0 for alpha', () => {
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Red = 255;
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Green = 100;
        colorManagerService.colorSelected[ColorChoice.backgroundColor].Dec.Blue = 0;
        const result = colorManagerService.getColorStringWithCustomAlpha(ColorChoice.backgroundColor, false);
        expect(result).toBe('rgba(255,100,0,0)');
    });
});
