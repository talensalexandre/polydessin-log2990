import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogConfig, MatDialogModule, MatDialogRef } from '@angular/material';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';
describe('ConfirmationDialogComponent', () => {
    const config: MatDialogConfig = {
        data: {
            message: 'Enregistrer au format TEST ?',
            buttonText: {
                ok: 'Oui',
                cancel: 'Non'
            }
        }
    };

    const mockDialogRef = {
      close: jasmine.createSpy('close')
    };

    let component: ConfirmationDialogComponent;
    let fixture: ComponentFixture<ConfirmationDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            declarations: [ConfirmationDialogComponent],
            providers: [
                { provide: MAT_DIALOG_DATA, useValue: config.data },
                { provide: MatDialogRef, useValue: mockDialogRef },
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmationDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should close', () => {
        component.closeDialog();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });
});
