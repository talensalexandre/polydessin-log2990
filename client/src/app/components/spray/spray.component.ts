import { Component } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SprayHandlerService } from 'src/app/services/spray-handler/spray-handler.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const MAXIMUM_SIZE = 200;
const DEFAULT_SIZE = 50;
const MINIMUM_SIZE = 20;
const MAXIMUM_EMISSION = 200;
const DEFAULT_EMISSION = 100;
const MINIMUM_EMISSION = 10;
const SECOND_IN_MS = 1000;
const NUMBER_OF_RANDOM_CIRCLES = 10;

@Component({
    selector: 'app-spray',
    templateUrl: './spray.component.html',
    styleUrls: ['./spray.component.scss']
})
export class SprayComponent extends AbstractToolComponent {
    emissions: number;
    intervalDestroy: Subscription;

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        private sprayHandler: SprayHandlerService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
        this.emissions = DEFAULT_EMISSION;
    }

    private verifyValue(minValue: number, maxValue: number, value: number): number {
        value = Math.round(value);
        value = (value < minValue) ? minValue : value;
        value = (value > maxValue) ? maxValue : value;
        return value;
    }

    changeEmissionSize(): void {
        this.emissions = this.verifyValue(MINIMUM_EMISSION, MAXIMUM_EMISSION, this.emissions);
        this.stateSelector.isModifyingInput = false;
    }

    changeSize(): void {
        this.size = this.verifyValue(MINIMUM_SIZE, MAXIMUM_SIZE, this.size);
        this.stateSelector.isModifyingInput = false;
    }

    protected mouseMouvementExtend(): void {
        return;
    }

    protected mouseDownExtend(coordinates: Coordinates): void {
        this.mouseHandler.actualCoordinates = coordinates;
        this.svgElement = this.sprayHandler.createSprayObject();
        const timeToWait = SECOND_IN_MS / this.emissions;
        this.intervalDestroy = interval(timeToWait).subscribe(() => this.addSpray());
    }

    private addSpray(): void {
        for (let i = 0; i < NUMBER_OF_RANDOM_CIRCLES; i++) {
            this.sprayHandler.addRandomCircle(this.svgElement, this.mouseHandler.actualCoordinates, this.size);
        }
    }

    protected destroyExtend(): void {
        return;
    }

    protected mouseUpExtend(): void {
        this.isDrawing = false;
        if (this.intervalDestroy) {
            this.intervalDestroy.unsubscribe();
        }
    }

    protected mouseLeft(): void {
        this.mouseHandler.onMouseLeave();
        this.mouseUpExtend();
    }

    increaseEmission(): void {
        this.emissions++;
        this.changeEmissionSize();
    }

    decreaseEmission(): void {
        this.emissions--;
        this.changeEmissionSize();
    }
}
