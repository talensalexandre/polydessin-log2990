import { Injectable, Renderer2 } from '@angular/core';
import { DrawingStored } from '../../interfaces-enums/drawing';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { CreateNewDrawingService } from '../create-new-drawing/create-new-drawing.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

const VIEWBOX_WIDTH = 200;
const VIEWBOX_HEIGHT = 150;
const VIEWBOX_SIZE: number[] = [VIEWBOX_WIDTH, VIEWBOX_HEIGHT];

@Injectable({
    providedIn: 'root'
})
export class DrawingGalleryService {
    private renderer: Renderer2;

    constructor(private svgHandler: SvgHandlerService,
                private newDrawingService: CreateNewDrawingService,
                private saveManager: SaveManagerService,
                private clickerHandler: ClickerHandlerService) {
        this.renderer = this.svgHandler.renderer;
    }

    createElementFromString(drawings: DrawingStored[]): SVGElement[] {
        const parser = new DOMParser();
        const svgArray: SVGElement[] = new Array<SVGElement>();

        drawings.forEach((drawing) => {
            const svgElement = this.createPrevisualization(drawing);
            drawing.svgContainer.forEach((element) => {
                const doc = parser.parseFromString(element, 'image/svg+xml');
                const childElement = doc.firstChild as SVGElement;
                this.renderer.appendChild(svgElement, childElement);
            });
            svgArray.push(svgElement);
        });
        return svgArray;
    }

    private createPrevisualization(drawing: DrawingStored): SVGElement {
        const svgElement = this.renderer.createElement('svg', this.svgHandler.SVG_LINK);
        this.renderer.setStyle(svgElement, 'background-color', drawing.metaDrawing.backgroundColor);
        this.renderer.setAttribute(svgElement, 'viewBox', '0 0 ' + drawing.metaDrawing.width + ' ' + drawing.metaDrawing.height);
        const ratio = Math.max(drawing.metaDrawing.width / VIEWBOX_SIZE[0],
            drawing.metaDrawing.height / VIEWBOX_SIZE[1]);
        this.renderer.setAttribute(svgElement, 'width', (drawing.metaDrawing.width / ratio).toString());
        this.renderer.setAttribute(svgElement, 'height', (drawing.metaDrawing.height / ratio).toString());
        this.svgHandler.addBlurFilter(svgElement);
        this.svgHandler.addRoundedFilter(svgElement);
        this.svgHandler.addFogFilter(svgElement);
        this.svgHandler.addChaosFilter(svgElement);
        this.svgHandler.addShadowFilter(svgElement);
        return svgElement;
    }

    createDrawing(drawing: DrawingStored): void {
        this.svgHandler.elements = [];
        const parser = new DOMParser();
        if (this.svgHandler.svgCanvas) {
            this.svgHandler.deleteCanvas();
        }
        this.newDrawingService.createNewDrawing(drawing.metaDrawing.width, drawing.metaDrawing.height, drawing.metaDrawing.backgroundColor);
        drawing.svgContainer.forEach((element) => {
            const doc = parser.parseFromString(element, 'image/svg+xml');
            const childElement = doc.firstChild as SVGElement;
            this.renderer.appendChild(this.svgHandler.svgCanvas, childElement);
            this.clickerHandler.addSelectionStyles(childElement, false);
            this.saveManager.addSvgElement(childElement);
        });
        this.svgHandler.drawingOnCanvas = true;
    }

    displayElement(element: SVGElement, array: SVGElement[], index: number): void {
        if (element.childNodes.length === 0) {
            this.renderer.appendChild(element, array[index]);
        }
    }
}
