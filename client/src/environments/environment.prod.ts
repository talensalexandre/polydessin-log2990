import { IEnvironment } from './i-environment';

export const environment: IEnvironment = {
    production: true,
    SEND_URL: 'http://localhost:3000/log2990/projet2/drawing/drawings',
    GET_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/',
    GET_ALL_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/drawings',
    DELETE_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/',
    SEND_EMAIL_URL: 'http://localhost:3000/log2990/projet2/email/send'
};
