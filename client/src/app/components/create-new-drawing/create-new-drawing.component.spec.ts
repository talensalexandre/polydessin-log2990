import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MatFormFieldModule, MatIconModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { AllColorGradientComponent } from '../color-components/all-color-gradient/all-color-gradient.component';
import { AlphaColorGradientComponent } from '../color-components/alpha-color-gradient/alpha-color-gradient.component';
import { ColorPopupComponent } from '../color-components/color-popup/color-popup.component';
import { MainColorGradientComponent } from '../color-components/main-color-gradient/main-color-gradient.component';
import { CreateNewDrawingComponent } from './create-new-drawing.component';

describe('CreateNewDrawingComponent', () => {
    let component: CreateNewDrawingComponent;
    let fixture: ComponentFixture<CreateNewDrawingComponent>;
    let newDrawingServiceSpy: jasmine.SpyObj<CreateNewDrawingService>;

    const mockDialogRef = {
        close: jasmine.createSpy('close')
    };

    beforeEach(() => {
        newDrawingServiceSpy = jasmine.createSpyObj(
            'CreateNewDrawingService',
            [
                'dimensionsChanged',
                'openConfirmation',
                'createNewDrawing',
                'createCanvas',
                'verifyDimensions',
                'updateDimensions',
                'findInnerDimensions',
                'selectPencil'
            ]
        );
        newDrawingServiceSpy.verifyDimensions.and.returnValue({ x: 10, y: 10 });
        newDrawingServiceSpy.updateDimensions.and.returnValue({ x: 20, y: 20 });
        newDrawingServiceSpy.findInnerDimensions.and.returnValue({ x: 30, y: 30 });
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatInputModule, MatFormFieldModule, FormsModule,
                    BrowserAnimationsModule, MatDialogModule, MatIconModule
                ],
            declarations:
                [
                    CreateNewDrawingComponent, ColorPopupComponent,
                    MainColorGradientComponent, AlphaColorGradientComponent,
                    AllColorGradientComponent
                ],
            providers: [{ provide: MatDialogRef, useValue: mockDialogRef },
                        { provide: CreateNewDrawingService, useValue: newDrawingServiceSpy }]
        })
        .overrideModule(
            BrowserDynamicTestingModule,
            { set: { entryComponents: [ColorPopupComponent] }
          });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateNewDrawingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should resize when window resizes', () => {
        window.dispatchEvent(new Event('resize'));
        expect(newDrawingServiceSpy.findInnerDimensions).toHaveBeenCalled();
        expect(newDrawingServiceSpy.updateDimensions).toHaveBeenCalled();
    });

    it('should findInnerDimensions when initialized', () => {
        expect(newDrawingServiceSpy.findInnerDimensions).toHaveBeenCalled();
    });

    it('should create a new drawing', () => {
        newDrawingServiceSpy.createNewDrawing.and.returnValue(false);
        component.createNewDrawing();
        expect(newDrawingServiceSpy.createNewDrawing).toHaveBeenCalled();
    });

    it('should create a new drawing', () => {
        newDrawingServiceSpy.createNewDrawing.and.returnValue(true);
        component.createNewDrawing();
        expect(newDrawingServiceSpy.createNewDrawing).toHaveBeenCalled();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should close itself', () => {
        component.closeDialog();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should create new drawing if confirmed was up', () => {
        component.confirmDrawing();
        expect(mockDialogRef.close).toHaveBeenCalled();
        expect(newDrawingServiceSpy.createCanvas).toHaveBeenCalledTimes(1);
    });

    it('should verify the dimensions', () => {
        component.verifyDimensions();
        expect(newDrawingServiceSpy.verifyDimensions).toHaveBeenCalledTimes(1);
    });

    it('should open color picker', () => {
        component.openColorPicker();
        expect(ColorPopupComponent).toBeTruthy();
    });

    it('should increase height', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'verifyDimensions').and.callThrough();
        component.increaseHeight();
        expect(component.verifyDimensions).toHaveBeenCalledTimes(1);
    });

    it('should decrease height', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'verifyDimensions').and.callThrough();
        component.decreaseHeight();
        expect(component.verifyDimensions).toHaveBeenCalledTimes(1);
    });

    it('should increase width', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'verifyDimensions').and.callThrough();
        component.increaseWidth();
        expect(component.verifyDimensions).toHaveBeenCalledTimes(1);
    });

    it('should decrease width', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'verifyDimensions').and.callThrough();
        component.decreaseWidth();
        expect(component.verifyDimensions).toHaveBeenCalledTimes(1);
    });
});
