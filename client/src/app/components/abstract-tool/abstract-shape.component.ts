import { HostListener } from '@angular/core';
import { SelectionType } from 'src/app/interfaces-enums/selection-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const FILL_ONLY_SIZE = 0;
const MINIMUM_SIZE = 1;
const DEFAULT_SIZE = 3;
const MAXIMUM_SIZE = 200;
const SHIFT_KEY = 'Shift';

export abstract class AbstractShapeComponent extends AbstractToolComponent {
    selectedType: SelectionType;
    isSelectionTooSmall: boolean;
    protected shiftKeyDown: boolean;
    protected previsualisationElement: SVGElement;
    protected fill: boolean;
    stroke: boolean;
    protected mouseMoved: boolean;

    @HostListener('window:keydown', ['$event']) handleKeyDown(keyEvent: KeyboardEvent): void {
        if (keyEvent.key === SHIFT_KEY) {
            this.shiftKeyDown = true;
            if (this.mouseHandler.isMouseDown) {
                this.handleKeyDownExtend();
            }
        }
    }

    @HostListener('window:keyup', ['$event']) handleKeyUp(keyEvent: KeyboardEvent): void {
        if (keyEvent.key === SHIFT_KEY) {
            this.shiftKeyDown = false;
            if (this.mouseHandler.isMouseDown) {
                this.handleKeyUpExtend();
            }
        }
    }

    constructor(
        protected mouseHandler: MouseHandlerService,
        protected svgHandler: SvgHandlerService,
        public stateSelector: StateSelectorService,
        protected drawingService: DrawingService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
        this.mouseMoved = false;
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
        this.selectedType = SelectionType.outline;
        this.stroke = true;
        this.isSelectionTooSmall = false;
    }

    layoutType(): void {
        switch (this.selectedType) {
            case SelectionType.outline:
                this.fill = false;
                this.stroke = true;
                this.size = DEFAULT_SIZE;
                break;
            case SelectionType.fill:
                this.fill = true;
                this.stroke = false;
                this.size = FILL_ONLY_SIZE;
                break;
            case SelectionType.outlineAndFill:
                this.fill = true;
                this.stroke = true;
                this.size = DEFAULT_SIZE;
                break;
        }
    }

    protected mouseMouvementExtend(): void {
        if (!this.mouseHandler.isMouseDown) {
            return;
        }
        this.mouseMoved = true;
        this.shiftKeyDown ? this.mouseMouvementCalculateShiftShape() : this.mouseMouvementCalculateNormalShape();
        this.mouseMouvementPrevisualization();
    }

    protected mouseUpExtend(): void {
        this.mouseUpExtendExtra();
        if (this.mouseMoved && !this.isSelectionTooSmall) {
            this.mouseUpCreateShape();
        }
        this.mouseMoved = false;
        this.removePrevisualization();
    }

    protected mouseLeft(): void {
        this.removePrevisualization();
        this.mouseLeftExtend();
        this.isDrawing = false;
        this.mouseHandler.isMouseDown = false;
        this.mouseHandler.onMouseLeave();
    }

    private removePrevisualization(): void {
        this.drawingService.removeElement(this.previsualisationElement);
        delete this.previsualisationElement;
    }

    protected mouseDownExtend(): void {
        return;
    }

    changeSize(): void {
        this.size = Math.round(this.size);
        if (this.selectedType === SelectionType.fill) {
            this.size = (this.size < FILL_ONLY_SIZE) ? FILL_ONLY_SIZE : this.size;
            this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        } else {
            this.size = (this.size < MINIMUM_SIZE) ? MINIMUM_SIZE : this.size;
            this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        }
        this.stateSelector.isModifyingInput = false;
    }

    protected abstract handleKeyDownExtend(): void;

    protected abstract handleKeyUpExtend(): void;

    protected abstract mouseMouvementCalculateShiftShape(): void;

    protected abstract mouseMouvementCalculateNormalShape(): void;

    protected abstract mouseMouvementPrevisualization(): void;

    protected abstract mouseUpCreateShape(): void;

    protected abstract mouseLeftExtend(): void;

    protected abstract mouseUpExtendExtra(): void;

    protected abstract destroyExtend(): void;
}
