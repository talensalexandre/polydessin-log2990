import { Component } from '@angular/core';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';

@Component({
    selector: 'app-shape-doc',
    templateUrl: './shape-doc.component.html',
    styleUrls: ['./shape-doc.component.scss']
})
export class ShapeDocComponent {

    constructor(public userGuideService: UserGuideService) { }
}
