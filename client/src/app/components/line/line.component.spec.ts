import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { LineHandlerService } from 'src/app/services/line-handler/line-handler.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { LineComponent } from './line.component';

// tslint:disable:no-magic-numbers max-file-line-count no-string-literal

describe('LineComponent', () => {
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let lineServiceSpy: jasmine.SpyObj<LineHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let fixture: ComponentFixture<LineComponent>;
    let component: LineComponent;

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj('SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        mouseHandlerServiceSpy = jasmine.createSpyObj('MouseHandlerService',
            [
                'onMouseLeave',
                'onMouseClick',
                'makeActualCoordsEndCoords',
                'updateEndCoords',
                'onMouseMovement',
                'onMouseDown',
                'onMouseUp'
            ]
        );
        lineServiceSpy = jasmine.createSpyObj('LineHandlerService',
            [
                'linePrevisualization',
                'shiftPrevisualization',
                'createJunction',
                'connectFirstAndLastPoint',
                'emptyChildren',
                'deleteLine',
                'removeJunction',
                'createGElement',
                'addElementToG'
            ]
        );
        drawingServiceSpy = jasmine.createSpyObj('DrawingService',
            [
                'createPolyLine',
                'addCoordinatesToPolyline',
                'deleteLastSegmentFromPolyline',
                'getLastCoordinatesOfPolyline',
                'removeElement'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                    FormsModule, MatSelectModule, MatIconModule, MatDialogModule,
                    HttpClientModule
                ],
            declarations: [LineComponent, ColorComponent],
            providers: [
              { provide: SvgHandlerService, useValue: svgHandlerServiceSpy },
              { provide: MouseHandlerService, useValue: mouseHandlerServiceSpy },
              { provide: LineHandlerService, useValue: lineServiceSpy },
              { provide : DrawingService, useValue: drawingServiceSpy }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LineComponent);
        component = fixture.componentInstance;
        component.previsualizationLine = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
        component.previsualizationLine.setAttribute('x1', '100');
        component.previsualizationLine.setAttribute('y1', '100');
        component.previsualizationLine.setAttribute('x2', '600');
        component.previsualizationLine.setAttribute('y2', '600');
        component['svgElement'] = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        component['svgElement'].setAttribute('points', '100,100 110,100, 150,150');
        mouseHandlerServiceSpy.startCoordinates = {x: 100, y: 100};
        mouseHandlerServiceSpy.actualCoordinates = {x: 100, y: 100};
        mouseHandlerServiceSpy.endCoordinates = {x: 100, y: 100};
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        component.ngOnInit();
        expect(svgHandlerServiceSpy.addListeners).toHaveBeenCalled();
    });

    it('should call appropriate functions when : first click, normal juctions', () => {
        spyOn(component, 'updatePrevisualization');
        component.clickCounter = 0;
        component.isShiftDown = false;
        component.isNormalJunction = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 500, clientY: 500 });
        component.updateMouseClick(mockMouseClick);
        expect(component.clickCounter).toEqual(1);
        expect(component.updatePrevisualization).toHaveBeenCalled();
        expect(drawingServiceSpy.createPolyLine).toHaveBeenCalledTimes(1);
        expect(lineServiceSpy.createJunction).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.makeActualCoordsEndCoords).toHaveBeenCalledTimes(0);
        expect(drawingServiceSpy.addCoordinatesToPolyline).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalledTimes(0);
    });

    it('UpdateMouseClick should call appropriate functions when : second (and more) click, normal juctions, shift not down', () => {
        spyOn(component, 'updatePrevisualization');
        component.clickCounter = 1;
        component.isShiftDown = false;
        component.isNormalJunction = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 500, clientY: 500 });
        component.updateMouseClick(mockMouseClick);
        expect(component.clickCounter).toEqual(2);
        expect(component.updatePrevisualization).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.makeActualCoordsEndCoords).toHaveBeenCalled();
        expect(drawingServiceSpy.addCoordinatesToPolyline).toHaveBeenCalled();
        expect(lineServiceSpy.createJunction).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalledTimes(0);
        expect(drawingServiceSpy.createPolyLine).toHaveBeenCalledTimes(0);
    });

    it('should change endCoordinates on click with shift down', () => {
        component.clickCounter = 1;
        component.isShiftDown = true;
        component.isNormalJunction = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 500, clientY: 500 });
        component.updateMouseClick(mockMouseClick);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalled();
    });

    it('should call "linePrevisualization" when shift not down', () => {
        component.clickCounter = 2;
        component.isShiftDown = false;
        const mockMouseClick = new MouseEvent('click', { clientX: 600, clientY: 600 });
        component.updatePrevisualization(mockMouseClick);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalled();
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalled();
    });

    it('should call "shiftPrevisualization" when shift down', () => {
        component.clickCounter = 2;
        component.isShiftDown = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 600, clientY: 600 });
        component.updatePrevisualization(mockMouseClick);
        expect(lineServiceSpy.shiftPrevisualization).toHaveBeenCalled();
    });

    it('should not call "shiftPrevisualization" or "linePrevisualization" when shift down', () => {
        component.clickCounter = 0;
        component.isShiftDown = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 600, clientY: 600 });
        component.updatePrevisualization(mockMouseClick);
        expect(lineServiceSpy.shiftPrevisualization).toHaveBeenCalledTimes(0);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should call "createJunction" when click with junction on', () => {
        component.clickCounter = 1;
        component.isShiftDown = false;
        component.isNormalJunction = false;
        const mockMouseClick = new MouseEvent('click', { clientX: 500, clientY: 500 });
        component.updateMouseClick(mockMouseClick);
        expect(lineServiceSpy.createJunction).toHaveBeenCalled();
    });

    it('should call appropriates fucntions when double click', () => {
        component.isShiftDown = false;
        component.isNormalJunction = true;
        component.clickCounter = 3;
        component['mouseHandler'].startCoordinates = { x: 500, y: 500 };
        component['mouseHandler'].endCoordinates = { x: 504, y: 504 };
        component.doubleClick();
        expect(drawingServiceSpy.deleteLastSegmentFromPolyline).toHaveBeenCalled();
        expect(lineServiceSpy.emptyChildren).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
        expect(drawingServiceSpy.removeElement).toHaveBeenCalled();
        expect(lineServiceSpy.connectFirstAndLastPoint).toHaveBeenCalledTimes(0);
    });

    it('should call connect first and last point when double click at max 3px of first point', () => {
        component.isShiftDown = false;
        component.isNormalJunction = true;
        component.clickCounter = 3;
        component['mouseHandler'].startCoordinates = { x: 500, y: 500 };
        component['mouseHandler'].endCoordinates = { x: 503, y: 503 };
        component.doubleClick();
        expect(lineServiceSpy.connectFirstAndLastPoint).toHaveBeenCalled();
        expect(lineServiceSpy.emptyChildren).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
        expect(drawingServiceSpy.removeElement).toHaveBeenCalled();
        expect(drawingServiceSpy.deleteLastSegmentFromPolyline).toHaveBeenCalled();
    });

    it('should not call connect first and last point when double click and clickcounter <= 2', () => {
        component.isShiftDown = false;
        component.isNormalJunction = true;
        component.clickCounter = 2;
        component['mouseHandler'].startCoordinates = { x: 500, y: 500 };
        component['mouseHandler'].endCoordinates = { x: 503, y: 503 };
        component.doubleClick();
        expect(lineServiceSpy.connectFirstAndLastPoint).toHaveBeenCalledTimes(0);
    });

    it('should not update shift previsualization when shift down and click counter = 0', () => {
        component.clickCounter = 0;
        const mockShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        component.onKeyDown(mockShiftKey);
        expect(lineServiceSpy.shiftPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should update shift previsualization when shift down and click counter > 0', () => {
        component.clickCounter = 1;
        const mockShiftKey = new KeyboardEvent('keydown', { key: 'Shift' });
        component.onKeyDown(mockShiftKey);
        expect(lineServiceSpy.shiftPrevisualization).toHaveBeenCalled();
    });

    it('should not update line previsualization when shift released and click counter < 1', () => {
        component.clickCounter = 0;
        const mockShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        component.onKeyUp(mockShiftKey);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should update line previsualization when shift released and click counter >= 1', () => {
        component.clickCounter = 1;
        const mockShiftKey = new KeyboardEvent('keyup', { key: 'Shift' });
        component.onKeyUp(mockShiftKey);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalled();
    });

    it('should not update line previsualization when x released and click counter >= 1', () => {
        component.clickCounter = 1;
        const mockXKey = new KeyboardEvent('keyup', { key: 'x' });
        component.onKeyUp(mockXKey);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should delete line when escape pressed', () => {
        component.clickCounter = 1;
        const mockEscapetKey = new KeyboardEvent('keydown', { key: 'Escape' });
        component.onKeyDown(mockEscapetKey);
        expect(lineServiceSpy.deleteLine).toHaveBeenCalled();
        expect(lineServiceSpy.emptyChildren).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
        expect(drawingServiceSpy.removeElement).toHaveBeenCalled();
    });

    it('should delete 1 segment when backspace pressed after second click (normal juction)', () => {
        component.clickCounter = 2;
        const mockBackspaceKey = new KeyboardEvent('keydown', { key: 'Backspace' });
        component.onKeyDown(mockBackspaceKey);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalled();
        expect(drawingServiceSpy.deleteLastSegmentFromPolyline).toHaveBeenCalled();
        expect(drawingServiceSpy.getLastCoordinatesOfPolyline).toHaveBeenCalled();
        expect(lineServiceSpy.removeJunction).toHaveBeenCalledTimes(0);
    });

    it('should delete 1 segment when backspace pressed after second click (point juction)', () => {
        component.clickCounter = 2;
        component.isNormalJunction = false;
        const mockBackspaceKey = new KeyboardEvent('keydown', { key: 'Backspace' });
        component.onKeyDown(mockBackspaceKey);
        expect(lineServiceSpy.removeJunction).toHaveBeenCalled();
    });

    it('should not delete 1 segment when backspace pressed before second click', () => {
        component.clickCounter = 0;
        const mockBackspaceKey = new KeyboardEvent('keydown', { key: 'Backspace' });
        component.onKeyDown(mockBackspaceKey);
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalledTimes(0);
        expect(drawingServiceSpy.deleteLastSegmentFromPolyline).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
    });

    it('should not change size if between 1 and 200', () => {
        const size = 50;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(size);
    });

    it('should change size if under 1', () => {
        const size = -50;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(1);
    });

    it('should change size if above 200', () => {
        const size = 500;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(200);
    });

    it('should call changeJunctionSize when changeSize is called', () => {
        spyOn(component, 'changeJunctionSize');
        component.changeSize();
        expect(component.changeJunctionSize).toHaveBeenCalled();
    });

    it('should not change Junction size if between 1 and 200', () => {
        const junctionSize = 50;
        component.junctionSize = junctionSize;
        component.changeJunctionSize();
        expect(component.junctionSize).toEqual(junctionSize);
    });

    it('should change Junction size if under 1', () => {
        const junctionSize = -50;
        component.junctionSize = junctionSize;
        component.changeJunctionSize();
        expect(component.junctionSize).toEqual(component.size);
    });

    it('should change Junction size if above 200', () => {
        const junctionSize = 500;
        component.junctionSize = junctionSize;
        component.changeJunctionSize();
        expect(component.junctionSize).toEqual(200);
    });

    it('should call updateMousePosition', () => {
        const mockMouse = new MouseEvent('mousemove');
        component['updateMousePosition'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalled();
    });

    it('should call updateMouseDown', () => {
        const mockMouse = new MouseEvent('mousedown');
        component['updateMouseDown'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
    });

    it('should call updateMouseUp', () => {
        const mockMouse = new MouseEvent('mouseup');
        component['mouseHandler'].isMouseDown = true;
        component['updateMouseUp'](mockMouse);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalled();
    });

    it('should call "linePrevisualization" when calling eventListeners.mouseMove', () => {
        component.clickCounter = 2;
        component.isShiftDown = false;
        const mockMouseClick = new MouseEvent('click', { clientX: 600, clientY: 600 });
        component['eventListeners'].mouseMove(mockMouseClick);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalled();
        expect(lineServiceSpy.linePrevisualization).toHaveBeenCalled();
    });

    it('should change endCoordinates on click when calling eventListeners.mouseClick', () => {
        component.clickCounter = 1;
        component.isShiftDown = true;
        component.isNormalJunction = true;
        const mockMouseClick = new MouseEvent('click', { clientX: 500, clientY: 500 });
        component['eventListeners'].mouseClick(mockMouseClick);
        expect(mouseHandlerServiceSpy.updateEndCoords).toHaveBeenCalled();
    });

    it('should not call connect first and last point when calling eventListeners.mouseDblClick', () => {
        component.isShiftDown = false;
        component.isNormalJunction = true;
        component.clickCounter = 2;
        const mouseEvent = new MouseEvent('click');
        component['mouseHandler'].startCoordinates = { x: 500, y: 500 };
        component['mouseHandler'].endCoordinates = { x: 503, y: 503 };
        component['eventListeners'].mouseDblClick(mouseEvent);
        expect(lineServiceSpy.connectFirstAndLastPoint).toHaveBeenCalledTimes(0);
    });

    it('should increase size', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeJunctionSize').and.callThrough();
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeSize').and.callThrough();
        component.increaseSize();
        component.increaseJunctionSize();
        expect(component.changeJunctionSize).toHaveBeenCalled();
        expect(component.changeSize).toHaveBeenCalled();
    });

    it('should decrease size', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeJunctionSize').and.callThrough();
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeSize').and.callThrough();
        component.decreaseSize();
        component.decreaseJunctionSize();
        expect(component.changeJunctionSize).toHaveBeenCalled();
        expect(component.changeSize).toHaveBeenCalled();
    });
});
