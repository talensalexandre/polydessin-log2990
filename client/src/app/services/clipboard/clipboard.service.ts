import { Injectable } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionCalculatorService } from '../selection-services/selection-calculator/selection-calculator.service';
import { SelectionMoveService } from '../selection-services/selection-move/selection-move.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const CUT = 'x';
const COPY = 'c';
const PASTE = 'v';
const DUPLICATE = 'd';
const DELETE = 'Delete';
const SELECT_ALL = 'a';
const PX_OFFSET = 10;

@Injectable({
    providedIn: 'root'
})
export class ClipboardService {
    private clipboard: SVGElement[];
    selectedObjects: SVGElement[];
    private firstSelectedObjectsToCopy: string[];
    private firstSelectedObjectsToDuplicate: string[];

    constructor(private selectionCalculator: SelectionCalculatorService,
                private selectionHandler: SelectionSvgHandlerService,
                private saveManager: SaveManagerService,
                private svgHandler: SvgHandlerService,
                private undoRedo: UndoRedoService,
                private selectionMove: SelectionMoveService) {
        this.selectedObjects = this.selectionCalculator.selectedObjects;
        this.clipboard = new Array<SVGElement>();
        this.firstSelectedObjectsToCopy = new Array<string>();
        this.firstSelectedObjectsToDuplicate = new Array<string>();
    }

    copy(isFirstTime: boolean): void {
        this.clipboard = new Array<SVGElement>();
        if (isFirstTime) {
            this.firstSelectedObjectsToCopy = new Array<string>();
        }
        if (this.selectedObjects.length > 0) {
            this.selectedObjects.forEach((element) => {
                if (isFirstTime) {
                    this.firstSelectedObjectsToCopy.push(element.getAttribute('transform') as string);
                }
                this.clipboard.push(element.cloneNode(true) as SVGElement);
            });
        }
    }

    cut(): void {
        this.copy(true);
        this.delete();
    }

    paste(): void {
        this.selectionHandler.unselectAll();
        this.appendSVGElement(this.clipboard, this.firstSelectedObjectsToCopy);
        this.copy(false);
    }

    duplicate(): void {
        this.firstSelectedObjectsToDuplicate = new Array<string>();
        this.selectedObjects.forEach((element) => {
            this.firstSelectedObjectsToDuplicate.push(element.getAttribute('transform') as string);
        });
        const tempClipboard = Array.from(this.selectedObjects);
        this.selectionHandler.unselectAll();
        this.appendSVGElement(tempClipboard, this.firstSelectedObjectsToDuplicate);
        this.undoRedo.updateCurrent(this.clipboard, ChangeType.addToCanvas);
    }

    private appendSVGElement(elementsToAppend: SVGElement[], firstSelected: string[]): void {
        const elementsToAppendUpdated: SVGElement[] = this.transformElements(elementsToAppend, firstSelected);
        this.undoRedo.updateCurrent(elementsToAppendUpdated, ChangeType.addToCanvas);
    }

    private transformElements(elements: SVGElement[], firstSelected: string[]): SVGElement[] {
        const transformedElements: SVGElement[] = new Array<SVGElement>();
        elements.forEach((element) => {
            const newElement = element.cloneNode(true) as SVGElement;
            this.selectionMove.translate(newElement, { x: PX_OFFSET, y: PX_OFFSET });
            this.svgHandler.renderer.appendChild(this.svgHandler.svgCanvas, newElement);
            this.svgHandler.elements.push(newElement);
            this.saveManager.addSvgElement(newElement);
            this.selectionHandler.selectObject(newElement);
            transformedElements.push(newElement);
        });
        const boundaryDimensions = this.selectionCalculator.boundaryDimensions;
        const limitCoordinates = {
            x: boundaryDimensions.width + boundaryDimensions.coordinates.x,
            y: boundaryDimensions.height + boundaryDimensions.coordinates.y
        };

        if (limitCoordinates.x > this.svgHandler.canvasWidth || limitCoordinates.y > this.svgHandler.canvasHeight) {
            transformedElements.forEach((element, index) => {
                const transform = firstSelected[index];
                this.svgHandler.renderer.setAttribute(element, 'transform', transform);
            });
        }
        return transformedElements;
    }

    delete(): void {
        this.undoRedo.updateCurrent(Array.from(this.selectedObjects), ChangeType.elementRemoved);
        const oldSelectedObject = new Array<SVGElement>();
        this.selectedObjects.forEach((element) => {
            this.svgHandler.renderer.removeChild(this.svgHandler.svgCanvas, element);
            this.saveManager.removeSvgElement(element);
            oldSelectedObject.push(element);
        });
        this.selectionHandler.unselectAll();
        oldSelectedObject.forEach((element) => {
            this.svgHandler.elements.splice(this.svgHandler.elements.indexOf(element), 1);
        });
    }

    selectAll(): void {
        this.selectionHandler.selectAll();
    }

    handleKey(key: string): void {
        switch (key) {
            case CUT: {
                this.cut();
                break;
            }
            case COPY: {
                this.copy(true);
                break;
            }
            case PASTE: {
                this.paste();
                break;
            }
            case DUPLICATE: {
                this.duplicate();
                break;
            }
            case DELETE: {
                this.delete();
                break;
            }
            case SELECT_ALL: {
                this.selectAll();
                break;
            }
            default: {
                break;
            }
        }
    }
}
