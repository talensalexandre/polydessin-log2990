import { Injectable } from '@angular/core';
import { Coordinates } from '../../../interfaces-enums/coordinates';
import { RectangleDimensions } from '../../../interfaces-enums/rectangle-dimensions';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';

const SELECTION_RECTANGLE_SIZE = 2;
const CONTROL_POINT_SIZE = 5;
const INITIAL_COORDS = -1;

@Injectable({
    providedIn: 'root'
})
export class SelectionCalculatorService {
    boundaryDimensions: RectangleDimensions;
    private boundaryMaxCoordinates: Coordinates;
    selectedObjects: SVGElement[];
    centerOfSelection: Coordinates;
    centersOfSelectedObjects: Coordinates[];

    constructor(
        private svgHandler: SvgHandlerService
    ) {
        this.boundaryDimensions = {coordinates: {x: INITIAL_COORDS, y: INITIAL_COORDS}, height: INITIAL_COORDS, width: INITIAL_COORDS};
        this.boundaryMaxCoordinates = {x: INITIAL_COORDS, y: INITIAL_COORDS};
        this.selectedObjects = new Array<SVGElement>();
        this.centerOfSelection = {x: INITIAL_COORDS, y: INITIAL_COORDS};
        this.centersOfSelectedObjects = new Array<Coordinates>();
    }

    private objectBoundaryCoordinates(svgElement: SVGElement): Coordinates {
        const halfSizeSvgElement = Number(svgElement.getAttribute('size')) / 2;
        const elementBoundingRect = svgElement.getBoundingClientRect();
        const canvasBoundingRect = this.svgHandler.svgCanvas.getBoundingClientRect();
        return {
            x: elementBoundingRect.left - canvasBoundingRect.left - halfSizeSvgElement,
            y: elementBoundingRect.top - halfSizeSvgElement - canvasBoundingRect.top - SELECTION_RECTANGLE_SIZE / 2
        };
    }

    private objectMaxCoordinates(svgElement: SVGElement): Coordinates {
        const halfSizeSvgElement = Number(svgElement.getAttribute('size')) / 2;
        const elementBoundingRect = svgElement.getBoundingClientRect();
        const canvasBoundingRect = this.svgHandler.svgCanvas.getBoundingClientRect();
        return {
            x: elementBoundingRect.right - canvasBoundingRect.left + halfSizeSvgElement + SELECTION_RECTANGLE_SIZE,
            y: elementBoundingRect.bottom + halfSizeSvgElement - canvasBoundingRect.top + SELECTION_RECTANGLE_SIZE / 2
        };
    }

    private findNewBoundingCoordinates(): void {
        this.boundaryDimensions.coordinates = this.objectBoundaryCoordinates(this.selectedObjects[0]);
        let objectCoordinates: Coordinates;
        this.selectedObjects.forEach( (value) => {
            objectCoordinates = this.objectBoundaryCoordinates(value);
            this.boundaryDimensions.coordinates.x = Math.min(objectCoordinates.x, this.boundaryDimensions.coordinates.x);
            this.boundaryDimensions.coordinates.y = Math.min(objectCoordinates.y, this.boundaryDimensions.coordinates.y);
        });
    }

    private findNewBoundingMaxCoordinates(): void {
        this.boundaryMaxCoordinates = this.objectMaxCoordinates(this.selectedObjects[0]);
        let objectMaxCoordinates: Coordinates;
        this.selectedObjects.forEach( (value) => {
            objectMaxCoordinates = this.objectMaxCoordinates(value);
            this.boundaryMaxCoordinates.x = Math.max(objectMaxCoordinates.x, this.boundaryMaxCoordinates.x);
            this.boundaryMaxCoordinates.y = Math.max(objectMaxCoordinates.y, this.boundaryMaxCoordinates.y);
        });
    }

    private calculateBoundaryCoordinates(svgElement: SVGElement, addedObject: boolean): void {
        let objectBoundingCoordinates = this.objectBoundaryCoordinates(svgElement);
        const isOnMinimumCoordinatesX = (this.boundaryDimensions.coordinates.x === objectBoundingCoordinates.x);
        const isOnMinimumCoordinatesY = (this.boundaryDimensions.coordinates.y === objectBoundingCoordinates.y);
        if (this.selectedObjects.length >= 2) {
            if (addedObject) {
                this.boundaryDimensions.coordinates.x = Math.min(objectBoundingCoordinates.x, this.boundaryDimensions.coordinates.x);
                this.boundaryDimensions.coordinates.y = Math.min(objectBoundingCoordinates.y , this.boundaryDimensions.coordinates.y);
            } else if (isOnMinimumCoordinatesX || isOnMinimumCoordinatesY) {
                this.findNewBoundingCoordinates();
            }
            return;
        }
        if (!addedObject) {
            objectBoundingCoordinates = this.objectBoundaryCoordinates(this.selectedObjects[0]);
        }
        this.boundaryDimensions.coordinates.x = objectBoundingCoordinates.x;
        this.boundaryDimensions.coordinates.y = objectBoundingCoordinates.y;
    }

    private calculateBoundarySizes(svgElement: SVGElement, addedObject: boolean): void {
        let objectMaxCoordinates = this.objectMaxCoordinates(svgElement);
        const isOnMaximumCoordinatesX = (this.boundaryMaxCoordinates.x === objectMaxCoordinates.x);
        const isOnMaximumCoordinatesY = (this.boundaryMaxCoordinates.y === objectMaxCoordinates.y);
        if (this.selectedObjects.length >= 2) {
            if (addedObject) {
                this.boundaryMaxCoordinates.x = Math.max(objectMaxCoordinates.x, this.boundaryMaxCoordinates.x);
                this.boundaryMaxCoordinates.y = Math.max(objectMaxCoordinates.y, this.boundaryMaxCoordinates.y);
            } else if (isOnMaximumCoordinatesX || isOnMaximumCoordinatesY) {
                this.findNewBoundingMaxCoordinates();
            }
        } else {
            if (!addedObject) {
                objectMaxCoordinates = this.objectMaxCoordinates(this.selectedObjects[0]);
            }
            this.boundaryMaxCoordinates.x = objectMaxCoordinates.x;
            this.boundaryMaxCoordinates.y = objectMaxCoordinates.y;
        }
        this.boundaryDimensions.width = this.boundaryMaxCoordinates.x - this.boundaryDimensions.coordinates.x;
        this.boundaryDimensions.height = this.boundaryMaxCoordinates.y - this.boundaryDimensions.coordinates.y;
    }

    getRectangleDimensions(svgElement: SVGElement, addedObject: boolean, shouldRecalculate: boolean): RectangleDimensions {
        if (shouldRecalculate) {
            this.findNewBoundingCoordinates();
            this.findNewBoundingMaxCoordinates();
            this.boundaryDimensions.width = this.boundaryMaxCoordinates.x - this.boundaryDimensions.coordinates.x;
            this.boundaryDimensions.height = this.boundaryMaxCoordinates.y - this.boundaryDimensions.coordinates.y;
        } else {
            this.calculateBoundaryCoordinates(svgElement, addedObject);
            this.calculateBoundarySizes(svgElement, addedObject);
        }
        return {
            coordinates: this.boundaryDimensions.coordinates,
            width: this.boundaryDimensions.width,
            height: this.boundaryDimensions.height
        };
    }

    calculateRectangleCoordinates(key: string): Coordinates {
        const rectangleCoordinates: Coordinates = {x: -1, y: -1};
        switch (key) {
            case 'top' :
                rectangleCoordinates.x = this.boundaryDimensions.coordinates.x + this.boundaryDimensions.width / 2 - CONTROL_POINT_SIZE;
                rectangleCoordinates.y = this.boundaryDimensions.coordinates.y - CONTROL_POINT_SIZE;
                break;
            case 'bottom' :
                rectangleCoordinates.x = this.boundaryDimensions.coordinates.x + this.boundaryDimensions.width / 2 - CONTROL_POINT_SIZE;
                rectangleCoordinates.y = this.boundaryDimensions.coordinates.y + this.boundaryDimensions.height - CONTROL_POINT_SIZE;
                break;
            case 'left' :
                rectangleCoordinates.x = this.boundaryDimensions.coordinates.x - CONTROL_POINT_SIZE;
                rectangleCoordinates.y = this.boundaryDimensions.coordinates.y + this.boundaryDimensions.height / 2 - CONTROL_POINT_SIZE;
                break;
            case 'right' :
                rectangleCoordinates.x = this.boundaryDimensions.coordinates.x + this.boundaryDimensions.width - CONTROL_POINT_SIZE;
                rectangleCoordinates.y = this.boundaryDimensions.coordinates.y + this.boundaryDimensions.height / 2 - CONTROL_POINT_SIZE;
                break;
            case 'boundary' :
                rectangleCoordinates.x = this.boundaryDimensions.coordinates.x;
                rectangleCoordinates.y = this.boundaryDimensions.coordinates.y;
                break;
        }
        return rectangleCoordinates;
    }

    private valueInside(value: number, minimum: number, maximum: number): boolean {
        return (value >= minimum && value <= maximum);
    }

    private objectCornersInSelection(
        objectMinCoords: Coordinates,
        objectMaxCoords: Coordinates,
        minCoords: Coordinates,
        maxCoords: Coordinates
    ): boolean {
        const objectMinXInSelection = this.valueInside(objectMinCoords.x, minCoords.x, maxCoords.x);
        const objectMinYInSelection = this.valueInside(objectMinCoords.y, minCoords.y, maxCoords.y);
        const objectMaxXInSelection = this.valueInside(objectMaxCoords.x, minCoords.x, maxCoords.x);
        const objectMaxYInSelection = this.valueInside(objectMaxCoords.y, minCoords.y, maxCoords.y);
        const topLeftInSelection = objectMinXInSelection && objectMinYInSelection;
        const topRightInSelection = objectMaxXInSelection && objectMinYInSelection;
        const bottomLeftInSelection = objectMinXInSelection && objectMaxYInSelection;
        const bottomRightInSelection = objectMaxXInSelection && objectMaxYInSelection;
        return (topLeftInSelection || topRightInSelection || bottomLeftInSelection || bottomRightInSelection);
    }

    private selectionOverObject(
        objectMinCoords: Coordinates,
        objectMaxCoords: Coordinates,
        minCoords: Coordinates,
        maxCoords: Coordinates
    ): boolean {
        const selectionMinXInObject = this.valueInside(minCoords.x, objectMinCoords.x, objectMaxCoords.x);
        const selectionMinYInObject = this.valueInside(minCoords.y, objectMinCoords.y, objectMaxCoords.y);
        const yCoordsInBoundary = (maxCoords.y > objectMinCoords.y && minCoords.y < objectMaxCoords.y);
        const xCoordsInBoundary = (maxCoords.x > objectMinCoords.x && minCoords.x < objectMaxCoords.x);
        const overlapY = selectionMinXInObject && yCoordsInBoundary;
        const overlapX = selectionMinYInObject && xCoordsInBoundary;
        return (overlapY || overlapX);
    }

    isObjectInSelection(svgElement: SVGElement, minCoords: Coordinates, maxCoords: Coordinates): boolean {
        const objectMinCoords = this.objectBoundaryCoordinates(svgElement);
        const objectMaxCoords = this.objectMaxCoordinates(svgElement);
        const objCornerInSelect = this.objectCornersInSelection(objectMinCoords, objectMaxCoords, minCoords, maxCoords);
        const selectionOverObj = this.selectionOverObject(objectMinCoords, objectMaxCoords, minCoords, maxCoords);
        return (objCornerInSelect || selectionOverObj);
    }

    computeCenters(): void {
        this.getRectangleDimensions(this.selectedObjects[0], false, true);
        this.centerOfSelection.x = this.boundaryDimensions.coordinates.x + this.boundaryDimensions.width / 2;
        this.centerOfSelection.y = this.boundaryDimensions.coordinates.y + this.boundaryDimensions.height / 2;
        this.centersOfSelectedObjects = new Array<Coordinates>();
        this.selectedObjects.forEach((value) => {
            const centerCoords: Coordinates = { x: 0, y: 0 };
            const topLeftCoords = this.objectBoundaryCoordinates(value);
            const bottomRightCoords = this.objectMaxCoordinates(value);
            centerCoords.x = (topLeftCoords.x + bottomRightCoords.x) / 2;
            centerCoords.y = (topLeftCoords.y + bottomRightCoords.y) / 2;
            this.centersOfSelectedObjects.push(centerCoords);
        });
    }
}
