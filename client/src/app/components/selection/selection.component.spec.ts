import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material';
import { NavigationArrows } from 'src/app/interfaces-enums/navigation-arrows';
import { TempElement } from 'src/app/interfaces-enums/temp-element';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PrevisualisationRectangleService } from 'src/app/services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SelectionCalculatorService } from 'src/app/services/selection-services/selection-calculator/selection-calculator.service';
import { SelectionMoveService } from 'src/app/services/selection-services/selection-move/selection-move.service';
import { SelectionRotateService } from 'src/app/services/selection-services/selection-rotate/selection-rotate.service';
import { SelectionSvgHandlerService } from 'src/app/services/selection-services/selection-svg-handler/selection-svg-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { UndoRedoService } from 'src/app/services/undo-redo/undo-redo.service';
import { SelectionComponent } from './selection.component';

// tslint:disable: max-file-line-count no-magic-numbers no-string-literal

describe('SelectionComponent', () => {
    let component: SelectionComponent;
    let fixture: ComponentFixture<SelectionComponent>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerSpy: jasmine.SpyObj<MouseHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let selectionSvgHandlerSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let selectionCalculatorSpy: jasmine.SpyObj<SelectionCalculatorService>;
    let selectionMoveSpy: jasmine.SpyObj<SelectionMoveService>;
    let previsualizationRectangleSpy: jasmine.SpyObj<PrevisualisationRectangleService>;
    const fakePrevisualizationRectangle = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeRectangle = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    let selectionRotateSpy: jasmine.SpyObj<SelectionRotateService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;

    const mockDialogRef = {
        close: jasmine.createSpy('close'),
        open: jasmine.createSpy('open')
    };

    beforeEach(() => {
        fakePrevisualizationRectangle.setAttribute('x', '100');
        fakePrevisualizationRectangle.setAttribute('y', '100');
        fakePrevisualizationRectangle.setAttribute('width', '600');
        fakePrevisualizationRectangle.setAttribute('height', '600');
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        mouseHandlerSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        selectionSvgHandlerSpy = jasmine.createSpyObj(
            'SelectionSvgHandlerService',
            [
                'selectAll',
                'addNewSelectionRectangle',
                'listenToBoundaryRectangles',
                'inSelectionTool',
                'unselectAll',
                'findAllObjectsInLimits',
                'toggleObjectsInLimits',
                'toggledItems',
                'changeRectangleStyles',
                'removeObjectFromCanvas',
                'mouseInSelection',
                'addToUndoRedo',
                'addToTempUndo',
                'clickedOnElement',
                'ngOnInit',
                'selectObject'
            ]
        );
        selectionCalculatorSpy = jasmine.createSpyObj(
            'SelectionCalculatorService',
            [
                'isObjectInSelection',
                'selectedObjects',
                'getRectangleDimensions',
                'calculateRectangleCoordinates',
                'computeCenters'
            ]
        );
        selectionMoveSpy = jasmine.createSpyObj(
            'SelectionMoveService',
            [
                'moveWithKeys',
                'resetMouseCoordinates',
                'moveWithMouse',
                'keyPressed',
                'keyboardDelay',
                'destroyInterval'
            ]
        );
        previsualizationRectangleSpy = jasmine.createSpyObj(
            'PrevisualizationRectangleService',
            [
                'createPrevisualizationRectangle',
                'deletePrevisualization'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'addElementBeforeTransform',
                'transformCurrent',
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        selectionRotateSpy = jasmine.createSpyObj(
            'SelectionRotateService',
            [
                'wheelMoved'
            ]
        );
        undoRedoSpy.current = new Array<TempElement>();
        previsualizationRectangleSpy.createPrevisualizationRectangle.and.returnValue(fakePrevisualizationRectangle);
        selectionSvgHandlerSpy.clickedOnElement = false;
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MatDialogModule, HttpClientModule ],
            declarations: [ SelectionComponent ],
            providers: [
                {provide: SelectionMoveService, useValue: selectionMoveSpy},
                {provide: SelectionCalculatorService, useValue: selectionCalculatorSpy},
                {provide: MatDialogRef, useValue: mockDialogRef},
                {provide: SvgHandlerService, useValue: svgHandlerSpy},
                {provide: MouseHandlerService, useValue: mouseHandlerSpy},
                {provide: PrevisualisationRectangleService, useValue: previsualizationRectangleSpy},
                {provide: SelectionSvgHandlerService, useValue: selectionSvgHandlerSpy},
                {provide: SelectionRotateService, useValue: selectionRotateSpy},
                {provide: UndoRedoService, useValue: undoRedoSpy},
                {provide: SaveManagerService, useValue: saveManagerSpy}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelectionComponent);
        component = fixture.componentInstance;
        component['svgElement'] = fakeRectangle;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create and add all listeners', () => {
        expect(component).toBeTruthy();
        expect(svgHandlerSpy.addListeners).toHaveBeenCalledTimes(1);
    });

    it('should add all selection rectangles and listen to the boundary rectangles', () => {
        expect(selectionSvgHandlerSpy.addNewSelectionRectangle).toHaveBeenCalledTimes(5);
        expect(selectionSvgHandlerSpy.listenToBoundaryRectangles).toHaveBeenCalledTimes(1);
    });

    it('should destroy and unselect all elements', () => {
        component['svgHandler'].elements = new Array(fakeRectangle);
        component.ngOnDestroy();
        expect(svgHandlerSpy.deleteListeners).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(1);
    });

    it('should select all when ctrl+a is called', () => {
        const keyboardEvent = new KeyboardEvent('keydown', {ctrlKey: true, key: 'a'});
        component.handleKeyDown(keyboardEvent);
        expect(selectionSvgHandlerSpy.selectAll).toHaveBeenCalledTimes(1);
    });

    it('should not select all when ctrl+b is called', () => {
        const keyboardEvent = new KeyboardEvent('keydown', {ctrlKey: true, key: 'b'});
        component.handleKeyDown(keyboardEvent);
        expect(selectionSvgHandlerSpy.selectAll).toHaveBeenCalledTimes(0);
    });

    it('should select all object in selection rectangle', () => {
        const mouseEvent = new MouseEvent('mousemove');
        component['mouseHandler'].isMouseDown = true;
        component['buttonUsed'] = 0;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(1);
        expect(previsualizationRectangleSpy.createPrevisualizationRectangle).toHaveBeenCalledTimes(1);
        expect(component['previsualisationRectangleValues'].minCoords.x).toBe(100);
        expect(component['previsualisationRectangleValues'].minCoords.y).toBe(100);
        expect(component['previsualisationRectangleValues'].maxCoords.x).toBe(700);
        expect(component['previsualisationRectangleValues'].maxCoords.y).toBe(700);
        expect(selectionSvgHandlerSpy.findAllObjectsInLimits).toHaveBeenCalledTimes(1);
    });

    it('should move selection with mouse', () => {
        const mouseEvent = new MouseEvent('mousemove');
        selectionSvgHandlerSpy.clickedOnElement = true;
        component['clickedInsideSelection'] = true;
        component['mouseHandler'].isMouseDown = true;
        component['buttonUsed'] = 0;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(selectionMoveSpy.moveWithMouse).toHaveBeenCalled();
    });

    it('should toggle all object in selection rectangle', () => {
        const mouseEvent = new MouseEvent('mousemove');
        component['mouseHandler'].isMouseDown = true;
        component['buttonUsed'] = 2;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(1);
        expect(previsualizationRectangleSpy.createPrevisualizationRectangle).toHaveBeenCalledTimes(1);
        expect(component['previsualisationRectangleValues'].minCoords.x).toBe(100);
        expect(component['previsualisationRectangleValues'].minCoords.y).toBe(100);
        expect(component['previsualisationRectangleValues'].maxCoords.x).toBe(700);
        expect(component['previsualisationRectangleValues'].maxCoords.y).toBe(700);
        expect(selectionSvgHandlerSpy.toggleObjectsInLimits).toHaveBeenCalledTimes(1);
    });

    it('should not toggle or select all object in selection rectangle if mouse is not down', () => {
        const mouseEvent = new MouseEvent('mousemove');
        component['mouseHandler'].isMouseDown = false;
        component['buttonUsed'] = 2;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(1);
        expect(previsualizationRectangleSpy.createPrevisualizationRectangle).toHaveBeenCalledTimes(0);
        expect(selectionSvgHandlerSpy.toggleObjectsInLimits).toHaveBeenCalledTimes(0);
        expect(selectionSvgHandlerSpy.findAllObjectsInLimits).toHaveBeenCalledTimes(0);
    });

    it('should not toggle or select all object in selection rectangle if mouse button is 1', () => {
        const mouseEvent = new MouseEvent('mousemove');
        component['mouseHandler'].isMouseDown = true;
        component['buttonUsed'] = 1;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(1);
        expect(previsualizationRectangleSpy.createPrevisualizationRectangle).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.toggleObjectsInLimits).toHaveBeenCalledTimes(0);
        expect(selectionSvgHandlerSpy.findAllObjectsInLimits).toHaveBeenCalledTimes(0);
    });

    it('should delete previsualizationRectangle', () => {
        selectionSvgHandlerSpy.mouseInSelection.and.returnValue(true);
        const mouseEvent = new MouseEvent('mouseup', {button: 0});
        component['mouseMoved'] = false;
        selectionSvgHandlerSpy.clickedOnElement = false;
        component['eventListeners'].mouseUp(mouseEvent);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(1);
        expect(component['isDrawing']).toBeFalsy();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(0);
    });

    it('should call addToUndoRedo when mouse up', () => {
        selectionSvgHandlerSpy.mouseInSelection.and.returnValue(true);
        const mouseEvent = new MouseEvent('mouseup', {button: 0});
        component['selectionMoved'] = true;
        component['selectionSvgHandler'].clickedOnElement = true;
        selectionSvgHandlerSpy.selectObject(fakeRectangle);
        component['eventListeners'].mouseUp(mouseEvent);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(1);
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(1);
        expect(component['selectionMoved']).toBeFalsy();
    });

    it('should delete previsualizationRectangle and remove it from canvas if eventListeners.mouseLeave is called', () => {
        const mouseEvent = new MouseEvent('mouseleave');
        component['eventListeners'].mouseLeave(mouseEvent);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(1);
        expect(component['isDrawing']).toBeFalsy();
    });

    it('should unselect all objects', () => {
        component['svgHandler'].elements = new Array(fakeRectangle);
        component.ngOnDestroy();
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(1);
    });

    it('should do nothing if mouse goes down with button 1', () => {
        const mouseEvent = new MouseEvent('mousedown', {button: 1});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(0);
    });

    it('should update mouse down and unselect all if mouse goes down with button 0', () => {
        const mouseEvent = new MouseEvent('mousedown', {button: 0});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(1);
    });

    it('should update mouse down and unselect all if mouse goes down with button 2', () => {
        const mouseEvent = new MouseEvent('mousedown', {button: 2});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(0);
    });

    it('should only set isMouseDown to true if already clicked on element', () => {
        const mouseEvent = new MouseEvent('mousedown', {button: 2});
        component['selectionCalculator'].selectedObjects = new Array(fakeRectangle);
        component['selectionSvgHandler'].clickedOnElement = true;
        component['eventListeners'].mouseDown(mouseEvent);
        expect(mouseHandlerSpy.isMouseDown).toBeTruthy();
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledWith(component['selectionCalculator'].selectedObjects);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(0);
    });

    it('should only update mouse down if eventListeners.mouseDown is called with button 0', () => {
        selectionSvgHandlerSpy.mouseInSelection.and.returnValue(true);
        component['selectionCalculator'].selectedObjects = new Array(fakeRectangle);
        const mouseEvent = new MouseEvent('mousedown', {button: 0});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(component['clickedInsideSelection']).toBeTruthy();
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledWith(component['selectionCalculator'].selectedObjects);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(0);
    });

    it('should toggle all object in selection rectangle when eventListeners.mouseMove is called', () => {
        const mouseEvent = new MouseEvent('mousemove');
        component['mouseHandler'].isMouseDown = true;
        component['buttonUsed'] = 2;
        component['eventListeners'].mouseMove(mouseEvent);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(1);
        expect(previsualizationRectangleSpy.createPrevisualizationRectangle).toHaveBeenCalledTimes(1);
        expect(component['previsualisationRectangleValues'].minCoords.x).toBe(100);
        expect(component['previsualisationRectangleValues'].minCoords.y).toBe(100);
        expect(component['previsualisationRectangleValues'].maxCoords.x).toBe(700);
        expect(component['previsualisationRectangleValues'].maxCoords.y).toBe(700);
        expect(selectionSvgHandlerSpy.toggleObjectsInLimits).toHaveBeenCalledTimes(1);
    });

    it('should update mouse down and unselect all if eventListeners.mouseDown is called with button 0', () => {
        const mouseEvent = new MouseEvent('mousedown', {button: 0});
        component['eventListeners'].mouseDown(mouseEvent);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(selectionSvgHandlerSpy.unselectAll).toHaveBeenCalledTimes(1);
    });

    it('should delete previsualizationRectangle and remove it from canvas if eventListeners.mouseUp is called', () => {
        const mouseEvent = new MouseEvent('mouseup');
        component['eventListeners'].mouseUp(mouseEvent);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(1);
        expect(component['isDrawing']).toBeFalsy();
    });

    it('should update mouse down and unselect all if if eventListeners.mouseUp is called', () => {
        const clickSpy = jasmine.createSpyObj('MouseEvent', ['preventDefault']);
        const result = component['eventListeners'].contextMenu(clickSpy);
        expect(clickSpy.preventDefault).toHaveBeenCalledTimes(1);
        expect(result).toBeFalsy();
    });

    it('should call move with keys when ArrowUp is pressed', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.up, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(NavigationArrows.up);
    });

    it('should call move with keys when ArrowUp is pressed and call addToTempUndo', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.up, false);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.right, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(0);
    });

    it('should call move with keys when ArrowUp is pressed and not call addToTempUndo', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.up, false);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.left, true);
        selectionMoveSpy['keysPressed'].set(NavigationArrows.right, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowUp' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(NavigationArrows.up);
    });

    it('should call move with keys when ArrowLeft is pressed', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.left, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowLeft' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(NavigationArrows.left);
    });

    it('should call move with keys when ArrowRight is pressed', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.right, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowRight' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(NavigationArrows.right);
    });

    it('should call move with keys when ArrowDown is pressed', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowDown' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledWith(NavigationArrows.down);
    });

    it('should call mouse wheel when wheel is moved', () => {
        const mockWheel = new WheelEvent('mousewheel');
        component['mouseWheel'](mockWheel);
        component['eventListeners'].mouseWheel(mockWheel);
        expect(selectionRotateSpy.wheelMoved).toHaveBeenCalledWith(false, false, false);
    });

    it('should show that shift is down and not alt', () => {
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { shiftKey: true });
        component.handleKeyDown(mockDownKey);
        expect(component['shiftDown']).toBeTruthy();
        expect(component['altDown']).toBeFalsy();
        expect(undoRedoSpy.addElementBeforeTransform).not.toHaveBeenCalledWith(component['selectionCalculator'].selectedObjects);
        expect(selectionMoveSpy['moveWithKeys']).toHaveBeenCalledTimes(0);
    });

    it('should show that alt is down and not shift', () => {
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { altKey: true });
        component.handleKeyDown(mockDownKey);
        expect(component['shiftDown']).toBeFalsy();
        expect(component['altDown']).toBeTruthy();
    });

    it('should not handle key down because selectedObjects.length is 0', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        component['selectionCalculator'].selectedObjects = new Array();
        const mockDownKey = new KeyboardEvent('keydown', { key: 'ArrowDown' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledTimes(0);
    });

    it('should show that shift is up and not alt', () => {
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { shiftKey: true });
        component.handleKeyUp(mockDownKey);
        expect(component['shiftDown']).toBeFalsy();
        expect(component['altDown']).toBeFalsy();
        expect(undoRedoSpy.addElementBeforeTransform).not.toHaveBeenCalledWith(component['selectionCalculator'].selectedObjects);
    });

    it('should show that alt is up and not shift', () => {
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { altKey: true });
        component.handleKeyUp(mockDownKey);
        expect(component['shiftDown']).toBeFalsy();
        expect(component['altDown']).toBeFalsy();
    });

    it('should call move with keys when ArrowLeft is lifted', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.left, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { key: 'ArrowLeft' });
        component.handleKeyUp(mockDownKey);
        expect(selectionMoveSpy.destroyInterval).toHaveBeenCalledWith(NavigationArrows.left);
    });

    it('should call move with keys when ArrowRight is lifted', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.right, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { key: 'ArrowRight' });
        component.handleKeyUp(mockDownKey);
        expect(selectionMoveSpy.destroyInterval).toHaveBeenCalledWith(NavigationArrows.right);
    });

    it('should call move with keys when ArrowDown is lifted', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { key: 'ArrowDown' });
        component.handleKeyUp(mockDownKey);
        expect(selectionMoveSpy.destroyInterval).toHaveBeenCalledWith(NavigationArrows.down);
    });

    it('should call move with keys when ArrowUp is lifted', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.up, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keyup', { key: 'ArrowUp' });
        component.handleKeyUp(mockDownKey);
        expect(selectionMoveSpy.destroyInterval).toHaveBeenCalledWith(NavigationArrows.up);
    });

    it('should not call move with keys because selectedObjects.length is 0', () => {
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.up, false);
        component['selectionCalculator'].selectedObjects = new Array();
        const mockDownKey = new KeyboardEvent('keyup', { key: 'ArrowUp' });
        component.handleKeyUp(mockDownKey);
        expect(selectionMoveSpy.destroyInterval).toHaveBeenCalledTimes(0);
    });

    it('should not move because dialog is open', () => {
        component['dialog'].openDialogs.length = 1;
        selectionMoveSpy['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveSpy['keysPressed'].set(NavigationArrows.down, false);
        component['selectionCalculator'].selectedObjects = new Array();
        component['selectionCalculator'].selectedObjects.push(fakePrevisualizationRectangle);
        const mockDownKey = new KeyboardEvent('keydown', { key: 'h' });
        component.handleKeyDown(mockDownKey);
        expect(selectionMoveSpy.keyboardDelay).toHaveBeenCalledTimes(0);
    });
});
