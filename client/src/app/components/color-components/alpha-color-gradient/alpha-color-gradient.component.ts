// Du code source externe a été consulté afin de réaliser ce composant
// Lukas Marx (2018) Creating a Color Picker Component with Angular [En ligne]
// Disponible : https://github.com/LukasMarx/angular-color-picker

import { AfterViewInit, Component, ElementRef, Inject, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';

const WIDTH = 20;
const HEIGHT = 200;

@Component({
    selector: 'app-alpha-color-gradient',
    templateUrl: './alpha-color-gradient.component.html',
    styleUrls: ['./alpha-color-gradient.component.scss']
})
export class AlphaColorGradientComponent implements OnChanges, AfterViewInit {
    @ViewChild('alphaColorGradient', { static: false }) allColorGradientSelectionCanvas: ElementRef;
    allGradientContext: CanvasRenderingContext2D;
    private gradientToBottom: CanvasGradient;
    @Input() shouldUpdateGradient: string;
    @Input() shouldUpdateForAlpha: string;

    constructor(@Inject(MAT_DIALOG_DATA) public colorChoice: ColorChoice,
                private colorManager: ColorManagerService) { }

    ngAfterViewInit(): void {
        this.createAlphaGradiantDisplayer();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.shouldUpdateGradient) {
            if (!changes.shouldUpdateGradient.isFirstChange()) {
                this.allGradientContext.clearRect(0, 0, WIDTH, HEIGHT);
                this.createAlphaGradiantDisplayer();
            }
        }
        if (changes.shouldUpdateForAlpha) {
            if (!changes.shouldUpdateForAlpha.isFirstChange()) {
                this.allGradientContext.clearRect(0, 0, WIDTH, HEIGHT);
                this.createAlphaGradiantDisplayer();
            }
        }
    }

    createAlphaGradiantDisplayer(): void {
        this.allGradientContext = this.allColorGradientSelectionCanvas.nativeElement.getContext('2d');
        this.gradientToBottom = this.allGradientContext.createLinearGradient(0, 0, 0, HEIGHT);
        this.gradientToBottom.addColorStop(0, this.colorManager.getColorStringWithCustomAlpha(this.colorChoice, true));
        this.gradientToBottom.addColorStop(1, this.colorManager.getColorStringWithCustomAlpha(this.colorChoice, false));
        this.allGradientContext.beginPath();
        this.allGradientContext.rect(0, 0, WIDTH, HEIGHT);
        this.allGradientContext.fillStyle = this.gradientToBottom;
        this.allGradientContext.fill();
        this.allGradientContext.closePath();
    }

    updateColorWithCoordinates(coordinates: Coordinates): void {
        const colorPixel = this.allGradientContext.getImageData(coordinates.x, coordinates.y, 1, 1).data;
        if (colorPixel) {
            this.colorManager.updateColorWithPixel(this.colorChoice, colorPixel);
        }
    }

    mouseDownFromGradient(event: MouseEvent): void {
        const coordinates: Coordinates = { x: event.offsetX, y: event.offsetY };
        this.updateColorWithCoordinates(coordinates);
    }
}
