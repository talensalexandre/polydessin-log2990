import { Component } from '@angular/core';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { PencilComponent } from '../pencil/pencil.component';

const DEFAULT_SIZE = 20;
const DEFAULT_FILTER = 'blur';

@Component({
    selector: 'app-brush',
    templateUrl: './brush.component.html',
    styleUrls: ['./brush.component.scss']
})
export class BrushComponent extends PencilComponent {
    selectedFilter: string;

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        protected drawingService: DrawingService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, drawingService, stateSelector, saveManager);
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
        this.selectedFilter = DEFAULT_FILTER;
    }

    protected mouseDownExtra(): void {
        this.drawingService.applyFilterToPolyLine(this.svgElement, this.selectedFilter);
    }
}
