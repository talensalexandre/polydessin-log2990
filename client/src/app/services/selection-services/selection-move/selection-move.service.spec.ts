import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Subscription } from 'rxjs';
import { NavigationArrows } from 'src/app/interfaces-enums/navigation-arrows';
import { MouseHandlerService } from '../../mouse-handler/mouse-handler.service';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { UndoRedoService } from '../../undo-redo/undo-redo.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';
import { SelectionSvgHandlerService } from '../selection-svg-handler/selection-svg-handler.service';
import { SelectionTransformService } from '../selection-transform/selection-transform.service';
import { SelectionMoveService } from './selection-move.service';

// tslint:disable:no-magic-numbers no-string-literal no-any max-file-line-count

describe('SelectionMoveService', () => {
    let selectionMoveService: SelectionMoveService;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const badSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const secondBadSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
    const fakeBoundingClientRectValues: DOMRect = new DOMRect(200, 200, 700, 700);
    const badBoundingClientRectValues: DOMRect = new DOMRect(-100, -100, 600, 600);
    const secondBadBoundingClientRectValues: DOMRect = new DOMRect(-100, 100, 600, 600);
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let selectionCalculatorSpy: jasmine.SpyObj<SelectionCalculatorService>;
    let selectionSvgHandlerSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let mouseHandlerSpy: jasmine.SpyObj<MouseHandlerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    const fakeCanvasRectValues: DOMRect = new DOMRect(100, 0, 1000, 1000);
    let canvasSpy: jasmine.SpyObj<SVGElement>;
    let selectionTransformSpy: jasmine.SpyObj<SelectionTransformService>;
    let fakeRegArray: RegExpExecArray;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;

    beforeEach(() => {
        fakeSvgElement.setAttribute('size', '10');
        fakeSvgElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        fakeRegArray = /matrix\(\s*([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)/
            .exec('matrix(1,0,0,1,0,0)') as RegExpExecArray;
        spyOn(fakeSvgElement, 'getBoundingClientRect').and.returnValue(fakeBoundingClientRectValues);
        spyOn(badSvgElement, 'getBoundingClientRect').and.returnValue(badBoundingClientRectValues);
        spyOn(secondBadSvgElement, 'getBoundingClientRect').and.returnValue(secondBadBoundingClientRectValues);
    });

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            { provide: SvgHandlerService, useValue: svgHandlerSpy },
            { provide: SelectionSvgHandlerService, useValue: selectionSvgHandlerSpy },
            { provide: SelectionCalculatorService, useValue: selectionCalculatorSpy },
            { provide: Renderer2, useValue: rendererSpy },
            { provide: MouseHandlerService, useValue: mouseHandlerSpy },
            { provide: SVGElement, useValue: canvasSpy },
            { provide: SelectionTransformService, useValue: selectionTransformSpy },
            { provide: UndoRedoService, useValue: undoRedoSpy }
        ]
    }));

    beforeEach(() => {
        selectionSvgHandlerSpy = jasmine.createSpyObj(
            'SelectionSvgHandlerService',
            [
                'selectAll',
                'addNewSelectionRectangle',
                'listenToBoundaryRectangles',
                'inSelectionTool',
                'unselectAll',
                'findAllObjectsInLimits',
                'toggleObjectsInLimits',
                'toggledItems',
                'changeRectangleStyles',
                'removeObjectFromCanvas',
                'mouseInSelection',
                'clickedOnElement'
            ]
        );
        selectionCalculatorSpy = jasmine.createSpyObj(
            'SelectionCalculatorService',
            [
                'isObjectInSelection',
                'selectedObjects',
                'getRectangleDimensions',
                'calculateRectangleCoordinates',
                'computeCenters'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer',
                'setAttribute'
            ]
        );
        mouseHandlerSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        canvasSpy = jasmine.createSpyObj(
            'SVGElement',
            [
                'getBoundingClientRect'
            ]
        );
        selectionTransformSpy = jasmine.createSpyObj(
            'SelectionTransformService',
            [
                'appendTransformationToElements',
                'getOldMatrix'
            ]
        );
        selectionTransformSpy.getOldMatrix.and.returnValue(fakeRegArray);
        undoRedoSpy = jasmine.createSpyObj(
            'UndoRedoService',
            [
                'addElementBeforeTransform',
                'transformCurrent'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        canvasSpy.getBoundingClientRect.and.returnValue(fakeCanvasRectValues);
        svgHandlerSpy.svgCanvas = canvasSpy;
        svgHandlerSpy.renderer = rendererSpy;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionMoveService = new SelectionMoveService(selectionCalculatorSpy, selectionSvgHandlerSpy,
            mouseHandlerSpy, selectionTransformSpy, svgHandlerSpy, undoRedoSpy, saveManagerSpy);
    });

    it('should be created', () => {
        expect(selectionMoveService).toBeTruthy();
    });

    it('should move with key up', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.up);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 0, -3]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move with key down', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.down);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 0, 3]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move with key left', () => {
        svgHandlerSpy.canvasHeight = 2000;
        svgHandlerSpy.canvasWidth = 2000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.left);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, -3, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move with key right', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.right);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 3, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move with mouse with start coords', () => {
        mouseHandlerSpy.startCoordinates = { x: 100, y: 100 };
        mouseHandlerSpy.actualCoordinates = { x: 100, y: 100 };
        mouseHandlerSpy.endCoordinates = { x: 100, y: 100 };
        selectionMoveService['oldCoordinates'].x = -1;
        selectionMoveService['oldCoordinates'].y = -1;
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService.moveWithMouse();
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 0, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move with mouse with other coords', () => {
        mouseHandlerSpy.actualCoordinates = { x: 100, y: 100 };
        selectionMoveService['oldCoordinates'].x = 20;
        selectionMoveService['oldCoordinates'].y = 30;
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService.moveWithMouse();
        expect(selectionMoveService).toBeTruthy();
    });

    it('should reset mouse coords', () => {
        selectionMoveService.resetMouseCoordinates();
        expect(selectionMoveService['oldCoordinates'].x).toBe(-1);
        expect(selectionMoveService['oldCoordinates'].y).toBe(-1);
    });

    it('should block movement to the top', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(badSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', badSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.up);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(badSvgElement, [1, 0, 0, 1, 0, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledTimes(1);
    });

    it('should block movement to the left', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(badSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', badSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.left);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(badSvgElement, [1, 0, 0, 1, 0, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledTimes(1);
    });

    it('should block movement to the right', () => {
        svgHandlerSpy.canvasHeight = -1000;
        svgHandlerSpy.canvasWidth = -10000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(badSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', badSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.right);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(badSvgElement, [1, 0, 0, 1, 0, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledTimes(1);
    });

    it('should block movement to the down', () => {
        svgHandlerSpy.canvasHeight = -1000;
        svgHandlerSpy.canvasWidth = -10000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(secondBadSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', secondBadSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.down);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(secondBadSvgElement, [1, 0, 0, 1, 0, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledTimes(1);
    });

    it('should move with key right and rewrite translate', () => {
        fakeSvgElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('boundary', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.right);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 3, 0]);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
    });

    it('should move right but using initial boundary coords', () => {
        svgHandlerSpy.canvasHeight = 1000;
        svgHandlerSpy.canvasWidth = 1000;
        selectionCalculatorSpy.selectedObjects = new Array<SVGElement>();
        selectionCalculatorSpy.selectedObjects.push(fakeSvgElement);
        selectionSvgHandlerSpy.selectionRectangles = new Map<string, SVGElement>();
        selectionSvgHandlerSpy.selectionRectangles.set('not working', fakeSvgElement);
        selectionMoveService['moveWithKeys'](NavigationArrows.right);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeSvgElement);
        expect(selectionTransformSpy.appendTransformationToElements).toHaveBeenCalledWith(fakeSvgElement, [1, 0, 0, 1, 3, 0]);
    });

    it('should stop interval when key "up" is not pressed and all other keys are not pressed', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        const subscription = jasmine.createSpyObj('Subscription', [ 'unsubscribe' ]);
        selectionMoveService['keysUnsubscribe'] = new Map<NavigationArrows, Subscription>();
        selectionMoveService['keysUnsubscribe'].set(NavigationArrows.up, subscription);
        selectionMoveService.destroyInterval(NavigationArrows.up);
        expect(subscription.unsubscribe).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalled();
    });

    it('should stop interval when key "up" is not pressed and checkKeyUnsubscribe is undefined', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        selectionMoveService.destroyInterval(NavigationArrows.up);
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalled();
    });

    it('should stop interval when key "up" is not pressed and "down" is pressed', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        const subscription = jasmine.createSpyObj('Subscription', [ 'unsubscribe' ]);
        selectionMoveService['keysUnsubscribe'] = new Map<NavigationArrows, Subscription>();
        selectionMoveService['keysUnsubscribe'].set(NavigationArrows.up, subscription);
        selectionMoveService.destroyInterval(NavigationArrows.up);
        expect(subscription.unsubscribe).toHaveBeenCalled();
        expect(undoRedoSpy.transformCurrent).toHaveBeenCalledTimes(0);
    });

    it('should do nothing when key "up" is pressed but is already true', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, true);
        const spy = spyOn<any>(selectionMoveService, 'moveWithKeys');
        selectionMoveService.keyboardDelay(NavigationArrows.up);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should start interval when key "up" is pressed and others not pressed', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        selectionMoveService['keysUnsubscribe'] = new Map<NavigationArrows, Subscription>();
        const spy = spyOn<any>(selectionMoveService, 'moveWithKeys');
        const spyKeysUnsubscribeSet = spyOn<any>(selectionMoveService['keysUnsubscribe'], 'set');
        selectionMoveService.keyboardDelay(NavigationArrows.up);
        expect(undoRedoSpy.addElementBeforeTransform).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalled();
        expect(spyKeysUnsubscribeSet).toHaveBeenCalled();
    });

    it('should start interval when key "up" is pressed and "down" is pressed', () => {
        selectionMoveService['keysPressed'] = new Map<NavigationArrows, boolean>();
        selectionMoveService['keysUnsubscribe'] = new Map<NavigationArrows, Subscription>();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        const spy = spyOn<any>(selectionMoveService, 'moveWithKeys');
        selectionMoveService.keyboardDelay(NavigationArrows.up);
        expect(undoRedoSpy.addElementBeforeTransform).not.toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalled();
    });

    it('should catch error because of an undefined from unselection', () => {
        selectionCalculatorSpy.computeCenters.and.throwError('error');
        spyOn<any>(selectionMoveService, 'destroyInterval').and.callThrough();
        selectionMoveService['keysPressed'].set(NavigationArrows.up, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.down, true);
        selectionMoveService['keysPressed'].set(NavigationArrows.left, false);
        selectionMoveService['keysPressed'].set(NavigationArrows.right, false);
        selectionMoveService['checkLimitsAndTranslate']({x: 3, y: 3});
        expect(selectionMoveService.destroyInterval).toHaveBeenCalledTimes(4);
    });
});
