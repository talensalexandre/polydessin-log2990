import {ScrollingModule} from '@angular/cdk/scrolling';
import { HttpClientModule } from '@angular/common/http';
import { NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule,
        MatListModule, MatOptionModule, MatProgressSpinnerModule, MatSelectModule, MatSidenavModule,
        MatSliderModule, MatSlideToggleModule, MatSnackBarModule, MatTooltipModule } from '@angular/material';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatChipsModule} from '@angular/material/chips';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './components/app/app.component';
import { BrushComponent } from './components/brush/brush.component';
import { AllColorGradientComponent } from './components/color-components/all-color-gradient/all-color-gradient.component';
import { AlphaColorGradientComponent } from './components/color-components/alpha-color-gradient/alpha-color-gradient.component';
import { ColorApplicatorComponent } from './components/color-components/color-applicator/color-applicator.component';
import { ColorPickerComponent } from './components/color-components/color-picker/color-picker.component';
import { ColorPopupComponent } from './components/color-components/color-popup/color-popup.component';
import { ColorComponent } from './components/color-components/color/color.component';
import { MainColorGradientComponent } from './components/color-components/main-color-gradient/main-color-gradient.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { CreateNewDrawingComponent } from './components/create-new-drawing/create-new-drawing.component';
import { DrawingGalleryComponent } from './components/drawing-gallery/drawing-gallery.component';
import { EllipseComponent } from './components/ellipse/ellipse.component';
import { EntryPointComponent } from './components/entry-point/entry-point.component';
import { EraserComponent } from './components/eraser/eraser.component';
import { ExportDrawingComponent } from './components/export-drawing/export-drawing.component';
import { GridComponent } from './components/grid/grid.component';
import { LineComponent } from './components/line/line.component';
import { PaintBucketComponent } from './components/paint-bucket/paint-bucket.component';
import { PencilComponent } from './components/pencil/pencil.component';
import { PolygonComponent } from './components/polygon/polygon.component';
import { RectangleComponent } from './components/rectangle/rectangle.component';
import { SaveManagerComponent } from './components/save-manager/save-manager.component';
import { SelectionComponent } from './components/selection/selection.component';
import { SprayComponent } from './components/spray/spray.component';
import { ColorDocComponent } from './components/user-guide/color-doc/color-doc.component';
import { DrawingDocComponent } from './components/user-guide/drawing-doc/drawing-doc.component';
import { FunctionalitiesDocComponent } from './components/user-guide/functionalities-doc/functionalities-doc.component';
import { SelectionDocComponent } from './components/user-guide/selection-doc/selection-doc.component';
import { ShapeDocComponent } from './components/user-guide/shape-doc/shape-doc.component';
import { UserGuideComponent } from './components/user-guide/user-guide.component';
import { CreateNewDrawingService } from './services/create-new-drawing/create-new-drawing.service';
import { PrevisualisationRectangleService } from './services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SvgHandlerService } from './services/svg-handler/svg-handler.service';
import { UserGuideService } from './services/user-guide/user-guide.service';

@NgModule({
    declarations:
        [
            AppComponent, ColorComponent, BrushComponent, ExportDrawingComponent,
            PencilComponent, RectangleComponent, LineComponent, SelectionComponent,
            EntryPointComponent, UserGuideComponent, CreateNewDrawingComponent, ColorPopupComponent, ColorPickerComponent,
            MainColorGradientComponent, AllColorGradientComponent, AlphaColorGradientComponent,
            ColorApplicatorComponent, SprayComponent, EraserComponent, GridComponent, EllipseComponent,
            ConfirmationDialogComponent, DrawingGalleryComponent, SaveManagerComponent, PolygonComponent,
            SelectionDocComponent, DrawingDocComponent, ShapeDocComponent, ColorDocComponent, FunctionalitiesDocComponent,
            PaintBucketComponent
        ],
    imports:
        [
            BrowserModule, HttpClientModule, MatSidenavModule,
            BrowserAnimationsModule, FormsModule, MatIconModule, MatListModule,
            MatButtonModule, MatSliderModule, MatFormFieldModule, MatInputModule,
            MatTooltipModule, MatDialogModule, MatSelectModule, MatOptionModule,
            MatButtonToggleModule, ReactiveFormsModule, MatSlideToggleModule,
            MatCardModule, MatChipsModule, ScrollingModule, MatProgressSpinnerModule,
            MatSnackBarModule
        ],
    entryComponents:
        [
            EntryPointComponent, CreateNewDrawingComponent, ColorPickerComponent,
            ExportDrawingComponent, ConfirmationDialogComponent, DrawingGalleryComponent,
            SaveManagerComponent, ColorPopupComponent
        ],
    providers:
        [
            UserGuideService, SvgHandlerService, CreateNewDrawingService, PrevisualisationRectangleService
        ],
    bootstrap: [AppComponent]
})
export class AppModule {}
