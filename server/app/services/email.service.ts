import * as axios from 'axios';
import * as dotenv from 'dotenv';
import * as FormData from 'form-data';
import { injectable } from 'inversify';
import 'reflect-metadata';
import { Message } from '../../../common/communication/message';

const TO = 1;
const NAME = 2;
const TYPE = 3;

@injectable()
export class EmailService {

    constructor() {
        dotenv.config();
    }

    homePage(): Message {
        return {
            title: 'Home Page',
            body: 'If this message appears, your request is invalid',
        };
    }

    async sendDrawing(data: string[]): Promise<void> {
        const form = new FormData();
        const formHeaders = form.getHeaders();
        const imageBuffer = Buffer.from(data[0], 'base64');
        const to = data[TO];
        const name = data[NAME];
        const type = data[TYPE];
        const fileName = name + '.' + type;
        let contentTypeConfig = 'image/' + type;
        if (type === 'svg') {
            contentTypeConfig += '+xml';
        }
        form.append('to', to);
        form.append('payload', imageBuffer,
            { contentType: contentTypeConfig,
            knownLength: imageBuffer.byteLength,
            filename: fileName});
        const options = {
            headers: {
                'X-Team-Key': process.env.X_TEAM_KEY,
                'content-type': 'multipart/form-data',
                ...formHeaders
            }
        };
        return axios.default.post(process.env.API_URL as string, form, options);
    }
}
