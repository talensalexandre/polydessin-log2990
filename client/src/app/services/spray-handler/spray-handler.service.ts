import { Injectable, Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const MAX_ANGLE = 360;

@Injectable({
    providedIn: 'root'
})
export class SprayHandlerService {
    private renderer: Renderer2;

    constructor(
        private svgHandler: SvgHandlerService,
        private colorManager: ColorManagerService,
        private clickerHandler: ClickerHandlerService,
        private undoRedo: UndoRedoService
    ) {
        this.renderer = this.svgHandler.renderer;
    }

    createSprayObject(): SVGElement {
        const actualSpray = this.renderer.createElement('svg:path', this.svgHandler.SVG_LINK);
        this.renderer.setAttribute(actualSpray, 'stroke', this.colorManager.colorSelected[ColorChoice.mainColor].inString);
        this.renderer.setAttribute(actualSpray, 'stroke-width', '2px');
        this.renderer.setAttribute(actualSpray, 'stroke-linecap', 'round');
        this.renderer.setAttribute(actualSpray, 'fill', 'none');
        this.renderer.setAttribute(actualSpray, 'd', '');
        this.renderer.setAttribute(actualSpray, 'typeElement', 'strokeElement');
        this.clickerHandler.addSelectionStyles(actualSpray);
        this.undoRedo.updateCurrent(actualSpray, ChangeType.addToCanvas);
        this.renderer.appendChild(this.svgHandler.svgCanvas, actualSpray);
        this.svgHandler.drawingOnCanvas = true;
        return actualSpray;
    }

    addRandomCircle(actualSpray: SVGElement, coordinates: Coordinates, diameter: number): void {
        const randomCoords = this.getRandomCoordinates(diameter);
        const coordString = String(randomCoords.x + coordinates.x) + ' ' + String(randomCoords.y + coordinates.y);
        let pathPoints = actualSpray.getAttribute('d');
        pathPoints = pathPoints + 'M ' + coordString + ' Z';
        this.renderer.setAttribute(actualSpray, 'd', pathPoints);
    }

    private getRandomCoordinates(diameter: number): Coordinates {
        const radiusMax = diameter / 2;
        const randomRadius = Math.round(((Math.random() * 2 - 1) * radiusMax));
        const randomAngle = Math.round(((Math.random() * 2 - 1) * MAX_ANGLE));
        const valueX = randomRadius * Math.cos(randomAngle);
        const valueY = randomRadius * Math.sin(randomAngle);
        return {x: valueX, y: valueY};
    }
}
