export enum FileType {
    jpeg = 'jpeg',
    png = 'png',
    svg = 'svg'
}
