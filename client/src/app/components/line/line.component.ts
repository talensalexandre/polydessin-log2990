import { Component, HostListener } from '@angular/core';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { LineHandlerService } from 'src/app/services/line-handler/line-handler.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const DEFAULT_SIZE = 5;
const MAXIMUM_SIZE = 200;
const MINIMUM_SIZE = 1;
const PIXELS_BEFORE_CONNECTION = 3;
const TWO_CLICKS = 2;

@Component({
    selector: 'app-line',
    templateUrl: './line.component.html',
    styleUrls: ['./line.component.scss']
})
export class LineComponent extends AbstractToolComponent {
    junctionSize: number;
    previsualizationLine: SVGElement;
    isNormalJunction: boolean;
    isShiftDown: boolean;
    clickCounter: number;

    constructor(
        protected mouseHandler: MouseHandlerService,
        protected svgHandler: SvgHandlerService,
        private lineService: LineHandlerService,
        private drawingService: DrawingService,
        public stateSelector: StateSelectorService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
    }

    protected initExtend(): void {
        this.eventListeners.mouseMove = ($event) => this.updatePrevisualization($event);
        this.eventListeners.mouseClick = ($event) => this.updateMouseClick($event);
        this.eventListeners.mouseDblClick = () => this.doubleClick();
        this.eventListeners.changedMouseClick = true;
        this.eventListeners.changedMouseDblClick = true;
        this.eventListeners.changedMouseMove = true;
        this.setDefaultSize();
        this.svgHandler.addListeners(this.eventListeners);
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
        this.junctionSize = DEFAULT_SIZE;
        this.isNormalJunction = true;
        this.isShiftDown = false;
        this.clickCounter = 0;
    }

    destroyExtend(): void {
        this.drawingService.removeElement(this.previsualizationLine);
        this.isDrawing = false;
    }

    @HostListener('document:keydown', ['$event']) onKeyDown(keyEvent: KeyboardEvent): void {
        if (keyEvent.key === 'Backspace' && this.clickCounter > 1) {
            this.clickCounter--;
            this.drawingService.deleteLastSegmentFromPolyline(this.svgElement);
            if (!this.isNormalJunction) {
                this.lineService.removeJunction();
            }
            this.mouseHandler.actualCoordinates = this.drawingService.getLastCoordinatesOfPolyline(this.svgElement);
            this.previsualizationLine = this.lineService.linePrevisualization(this.mouseHandler.actualCoordinates,
                this.mouseHandler.endCoordinates,
                String(this.size),
                this.previsualizationLine);
        }

        if (keyEvent.key === 'Escape' && this.clickCounter > 0) {
            this.clickCounter = 0;
            this.lineService.deleteLine(this.svgElement);
            this.mouseHandler.onMouseLeave();
            this.lineService.emptyChildren();
            this.drawingService.removeElement(this.previsualizationLine);
            delete this.previsualizationLine;
        }

        if (keyEvent.key === 'Shift') {
            this.isShiftDown = true;
            if (this.clickCounter > 0) {
                this.previsualizationLine = this.lineService.shiftPrevisualization(this.mouseHandler.actualCoordinates,
                    this.mouseHandler.endCoordinates,
                    String(this.size),
                    this.previsualizationLine);
            }
        }
    }

    @HostListener('document:keyup', ['$event']) onKeyUp(keyEvent: KeyboardEvent): void {
        if (keyEvent.key === 'Shift') {
            this.isShiftDown = false;
            if (this.clickCounter > 0) {
                this.previsualizationLine = this.lineService.linePrevisualization(this.mouseHandler.actualCoordinates,
                    this.mouseHandler.endCoordinates,
                    String(this.size),
                    this.previsualizationLine);
            }
        }
    }

    changeSize(): void {
        this.size = (this.size < MINIMUM_SIZE) ? MINIMUM_SIZE : this.size;
        this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        this.changeJunctionSize();
    }

    changeJunctionSize(): void {
        this.junctionSize = (this.junctionSize < this.size) ? this.size : this.junctionSize;
        this.junctionSize = (this.junctionSize > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.junctionSize;
        this.stateSelector.isModifyingInput = false;
    }

    updateMouseClick(event: MouseEvent): void {
        this.svgHandler.isLineBeingCreated = true;
        const coordinates: Coordinates = {
            x: event.offsetX,
            y: event.offsetY
        };
        this.isDrawing = false;
        this.mouseHandler.onMouseClick(coordinates, this.clickCounter);
        this.clickCounter++;
        if (this.clickCounter > 1) {
            if (this.isShiftDown) {
                const xPrevisualization = Number(this.previsualizationLine.getAttribute('x2'));
                const yPrevisualization = Number(this.previsualizationLine.getAttribute('y2'));
                const coords: Coordinates = { x: xPrevisualization, y: yPrevisualization };
                this.mouseHandler.updateEndCoords(coords);
            }
            this.drawingService.addCoordinatesToPolyline(this.svgElement, this.mouseHandler.endCoordinates);
            this.mouseHandler.makeActualCoordsEndCoords();
        } else {
            this.lineService.createGElement();
            this.svgElement = this.drawingService.createPolyLine(
                coordinates,
                String(this.size),
                false
            );
            this.lineService.addElementToG(this.svgElement, String(this.size));
        }
        if (!this.isNormalJunction) {
            this.lineService.createJunction(this.mouseHandler.actualCoordinates, String(this.junctionSize));
        }
        this.updatePrevisualization(event);
    }

    updatePrevisualization(event: MouseEvent): void {
        const coordinates: Coordinates = {
            x: event.offsetX,
            y: event.offsetY
        };
        this.mouseHandler.updateEndCoords(coordinates);
        if (this.clickCounter > 0 && !this.isShiftDown) {
            this.previsualizationLine = this.lineService.linePrevisualization(this.mouseHandler.actualCoordinates,
                coordinates,
                String(this.size),
                this.previsualizationLine);

        } else if (this.clickCounter > 0 && this.isShiftDown) {
            this.previsualizationLine = this.lineService.shiftPrevisualization(this.mouseHandler.actualCoordinates,
                coordinates,
                String(this.size),
                this.previsualizationLine);
        }
    }

    doubleClick(): void {
        this.svgHandler.isLineBeingCreated = false;
        this.drawingService.deleteLastSegmentFromPolyline(this.svgElement);
        if (this.clickCounter > TWO_CLICKS) {
            if (Math.abs(this.mouseHandler.endCoordinates.x - this.mouseHandler.startCoordinates.x) <= PIXELS_BEFORE_CONNECTION &&
                Math.abs(this.mouseHandler.endCoordinates.y - this.mouseHandler.startCoordinates.y) <= PIXELS_BEFORE_CONNECTION) {
                this.lineService.connectFirstAndLastPoint(this.mouseHandler.startCoordinates,
                    this.previsualizationLine,
                    this.svgElement,
                    this.isNormalJunction);
            }
        }
        this.mouseHandler.onMouseLeave();
        this.lineService.emptyChildren();
        this.clickCounter = 0;
        this.isDrawing = false;
        this.drawingService.removeElement(this.previsualizationLine);
        delete this.previsualizationLine;
        this.saveManager.addSvgElement(this.svgElement);
    }

    increaseJunctionSize(): void {
        this.junctionSize++;
        this.changeJunctionSize();
    }

    decreaseJunctionSize(): void {
        this.junctionSize--;
        this.changeJunctionSize();
    }

    protected mouseMouvementExtend(): void {
        return;
    }

    protected mouseDownExtend(): void {
        return;
    }

    protected mouseUpExtend(): void {
        return;
    }
}
