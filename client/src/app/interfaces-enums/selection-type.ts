export enum SelectionType {
    outline = 'outline',
    fill = 'fill',
    outlineAndFill = 'outlineAndFill'
}
