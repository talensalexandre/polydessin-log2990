
import { expect } from 'chai';
import * as supertest from 'supertest';
import { Message } from '../../../common/communication/message';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { EmailService } from '../services/email.service';
import Types from '../types';

// tslint:disable:no-any
const HTTP_NOT_FOUND = 404;
const SUCCESS = 201;
const OK = 200;

describe('EmailController', () => {
    const baseMessage = { title: 'Home page', body: 'If this message appears, your request is invalid' } as Message;
    let emailService: Stubbed<EmailService>;
    let app: Express.Application;

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.EmailService).toConstantValue({
            homePage: sandbox.stub().resolves(baseMessage),
            sendDrawing: sandbox.stub()
        });
        emailService = container.get(Types.EmailService);
        app = container.get<Application>(Types.Application).app;
    });

    it('should email service on valid get request to root', async () => {
        return supertest(app)
            .get('/log2990/projet2/email')
            .expect(OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(baseMessage);
            });
    });

    it('should return error', async () => {
        emailService.sendDrawing.rejects({ response: {
            status: 404
        }});
        return supertest(app)
            .post('/log2990/projet2/email/send')
            .expect(HTTP_NOT_FOUND);
    });

    it('should return success', async () => {
        emailService.sendDrawing.resolves({ response: {
            status: 201
        }});
        return supertest(app)
            .post('/log2990/projet2/email/send')
            .expect(SUCCESS);
    });

});
