import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MouseHandlerService } from '../mouse-handler/mouse-handler.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { PrevisualisationRectangleService } from './previsualisation-rectangle.service';

describe('PrevisualizationRectangleService', () => {
    let service: PrevisualisationRectangleService;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let rectangleHandlerSpy: jasmine.SpyObj<RectangleHandlerService>;
    let mouseHandlerSpy: jasmine.SpyObj<MouseHandlerService>;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'setStyle',
                'removeChild'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'renderer'
            ]
        );
        rectangleHandlerSpy = jasmine.createSpyObj(
            'RectangleHandlerService',
            [
                'calculateRectangle',
                'elementPrevisualization'
            ]
        );
        mouseHandlerSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'startCoordinates',
                'actualCoordinates'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new PrevisualisationRectangleService(rectangleHandlerSpy, mouseHandlerSpy, svgHandlerServiceSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create previsualisation rectangle', () => {
        service.createPrevisualizationRectangle();
        expect(rectangleHandlerSpy.calculateRectangle).toHaveBeenCalledWith(
            mouseHandlerSpy.startCoordinates,
            mouseHandlerSpy.actualCoordinates,
            2
        );
        expect(rectangleHandlerSpy.elementPrevisualization).toHaveBeenCalledWith(
            false, true, 2, 'rect', service.previsualizationRectangle
        );
        expect(rendererSpy.setStyle).toHaveBeenCalledWith(service.previsualizationRectangle, 'stroke', '#08428d');
        expect(rendererSpy.setStyle).toHaveBeenCalledWith(service.previsualizationRectangle, 'stroke-dasharray', '4 2');
    });

    it('should delete previsualisation rectangle', () => {
        service.deletePrevisualization();
        expect(service.previsualizationRectangle).toBeFalsy();
    });

    it('should delete previsualisation rectangle', () => {
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
        service.previsualizationRectangle = fakeElement;
        service.deletePrevisualization();
        expect(rendererSpy.removeChild).toHaveBeenCalled();
        expect(service.previsualizationRectangle).toBeFalsy();
    });
});
