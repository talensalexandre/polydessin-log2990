import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from '../../../environments/environment';
import { DrawingStored, MetaDrawing } from '../../interfaces-enums/drawing';
import { SaveManagerService } from './save-manager.service';

// tslint:disable: no-string-literal

const meta: MetaDrawing = {
    timestamp: 1,
    name: 'test',
    labels: ['test1', 'test2'],
    width: 200,
    height: 250,
    backgroundColor: 'rgba(0,0,0,0)'
};

const SEND_URL = environment.SEND_URL;
const GET_URL = environment.GET_URL;
const GET_ALL_URL = environment.GET_ALL_URL;
const DELETE_URL = environment.DELETE_URL;
const FAILED = 400;
const SUCCESS = 201;
const WIDTH = 200;
const HEIGHT = 250;

describe('SaveManagerService', () => {
    let service: SaveManagerService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, HttpClientTestingModule],
        providers: [SaveManagerService],
    }));

    beforeEach(() => {
        service = TestBed.get(SaveManagerService);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should post successfully', () => {
        service['svgContainer'] = ['svg1', 'svg2'];
        const mockResponsePost = SUCCESS;
        service.sendDrawing(meta).subscribe((response) => {
            expect(response).toBe(SUCCESS);
        });
        const request = httpTestingController.expectOne(SEND_URL);
        expect(request.request.method).toEqual('POST');
        request.flush(mockResponsePost);
        httpTestingController.verify();
    });

    it('should get drawing successfully', () => {
        const drawingStored: DrawingStored = {
            metaDrawing: meta,
            svgContainer: ['svg1', 'svg2']
        };
        service.getDrawing('1').subscribe((response) => {
            expect(response).toBe(drawingStored);
        });
        const request = httpTestingController.expectOne(GET_URL + '1');
        expect(request.request.method).toEqual('GET');
        request.flush(drawingStored);
        httpTestingController.verify();
    });

    it('should get all drawings successfully', () => {
        const drawingsArray: DrawingStored[] = new Array<DrawingStored>();
        const drawingStored: DrawingStored = {
            metaDrawing: meta,
            svgContainer: ['svg1', 'svg2']
        };
        drawingsArray.push(drawingStored);
        service.getDrawings().subscribe((response) => {
            expect(response).toBe(drawingsArray);
        });
        const request = httpTestingController.expectOne(GET_ALL_URL);
        expect(request.request.method).toEqual('GET');
        request.flush(drawingsArray);
        httpTestingController.verify();
    });

    it('should delete drawing successfully', () => {
        const drawingStored: DrawingStored = {
            metaDrawing: meta,
            svgContainer: ['svg1', 'svg2']
        };
        service.deleteDrawing('1').subscribe((response) => {
            expect(response).toBe(drawingStored);
        });
        const request = httpTestingController.expectOne(DELETE_URL + '1');
        expect(request.request.method).toEqual('DELETE');
        request.flush(drawingStored);
        httpTestingController.verify();
    });

    it('should handle errors', () => {
        service.getDrawing('69').subscribe((response) => {
            expect(response).toBe(FAILED);
        });
        httpTestingController.expectOne(GET_URL + '69')
            .error(new ErrorEvent('error'), { status: 400, statusText: 'Bad Request' });
        httpTestingController.verify();

        service.sendDrawing(meta).subscribe((response) => {
            expect(response).toBe(FAILED);
        });
        httpTestingController.expectOne(SEND_URL)
            .error(new ErrorEvent('error'), { status: 400, statusText: 'Bad Request' });
        httpTestingController.verify();

        service.deleteDrawing('69').subscribe((response) => {
            expect(response).toBe(FAILED);
        });
        httpTestingController.expectOne(DELETE_URL + '69')
            .error(new ErrorEvent('error'), { status: 400, statusText: 'Bad Request' });
        httpTestingController.verify();

        service.getDrawings().subscribe((response) => {
            expect(response).toBe(FAILED);
        });
        httpTestingController.expectOne(GET_ALL_URL)
            .error(new ErrorEvent('error'), { status: 400, statusText: 'Bad Request' });
        httpTestingController.verify();
    });

    it('should remove svgElement from svgContainer', () => {
        service['svgContainer'] =
            ['<svg:rect xmlns:svg="http://www.w3.org/2000/svg"/>',
                '<svg:line xmlns:svg="http://www.w3.org/2000/svg"/>'];
        const fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
        service.removeSvgElement(fakeElement);
        expect(service['svgContainer']).toEqual(['<svg:rect xmlns:svg="http://www.w3.org/2000/svg"/>']);
    });

    it('should initialise elements', () => {
        service.svgContainer = new Array<string>('Hello');
        service.initialiseElements();
        expect(service.svgContainer.length).toEqual(0);
    });

    it('should autoSave background', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'autosave');
        service.svgContainer = new Array<string>('Hello');
        const exprectedMeta: MetaDrawing = {
            timestamp: new Date().getTime(),
            name: '',
            labels: [],
            width: 200,
            height: 250,
            backgroundColor: 'rgba(0, 0, 0, 0)'
        };
        const exepectedDrawing: DrawingStored = { metaDrawing: exprectedMeta, svgContainer: service.svgContainer };
        service.autosaveBackground('rgba(0, 0, 0, 0)', WIDTH, HEIGHT);
        exepectedDrawing.metaDrawing.timestamp = service['autoSavedDrawing'].metaDrawing.timestamp;
        expect(service['autosave']).toHaveBeenCalled();
        expect(service['autoSavedDrawing']).toEqual(exepectedDrawing);
    });
});
