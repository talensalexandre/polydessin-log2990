import { Coordinates } from './coordinates';

export interface MinMaxCoords {
    minCoords: Coordinates;
    maxCoords: Coordinates;
}
