import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { SvgCanvasConverterService } from '../../../services/svg-canvas-converter/svg-canvas-converter.service';
import { AbstractToolComponent } from '../../abstract-tool/abstract-tool.component';

const WAIT_3SEC = 3000;
const COLORS = ['primaire', 'secondaire'];

@Component({
    selector: 'app-color-picker',
    templateUrl: './color-picker.component.html',
    styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent extends AbstractToolComponent implements AfterViewInit {
    @ViewChild('htmlCanvas', { static: false }) htmlCanvas: ElementRef;
    context: CanvasRenderingContext2D;
    coordinates: Coordinates;
    colorChoice: ColorChoice;
    colorPixel: Uint8ClampedArray;

    constructor(private colorManager: ColorManagerService,
                public svgHandler: SvgHandlerService,
                public mouseHandler: MouseHandlerService,
                public stateSelector: StateSelectorService,
                public saveManager: SaveManagerService,
                private snackBar: MatSnackBar,
                private svgCanvasConverter: SvgCanvasConverterService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
    }

    protected initExtend(): void {
        this.eventListeners.mouseDown = ($event) => this.onMouseDown($event);
        this.eventListeners.contextMenu = ($event) => this.onContextMenu($event);
        this.eventListeners.changedMouseDown = true;
        this.eventListeners.changedContextMenu = true;
        this.svgHandler.addListeners(this.eventListeners);
        this.setDefaultSize();
    }

    ngAfterViewInit(): void {
        this.context = this.svgCanvasConverter.convertSvgToCanvas(this.htmlCanvas);
    }

    private onContextMenu(event: MouseEvent): boolean {
        this.updateContextMenu(event);
        return false;
    }

    private updateContextMenu(event: MouseEvent): void {
        event.preventDefault();
    }

    private colorPicker(coordinates: Coordinates, colorChoice: ColorChoice, isPrimary: string): void {
        this.colorPixel = this.context.getImageData(coordinates.x, coordinates.y, 1, 1).data;
        if (this.colorPixel) {
            this.colorManager.updateColorWithPixel(colorChoice, this.colorPixel);
            this.openSnackBar(isPrimary);
        }
    }

    private onMouseDown(event: MouseEvent): void {
        event.preventDefault();
        this.coordinates = { x : event.offsetX, y : event.offsetY};
        if (event.button === 0) {
            this.colorPicker(this.coordinates, ColorChoice.mainColor, COLORS[0]);
        } else if (event.button === 2) {
            this.colorPicker(this.coordinates, ColorChoice.secondaryColor, COLORS[1]);
        }
    }

    private openSnackBar(isPrimaryColor: string): void {
        this.snackBar.open('La couleur ' + isPrimaryColor + ' a été mise à jour.', 'OK', {
            duration: WAIT_3SEC,
        });
    }

    protected destroyExtend(): void {
        return;
    }

    protected mouseMouvementExtend(): void {
        return;
    }

    protected mouseDownExtend(): void {
        return;
    }

    protected mouseUpExtend(): void {
        return;
    }

    protected setDefaultSize(): void {
        return;
    }
}
