import { ElementRef, Injectable } from '@angular/core';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

@Injectable({
    providedIn: 'root'
})
export class SvgCanvasConverterService {
    constructor(private svgHandler: SvgHandlerService) {}

    convertSvgToCanvas(htmlCanvas: ElementRef): CanvasRenderingContext2D {
        const svgCanvasNode = this.svgHandler.svgCanvas.cloneNode(true);
        const svgString = new XMLSerializer().serializeToString(svgCanvasNode);
        const svg64 = btoa(svgString);
        const img64 = 'data:image/svg+xml;base64,' + svg64;
        htmlCanvas.nativeElement.setAttribute('width', this.svgHandler.canvasWidth);
        htmlCanvas.nativeElement.setAttribute('height', this.svgHandler.canvasHeight);
        const context = htmlCanvas.nativeElement.getContext('2d');
        const img = new Image();
        img.onload = () => {
            context.drawImage(img, 0, 0);
        };
        img.src = img64;
        return context;
    }
}
