import { ElementRef, Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { CreateNewDrawingService } from '../create-new-drawing/create-new-drawing.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgCanvasConverterService } from '../svg-canvas-converter/svg-canvas-converter.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { PaintBucketHandlerService } from './paint-bucket-handler.service';

// tslint:disable:no-magic-numbers no-any no-string-literal

describe('PaintBucketHandlerService', () => {
    let service: PaintBucketHandlerService;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let rectangleHandlerSpy: jasmine.SpyObj<RectangleHandlerService>;
    let newDrawingServiceSpy: jasmine.SpyObj<CreateNewDrawingService>;
    let saveManagerServiceSpy: jasmine.SpyObj<SaveManagerService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    const fakeSvgCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const fakePath = document.createElementNS('http://www.w3.org/2000/svg', 'svg:path');
    let htmlCanvas: ElementRef;
    let fakeCanvas: jasmine.SpyObj<HTMLCanvasElement>;
    let contextSpy: jasmine.SpyObj<CanvasRenderingContext2D>;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj('SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'svgCanvas'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'addSelectionStyles'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        rectangleHandlerSpy = jasmine.createSpyObj(
            'RectangleHandlerService',
            [
                'calculateRectangle',
                'elementPrevisualization',
                'createElement'
            ]
        );
        fakeCanvas = jasmine.createSpyObj(
            'HTMLCanvasElement',
            [
                'setAttribute',
                'getContext'
            ]
        );
        contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'drawImage',
                'getImageData',
                'putImageData'
            ]
        );
        newDrawingServiceSpy = jasmine.createSpyObj(
            'CreateNewDrawingService',
            [
                'blockNavigation'
            ]
        );
        saveManagerServiceSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement'
            ]
        );

        const fakeImage = new ImageData(1, 1);
        contextSpy.getImageData.and.returnValue(fakeImage);
        fakeCanvas.getContext.and.returnValue(contextSpy);
        svgHandlerSpy.renderer = rendererSpy;
        svgHandlerSpy.svgCanvas = fakeSvgCanvas;
        htmlCanvas = new ElementRef(fakeCanvas);
        htmlCanvas.nativeElement = fakeCanvas;
        service = new PaintBucketHandlerService(
            svgHandlerSpy,
            new ColorManagerService(),
            clickerHandlerSpy,
            undoRedoSpy,
            rectangleHandlerSpy,
            new SvgCanvasConverterService(svgHandlerSpy),
            newDrawingServiceSpy,
            saveManagerServiceSpy
        );
        service['path'] = fakePath;
        service['context'] = contextSpy;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create path', () => {
        service['createPath']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(6);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(1);
    });

    it('should calculate paint bucket area', async () => {
        const spyGetBaseColor = spyOn<any>(service, 'getBaseColor');
        const spyCreatePath = spyOn<any>(service, 'createPath');
        const spyFindAllAvailableSpace = spyOn<any>(service, 'findAllAvailableSpace');
        const spyConvert = spyOn<any>(service, 'convertSvgToCanvas');
        service['tolerance'] = 0;
        service['svgHandler'].drawingOnCanvas = true;
        const fakeCoord = { x: 20, y: 20 };
        await service.calculatePaintBucketArea(fakeCoord, htmlCanvas);
        expect(spyConvert).toHaveBeenCalledWith(htmlCanvas);
        expect(spyGetBaseColor).toHaveBeenCalledTimes(1);
        expect(spyCreatePath).toHaveBeenCalledTimes(1);
        expect(spyFindAllAvailableSpace).toHaveBeenCalledTimes(1);
    });

    it('should add pixel to path', () => {
        service['addPixelToPath']({ x: 20, y: 20 });
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
    });

    it('should change pixel color', () => {
        service.convertSvgToCanvas(htmlCanvas);
        service['changePixelColor']({ x: 20, y: 20 });
        expect(contextSpy.getImageData).toHaveBeenCalledTimes(1);
        expect(contextSpy.putImageData).toHaveBeenCalledTimes(1);
    });

    it('should create full rectangle', () => {
        service['createFullCanvasRectangle']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
    });

    it('should get base color', () => {
        service['tolerance'] = 10;
        service.convertSvgToCanvas(htmlCanvas);
        service['getBaseColor']({ x: 20, y: 20 });
        expect(service['basePixelTolerance'][0]).toBe(0);
        expect(service['basePixelTolerance'][1]).toBe(25.5);
        expect(service['basePixelTolerance'][2]).toBe(0);
        expect(service['basePixelTolerance'][3]).toBe(25.5);
    });

    it('should match base color', () => {
        spyOn<any>(service, 'getPixelColor').and.returnValue([10, 10, 10, 10]);
        service['basePixelTolerance'] = [5, 15, 5, 15, 5, 15];
        service['matchesBaseColor']({ x: 20, y: 20 });
        expect(service['matchesBaseColor']).toBeTruthy();
    });

    it('should add a starting line point to the path', () => {
        const fakeCoords = { x: 10, y: 10 };
        service['addStartingPoint'](fakeCoords);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(service['path'], 'd', 'null M 10,10');
    });

    it('should add a Z for the end of path', () => {
        service['addZToPath']();
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(service['path'], 'd', 'null Z');
    });

    it('should find left coord of area', () => {
        const fakeCoords = { x: 10, y: 10 };
        spyOn<any>(service, 'matchesBaseColor').and.returnValue(true);
        service['findLeftCoord'](fakeCoords);
        expect(fakeCoords.x).toEqual(1);
    });

    it('should find right coord of area and set all new starting coords', () => {
        const fakeCoords = { x: 10, y: 10 };
        const pixelStack: Coordinates[] = new Array<Coordinates>();
        spyOn<any>(service, 'matchesBaseColor').and.returnValue(true);
        service['svgHandler'].canvasWidth = 20;
        service['svgHandler'].canvasHeight = 20;
        service['findRightCoord'](fakeCoords, false, false, pixelStack);
        expect(fakeCoords.x).toBe(19);
        expect(fakeCoords.y).toBe(10);
        expect(pixelStack[0].x).toBe(10);
        expect(pixelStack[0].y).toBe(9);
        expect(pixelStack[1].x).toBe(10);
        expect(pixelStack[1].y).toBe(11);
    });

    it('should find right coord of area and set all new starting coords when colors not match and reach to true', () => {
        const fakeCoords = { x: 10, y: 10 };
        const pixelStack: Coordinates[] = new Array<Coordinates>();
        spyOn<any>(service, 'matchesBaseColor').and.callFake((coords: Coordinates) => {
            if (coords.y === 10) {
                return true;
            }
            return false;
        });
        service['svgHandler'].canvasWidth = 20;
        service['svgHandler'].canvasHeight = 20;
        service['findRightCoord'](fakeCoords, true, true, pixelStack);
        expect(fakeCoords.x).toBe(19);
        expect(fakeCoords.y).toBe(10);
    });

    it('should find right coord of area if at top', () => {
        const fakeCoords = { x: 10, y: 0 };
        const pixelStack: Coordinates[] = new Array<Coordinates>();
        spyOn<any>(service, 'matchesBaseColor').and.returnValue(true);
        service['svgHandler'].canvasWidth = 20;
        service['svgHandler'].canvasHeight = 20;
        service['findRightCoord'](fakeCoords, true, true, pixelStack);
        expect(fakeCoords.x).toBe(19);
        expect(fakeCoords.y).toBe(0);
    });

    it('should find right coord of area if at bottom', () => {
        const fakeCoords = { x: 10, y: 20 };
        const pixelStack: Coordinates[] = new Array<Coordinates>();
        spyOn<any>(service, 'matchesBaseColor').and.returnValue(true);
        service['svgHandler'].canvasWidth = 20;
        service['svgHandler'].canvasHeight = 20;
        service['findRightCoord'](fakeCoords, true, true, pixelStack);
        expect(fakeCoords.x).toBe(19);
        expect(fakeCoords.y).toBe(20);
    });

    it('should find all available space but fill with one big boi rect because no element', () => {
        const spyCreateFullCanvasRectangle = spyOn<any>(service, 'createFullCanvasRectangle');
        const fakeCoords = { x: 10, y: 10 };
        service['findAllAvailableSpace'](fakeCoords);
        service['svgHandler'].drawingOnCanvas = false;
        expect(spyCreateFullCanvasRectangle).toHaveBeenCalledTimes(1);
    });

    it('should find all available space but fill with one big boi rect because tolerance is 100', () => {
        const spyCreateFullCanvasRectangle = spyOn<any>(service, 'createFullCanvasRectangle');
        const fakeCoords = { x: 10, y: 10 };
        service['findAllAvailableSpace'](fakeCoords);
        service['tolerance'] = 100;
        expect(spyCreateFullCanvasRectangle).toHaveBeenCalledTimes(1);
    });

    it('should find all available space', async () => {
        const addStartingPointSpy = spyOn<any>(service, 'addStartingPoint');
        const findLeftCoordSpy = spyOn<any>(service, 'findLeftCoord');
        const addPixelToPathSpy = spyOn<any>(service, 'addPixelToPath');
        const findRightCoordSpy = spyOn<any>(service, 'findRightCoord');
        const addZToPathSpy = spyOn<any>(service, 'addZToPath');
        const fakeCoords = { x: 10, y: 10 };
        service['svgHandler'].drawingOnCanvas = true;
        service['tolerance'] = 0;
        await service['findAllAvailableSpace'](fakeCoords);
        expect(addStartingPointSpy).toHaveBeenCalledTimes(1);
        expect(findLeftCoordSpy).toHaveBeenCalledTimes(1);
        expect(addPixelToPathSpy).toHaveBeenCalledTimes(1);
        expect(findRightCoordSpy).toHaveBeenCalledTimes(1);
        expect(addZToPathSpy).toHaveBeenCalledTimes(1);
    });

    it('should remove path from canvas because kill process was asked', () => {
        service.killProcess = true;
        service['addZToPath']();
        expect(rendererSpy.removeChild).toHaveBeenCalledWith(svgHandlerSpy.svgCanvas, service['path']);
    });

    it('should not find right coord of area if at bottom because kill process is activated', () => {
        service.killProcess = true;
        const fakeCoords = { x: 10, y: 20 };
        const pixelStack: Coordinates[] = new Array<Coordinates>();
        spyOn<any>(service, 'matchesBaseColor').and.returnValue(true);
        service['svgHandler'].canvasWidth = 20;
        service['svgHandler'].canvasHeight = 20;
        service['findRightCoord'](fakeCoords, true, true, pixelStack);
        expect(fakeCoords.x).toBe(10);
        expect(fakeCoords.y).toBe(20);
    });
});
