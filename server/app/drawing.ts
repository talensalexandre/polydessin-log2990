export interface MetaDrawing {
    timestamp: number;
    name: string;
    labels: string[];
    width: number;
    height: number;
    backgroundColor: string;
}

export interface DrawingStored {
    metaDrawing: MetaDrawing;
    svgContainer: string[];
}
