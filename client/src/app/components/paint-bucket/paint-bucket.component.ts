import { AfterViewInit, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PaintBucketHandlerService } from 'src/app/services/paint-bucket-handler/paint-bucket-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const MAX_TOLERANCE = 100;
const MIN_TOLERANCE = 0;

@Component({
    selector: 'app-paint-bucket',
    templateUrl: './paint-bucket.component.html',
    styleUrls: ['./paint-bucket.component.scss']
})
export class PaintBucketComponent extends AbstractToolComponent implements AfterViewInit {
    @ViewChild('htmlCanvas', { static: false }) htmlCanvas: ElementRef;

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        protected saveManager: SaveManagerService,
        public paintBucketHandler: PaintBucketHandlerService,
        public newDrawingService: CreateNewDrawingService,
        private dialog: MatDialog
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
        this.paintBucketHandler.tolerance = 0;
    }

    ngAfterViewInit(): void {
        this.paintBucketHandler.convertSvgToCanvas(this.htmlCanvas);
    }

    changeSize(): void {
        this.paintBucketHandler.tolerance = Math.round(this.paintBucketHandler.tolerance);
        this.paintBucketHandler.tolerance =
            (this.paintBucketHandler.tolerance < MIN_TOLERANCE) ? MIN_TOLERANCE : this.paintBucketHandler.tolerance;
        this.paintBucketHandler.tolerance =
            (this.paintBucketHandler.tolerance > MAX_TOLERANCE) ? MAX_TOLERANCE : this.paintBucketHandler.tolerance;
        this.stateSelector.isModifyingInput = false;
    }

    protected destroyExtend(): void {
        this.isDrawing = false;
    }

    protected mouseDownExtend(coordinates: Coordinates): void {
        if ( !this.paintBucketHandler.filling) {
            this.paintBucketHandler.calculatePaintBucketArea(coordinates, this.htmlCanvas);
        }
    }

    @HostListener('window:keydown', ['$event']) handleKeyDown(event: KeyboardEvent): void {
        if (!this.dialog.openDialogs.length) {
            if (event.key === 'Escape') {
                event.preventDefault();
                this.paintBucketHandler.killProcess = true;
            }
        }
    }

    increaseTolerance(): void {
        this.paintBucketHandler.tolerance++;
        this.changeSize();
    }

    decreaseTolerance(): void {
        this.paintBucketHandler.tolerance--;
        this.changeSize();
    }

    protected setDefaultSize(): void { return; }

    protected mouseMouvementExtend(): void { return; }

    protected mouseUpExtend(): void { return; }
}
