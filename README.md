# Poly Dessin - LOG2990
Poly Dessin est une application Web de dessin vectoriel. Elle a été conçue dans le cadre du cours LOG2990 de Polytechnique Montréal. Le développement a été mené par Alexandre Talens, Théo Quiquempoix, Philippe Babin, Nour El Houda Lharch et Hugo Perronnet.
Projet généré avec [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

Une version de démonstration est actuellement mis en ligne sur : http://pigment.s3-website-us-east-1.amazonaws.com/

**ATTENTION :** Application uniquement supporté sur Google Chrome

![Semantic description of image](/client/src/assets/new-user-guide/undo-redo.gif "Image Title")

## Compilation du projet

Pour lancer l'application, il suffit d'exécuter: `npm start` ou `yarn start`. Vous devez lancer cette commande dans le dossier `client` et `server`

Pour le client :
Une page menant vers `http://localhost:4200/` s'ouvrira automatiquement.