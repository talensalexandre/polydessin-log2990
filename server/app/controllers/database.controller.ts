import { NextFunction, Request, Response, Router } from 'express';
import * as Httpstatus from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Message } from '../../../common/communication/message';
import { DrawingStored } from '../drawing';
import { DatabaseService } from '../services/database.service';
import Types from '../types';

@injectable()
export class DatabaseController {
    router: Router;

    constructor(@inject(Types.DatabaseService) private databaseService: DatabaseService) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            const message: Message = await this.databaseService.homePage();
            res.json(message);
        });

        this.router.get('/drawing/drawings', async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.getAllDrawings()
                .then((drawings: DrawingStored[]) => {
                    res.json(drawings);
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.get('/drawing/:timestamp', async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.getDrawingWithTimestamp(req.params.timestamp)
                .then((drawing: DrawingStored) => {
                    res.json(drawing);
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });

        this.router.post('/drawings', async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.addDrawing(req.body)
                .then(() => {
                    res.sendStatus(Httpstatus.CREATED).send();
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.BAD_REQUEST).send(error.message);
                });
        });

        this.router.delete('/drawing/:timestamp', async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.deleteDrawing(req.params.timestamp)
                .then(() => {
                    res.sendStatus(Httpstatus.OK).send();
                })
                .catch((error: Error) => {
                    res.sendStatus(Httpstatus.BAD_REQUEST).send();
                });
        });

        this.router.patch('/drawing/:id', async (req: Request, res: Response, next: NextFunction) => {
            this.databaseService.modifyDrawing(req.body)
                .then(() => {
                    res.sendStatus(Httpstatus.OK);
                })
                .catch((error: Error) => {
                    res.status(Httpstatus.NOT_FOUND).send(error.message);
                });
        });
    }
}
