import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule, MatIconModule, MatSidenavModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorDocComponent } from './color-doc/color-doc.component';
import { DrawingDocComponent } from './drawing-doc/drawing-doc.component';
import { FunctionalitiesDocComponent } from './functionalities-doc/functionalities-doc.component';
import { SelectionDocComponent } from './selection-doc/selection-doc.component';
import { ShapeDocComponent } from './shape-doc/shape-doc.component';
import { UserGuideComponent } from './user-guide.component';

describe('UserGuideComponent', () => {
    let component: UserGuideComponent;
    let fixture: ComponentFixture<UserGuideComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatSidenavModule, MatIconModule, HttpClientModule, BrowserAnimationsModule, MatDialogModule],
            declarations: [
                UserGuideComponent, ColorDocComponent, DrawingDocComponent, FunctionalitiesDocComponent,
                SelectionDocComponent, ShapeDocComponent
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserGuideComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
