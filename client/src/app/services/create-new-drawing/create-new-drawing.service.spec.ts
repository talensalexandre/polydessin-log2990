import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material';
import { DrawingStored, MetaDrawing } from 'src/app/interfaces-enums/drawing';
import { CreateNewDrawingComponent } from '../../components/create-new-drawing/create-new-drawing.component';
import { States } from '../../interfaces-enums/states';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionTransformService } from '../selection-services/selection-transform/selection-transform.service';
import { StateSelectorService } from '../state-selector/state-selector.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { CreateNewDrawingService } from './create-new-drawing.service';

// tslint:disable:no-magic-numbers no-string-literal

describe('CreateNewDrawingService', () => {
    let service: CreateNewDrawingService;
    let dialogRefSpy: jasmine.SpyObj<MatDialogRef<CreateNewDrawingComponent>>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let stateServiceSpy: jasmine.SpyObj<StateSelectorService>;
    let selectionTransformSpy: jasmine.SpyObj<SelectionTransformService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;

    beforeEach(() => TestBed.configureTestingModule({
        imports: [MatDialogModule, HttpClientModule, HttpClientTestingModule],
        providers: [
            { provide: MatDialogRef, useValue: dialogRefSpy },
            { provide: SaveManagerService, useValue: saveManagerSpy }
        ]
    }));

    beforeEach(() => {
        service = TestBed.get(CreateNewDrawingService);
    });

    beforeEach(() => {
        dialogRefSpy = jasmine.createSpyObj('MatDialogRef',
            [
                'updateSize'
            ]
        );
        svgHandlerSpy = jasmine.createSpyObj('SvgHandlerService',
            [
                'drawingOnCanvas',
                'deleteCanvas',
                'createCanvas',
                'createSvgFromSave'
            ]
        );
        stateServiceSpy = jasmine.createSpyObj('StateSelectorService',
            [
                'changeState'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj('SaveManagerService',
            [
                'autosaveBackground',
                'initialiseElements'
            ]
        );
        selectionTransformSpy = jasmine.createSpyObj('SelectionTransformService',
            [
                'ngOnInit'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'addSelectionStyles'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerSpy.renderer = rendererSpy;
        service = new CreateNewDrawingService(svgHandlerSpy, stateServiceSpy, selectionTransformSpy, saveManagerSpy, clickerHandlerSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('verifyDimensions should keep good dimensions value', () => {
        const dummyDimension = { x: 300, y: 300 };
        const verifiedDimensions = service.verifyDimensions(dummyDimension);
        expect(verifiedDimensions.x).toBe(300);
        expect(verifiedDimensions.y).toBe(300);
    });

    it('verifyDimensions should keep the value at MAXIMUM_DIMENSION', () => {
        const dummyDimension = { x: 6000, y: 6000 };
        const verifiedDimensions = service.verifyDimensions(dummyDimension);
        expect(verifiedDimensions.x).toBe(5000);
        expect(verifiedDimensions.y).toBe(5000);
    });

    it('verifyDimensions should keep the value at MINIMUM_DIMENSION', () => {
        const dummyDimension = { x: -5, y: -5 };
        const verifiedDimensions = service.verifyDimensions(dummyDimension);
        expect(verifiedDimensions.x).toBe(200);
        expect(verifiedDimensions.y).toBe(200);
    });

    it('should create a canvas and set the dimensions', () => {
        const serviceSpy = spyOn(service, 'createCanvas');
        const dimensions = 50;
        const color = 'red';
        svgHandlerSpy.drawingOnCanvas = false;
        const shouldConfirm = service.createNewDrawing(dimensions, dimensions, color, dialogRefSpy);
        expect(shouldConfirm).toBeTruthy();
        expect(serviceSpy).toHaveBeenCalledTimes(1);
    });

    it('should create a canvas and set the dimensions', () => {
        const dimensions = 50;
        const color = 'red';
        svgHandlerSpy.drawingOnCanvas = true;
        const shouldConfirm = service.createNewDrawing(dimensions, dimensions, color, dialogRefSpy);
        expect(shouldConfirm).toBeFalsy();
        expect(dialogRefSpy.updateSize).toHaveBeenCalledWith('auto', 'auto');
    });

    it('should create a canvas after first creation', () => {
        service.createCanvas();
        service.createCanvas();
        expect(svgHandlerSpy.deleteCanvas).toHaveBeenCalledTimes(1);
        expect(saveManagerSpy.initialiseElements).toHaveBeenCalled();
        expect(saveManagerSpy.autosaveBackground).toHaveBeenCalled();
        expect(svgHandlerSpy.createCanvas).toHaveBeenCalledTimes(2);
        expect(stateServiceSpy.changeState).toHaveBeenCalledWith(States.selection, false);
    });

    it('should create a canvas if first time creating', () => {
        service.createCanvas();
        expect(svgHandlerSpy.createCanvas).toHaveBeenCalled();
        expect(stateServiceSpy.changeState).toHaveBeenCalled();
        expect(saveManagerSpy.initialiseElements).not.toHaveBeenCalled();
        expect(saveManagerSpy.autosaveBackground).toHaveBeenCalled();
    });

    it('should update dimensions if dimensions not changed', () => {
        service.dimensionsChanged = false;
        const fakeDimensions = { x: 50, y: 50 };
        const fakeInnerDimensions = { x: 100, y: 100 };
        const updatedDimensions = service.updateDimensions(fakeDimensions, fakeInnerDimensions);
        expect(updatedDimensions).toBe(fakeInnerDimensions);
    });

    it('should not update dimensions if dimensions changed', () => {
        service.dimensionsChanged = true;
        const fakeDimensions = { x: 50, y: 50 };
        const fakeInnerDimensions = { x: 100, y: 100 };
        const updatedDimensions = service.updateDimensions(fakeDimensions, fakeInnerDimensions);
        expect(updatedDimensions).toBe(fakeDimensions);
    });

    it('should continue drawing', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'createElementFromString');
        spyOn(service, 'createNewDrawing');
        const fakeContainer = ['firstElement', 'secondElement'];
        const metaDrawing: MetaDrawing = {
            timestamp: 1, name: '', labels: [], width: 100, height: 100,
            backgroundColor: 'rgb(255, 0, 255)'
        };
        const fakeDrawingStored: DrawingStored = { metaDrawing, svgContainer: fakeContainer };
        service.continueDrawing(fakeDrawingStored);
        // tslint:disable-next-line: no-string-literal
        expect(service['createElementFromString']).toHaveBeenCalled();
        expect(service.createNewDrawing).toHaveBeenCalled();
    });

    it('should create element from string', () => {
        // tslint:disable-next-line: no-any
        const spyCreateSvgFromSave = spyOn<any>(service, 'createSvgFromSave');
        const fakeContainer = ['firstElement', 'secondElement'];
        const metaDrawing: MetaDrawing = {
            timestamp: 1, name: '', labels: [], width: 100, height: 100,
            backgroundColor: 'rgb(255, 0, 255)'
        };
        const fakeDrawingStored: DrawingStored = { metaDrawing, svgContainer: fakeContainer };
        // tslint:disable-next-line: no-string-literal
        service['createElementFromString'](fakeDrawingStored);
        expect(spyCreateSvgFromSave).toHaveBeenCalledTimes(fakeContainer.length);
    });

    it('should create svgCanvas from save', () => {
        const svgContainer = ['svg1', 'svg2', 'svg3'];
        svgContainer.forEach((element) => {
            service['createSvgFromSave'](element);
        });
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(svgContainer.length);
        expect(clickerHandlerSpy.addSelectionStyles).toHaveBeenCalledTimes(svgContainer.length);
    });
});
