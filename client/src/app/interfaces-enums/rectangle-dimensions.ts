import { Coordinates } from './coordinates';

export interface RectangleDimensions {
    coordinates: Coordinates;
    width: number;
    height: number;
}
