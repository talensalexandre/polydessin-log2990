import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { BrushComponent } from './brush.component';

// tslint:disable:no-string-literal

describe('BrushComponent', () => {
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let component: BrushComponent;
    let fixture: ComponentFixture<BrushComponent>;
    let mouseHandler: MouseHandlerService;
    const MAX_SIZE = 200;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        drawingServiceSpy = jasmine.createSpyObj(
            'DrawingService',
            [
                'createPolyLine',
                'addCoordinatesToPolyline',
                'addFilterToCanvas',
                'applyFilterToPolyLine'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
            [
                MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                FormsModule, MatSelectModule, MatIconModule, MatDialogModule, HttpClientModule
            ],
            declarations: [ BrushComponent, ColorComponent ],
            providers: [{provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
                        {provide: MouseHandlerService, useValue: mouseHandlerServiceSpy},
                        {provide: DrawingService, useValue: drawingServiceSpy}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BrushComponent);
        mouseHandler = TestBed.get(MouseHandlerService);
        component = fixture.componentInstance;
        component['svgElement'] = fakeSvgElement;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        component.ngOnInit();
        expect(svgHandlerServiceSpy.addListeners).toHaveBeenCalled();
    });

    it('should not change size if between 1 and 200', () => {
        const size = 50;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(size);
    });

    it('should change size if under 1', () => {
        const size = -50;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(1);
    });

    it('should change size if above 200', () => {
        const size = 500;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(MAX_SIZE);
    });

    it('should call eventListeners.mouseMove when mouse moves', () => {
        const mockMouse = new MouseEvent('mousemove');
        mouseHandler.isMouseDown = false;
        component['eventListeners'].mouseMove(mockMouse);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseDown when mouse goes down', () => {
        const mockMouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouseDown);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseUp when mouse goes up', () => {
        const mockMouseUp = new MouseEvent('mouseup');
        component['eventListeners'].mouseUp(mockMouseUp);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseLeave when mouse leaves', () => {
        const mockMouseLeave = new MouseEvent('mouseleave');
        component['eventListeners'].mouseLeave(mockMouseLeave);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });
});
