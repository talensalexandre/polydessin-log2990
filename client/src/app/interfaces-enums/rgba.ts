export interface RGBA {
    Dec: {
        Red: number,
        Green: number,
        Blue: number,
        Alpha: number
    };
    Hex: {
        Red: string,
        Green: string,
        Blue: string
    };
    inString: string;
}
