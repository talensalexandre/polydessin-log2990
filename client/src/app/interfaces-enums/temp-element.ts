import { ChangeType } from './change-type';

export interface TempElement {
    changeType: ChangeType;
    change: string[];
    svgElement: SVGElement[];
}
