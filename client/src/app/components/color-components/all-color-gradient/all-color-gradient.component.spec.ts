import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogModule } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { AllColorGradientComponent } from './all-color-gradient.component';

const FIRST_NUMBER = 75;
const SECOND_AND_THIRD_NUMBER = 25;
const LAST_NUMBER = 255;
const CLAMPED_ARRAY = [FIRST_NUMBER, SECOND_AND_THIRD_NUMBER, SECOND_AND_THIRD_NUMBER, LAST_NUMBER];

describe('AllColorGradientComponent', () => {
    let component: AllColorGradientComponent;
    let fixture: ComponentFixture<AllColorGradientComponent>;
    let colorManagerSpy: jasmine.SpyObj<ColorManagerService>;
    const mainDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const secondaryDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const backgroundDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };

    beforeEach(() => {
        colorManagerSpy = jasmine.createSpyObj(
            'ColorManagerService',
                [
                    'updateColorWithPixel'
                ]
        );
        colorManagerSpy.colorSelected = new Array<RGBA>();
        colorManagerSpy.colorSelected[ColorChoice.mainColor] = mainDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.secondaryColor] = secondaryDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.backgroundColor] = backgroundDefaultColor;
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule],
            declarations: [ AllColorGradientComponent ],
            providers: [{ provide: MAT_DIALOG_DATA, useValue: ColorChoice.mainColor},
                { provide: ColorManagerService, useValue: colorManagerSpy}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AllColorGradientComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should update gradient with color picked', () => {
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        const pixels: Uint8ClampedArray = new Uint8ClampedArray(CLAMPED_ARRAY);
        const colorPixel: ImageData = new ImageData(pixels, 1, 1);
        contextSpy.getImageData.and.returnValue(colorPixel);
        component.updateColorWithCoordinates({x: 1, y: 1});
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalled();
    });

    it ('should update color if user clic on gradient', () => {
        spyOn(component.shouldUpdateGradient, 'emit').and.callThrough();
        const mockMouse = new MouseEvent('mousedown');
        component.mouseDownFromGradient(mockMouse);
        fixture.detectChanges();
        expect(component.shouldUpdateGradient.emit).toHaveBeenCalledTimes(1);
    });

    it('should not update gradient with color picked', () => {
        const contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'getImageData'
            ]
        );
        contextSpy.getImageData.and.returnValue({data: undefined});
        component.allGradientContext = contextSpy;
        component.updateColorWithCoordinates({x: 1, y: 1});
        expect(colorManagerSpy.updateColorWithPixel).toHaveBeenCalledTimes(0);
    });
});
