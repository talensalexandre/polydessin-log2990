import { NextFunction, Request, Response, Router } from 'express';
import * as Httpstatus from 'http-status-codes';
import { inject, injectable } from 'inversify';
import { Message } from '../../../common/communication/message';
import { EmailService } from '../services/email.service';
import Types from '../types';

@injectable()
export class EmailController {
    router: Router;

    constructor(@inject(Types.EmailService) private emailService: EmailService) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();

        this.router.get('/', async (req: Request, res: Response, next: NextFunction) => {
            const message: Message = await this.emailService.homePage();
            res.json(message);
        });

        this.router.post('/send', async (req: Request, res: Response, next: NextFunction) => {
            this.emailService.sendDrawing(req.body)
                .then(() => {
                    res.sendStatus(Httpstatus.CREATED).send();
                })
                .catch((error) => {
                    res.status(error.response.status).send();
                });
        });

    }
}
