import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material';
import { StateSelectorService } from '../state-selector/state-selector.service';
import { UserGuideService } from './user-guide.service';

// tslint:disable:no-magic-numbers

describe('UserGuideService', () => {
    let stateSelectorSpy: jasmine.SpyObj<StateSelectorService>;
    let dialogSpy: jasmine.SpyObj<MatDialog>;
    let service: UserGuideService;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        stateSelectorSpy = jasmine.createSpyObj('StateSelectorService', ['changeState']);
        dialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
        service = new UserGuideService(stateSelectorSpy, dialogSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should close document and go to entry point', () => {
        service.accesFromEntryPoint = true;
        service.closeGuide();
        expect(dialogSpy.open).toHaveBeenCalled();
    });

    it('should close document and go to canvas', () => {
        service.accesFromEntryPoint = false;
        service.closeGuide();
        expect(stateSelectorSpy.changeState).toHaveBeenCalled();
    });

    it('should go to next page', () => {
        service.currentPage = 2;
        service.goToNextPage();
        expect(service.currentPage).toBe(3);
    });

    it('should not go to next page', () => {
        service.currentPage = 1000;
        service.goToNextPage();
        expect(service.currentPage).toBe(0);
    });

    it('should go to previous page', () => {
        service.currentPage = 4;
        service.goToPreviousPage();
        expect(service.currentPage).toBe(3);
    });

    it('should not go to previous page', () => {
        service.currentPage = -1000;
        service.goToPreviousPage();
        expect(service.currentPage).toBe(0);
    });

    it('should go to page', () => {
        service.currentPage = 4;
        service.goToPage(2);
        expect(service.currentPage).toBe(2);
    });

    it('should not go to page', () => {
        service.currentPage = 100;
        service.goToPage(5);
        expect(service.currentPage).toBe(0);
    });
});
