// Inspired by:
// W. Malone. (2012) Create a paint bucket tool in HTML5 and javascript. [Online]
// Available : http://www.williammalone.com/articles/html5-canvas-javascript-paint-bucket-tool/

import { ElementRef, Injectable, Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { ElementType } from '../../interfaces-enums/element-type';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { CreateNewDrawingService } from '../create-new-drawing/create-new-drawing.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgCanvasConverterService } from '../svg-canvas-converter/svg-canvas-converter.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const NUMBER_OF_RGB_COLORS = 3;
const MAX_PERCENT = 100;
const MAX_BYTE_VALUE = 255;

@Injectable({
    providedIn: 'root',
})
export class PaintBucketHandlerService {
    private context: CanvasRenderingContext2D;
    private renderer: Renderer2;
    private path: SVGElement;
    private basePixelColor: Uint8ClampedArray;
    private basePixelTolerance: number[];
    tolerance: number;
    filling: boolean;
    killProcess: boolean;

    constructor(
        private svgHandler: SvgHandlerService,
        private colorManager: ColorManagerService,
        private clickerHandler: ClickerHandlerService,
        private undoRedo: UndoRedoService,
        private rectangleHandler: RectangleHandlerService,
        private svgCanvasConverter: SvgCanvasConverterService,
        private newDrawingService: CreateNewDrawingService,
        private saveManager: SaveManagerService
    ) {
        this.renderer = this.svgHandler.renderer;
        this.basePixelTolerance = new Array<number>();
        this.filling = false;
        this.killProcess = false;
    }

    convertSvgToCanvas(htmlCanvas: ElementRef): void {
        this.context = this.svgCanvasConverter.convertSvgToCanvas(htmlCanvas);
    }

    private getPixelColor(coordinates: Coordinates): Uint8ClampedArray {
        return this.context.getImageData(coordinates.x, coordinates.y, 1, 1).data;
    }

    private createPath(): void {
        this.path = this.renderer.createElement('svg:path', this.svgHandler.SVG_LINK);
        this.renderer.setAttribute(this.path, 'stroke', this.colorManager.colorSelected[ColorChoice.mainColor].inString);
        this.renderer.setAttribute(this.path, 'stroke-width', '1px');
        this.renderer.setAttribute(this.path, 'stroke-linecap', 'round');
        this.renderer.setAttribute(this.path, 'fill', 'none');
        this.renderer.setAttribute(this.path, 'typeElement', 'strokeElement');
        this.clickerHandler.addSelectionStyles(this.path);
        this.undoRedo.updateCurrent(new Array(this.path), ChangeType.addToCanvas);
        this.renderer.setAttribute(this.path, 'd', '');
        this.renderer.appendChild(this.svgHandler.svgCanvas, this.path);
    }

    private addPixelToPath(coordinates: Coordinates): void {
        const coordString = String(coordinates.x) + ',' + String(coordinates.y);
        let pathPoints = this.path.getAttribute('d');
        pathPoints = pathPoints + ' ' + coordString;
        this.renderer.setAttribute(this.path, 'd', pathPoints);
    }

    private changePixelColor(coordinates: Coordinates): void {
        const pixelColor = this.context.getImageData(coordinates.x, coordinates.y, 1, 1);
        pixelColor.data[0] = this.colorManager.colorSelected[ColorChoice.mainColor].Dec.Red;
        pixelColor.data[1] = this.colorManager.colorSelected[ColorChoice.mainColor].Dec.Green;
        pixelColor.data[2] = this.colorManager.colorSelected[ColorChoice.mainColor].Dec.Blue;
        pixelColor.data[NUMBER_OF_RGB_COLORS] = this.colorManager.colorSelected[ColorChoice.mainColor].Dec.Alpha;
        this.context.putImageData(pixelColor, coordinates.x, coordinates.y);
    }

    private addStartingPoint(pixelCoords: Coordinates): void {
        let startPathPoints = this.path.getAttribute('d');
        startPathPoints = startPathPoints + ' M ' + pixelCoords.x + ',' + pixelCoords.y ;
        this.renderer.setAttribute(this.path, 'd', startPathPoints);
    }

    private addZToPath(): void {
        let pathPoints = this.path.getAttribute('d');
        pathPoints = pathPoints + ' Z';
        this.renderer.setAttribute(this.path, 'd', pathPoints);
        if (this.killProcess) {
            this.svgHandler.renderer.removeChild(this.svgHandler.svgCanvas, this.path);
        }
    }

    private findLeftCoord(pixelCoords: Coordinates): void {
        while (pixelCoords.x - 1 >= 0 && this.matchesBaseColor(pixelCoords)) {
            pixelCoords.x--;
        }
        pixelCoords.x++;
    }

    private findRightCoord(pixelCoords: Coordinates, reachTop: boolean, reachBottom: boolean, pixelStack: Coordinates[]): void {
        while (pixelCoords.x + 1 < this.svgHandler.canvasWidth && this.matchesBaseColor(pixelCoords)) {
            if (this.killProcess) {
                break;
            }
            this.changePixelColor(pixelCoords);
            if (pixelCoords.y > 0) {
                const pixelCoordsUp: Coordinates = { x: pixelCoords.x, y: pixelCoords.y - 1 };
                if (this.matchesBaseColor(pixelCoordsUp)) {
                    if (!reachTop) {
                        pixelStack.push(pixelCoordsUp);
                        reachTop = true;
                    }
                } else if (reachTop) {
                    reachTop = false;
                }
            }
            if (pixelCoords.y < this.svgHandler.canvasHeight) {
                const pixelCoordsDown: Coordinates = { x: pixelCoords.x, y: pixelCoords.y + 1 };
                if (this.matchesBaseColor(pixelCoordsDown)) {
                    if (!reachBottom) {
                        pixelStack.push(pixelCoordsDown);
                        reachBottom = true;
                    }
                } else if (reachBottom) {
                    reachBottom = false;
                }
            }
            pixelCoords.x++;
        }
    }

    private async findAllAvailableSpace(coordinates: Coordinates): Promise<void> {
        const pixelStack = new Array<Coordinates>();
        let pixelCoords: Coordinates;
        pixelStack.push(coordinates);
        this.filling = true;
        if (this.tolerance === MAX_PERCENT || !this.svgHandler.drawingOnCanvas) {
            this.createFullCanvasRectangle();
            return;
        }
        while (pixelStack.length) {
            pixelCoords = pixelStack.pop() as Coordinates;
            const reachTop = false;
            const reachBottom = false;
            await this.wait(0);
            this.findLeftCoord(pixelCoords);
            this.addStartingPoint(pixelCoords);
            this.findRightCoord(pixelCoords, reachTop, reachBottom, pixelStack);
            this.addPixelToPath(pixelCoords);
            this.addZToPath();
        }
        this.filling = false;
    }

    private async wait(ms: number): Promise<unknown> {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }

    private createFullCanvasRectangle(): void {
        const fullCanvasRectangle = this.rectangleHandler.createElement(true, false, 0, ElementType.rect);
        this.renderer.setAttribute(fullCanvasRectangle, 'width', String(this.svgHandler.canvasWidth));
        this.renderer.setAttribute(fullCanvasRectangle, 'height', String(this.svgHandler.canvasHeight));
        this.renderer.setAttribute(fullCanvasRectangle, 'x', '0');
        this.renderer.setAttribute(fullCanvasRectangle, 'y', '0');
    }

    private getBaseColor(coordinates: Coordinates): void {
        this.basePixelColor = this.getPixelColor(coordinates);
        const toleranceFactor = MAX_BYTE_VALUE * (this.tolerance / MAX_PERCENT);
        let colorFactor: number;
        for (let i = 0; i < NUMBER_OF_RGB_COLORS; i++) {
            colorFactor = this.basePixelColor[i] / MAX_BYTE_VALUE;
            this.basePixelTolerance[i * 2] = this.basePixelColor[i] - (toleranceFactor * colorFactor);
            this.basePixelTolerance[i * 2 + 1] = this.basePixelColor[i] + (toleranceFactor * (1 - colorFactor));
        }
    }

    private matchesBaseColor(coordinates: Coordinates): boolean {
        const pixelColor = this.getPixelColor(coordinates);
        const rgbEquals = new Array<boolean>();
        for (let i = 0; i < NUMBER_OF_RGB_COLORS; i++) {
            rgbEquals[i] = (pixelColor[i] >= this.basePixelTolerance[i * 2]) && (pixelColor[i] <= this.basePixelTolerance[i * 2 + 1]);
        }
        return rgbEquals[0] && rgbEquals[1] && rgbEquals[2];
    }

    async calculatePaintBucketArea(coordinates: Coordinates, htmlCanvas: ElementRef): Promise<void> {
        this.newDrawingService.blockNavigation = true;
        this.convertSvgToCanvas(htmlCanvas);
        await this.wait(0);
        this.getBaseColor(coordinates);
        this.createPath();
        await this.findAllAvailableSpace(coordinates);
        this.newDrawingService.blockNavigation = false;
        this.saveManager.addSvgElement(this.path);
        this.killProcess = false;
    }
}
