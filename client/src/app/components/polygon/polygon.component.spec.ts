import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectionType } from 'src/app/interfaces-enums/selection-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { LineHandlerService } from 'src/app/services/line-handler/line-handler.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PolygonHandlerService } from 'src/app/services/polygon-handler/polygon-handler.service';
import { PrevisualisationRectangleService } from 'src/app/services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { PolygonComponent } from './polygon.component';

// tslint:disable:no-magic-numbers no-string-literal

describe('PolygonComponent', () => {
    let component: PolygonComponent;
    let fixture: ComponentFixture<PolygonComponent>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let mouseHandler: MouseHandlerService;
    let polygonHandlerSpy: jasmine.SpyObj<PolygonHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let previsualizationRectangleSpy: jasmine.SpyObj<PrevisualisationRectangleService>;
    let lineHandlerServiceSpy: jasmine.SpyObj<LineHandlerService>;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polygon');

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj('SvgHandlerService',
                                                    ['createCanvas',
                                                    'addListeners',
                                                    'deleteListeners',
                                                    'renderer']);

        mouseHandlerServiceSpy = jasmine.createSpyObj('MouseHandlerService',
                                                    ['onMouseDown',
                                                    'onMouseMovement',
                                                    'onMouseUp',
                                                    'onMouseLeave',
                                                    'isMouseDown']);
        polygonHandlerSpy = jasmine.createSpyObj('PolygonHandlerService',
                                                ['getCoordinatesForPolygon',
                                                'removeAllCoordinatesFromPolygon',
                                                'createElement',
                                                'addCoordinatesToPolygon',
                                                'elementPrevisualization',
                                                'isShapeTooBigComparedToSize']);
        drawingServiceSpy = jasmine.createSpyObj('DrawingService',
                                                ['removeElement',
                                                'addCoordinatesToPolyline',
                                                'createPolyLine']);
        lineHandlerServiceSpy = jasmine.createSpyObj('LineHandlerService',
                                                    ['createGElement',
                                                    'addElementToG']);
        previsualizationRectangleSpy = jasmine.createSpyObj('PrevisualizationRectangleService',
                                                    ['createPrevisualizationRectangle',
                                                        'deletePrevisualization'
                                                    ]);
    });
    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports:
            [
                MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                FormsModule, MatSelectModule, MatIconModule, MatDialogModule,
                HttpClientModule
            ],
            declarations: [ PolygonComponent, ColorComponent ],
            providers: [{provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
                        {provide: MouseHandlerService, useValue: mouseHandlerServiceSpy},
                        {provide: PolygonHandlerService, useValue: polygonHandlerSpy},
                        {provide: DrawingService, useValue: drawingServiceSpy},
                        {provide: LineHandlerService, useValue: lineHandlerServiceSpy},
                        {provide: PrevisualisationRectangleService, useValue: previsualizationRectangleSpy}]
            })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PolygonComponent);
        mouseHandler = TestBed.get(MouseHandlerService);
        component = fixture.componentInstance;
        component['svgElement'] = fakeSvgElement;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call mouseMouvementExtend when mouse moves and mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        component['mouseHandler'].startCoordinates = {x: 10, y: 10};
        component['mouseHandler'].actualCoordinates = {x: 20, y: 20};
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        mouseHandler.isMouseDown = true;
        polygonHandlerSpy.getCoordinatesForPolygon.and.returnValue(
            [
                {x: 0, y: 0},
                {x: 10, y: 15},
                {x: 5, y: 10}
            ]

        );
        component['updateMousePosition'](mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(3);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);

        component.selectedType = SelectionType.fill;
        component.layoutType();
        component['updateMousePosition'](mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeFalsy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(6);

        component.selectedType = SelectionType.outlineAndFill;
        component.layoutType();
        component['updateMousePosition'](mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(9);

        component['shiftKeyDown'] = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(12);

        polygonHandlerSpy.isShapeTooBigComparedToSize.and.returnValue(true);
        component['shiftKeyDown'] = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(12);
    });

    it('should call mouseMouvementExtend when eventListeners.mouseMoved is called', () => {
        const mockMouse = new MouseEvent('mousemove');
        component['mouseHandler'].startCoordinates = {x: 10, y: 10};
        component['mouseHandler'].actualCoordinates = {x: 20, y: 20};
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        mouseHandler.isMouseDown = true;
        polygonHandlerSpy.getCoordinatesForPolygon.and.returnValue(
            [
                {x: 0, y: 0},
                {x: 10, y: 15},
                {x: 5, y: 10}
            ]

        );
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(3);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);

        component.selectedType = SelectionType.fill;
        component.layoutType();
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeFalsy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(6);

        component.selectedType = SelectionType.outlineAndFill;
        component.layoutType();
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(9);

        component['shiftKeyDown'] = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(12);

        polygonHandlerSpy.isShapeTooBigComparedToSize.and.returnValue(true);
        component['shiftKeyDown'] = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(12);
    });

    it('should not update coordinates when mouse move with no mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        mouseHandlerServiceSpy.startCoordinates = {x: 10, y: 10};
        mouseHandlerServiceSpy.actualCoordinates = {x: 20, y: 20};
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        mouseHandler.isMouseDown = false;
        component['updateMousePosition'](mockMouse);
        expect(polygonHandlerSpy.removeAllCoordinatesFromPolygon).toHaveBeenCalledTimes(0);
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should create polygon previsualization on mouse down no matter with shift key', () => {
        const mockMouse = new MouseEvent('mousedown');
        const shiftKeyMockDown = new KeyboardEvent('keydown', {key: 'Shift'});
        component.handleKeyDown(shiftKeyMockDown);
        expect(component['shiftKeyDown']).toBeTruthy();
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        mouseHandler.isMouseDown = true;
        component['updateMouseDown'](mockMouse);
        expect(polygonHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(1);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        const shiftKeyMockUp = new KeyboardEvent('keyup', {key: 'Shift'});
        component.handleKeyUp(shiftKeyMockUp);
        expect(component['shiftKeyDown']).toBeFalsy();
        component['updateMouseDown'](mockMouse);
        expect(polygonHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(2);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should create polygon previusalization when eventListeners.mouseDown is called', () => {
        const mockMouse = new MouseEvent('mousedown');
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        mouseHandler.isMouseDown = false;
        component['eventListeners'].mouseDown(mockMouse);
        expect(polygonHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(1);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should create shape when mouse up', () => {
        const mockMouse = new MouseEvent('mouseup');
        polygonHandlerSpy.createElement.and.returnValue(fakeSvgElement);
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        component['mouseMoved'] = true;
        component['polygonCoordinates'] = [{x: 1, y: 1}, {x: 2, y: 2}, {x: 3, y: 3}];
        component['updateMouseUp'](mockMouse);
        expect(polygonHandlerSpy.removeAllCoordinatesFromPolygon).toHaveBeenCalledTimes(0);
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(3);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should create shape when eventListerners.mouseUp is called', () => {
        const mockMouse = new MouseEvent('mouseup');
        polygonHandlerSpy.createElement.and.returnValue(fakeSvgElement);
        component.numOfSides = 3;
        component.selectedType = SelectionType.outline;
        component['mouseMoved'] = true;
        component['polygonCoordinates'] = [{x: 1, y: 1}, {x: 2, y: 2}, {x: 3, y: 3}];
        component['eventListeners'].mouseUp(mockMouse);
        expect(polygonHandlerSpy.createElement).toHaveBeenCalledTimes(1);
        expect(polygonHandlerSpy.removeAllCoordinatesFromPolygon).toHaveBeenCalledTimes(0);
        expect(polygonHandlerSpy.addCoordinatesToPolygon).toHaveBeenCalledTimes(3);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should should call eventListeners.mouseLeave when mouse moves out of canvas', () => {
        const mouseEvent = new MouseEvent('click');
        component['mouseHandler'].isMouseDown = true;
        component['eventListeners'].mouseLeave(mouseEvent);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
        expect(previsualizationRectangleSpy.deletePrevisualization).toHaveBeenCalled();
        expect(drawingServiceSpy.removeElement).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', {key: 'Shift'});
        mouseHandler.isMouseDown = true;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeTruthy();
    });

    it('should not change size if between 1 and 200', () => {
        const size = 50;
        component.size = size;
        component['changeSize']();
        expect(component.size).toEqual(size);
    });

    it('should change size if under 1', () => {
        const size = -50;
        component.size = size;
        component['changeSize']();
        expect(component.size).toEqual(1);
    });

    it('should change size if above 200', () => {
        const size = 500;
        component.size = size;
        component['changeSize']();
        expect(component.size).toEqual(200);
    });

    it('should not change number of sides if between 3 and 12', () => {
        component.numOfSides = 5;
        component.changeNumOfSides();
        expect(component.numOfSides).toEqual(5);
    });

    it('should change number of sides if under 3', () => {
        component.numOfSides = -5;
        component.changeNumOfSides();
        expect(component.numOfSides).toEqual(3);
    });

    it('should change number of sides if above 12', () => {
        component.numOfSides = 24;
        component.changeNumOfSides();
        expect(component.numOfSides).toEqual(12);
    });

    it('should increase numOfSides', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeNumOfSides').and.callThrough();
        component.increaseNumOfSides();
        expect(component.changeNumOfSides).toHaveBeenCalledTimes(1);
    });

    it('should decrease numOfSides', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeNumOfSides').and.callThrough();
        component.decreaseNumOfSides();
        expect(component.changeNumOfSides).toHaveBeenCalledTimes(1);
    });
});
