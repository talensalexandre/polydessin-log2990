import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ColorComponent } from 'src/app/components/color-components/color/color.component';
import { EllipseComponent } from 'src/app/components/ellipse/ellipse.component';
import { ElementType } from '../../interfaces-enums/element-type';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { PrevisualisationRectangleService } from '../previsualisation-rectangle/previsualisation-rectangle.service';
import { RectangleHandlerService } from '../rectangle-handler/rectangle-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { EllipseHandlerService } from './ellipse-handler.service';

// tslint:disable:no-magic-numbers

describe('EllipseHandlerService', () => {
    let fill: boolean;
    let stroke: boolean;
    let oldChildMock: jasmine.SpyObj<SVGElement>;
    const size = 20;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let service: EllipseHandlerService;
    const colorManager = new ColorManagerService();
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let rectangleHandlerSpy: jasmine.SpyObj<RectangleHandlerService>;
    let previsualizationRectangleSpy: jasmine.SpyObj<PrevisualisationRectangleService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;

    beforeEach(() => TestBed.configureTestingModule({
        declarations: [EllipseComponent, ColorComponent]
    }));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer'
            ]
        );
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'clickOnObject',
                'addSelectionStyles'
            ]
        );
        rectangleHandlerSpy = jasmine.createSpyObj(
            'RectangleHandlerService',
            [
                'calculateRectangle',
                'calculateSquare',
                'dimensions'
            ]
        );
        previsualizationRectangleSpy = jasmine.createSpyObj(
            'PrevisualizationRectangleService',
            [
                'createPrevisualizationRectangle',
                'deletePrevisualization'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'undoRedoService',
            [
                'updateCurrent'
            ]
        );
        rectangleHandlerSpy.dimensions = {coordinates: {x: 0, y: 0}, width: 0, height: 0};
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new EllipseHandlerService(svgHandlerServiceSpy, colorManager,
            clickerHandlerSpy, rectangleHandlerSpy, undoRedoSpy, previsualizationRectangleSpy);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create a ellipse with fill true', () => {
        fill = true;
        stroke = true;
        service.createElement(fill, stroke, size, ElementType.ellipse);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(8);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a ellipse with fill false', () => {
        fill = false;
        stroke = false;
        service.createElement(fill, stroke, size, ElementType.ellipse);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(8);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a ellipse previsualization with fill false and no oldchild', () => {
        fill = false;
        stroke = true;
        service.elementPrevisualization(fill, stroke, size, ElementType.ellipse, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a ellipse previsualization with fill true and no oldchild', () => {
        fill = true;
        stroke = false;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.ellipse, undefined);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should create a ellipse previsualization with fill true and oldchild', () => {
        fill = true;
        stroke = true;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.ellipse, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should create a ellipse previsualization with fill false and oldchild', () => {
        fill = false;
        stroke = false;
        oldChildMock = jasmine.createSpyObj('SVGElement', ['getAttribute']);
        service.elementPrevisualization(fill, stroke, size, ElementType.ellipse, oldChildMock);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(5);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
    });

    it('should calculate square with startCoordinates > endCoordinates', () => {
        rectangleHandlerSpy.dimensions.coordinates.x = 15;
        rectangleHandlerSpy.dimensions.coordinates.y = 70;
        rectangleHandlerSpy.dimensions.width = 100;
        rectangleHandlerSpy.dimensions.height = 70;
        const startCoordinates = { x: 200, y: 250 };
        const endCoordinates = { x: 100, y: 100 };
        service.calculateEllipse(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(155);
        expect(service.dimensions.coordinates.y).toEqual(220);
        expect(service.dimensions.width).toEqual(35);
        expect(service.dimensions.height).toEqual(20);
    });

    it('should calculate circle', () => {
        rectangleHandlerSpy.dimensions.coordinates.x = 15;
        rectangleHandlerSpy.dimensions.coordinates.y = 70;
        rectangleHandlerSpy.dimensions.width = 100;
        rectangleHandlerSpy.dimensions.height = 70;
        service.calculateCircle(size);
        expect(service.dimensions.coordinates.x).toEqual(65);
        expect(service.dimensions.coordinates.y).toEqual(105);
        expect(service.dimensions.width).toEqual(25);
        expect(service.dimensions.height).toEqual(25);
    });

    it('should calculate ellipse with startCoordinates < endCoordinates', () => {
        rectangleHandlerSpy.dimensions.coordinates.x = 2;
        rectangleHandlerSpy.dimensions.coordinates.y = 2;
        rectangleHandlerSpy.dimensions.width = 10;
        rectangleHandlerSpy.dimensions.height = 20;
        const startCoordinates = { x: 100, y: 100 };
        const endCoordinates = { x: 200, y: 250 };
        service.calculateEllipse(startCoordinates, endCoordinates, size);
        expect(service.dimensions.coordinates.x).toEqual(2);
        expect(service.dimensions.coordinates.y).toEqual(7);
        expect(service.dimensions.width).toEqual(0);
        expect(service.dimensions.height).toEqual(5);
    });

    it('should delete previsualization', () => {
        service.deletePrevisualization();
        expect(previsualizationRectangleSpy.deletePrevisualization).toHaveBeenCalledTimes(1);
    });
});
