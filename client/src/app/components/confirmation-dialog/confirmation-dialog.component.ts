import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogData } from 'src/app/interfaces-enums/dialog-data';

@Component({
    selector: 'app-confirmation-dialog',
    templateUrl: './confirmation-dialog.component.html',
    styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {
    message: string;
    confirm: string;
    cancel: string;

    constructor(
        @Inject(MAT_DIALOG_DATA) data: DialogData,
        private dialogRef: MatDialogRef<ConfirmationDialogComponent>
    ) {
        this.message = data.message;
    }

    closeDialog(): void {
        this.dialogRef.close(true);
    }

}
