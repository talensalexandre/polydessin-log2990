import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { AllColorGradientComponent } from '../all-color-gradient/all-color-gradient.component';
import { AlphaColorGradientComponent } from '../alpha-color-gradient/alpha-color-gradient.component';
import { MainColorGradientComponent } from '../main-color-gradient/main-color-gradient.component';
import { ColorPopupComponent } from './color-popup.component';

describe('ColorPopupComponent', () => {
    let component: ColorPopupComponent;
    let fixture: ComponentFixture<ColorPopupComponent>;
    let colorManagerSpy: jasmine.SpyObj<ColorManagerService>;
    const mainDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const secondaryDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const backgroundDefaultColor: RGBA = {
        Dec: { Red: 255, Green: 255, Blue: 255, Alpha: 1 },
        Hex : { Red: 'ff', Green: 'ff', Blue: 'ff' },
        inString: 'rgba(255, 255, 255, 1)'
    };

    const mockDialogRef = {
        close: jasmine.createSpy('close')
    };

    beforeEach(() => {
        colorManagerSpy = jasmine.createSpyObj(
            'ColorManagerService',
            [
                'updateColorWithHex',
                'updateColorWithRGBA',
                'getColorStringWithCustomAlpha'
            ]
        );
        colorManagerSpy.colorSelected = new Array<RGBA>();
        colorManagerSpy.colorSelected[ColorChoice.mainColor] = mainDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.secondaryColor] = secondaryDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.backgroundColor] = backgroundDefaultColor;
        colorManagerSpy.getColorStringWithCustomAlpha.and.returnValue('rgba(255,255,255,1)');
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatFormFieldModule, MatInputModule, FormsModule,
                    BrowserAnimationsModule
                ],
            declarations:
                [
                    ColorPopupComponent, MainColorGradientComponent,
                    AlphaColorGradientComponent, AllColorGradientComponent
                ],
            providers: [{ provide: MatDialogRef, useValue: mockDialogRef },
            { provide: MAT_DIALOG_DATA, useValue: ColorChoice.mainColor },
            { provide: ColorManagerService, useValue: colorManagerSpy }]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorPopupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should close dialog', () => {
        component.closeWindow();
        expect(mockDialogRef.close).toHaveBeenCalledTimes(1);
    });

    it('should update hex', () => {
        component.updateHex();
        expect(colorManagerSpy.updateColorWithHex).toHaveBeenCalledTimes(1);
    });

    it('should update history when clicked left', () => {
        const buttonType = new MouseEvent('click', {buttons: 1});
        component.colorHistory = [mainDefaultColor, secondaryDefaultColor, backgroundDefaultColor];
        component.mouseClickOnHistory(buttonType, mainDefaultColor);
        expect(colorManagerSpy.updateColorWithRGBA).toHaveBeenCalledTimes(1);
    });

    it('should update history when clicked right', () => {
        const buttonType = new MouseEvent('click', {button: 2});
        component.colorHistory = [mainDefaultColor, secondaryDefaultColor, backgroundDefaultColor];
        component.mouseClickOnHistory(buttonType, mainDefaultColor);
        expect(colorManagerSpy.updateColorWithRGBA).toHaveBeenCalledTimes(1);
    });

    it('should not update history when clicked 3', () => {
        const buttonType = new MouseEvent('click', {buttons: 3});
        component.colorHistory = [mainDefaultColor, secondaryDefaultColor, backgroundDefaultColor];
        component.mouseClickOnHistory(buttonType, mainDefaultColor);
        expect(colorManagerSpy.updateColorWithRGBA).toHaveBeenCalledTimes(0);
    });

    it('should prevent default action for right click', () => {
        const clickSpy = jasmine.createSpyObj('MouseEvent', ['preventDefault']);
        component.contextMenu(clickSpy);
        expect(clickSpy.preventDefault).toHaveBeenCalledTimes(1);
    });
});
