import { TestBed } from '@angular/core/testing';
import { GridService } from './grid.service';

// tslint:disable:no-magic-numbers max-file-line-count

const MAXIMUM_SIZE = 200;
const MINIMUM_SIZE = 50;

describe('GridService', () => {
    let service: GridService;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        service = TestBed.get(GridService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should compute size indicators', () => {
        service.size = 50;
        service.changeGridSize();
        expect(service.verticalPath).toBe('M 50 0 V50');
        expect(service.horizontalPath).toBe('M 0 50 H50');

        service.size = 150;
        service.changeGridSize();
        expect(service.verticalPath).toBe('M 150 0 V150');
        expect(service.horizontalPath).toBe('M 0 150 H150');
    });

    it('should have default values', () => {
        expect(service.size).toBe(100);
        expect(service.verticalPath).toBe('M 100 0 V100');
        expect(service.horizontalPath).toBe('M 0 100 H100');
    });

    it('should handle shortcuts', () => {
        expect(service.isActive).toBeFalsy();
        service.handleKey('g');
        expect(service.isActive).toBeTruthy();
        expect(service.size).toBe(100);
        expect(service.opacity).toBe(100);

        service.handleKey('+');
        expect(service.isActive).toBeTruthy();
        expect(service.size).toBe(105);
        expect(service.opacity).toBe(100);

        service.handleKey('-');
        expect(service.isActive).toBeTruthy();
        expect(service.size).toBe(100);
        expect(service.opacity).toBe(100);
    });

    it('should do nothing is other key is pressed', () => {
        service.handleKey('c');
        expect(service.isActive).toBeFalsy();
        expect(service.size).toBe(100);
        expect(service.opacity).toBe(100);
    });

    it('should update size if below lower limit', () => {
        service.size = -10;
        service.changeGridSize();
        expect(service.size).toEqual(MINIMUM_SIZE);
    });

    it('should update size if above upper limit', () => {
        service.size = 300;
        service.changeGridSize();
        expect(service.size).toEqual(MAXIMUM_SIZE);
    });

    it('should not update size if input is valid', () => {
        service.size = 180;
        service.changeGridSize();
        expect(service.size).toEqual(180);
    });

    it('should handle limit with shortcuts', () => {
        service.size = MAXIMUM_SIZE;
        service.handleKey('+');
        expect(service.size).toBe(MAXIMUM_SIZE);

        service.size = MINIMUM_SIZE;
        service.handleKey('-');
        expect(service.size).toBe(MINIMUM_SIZE);
    });

    it('should increase grid size', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'changeGridSize').and.callThrough();
        service.increaseGridSize();
        expect(service.changeGridSize).toHaveBeenCalledTimes(1);
    });

    it('should decrease grid size', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'changeGridSize').and.callThrough();
        service.decreaseGridSize();
        expect(service.changeGridSize).toHaveBeenCalledTimes(1);
    });
});
