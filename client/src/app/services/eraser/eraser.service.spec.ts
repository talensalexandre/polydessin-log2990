import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { DrawingService } from '../drawing/drawing.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
import { EraserService } from './eraser.service';

// tslint:disable: no-string-literal no-magic-numbers

describe('EraserService', () => {
    let service: EraserService;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let undoRedoSpy: jasmine.SpyObj<UndoRedoService>;
    let elementMock: jasmine.SpyObj<SVGElement>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    let fakeElement: SVGElement;
    let fakeArray: Element[];
    const coordinates = { x: 100, y: 100 };
    const size = 18;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'removeStyle',
                'setStyle'
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        drawingServiceSpy = jasmine.createSpyObj(
            'DrawingService',
            [
                'removeElement'
            ]
        );
        undoRedoSpy = jasmine.createSpyObj(
            'UndoRedoService',
            [
                'updateCurrent'
            ]
        );
        elementMock = jasmine.createSpyObj(
            'SVGElement',
            [
                'getAttribute',
                'setAttribute',
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        service = new EraserService(svgHandlerServiceSpy, drawingServiceSpy, undoRedoSpy, saveManagerSpy);
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
        fakeElement.setAttribute('stroke-width', '10');
        fakeArray = new Array();
        fakeArray.push(fakeElement);
        svgHandlerServiceSpy.elements = fakeArray as SVGElement[];
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create eraser', () => {
        rendererSpy.createElement.and.returnValue(elementMock);
        const child = service.moveEraser(coordinates, size);
        expect(rendererSpy.createElement).toHaveBeenCalled();
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(4);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
        expect(child).toBe(elementMock);
    });

    it('should not create eraser, juste change attributes', () => {
        service['eraser'] = elementMock;
        const child = service.moveEraser(coordinates, size);
        expect(rendererSpy.createElement).toHaveBeenCalledTimes(0);
        expect(rendererSpy.setAttribute).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(0);
        expect(child).toBe(elementMock);
    });

    it('should find selectedElements and highlight if not mouse Down', () => {
        spyOn(document, 'elementsFromPoint').and.returnValue(fakeArray);
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'highlightElements');
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'eraseElements');
        service.getSelectedElements(size, coordinates, false);
        expect(document.elementsFromPoint).toHaveBeenCalledTimes(49);
        expect(service['highlightElements']).toHaveBeenCalled();
        expect(service['eraseElements']).not.toHaveBeenCalled();
    });

    it('should find selectedElements and highlight if mouse Down', () => {
        spyOn(document, 'elementsFromPoint').and.returnValue(fakeArray);
        svgHandlerServiceSpy.elements.push(fakeElement);
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'highlightElements');
        // tslint:disable-next-line: no-any
        spyOn<any>(service, 'eraseElements');
        service.getSelectedElements(size, coordinates, true);
        expect(document.elementsFromPoint).toHaveBeenCalledTimes(49);
        expect(service['highlightElements']).not.toHaveBeenCalled();
        expect(service['eraseElements']).toHaveBeenCalled();
    });

    it('should find svg:g selected Elements', () => {
        const fakesvgG = document.createElementNS('http://www.w3.org/2000/svg', 'svg:g');
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
        fakesvgG.appendChild(fakeElement);
        fakeArray = new Array(fakeElement, fakeElement);
        spyOn(document, 'elementsFromPoint').and.returnValue(fakeArray);
        svgHandlerServiceSpy.elements.push(fakeElement);
        service.getSelectedElements(size, coordinates, true);
        expect(document.elementsFromPoint).toHaveBeenCalledTimes(49);
    });

    it('should erase Elements', () => {
        service['eraseElements'](fakeArray as SVGElement[]);
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(1);
    });

    it('should highlight Elements in darker red', () => {
        fakeElement.setAttribute('stroke', 'rgba(255, 0, 0)');
        fakeArray = new Array(fakeElement, fakeElement);
        service['highlightElements'](fakeArray as SVGElement[]);
        expect(rendererSpy.setStyle).toHaveBeenCalled();
        expect(rendererSpy.removeStyle).not.toHaveBeenCalled();
    });

    it('should highlight Elements in red', () => {
        service['highlightElements'](fakeArray as SVGElement[]);
        expect(rendererSpy.setStyle).not.toHaveBeenCalled();
        expect(rendererSpy.removeStyle).not.toHaveBeenCalled();
    });

    it('should highlight Elements and change size if size to small', () => {
      fakeElement.setAttribute('stroke', 'rgba(0, 255, 0)');
      fakeElement.setAttribute('stroke-width', '4');
      const otherFakeElem = fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
      service['lastSelectedElements'].push(otherFakeElem);
      service['highlightElements'](fakeArray as SVGElement[]);
      expect(rendererSpy.setStyle).toHaveBeenCalled();
      expect(rendererSpy.removeStyle).toHaveBeenCalledTimes(2);
    });

    it('should not highlight Elements if no stroke', () => {
        fakeElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:line');
        service['highlightElements'](fakeArray as SVGElement[]);
        expect(rendererSpy.setStyle).not.toHaveBeenCalled();
        expect(rendererSpy.removeStyle).not.toHaveBeenCalled();
   });

    it('should clear hightLights', () => {
        service['lastSelectedElements'] = fakeArray as SVGElement[];
        service.clearHighlights();
        expect(rendererSpy.removeStyle).toHaveBeenCalledTimes(2);
    });

    it('should add to undo redo', () => {
        service['erasedElements'] = fakeArray as SVGElement[];
        service.addToUndoRedo();
        expect(undoRedoSpy.updateCurrent).toHaveBeenCalledWith(fakeArray, ChangeType.elementRemoved);
        expect(service['erasedElements']).toEqual(new Array());
    });

    it('should not add to undo redo', () => {
        fakeArray = new Array();
        service['erasedElements'] = fakeArray as SVGElement[];
        service.addToUndoRedo();
        expect(rendererSpy.createElement).not.toHaveBeenCalled();
        expect(rendererSpy.appendChild).not.toHaveBeenCalled();
    });

    it('should delete eraser', () => {
        service['eraser'] = fakeElement;
        service.deleteEraser();
        expect(service['eraser']).toBeFalsy();
    });
});
