import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { States } from 'src/app/interfaces-enums/states';
import { ClipboardService } from 'src/app/services/clipboard/clipboard.service';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { GridService } from 'src/app/services/grid/grid.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';
import { ShortcutHandlerService } from '../../services/shortcut-handler/shortcut-handler.service';
import { UndoRedoService } from '../../services/undo-redo/undo-redo.service';
import { CreateNewDrawingComponent } from '../create-new-drawing/create-new-drawing.component';
import { DrawingGalleryComponent } from '../drawing-gallery/drawing-gallery.component';
import { EntryPointComponent } from '../entry-point/entry-point.component';
import { ExportDrawingComponent } from '../export-drawing/export-drawing.component';
import { SaveManagerComponent } from '../save-manager/save-manager.component';

const SIDENAV_NORMAL_WIDTH = 100;
const SIDENAV_MAX_WIDTH = 115;
const STATES_KEYS = new Map<string, States>();
STATES_KEYS.set('c', States.pencil)
    .set('a', States.spray)
    .set('w', States.brush)
    .set('l', States.line)
    .set('1', States.rectangle)
    .set('s', States.selection)
    .set('2', States.ellipse)
    .set('r', States.colorApplicator)
    .set('i', States.colorPicker)
    .set('e', States.eraser)
    .set('3', States.polygon)
    .set('b', States.paintBucket);
const EXPORT_KEY = 'e';
const NEW_DRAWING_KEY = 'o';
const SAVE_KEY = 's';
const UNDO_KEY = 'z';
const REDO_KEY = 'Z';
const GRID_KEYS = ['+', '-', 'g'];
const DRAWING_GALLERY_KEY = 'g';
const MANIPULATION_KEYS_CTRL = ['x', 'c', 'v', 'd', 'a'];
const MANIPULATION_KEY_DELETE = 'Delete';
const ENTRY_POINT_HEIGHT = '675px';
const ENTRY_POINT_EXPORT_WIDTH = '850px';
const AUTO_SIZE = 'auto';
const SIZE_30_PERCENT = '30%';
const SIZE_80_PERCENT = '80%';
const DIALOG_MIN_WIDTH = '300px';
const SIDENAV_HEIGHT_LIMIT = 881;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
    sidenavOpen: boolean;
    States: typeof States = States;
    iconFooterClass: string;
    sidenavClass: string;

    constructor(
        private dialog: MatDialog,
        private stateService: StateSelectorService,
        private userGuideService: UserGuideService,
        public newDrawingService: CreateNewDrawingService,
        public svgHandlerService: SvgHandlerService,
        private mouseHandlerService: MouseHandlerService,
        public undoRedoService: UndoRedoService,
        private renderer: Renderer2,
        public gridService: GridService,
        private shortcutHandler: ShortcutHandlerService,
        public clipboardHandler: ClipboardService,
        private cdRef: ChangeDetectorRef,
    ) {
        this.svgHandlerService.renderer = this.renderer;
        this.bindStateKeys();
        this.sidenavOpen = false;
        this.iconFooterClass = '';
        this.sidenavClass = '';
        this.getScreenHeight();
        this.newDrawingService.sidenavWidth = SIDENAV_NORMAL_WIDTH;
    }

    @ViewChild('workspace', { static: false }) workspace: ElementRef;

    @HostListener('window:keydown', ['$event']) handleKeyDown(event: KeyboardEvent): void {
        if (
            !this.dialog.openDialogs.length &&
            !this.mouseHandlerService.isMouseDown &&
            !this.svgHandlerService.isLineBeingCreated &&
            !this.newDrawingService.blockNavigation
        ) {
            (!this.stateService.isModifyingInput && !event.ctrlKey) ? this.shortcutHandler.executeShortcut(event) :
                this.shortcutHandler.executeCtrlShortcut(event);
        }
        if (this.dialog.openDialogs.length && event.ctrlKey) {
            event.preventDefault();
        }
    }

    @HostListener('window:resize') getScreenHeight(): void {
        if (window.innerHeight <= SIDENAV_HEIGHT_LIMIT) {
            this.iconFooterClass = 'bottomRelative';
            this.sidenavClass = 'leftSidenavLong';
            this.newDrawingService.sidenavWidth = SIDENAV_MAX_WIDTH;
        } else {
            this.iconFooterClass = 'bottomStick';
            this.sidenavClass = 'leftSidenavNormal';
            this.newDrawingService.sidenavWidth = SIDENAV_NORMAL_WIDTH;
        }
    }

    ngOnInit(): void {
        this.dialog.open(EntryPointComponent, {
            height: ENTRY_POINT_HEIGHT,
            width: ENTRY_POINT_EXPORT_WIDTH,
            disableClose: true,
            autoFocus: false,
        });
    }

    ngAfterViewInit(): void {
        this.svgHandlerService.workspace = this.workspace.nativeElement;
    }

    saveDrawing(): void {
        this.dialog.open(SaveManagerComponent, {
            height: AUTO_SIZE,
            width: SIZE_30_PERCENT,
            autoFocus: false
        });
    }

    private bindStateKeys(): void {
        STATES_KEYS.forEach((value, key) => {
            this.shortcutHandler.addShortcut(key, () => this.changeState(value));
        });
        GRID_KEYS.forEach((value) => {
            this.shortcutHandler.addShortcut(value, () => this.gridService.handleKey(value));
        });
        MANIPULATION_KEYS_CTRL.forEach((value) => {
            this.shortcutHandler.addCtrlShortcut(value, () => {
                this.changeState(States.selection);
                this.clipboardHandler.handleKey(value);
            });
        });
        this.shortcutHandler.addShortcut(MANIPULATION_KEY_DELETE, () => this.clipboardHandler.handleKey(MANIPULATION_KEY_DELETE));
        this.shortcutHandler.addCtrlShortcut(NEW_DRAWING_KEY, () => {
            this.changeState(States.newDrawing);
            this.createNewDrawing();
        });
        this.shortcutHandler.addCtrlShortcut(EXPORT_KEY, () => {
            this.changeState(States.drawingExport);
            this.exportDrawing();
        });
        this.shortcutHandler.addCtrlShortcut(DRAWING_GALLERY_KEY, () => {
            this.changeState(States.drawingGallery);
            this.openDrawingGallery();
        });
        this.shortcutHandler.addCtrlShortcut(SAVE_KEY, () => {
            this.changeState(States.save);
            this.saveDrawing();
        });
        this.shortcutHandler.addCtrlShortcut(UNDO_KEY, () => {
            this.undoRedoService.undo();
        });
        this.shortcutHandler.addCtrlShortcut(REDO_KEY, () => {
            this.undoRedoService.redo();
        });
    }

    changeState(stateWanted: States): void {
        this.sidenavOpen = this.stateService.changeState(stateWanted, this.sidenavOpen);
        this.userGuideService.accesFromEntryPoint = false;
        this.cdRef.detectChanges();
    }

    isCorrectState(stateWanted: States): boolean {
        if (stateWanted === this.stateService.stateSelected) {
            return true;
        }
        return false;
    }

    createNewDrawing(): void {
        this.newDrawingService.isFromEntryPoint = false;
        this.sidenavOpen = this.stateService.changeState(States.newDrawing, this.sidenavOpen);
        this.dialog.open(CreateNewDrawingComponent,
            {
                height: AUTO_SIZE,
                width: AUTO_SIZE,
                minWidth: DIALOG_MIN_WIDTH,
                disableClose: true,
                autoFocus: false
            });
    }

    openDrawingGallery(): void {
        this.newDrawingService.isFromEntryPoint = false;
        this.stateService.changeState(States.drawingGallery, this.sidenavOpen);
        this.dialog.open(DrawingGalleryComponent,
            {
                height: SIZE_80_PERCENT,
                width: SIZE_80_PERCENT, minWidth: DIALOG_MIN_WIDTH,
                autoFocus: false
            });
    }

    exportDrawing(): void {
        this.stateService.changeState(States.drawingExport, this.sidenavOpen);
        this.dialog.open(ExportDrawingComponent,
            {
                height: AUTO_SIZE,
                width: ENTRY_POINT_EXPORT_WIDTH,
                disableClose: true,
                autoFocus: false
            });
    }
}
