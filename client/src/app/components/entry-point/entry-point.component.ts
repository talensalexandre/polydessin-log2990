import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DrawingStored } from 'src/app/interfaces-enums/drawing';
import { States } from 'src/app/interfaces-enums/states';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { CreateNewDrawingComponent } from '../create-new-drawing/create-new-drawing.component';
import { DrawingGalleryComponent } from '../drawing-gallery/drawing-gallery.component';

const ENTRY_POINT_HEIGHT = '675px';
const ENTRY_POINT_WIDTH = '850px';
const AUTO_SIZE = 'auto';
const SIZE_80_PERCENT = '80%';
const DIALOG_MIN_WIDTH = '300px';

@Component({
    selector: 'app-entry-point',
    templateUrl: './entry-point.component.html',
    styleUrls: ['./entry-point.component.scss'],
})
export class EntryPointComponent {
    localSaveDrawing: DrawingStored;
    subscribeMethod = () => this.openEntryPoint();

    constructor(public dialogRef: MatDialogRef<EntryPointComponent>,
                private stateSelector: StateSelectorService,
                public dialog: MatDialog,
                private newDrawingService: CreateNewDrawingService) {
                    this.localSaveDrawing = JSON.parse(localStorage.getItem('drawing') as string);
                }

    closeDialog(): void {
        this.dialogRef.close();
    }

    createNewDrawing(): void {
        this.closeDialog();
        this.stateSelector.changeState(States.newDrawing, false);
        const newDrawingDialog = this.dialog.open(CreateNewDrawingComponent,
            {
                height: AUTO_SIZE,
                width: AUTO_SIZE,
                disableClose: true,
                autoFocus: false
            });
        newDrawingDialog.componentInstance.openEntryPoint.subscribe(this.subscribeMethod);
    }

    continueDrawing(): void {
        this.newDrawingService.continueDrawing(this.localSaveDrawing);
        this.closeDialog();
    }

    openDrawingGallery(): void {
        this.closeDialog();
        this.stateSelector.changeState(States.drawingGallery, false);
        const drawingGalleryDialog = this.dialog.open(DrawingGalleryComponent,
            {
                height: SIZE_80_PERCENT,
                width: SIZE_80_PERCENT, minWidth: DIALOG_MIN_WIDTH,
                disableClose: true,
                autoFocus: false
            });
        drawingGalleryDialog.componentInstance.openEntryPoint.subscribe(this.subscribeMethod);
    }

    openEntryPoint(): void {
        this.dialog.open(EntryPointComponent, {
            height: ENTRY_POINT_HEIGHT,
            width: ENTRY_POINT_WIDTH,
            disableClose: true,
            autoFocus: false
        });
    }

    goToDocumentation(): void {
        this.stateSelector.changeState(States.documentation, false);
        this.dialogRef.close();
    }
}
