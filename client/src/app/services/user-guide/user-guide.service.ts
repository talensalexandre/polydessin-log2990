import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { EntryPointComponent } from '../../components/entry-point/entry-point.component';
import { States } from '../../interfaces-enums/states';
import { UserGuidePages } from '../../interfaces-enums/user-guide-pages';
import { StateSelectorService } from '../state-selector/state-selector.service';

const ENTRY_POINT_HEIGHT = '675px';
const ENTRY_POINT_WIDTH = '850px';

@Injectable({
    providedIn: 'root',
})
export class UserGuideService {
    accesFromEntryPoint: boolean;
    pages: typeof UserGuidePages = UserGuidePages;
    currentPage: number;
    readonly NUMBER_OF_PAGES: number = 22;

    constructor(private stateSelectorService: StateSelectorService,
                public dialog: MatDialog) {
        this.accesFromEntryPoint = true;
    }

    closeGuide(): void {
        if (this.accesFromEntryPoint) {
            this.dialog.open(EntryPointComponent, {
                height: ENTRY_POINT_HEIGHT,
                width: ENTRY_POINT_WIDTH,
                disableClose: true,
                autoFocus: false
            });
        }
        this.stateSelectorService.changeState(States.defaultState, true);
    }

    goToNextPage(): void {
        if (this.checkCurrentPage()) {
            this.currentPage++;
        }
    }

    goToPreviousPage(): void {
        if (this.checkCurrentPage()) {
            this.currentPage--;
        }
    }

    goToPage(page: number): void {
        if (this.checkCurrentPage()) {
            this.currentPage = page;
        }
    }

    checkPageNumber(page: number): boolean {
        return this.currentPage === page;
    }

    private checkCurrentPage(): boolean {
        if (this.currentPage > this.NUMBER_OF_PAGES - 1) {
            this.currentPage = 0;
            return false;
        } else if (this.currentPage < 0) {
            this.currentPage = 0;
            return false;
        }
        return true;
    }
}
