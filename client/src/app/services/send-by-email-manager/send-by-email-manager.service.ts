import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SendByEmailManagerService {

    constructor(private http: HttpClient) { }

    sendDrawingbyEmail(to: string, payload: string, name: string, type: string): Observable<number> {
        const payloadInBase64 = payload.split(',')[1];
        const data = [payloadInBase64, to, name, type];
        return this.http.post<number>(environment.SEND_EMAIL_URL, data)
            .pipe(catchError((error: HttpErrorResponse) => {
                return of(error.status);
        }));
    }
}
