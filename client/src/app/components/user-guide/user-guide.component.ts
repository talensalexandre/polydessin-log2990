import { Component } from '@angular/core';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';

@Component({
    selector: 'app-user-guide',
    templateUrl: './user-guide.component.html',
    styleUrls: ['./user-guide.component.scss'],
})
export class UserGuideComponent {

    constructor(public userGuideService: UserGuideService) {
        this.userGuideService.currentPage = 0;
    }
}
