import { Component } from '@angular/core';
import { UserGuideService } from 'src/app/services/user-guide/user-guide.service';

@Component({
    selector: 'app-functionalities-doc',
    templateUrl: './functionalities-doc.component.html',
    styleUrls: ['./functionalities-doc.component.scss']
})
export class FunctionalitiesDocComponent {

    constructor(public userGuideService: UserGuideService) { }
}
