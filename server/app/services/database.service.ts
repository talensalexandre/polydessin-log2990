import * as dotenv from 'dotenv';
import { injectable } from 'inversify';
import { Collection, FilterQuery, MongoClient, MongoClientOptions, UpdateQuery } from 'mongodb';
import 'reflect-metadata';
import { Message } from '../../../common/communication/message';
import { DrawingStored } from '../drawing';

@injectable()
export class DatabaseService {
    private collection: Collection<DrawingStored>;

    private options: MongoClientOptions = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };

    constructor() {
        dotenv.config();
        MongoClient.connect(process.env.DB_URL as string, this.options)
            .then((client: MongoClient) => {
                this.collection = client.db(process.env.DB_NAME as string).collection(process.env.DB_COLLECTION as string);
            })
            .catch(() => {
                console.error('Connection error to DB. Please try again or contact the sysadmin.');
                process.exit(1);
            });
    }

    homePage(): Message {
        return {
            title: 'Home Page',
            body: 'If this message appears, your request is invalid',
        };
    }

    async getAllDrawings(): Promise<DrawingStored[]> {
        return this.collection
            .find({})
            .toArray()
            .then((drawings: DrawingStored[]) => {
                return drawings;
            });
    }

    async getDrawingWithTimestamp(timestamp: string): Promise<DrawingStored> {
        return this.collection
            .findOne({
                $or: [{ 'metaDrawing.timestamp': Number(timestamp) }],
            })
            .then((drawing: DrawingStored) => {
                return drawing;
            });
    }

    async addDrawing(drawing: DrawingStored): Promise<void> {
        if (this.validateDrawing(drawing)) {
            this.collection.insertOne(drawing);
        } else {
            throw new Error('Invalid drawing.');
        }
    }

    async deleteDrawing(timestamp: string): Promise<void> {
        this.collection.findOneAndDelete({
            $or: [{ 'metaDrawing.timestamp': Number(timestamp) }],
        });
    }

    async modifyDrawing(drawing: DrawingStored): Promise<void> {
        const filterQuery: FilterQuery<DrawingStored> = { timestamp: drawing.metaDrawing.timestamp };
        const updateQuery: UpdateQuery<DrawingStored> = {
            $set: {
                timestamp: drawing.metaDrawing.timestamp,
                name: drawing.metaDrawing.name,
                labels: drawing.metaDrawing.labels,
                drawingData: drawing.svgContainer,
            },
        };
        this.collection.updateOne(filterQuery, updateQuery).then(() => {
            return;
        });
    }

    private validateDrawing(drawing: DrawingStored): boolean {
        return this.validateName(drawing.metaDrawing.name) && this.validateDrawingData(drawing.svgContainer);
    }

    validateName(name: string): boolean {
        return name.length > 0;
    }

    validateDrawingData(dataDrawing: string[]): boolean {
        return dataDrawing.length > 0;
    }
}
