import { Component } from '@angular/core';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { ElementType } from 'src/app/interfaces-enums/element-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PolygonHandlerService } from 'src/app/services/polygon-handler/polygon-handler.service';
import { PrevisualisationRectangleService } from 'src/app/services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractShapeComponent } from '../abstract-tool/abstract-shape.component';

const DEFAULT_SIDES = 3;
const MAX_NUM_OF_SIDES = 12;
const MIN_NUM_OF_SIDES = 3;

@Component({
    selector: 'app-polygon',
    templateUrl: './polygon.component.html',
    styleUrls: ['./polygon.component.scss']
})
export class PolygonComponent extends AbstractShapeComponent {
    numOfSides: number;
    private polygonCoordinates: Coordinates[];

    constructor(
        protected mouseHandler: MouseHandlerService,
        protected svgHandler: SvgHandlerService,
        protected drawingService: DrawingService,
        public stateSelector: StateSelectorService,
        private polygoneHandler: PolygonHandlerService,
        protected saveManager: SaveManagerService,
        private previsualizationRectangle: PrevisualisationRectangleService
    ) {
        super(mouseHandler, svgHandler, stateSelector, drawingService, saveManager);
        this.numOfSides = DEFAULT_SIDES;
    }

    protected destroyExtend(): void {
        this.isDrawing = false;
        return;
    }

    changeNumOfSides(): void {
        this.numOfSides = Math.round(this.numOfSides);
        this.numOfSides = (this.numOfSides < MIN_NUM_OF_SIDES) ? MIN_NUM_OF_SIDES : this.numOfSides;
        this.numOfSides = (this.numOfSides > MAX_NUM_OF_SIDES) ? MAX_NUM_OF_SIDES : this.numOfSides;
        this.stateSelector.isModifyingInput = false;
    }

    protected mouseDownExtend(): void {
        this.previsualisationElement = this.polygoneHandler.elementPrevisualization(
            this.fill,
            this.stroke,
            this.size,
            ElementType.polygon,
            this.previsualisationElement);
    }

    protected handleKeyDownExtend(): void {
        return;
    }

    protected handleKeyUpExtend(): void {
        return;
    }

    protected mouseMouvementCalculateShiftShape(): void {
        return;
    }

    protected mouseMouvementCalculateNormalShape(): void {
        return;
    }

    protected mouseMouvementPrevisualization(): void {
        this.polygoneHandler.removeAllCoordinatesFromPolygon(this.previsualisationElement);
        const startCoords = this.mouseHandler.startCoordinates;
        const actCoords = this.mouseHandler.actualCoordinates;
        this.polygonCoordinates =
        this.polygoneHandler.getCoordinatesForPolygon(startCoords, actCoords, this.numOfSides, this.size);
        this.isSelectionTooSmall = this.polygoneHandler.isShapeTooBigComparedToSize(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        if (!this.isSelectionTooSmall) {
            this.polygonCoordinates.forEach((coords) => {
                this.polygoneHandler.addCoordinatesToPolygon(this.previsualisationElement, coords);
            });
        }
        this.previsualizationRectangle.createPrevisualizationRectangle();
    }

    protected mouseUpCreateShape(): void {
        this.svgElement = this.polygoneHandler.createElement(this.fill, this.stroke, this.size, ElementType.polygon);
        this.polygonCoordinates.forEach((coords) => {
            this.polygoneHandler.addCoordinatesToPolygon(this.svgElement, coords);
        });
    }

    protected mouseLeftExtend(): void {
        this.previsualizationRectangle.deletePrevisualization();
    }

    protected mouseUpExtendExtra(): void {
        this.drawingService.removeElement(this.previsualisationElement);
        delete this.previsualisationElement;
        this.previsualizationRectangle.deletePrevisualization();
    }

    increaseNumOfSides(): void {
        this.numOfSides++;
        this.changeNumOfSides();
    }

    decreaseNumOfSides(): void {
        this.numOfSides--;
        this.changeNumOfSides();
    }
}
