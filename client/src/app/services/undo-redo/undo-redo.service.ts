import { Injectable } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { AttributeType } from '../../interfaces-enums/attribute-type';
import { TempElement } from '../../interfaces-enums/temp-element';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';

@Injectable({
    providedIn: 'root',
})
export class UndoRedoService {
    current: TempElement[] ;
    private undos: TempElement[];
    private redos: TempElement[];
    showUndo: boolean;
    showRedo: boolean;
    private canRedo: boolean;
    private elementToAdd: TempElement;
    private elementBeforeTransform: TempElement;
    private oldColorFill: string;
    private oldColorStroke: string;

    constructor(private svgHandler: SvgHandlerService,
                private saveManager: SaveManagerService,
                private selectionSvgHandler: SelectionSvgHandlerService) {
        this.current = new Array<TempElement>();
        this.undos = new Array<TempElement>();
        this.redos = new Array<TempElement>();
        this.showRedo = false;
        this.showUndo = false;
        this.canRedo = false;
    }

    private getColor(svgElement: SVGElement, type: AttributeType): string {
        const strokeColor = svgElement.getAttribute('stroke');
        return (type === AttributeType.stroke) ? (strokeColor ? strokeColor : '') : (svgElement.style.fill ? svgElement.style.fill : '');
    }

    private getTransform(svgElement: SVGElement): string {
        let transform = svgElement.getAttribute('transform');
        transform = transform !== null ? transform : 'undefined';
        return transform;
    }

    addFromColorApplicator(svgElement: SVGElement, type: AttributeType): void {
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = svgElement;
        changeArray[0] = this.getColor(svgElement, type);
        let changeType = ChangeType.colorStroke;
        if (type === AttributeType.fill) {
            changeType = ChangeType.colorFill;
        }
        this.elementToAdd = { changeType, change: changeArray, svgElement: svgElementArray };
        this.current.push(this.elementToAdd);
        this.showUndo = true;
    }

    updateCurrent(svgElements: SVGElement[], changeType: ChangeType): void {
        const changeArray = new Array<string>();
        if (this.canRedo) {
            this.redos = new Array<TempElement>();
            this.showRedo = false;
            this.canRedo = false;
        }
        this.showUndo = true;
        this.elementToAdd = { changeType, change: changeArray, svgElement: svgElements };
        this.current.push(this.elementToAdd);
    }

    addElementBeforeTransform(svgElements: SVGElement[]): void {
        const changeArray = new Array<string>();
        svgElements.forEach((element) => {
            changeArray.push(this.getTransform(element));
        });
        const svgElementsToAdd = new Array<SVGElement>();
        svgElements.forEach((element) => {
            svgElementsToAdd.push(element);
        });
        this.elementBeforeTransform = { changeType: ChangeType.transform, change: changeArray, svgElement: svgElementsToAdd };
    }

    transformCurrent(): void {
        this.current.push(this.elementBeforeTransform);
        this.showUndo = true;
    }

    undo(): void {
        this.canRedo = true;
        let lastElement = this.current.pop();
        if (lastElement !== undefined) {
            this.selectionSvgHandler.unselectAll();
            this.undos.push(lastElement);
            this.undoSwitch();
        }
        lastElement = this.undos.pop();
        if (lastElement !== undefined) {
            this.redos.push(lastElement);
            this.showRedo = true;
        }
        if (this.current.length === 0) {
            this.showUndo = false;
        }
    }

    private undoSwitch(): void {
        const element = this.undos[this.undos.length - 1];
        this.commonSwitch(element);
        switch (element.changeType) {
            case ChangeType.addToCanvas:
                element.svgElement.forEach((value) => {
                    this.svgHandler.renderer.removeChild(this.svgHandler.svgCanvas, value);
                    this.saveManager.removeSvgElement(value);
                    this.svgHandler.elements.splice(this.svgHandler.elements.indexOf(value), 1);
                });
                break;
            case ChangeType.elementRemoved:
                element.svgElement.forEach((value) => {
                    this.svgHandler.renderer.appendChild(this.svgHandler.svgCanvas, value);
                    this.saveManager.addSvgElement(value);
                    this.svgHandler.elements.push(value);
                });
                break;
            default:
                break;
        }
    }

    redo(): void {
        this.canRedo = false;
        if (this.redos.length !== 0) {
            this.selectionSvgHandler.unselectAll();
            this.redoSwitch();
        }
        const lastElement = this.redos.pop();
        if (lastElement !== undefined) {
            this.current.push(lastElement);
            this.showUndo = true;
        }
        if (this.redos.length === 0) {
            this.showRedo = false;
        }
    }

    private redoSwitch(): void {
        const element = this.redos[this.redos.length - 1];
        this.commonSwitch(element);
        switch (element.changeType) {
            case ChangeType.addToCanvas:
                element.svgElement.forEach((value) => {
                    this.svgHandler.renderer.appendChild(this.svgHandler.svgCanvas, value);
                    this.saveManager.addSvgElement(value);
                    this.svgHandler.elements.push(value);
                });
                break;
            case ChangeType.elementRemoved:
                element.svgElement.forEach((value) => {
                    this.svgHandler.renderer.removeChild(this.svgHandler.svgCanvas, value);
                    this.saveManager.removeSvgElement(value);
                    this.svgHandler.elements.splice(this.svgHandler.elements.indexOf(value), 1);
                });
                break;
            default:
                break;
        }
    }

    private commonSwitch(element: TempElement): void {
        switch (element.changeType) {
            case ChangeType.colorStroke:
                this.saveManager.removeSvgElement(element.svgElement[0]);
                this.changeColorStroke(element);
                this.saveManager.addSvgElement(element.svgElement[0]);
                break;
            case ChangeType.colorFill:
                this.saveManager.removeSvgElement(element.svgElement[0]);
                this.changeColorFill(element);
                this.saveManager.addSvgElement(element.svgElement[0]);
                break;
            case ChangeType.transform:
                this.changeTransform(element);
            default:
                break;
        }
    }

    private changeColorStroke(element: TempElement): void {
        this.oldColorStroke = this.getColor(element.svgElement[0], AttributeType.stroke);
        this.svgHandler.renderer.setAttribute(element.svgElement[0], 'stroke', element.change[0]);
        element.change[0] = this.oldColorStroke;
    }

    private changeColorFill(element: TempElement): void {
        this.oldColorFill = this.getColor(element.svgElement[0], AttributeType.fill);
        this.svgHandler.renderer.setStyle(element.svgElement[0], 'fill', element.change[0]);
        element.change[0] = this.oldColorFill;
    }

    private changeTransform(element: TempElement): void {
        let oldTransform: string;
        element.svgElement.forEach((value, key) => {
            this.saveManager.removeSvgElement(value);
            oldTransform = this.getTransform(value);
            this.svgHandler.renderer.setAttribute(value, 'transform', element.change[key]);
            element.change[key] = oldTransform;
            this.saveManager.addSvgElement(value);
        });
    }

    clearAll(): void {
        this.current = new Array<TempElement>();
        this.undos = new Array<TempElement>();
        this.redos = new Array<TempElement>();
        this.showUndo = false;
        this.showRedo = false;
        this.canRedo = false;
    }
}
