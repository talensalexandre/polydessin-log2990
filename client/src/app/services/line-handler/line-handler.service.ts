import { Injectable, Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { DrawingService } from '../drawing/drawing.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const ANGLE_0 = 0;
const ANGLE_45 = 45;
const ANGLE_90 = 90;
const ANGLE_135 = 135;
const ANGLE_180 = 180;
const ANGLE_225 = 225;
const ANGLE_270 = 270;
const ANGLE_315 = 315;
const ANGLE_360 = 360;
const ANGLES = [ANGLE_0, ANGLE_45, ANGLE_90, ANGLE_135, ANGLE_180, ANGLE_225, ANGLE_270, ANGLE_315, ANGLE_360];
const HALF_OPACITY = '0.5';

@Injectable({
    providedIn: 'root'
})
export class LineHandlerService {
    private renderer: Renderer2;
    junctionPoints: SVGElement[];
    private svgG: SVGElement;

    constructor(
        private svgHandler: SvgHandlerService,
        private drawingService: DrawingService,
        private colorManager: ColorManagerService,
        private clickerHandler: ClickerHandlerService,
        private undoRedoService: UndoRedoService
    ) {
        this.renderer = this.svgHandler.renderer;
        this.junctionPoints = new Array<SVGElement>();
    }

    linePrevisualization(
        startCoordinates: Coordinates,
        endCoordinates: Coordinates,
        size: string,
        oldChild: (SVGElement | undefined)
    ): SVGElement {
        if (!oldChild) {
            oldChild = this.renderer.createElement('svg:line', this.svgHandler.SVG_LINK);
            this.renderer.setStyle(oldChild, 'stroke-opacity', HALF_OPACITY);
            this.renderer.setStyle(oldChild, 'stroke-linecap', 'round');
            this.renderer.appendChild(this.svgHandler.svgCanvas, oldChild);
        }
        this.renderer.setAttribute(oldChild, 'stroke', this.colorManager.colorSelected[ColorChoice.mainColor].inString);
        this.renderer.setAttribute(oldChild, 'stroke-width', size);
        this.renderer.setAttribute(oldChild, 'x1', String(startCoordinates.x));
        this.renderer.setAttribute(oldChild, 'y1', String(startCoordinates.y));
        this.renderer.setAttribute(oldChild, 'x2', String(endCoordinates.x));
        this.renderer.setAttribute(oldChild, 'y2', String(endCoordinates.y));
        return oldChild as SVGElement;
    }

    shiftPrevisualization(
        startCoordinates: Coordinates,
        endCoordinates: Coordinates,
        size: string,
        oldChild: SVGElement
    ): SVGElement {
        const vector: Coordinates = { x: endCoordinates.x - startCoordinates.x, y: startCoordinates.y - endCoordinates.y };
        let angle = (vector.x === 0) ? 0 : Math.atan(vector.y / vector.x) * (ANGLE_180 / Math.PI);

        if (vector.x > 0) {
            angle += (angle > 0) ? 0 : ANGLE_360;
        } else if (vector.x < 0) {
            angle += ANGLE_180;
        } else {
            angle = (vector.y >= 0) ? ANGLE_90 : ANGLE_270;
        }

        const closestAngle = ANGLES.reduce((prev, curr) => Math.abs(curr - angle) < Math.abs(prev - angle) ? curr : prev);
        const newCoordinates: Coordinates = { x: endCoordinates.x, y: endCoordinates.y };

        switch (closestAngle) {
            case ANGLE_0:
            case ANGLE_180:
            case ANGLE_360:
                newCoordinates.y = startCoordinates.y;
                break;
            case ANGLE_90:
            case ANGLE_270:
                newCoordinates.x = startCoordinates.x;
                break;
            default:
                newCoordinates.y = ((vector.x * vector.y) > 0) ? (startCoordinates.y - vector.x) : (startCoordinates.y + vector.x);
        }
        if (newCoordinates.y <= 0 || newCoordinates.y >= this.svgHandler.canvasHeight) {
            newCoordinates.x = Number(oldChild.getAttribute('x2'));
            newCoordinates.y = Number(oldChild.getAttribute('y2'));
        }

        oldChild = this.linePrevisualization(startCoordinates, newCoordinates, size, oldChild) as SVGElement;
        return oldChild;
    }

    connectFirstAndLastPoint(
        endCoordinates: Coordinates,
        lastprevisualisation: SVGElement,
        polyline: SVGElement,
        isNormalJunction: boolean
    ): void {
        if (!isNormalJunction) {
            for (let i = 0; i < 2; i++) {
                this.renderer.removeChild(this.svgHandler.svgCanvas, this.junctionPoints.pop());
            }
        }
        this.renderer.setAttribute(lastprevisualisation, 'x2', String(endCoordinates.x));
        this.renderer.setAttribute(lastprevisualisation, 'y2', String(endCoordinates.y));
        let points = polyline.getAttribute('points');
        if (points !== null) {
            points = points.substr(0, points.lastIndexOf(' '));
            points += ' ' + String(endCoordinates.x) + ',' + String(endCoordinates.y);
            this.renderer.setAttribute(polyline, 'points', points);
        }
    }

    createJunction(coordinates: Coordinates, size: string): void {
        const junction = this.drawingService.createPolyLine(coordinates, size, false);
        this.junctionPoints.push(junction);
        this.addElementToG(junction, size);
    }

    removeJunction(): void {
        const junction = this.junctionPoints.pop();
        this.renderer.removeChild(this.svgHandler.svgCanvas, junction);
    }

    deleteLine(polyline: SVGElement): void {
        this.renderer.removeChild(this.svgHandler.svgCanvas, polyline);
        for (const child of this.junctionPoints) {
            this.renderer.removeChild(this.svgHandler.svgCanvas, child);
        }
    }

    emptyChildren(): void {
        this.junctionPoints = [];
    }

    createGElement(): void {
        this.svgG = this.renderer.createElement('svg:g', this.svgHandler.SVG_LINK);
        this.undoRedoService.updateCurrent(new Array(this.svgG), ChangeType.addToCanvas);
        this.clickerHandler.addSelectionStyles(this.svgG);
        this.renderer.appendChild(this.svgHandler.svgCanvas, this.svgG);
        this.renderer.setAttribute(this.svgG, 'typeElement', 'strokeElement');
        this.renderer.setAttribute(this.svgG, 'stroke', this.colorManager.colorSelected[ColorChoice.mainColor].inString);
    }

    addElementToG(element: SVGElement, size: string): void {
        this.renderer.appendChild(this.svgG, element);
        this.renderer.setAttribute(this.svgG, 'size', size);
    }
}
