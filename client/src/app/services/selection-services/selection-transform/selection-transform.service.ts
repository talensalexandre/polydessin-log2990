import { Injectable, OnInit, Renderer2 } from '@angular/core';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';

const FIRST_INDEX = 0;
const SECOND_INDEX = 1;
const THIRD_INDEX = 2;
const FOURTH_INDEX = 3;
const FIFTH_INDEX = 4;
const SIXTH_INDEX = 5;

@Injectable({
    providedIn: 'root',
})
export class SelectionTransformService implements OnInit {
    private renderer: Renderer2;

    constructor(
        private svgHandler: SvgHandlerService
    ) {}

    ngOnInit(): void {
        this.renderer = this.svgHandler.renderer;
    }

    appendTransformationToElements(element: SVGElement, matrix: number[]): void {
        this.renderer.setAttribute(element, 'transform',
        'matrix(' + matrix[FIRST_INDEX] + ',' + matrix[SECOND_INDEX] + ',' + matrix[THIRD_INDEX] + ',' + matrix[FOURTH_INDEX] +
         ',' + matrix[FIFTH_INDEX] + ',' + matrix[SIXTH_INDEX] + ')');
    }

    getOldMatrix(element: SVGElement): RegExpExecArray {
        const oldMatrix = element.getAttribute('transform') as string;
        const tempMatrix = /matrix\(\s*([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)/.exec(oldMatrix);
        return tempMatrix as RegExpExecArray;
    }
}
