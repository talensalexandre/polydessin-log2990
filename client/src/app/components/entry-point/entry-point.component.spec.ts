import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatCardModule, MatChipsModule, MatDialogModule, MatDialogRef, MatFormFieldModule, MatIconModule,
    MatProgressSpinnerModule, MatSidenavModule, MatSnackBarModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { CreateNewDrawingComponent } from '../create-new-drawing/create-new-drawing.component';
import { DrawingGalleryComponent } from '../drawing-gallery/drawing-gallery.component';
import { EntryPointComponent } from './entry-point.component';

describe('EntryPointComponent', () => {
    let component: EntryPointComponent;
    let fixture: ComponentFixture<EntryPointComponent>;
    let stateServiceSpy: jasmine.SpyObj<StateSelectorService>;

    const mockDialogRef = {
        close: jasmine.createSpy('close')
    };

    beforeEach(() => {
        stateServiceSpy = jasmine.createSpyObj('StateSelectorService', ['changeState']);
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatDialogModule, MatIconModule, MatSidenavModule, BrowserAnimationsModule,
                      NoopAnimationsModule, MatFormFieldModule, MatInputModule, FormsModule,
                      MatCardModule, MatProgressSpinnerModule, MatChipsModule, MatSnackBarModule,
                      HttpClientModule],
            declarations: [EntryPointComponent, CreateNewDrawingComponent, DrawingGalleryComponent],
            providers: [
                { provide: MatDialogRef, useValue: mockDialogRef },
                { provide: StateSelectorService, useValue: stateServiceSpy },
            ]
        })
            .overrideModule(
                BrowserDynamicTestingModule,
                { set: { entryComponents: [EntryPointComponent, CreateNewDrawingComponent, DrawingGalleryComponent] } })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EntryPointComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should close when clicking "continue"', () => {
        component.closeDialog();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should open newDrawingDialog', () => {
        component.createNewDrawing();
        expect(mockDialogRef.close).toHaveBeenCalled();
        expect(stateServiceSpy.changeState).toHaveBeenCalledTimes(1);
        expect(CreateNewDrawingComponent).toBeTruthy();
    });

    it('should open drawingGalleryDialog', () => {
        component.openDrawingGallery();
        expect(mockDialogRef.close).toHaveBeenCalled();
        expect(stateServiceSpy.changeState).toHaveBeenCalledTimes(1);
        expect(DrawingGalleryComponent).toBeTruthy();
    });

    it('should go to documentation', () => {
        component.goToDocumentation();
        expect(mockDialogRef.close).toHaveBeenCalled();
        expect(stateServiceSpy.changeState).toHaveBeenCalled();
    });

    it('should open itself (entry point)', () => {
        component.openEntryPoint();
        expect(EntryPointComponent).toBeTruthy();
    });

    it('should open itself (entry point) with the subscribe method', () => {
        component.subscribeMethod();
        expect(EntryPointComponent).toBeTruthy();
    });

    it('should continue drawing', () => {
        const debugElement = fixture.debugElement;
        const createNewDrawingService = debugElement.injector.get(CreateNewDrawingService);
        const spy = spyOn(createNewDrawingService, 'continueDrawing');
        component.continueDrawing();
        expect(mockDialogRef.close).toHaveBeenCalled();
        expect(spy).toHaveBeenCalled();
    });

});
