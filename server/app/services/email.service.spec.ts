import { expect } from 'chai';
import { EmailService } from './email.service';

// tslint:disable:no-any

let emailService: EmailService;
const FAIL = 400;

before(async () => {
    emailService = new EmailService();
});

describe('email service', () => {

    it('should go to home page', () => {
        const expectedTitle = 'Home Page';
        const expectedBody = 'If this message appears, your request is invalid';
        const message = emailService.homePage();
        expect(message.title).to.equals(expectedTitle);
        expect(message.body).to.equals(expectedBody);
    });

    it('should send a basic image', async () => {
        const request = new Array<string>();
        request.push('base64Buffer');
        request.push('test@test.test');
        request.push('name');
        request.push('jpeg');
        return emailService.sendDrawing(request).catch((result: any) => {
            return expect(result.response.status).to.equals(FAIL);
        });
    });

    it('should send a svg image', async () => {
        const request = new Array<string>();
        request.push('base64Buffer');
        request.push('test@test.test');
        request.push('name');
        request.push('svg');
        return emailService.sendDrawing(request).catch((result: any) => {
            return expect(result.response.status).to.equals(FAIL);
        });
    });

});
