import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { EraserService } from 'src/app/services/eraser/eraser.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { EraserComponent } from './eraser.component';

const MIN_SIZE = 3;
const MAX_SIZE = 40;

// tslint:disable:no-magic-numbers no-string-literal

describe('EraserComponent', () => {
    let component: EraserComponent;
    let fixture: ComponentFixture<EraserComponent>;
    let eraserServiceSpy: jasmine.SpyObj<EraserService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;

    beforeEach(() => {
        eraserServiceSpy = jasmine.createSpyObj(
            'EraserService',
            [
                'moveEraser',
                'getSelectedElements',
                'clearHighlights',
                'addToUndoRedo',
                'deleteEraser'
            ]);

        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
            ]
        );
        drawingServiceSpy = jasmine.createSpyObj(
            'DrawingService',
            [
                'removeElement',
            ]
        );
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatFormFieldModule, MatInputModule, BrowserAnimationsModule, FormsModule,
                HttpClientModule, MatIconModule],
            declarations: [EraserComponent],
            providers: [
              {provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
              {provide: MouseHandlerService, useValue: mouseHandlerServiceSpy},
              {provide: EraserService, useValue: eraserServiceSpy},
              {provide: DrawingService, useValue: drawingServiceSpy}
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EraserComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        component.ngOnInit();
        expect(svgHandlerServiceSpy.addListeners).toHaveBeenCalled();
    });

    it('should not change size if between 1 and 200', () => {
        const size = 30;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(size);
    });

    it('should change size if under 3', () => {
        const size = -50;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(MIN_SIZE);
    });

    it('should change size if above 200', () => {
        const size = 500;
        component.size = size;
        component.changeSize();
        expect(component.size).toEqual(MAX_SIZE);
    });

    it('should call updateMouseDown', () => {
        const mockMouse = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouse);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
        expect(eraserServiceSpy.getSelectedElements).toHaveBeenCalled();
    });

    it('should call updateMousePosition', () => {
        const mockMouse = new MouseEvent('mousemove');
        component['eventListeners'].mouseMove(mockMouse);
        expect(eraserServiceSpy.moveEraser).toHaveBeenCalled();
        expect(eraserServiceSpy.getSelectedElements).toHaveBeenCalled();
    });

    it('should call mouseUpExtend', () => {
        component['mouseUpExtend']();
        expect(eraserServiceSpy.addToUndoRedo).toHaveBeenCalled();
    });

    it('should clear highlights on mouse leave', () => {
        const mockMouse = new MouseEvent('mouseleave');
        component['eventListeners'].mouseLeave(mockMouse);
        expect(eraserServiceSpy.clearHighlights).toHaveBeenCalled();
        expect(eraserServiceSpy.deleteEraser).toHaveBeenCalled();
    });
});
