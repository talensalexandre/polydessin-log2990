import { Injectable } from '@angular/core';

const INCREASE_SIZE_GRID = '+';
const DECREASE_SIZE_GRID = '-';
const TOGGLE_GRID = 'g';
const MAXIMUM_SIZE = 200;
const MINIMUM_SIZE = 50;
const DEFAULT_SIZE = 100;
const KEY_SIZE_CHANGE = 5;

@Injectable({
  providedIn: 'root'
})

export class GridService {
    isActive: boolean;
    size: number;
    sizeChecked: number;
    opacity: number;
    verticalPath: string;
    horizontalPath: string;

    constructor() {
        this.isActive = false;
        this.size = DEFAULT_SIZE;
        this.sizeChecked = DEFAULT_SIZE;
        this.opacity = DEFAULT_SIZE;
        this.verticalPath = 'M 100 0 V100';
        this.horizontalPath = 'M 0 100 H100';
    }

    changeGridSize(): void {
        this.size = (this.size < MINIMUM_SIZE) ? MINIMUM_SIZE : this.size;
        this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        this.sizeChecked = this.size;
        const sizeString = this.size.toString();
        this.verticalPath = 'M ' + sizeString + ' 0 V' + sizeString;
        this.horizontalPath = 'M 0 ' + sizeString +  ' H' + sizeString;
    }

    handleKey(key: string): void {
        switch (key) {
            case TOGGLE_GRID: {
                this.isActive = !this.isActive;
                break;
            }
            case INCREASE_SIZE_GRID: {
                this.size = this.size + KEY_SIZE_CHANGE;
                this.changeGridSize();
                break;
            }
            case DECREASE_SIZE_GRID: {
                this.size = this.size - KEY_SIZE_CHANGE;
                this.changeGridSize();
                break;
            }
            default: {
                break;
            }
        }
    }

    increaseGridSize(): void {
        this.size++;
        this.changeGridSize();
    }

    decreaseGridSize(): void {
        this.size--;
        this.changeGridSize();
    }
}
