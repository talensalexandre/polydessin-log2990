import { Injectable, Renderer2 } from '@angular/core';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { EventListeners } from '../../interfaces-enums/event-listeners';

@Injectable({
    providedIn: 'root',
})
export class SvgHandlerService {
    readonly SVG_LINK: string;
    svgCanvas: SVGElement;
    canvasOffset: Coordinates;
    renderer: Renderer2;
    isLineBeingCreated: boolean;
    workspace: HTMLElement;
    drawingOnCanvas: boolean;
    canvasHeight: number;
    canvasWidth: number;
    backgroundColor: string;
    private mouseMovelistener: () => void;
    private mouseDownlistener: () => void;
    private mouseUplistener: () => void;
    private mouseLeavelistener: () => void;
    private mouseClickListener: () => void;
    private mouseDblClickListener: () => void;
    private contextMenuListener: () => void;
    private wheelListener: () => void;
    elements: SVGElement[];
    svgFilterBlur: SVGElement;
    svgFilterRound: SVGElement;
    svgFilterFog: SVGElement;
    svgFilterChaos: SVGElement;
    svgFilterShadow: SVGElement;

    constructor() {
        this.drawingOnCanvas = false;
        this.canvasHeight = 0;
        this.canvasWidth = 0;
        this.SVG_LINK = 'http://www.w3.org/2000/svg';
        this.isLineBeingCreated = false;
        this.elements = new Array<SVGElement>();
    }

    createCanvas(width: number, height: number, color: string): void {
        this.canvasHeight = height;
        this.canvasWidth = width;
        this.backgroundColor = color;
        this.svgCanvas = this.renderer.createElement('svg', this.SVG_LINK);
        this.renderer.setStyle(this.svgCanvas, 'width', String(width));
        this.renderer.setStyle(this.svgCanvas, 'height', String(height));
        this.renderer.setStyle(this.svgCanvas, 'background-color', color);
        this.renderer.setStyle(this.svgCanvas, 'border-color', 'whitesmoke');
        this.renderer.setStyle(this.svgCanvas, 'border-width', '0');
        this.renderer.setStyle(this.svgCanvas, 'border-right-width', '1');
        this.renderer.setStyle(this.svgCanvas, 'border-bottom-width', '1');
        this.renderer.setStyle(this.svgCanvas, 'border-style', 'dashed');
        this.renderer.appendChild(this.workspace, this.svgCanvas);
        this.addBlurFilter();
        this.addRoundedFilter();
        this.addFogFilter();
        this.addChaosFilter();
        this.addShadowFilter();
        this.drawingOnCanvas = false;
    }

    updateBackgroundColor(color: string): void {
        this.renderer.setStyle(this.svgCanvas, 'background-color', color);
        this.renderer.appendChild(this.workspace, this.svgCanvas);
    }

    deleteCanvas(): void {
        this.renderer.removeChild(this.workspace, this.svgCanvas);
    }

    addListeners(eventListeners: EventListeners): void {
        if (eventListeners.changedMouseMove) {
            this.mouseMovelistener = this.renderer.listen(this.svgCanvas, 'mousemove', eventListeners.mouseMove);
        }
        if (eventListeners.changedMouseDown) {
            this.mouseDownlistener = this.renderer.listen(this.svgCanvas, 'mousedown', eventListeners.mouseDown);
        }
        if (eventListeners.changedMouseUp) {
            this.mouseUplistener = this.renderer.listen(this.svgCanvas, 'mouseup', eventListeners.mouseUp);
        }
        if (eventListeners.changedMouseLeave) {
            this.mouseLeavelistener = this.renderer.listen(this.svgCanvas, 'mouseleave', eventListeners.mouseLeave);
        }
        if (eventListeners.changedMouseClick) {
            this.mouseClickListener = this.renderer.listen(this.svgCanvas, 'click', eventListeners.mouseClick);
        }
        if (eventListeners.changedMouseDblClick) {
            this.mouseDblClickListener = this.renderer.listen(this.svgCanvas, 'dblclick', eventListeners.mouseDblClick);
        }
        if (eventListeners.changedContextMenu) {
            this.contextMenuListener = this.renderer.listen(this.svgCanvas, 'contextmenu', eventListeners.contextMenu);
        }
        if (eventListeners.changedWheelPosition) {
            this.wheelListener = this.renderer.listen(this.svgCanvas, 'mousewheel', eventListeners.mouseWheel);
        }
    }

    deleteListeners(eventListeners: EventListeners): void {
        if (eventListeners.changedMouseMove) {
            this.mouseMovelistener();
        }
        if (eventListeners.changedMouseDown) {
            this.mouseDownlistener();
        }
        if (eventListeners.changedMouseUp) {
            this.mouseUplistener();
        }
        if (eventListeners.changedMouseLeave) {
            this.mouseLeavelistener();
        }
        if (eventListeners.changedMouseClick) {
            this.mouseClickListener();
        }
        if (eventListeners.changedMouseDblClick) {
            this.mouseDblClickListener();
        }
        if (eventListeners.changedContextMenu) {
            this.contextMenuListener();
        }
        if (eventListeners.changedWheelPosition) {
            this.wheelListener();
        }
    }

    addBlurFilter(svgElement: SVGElement = this.svgCanvas): void {
        this.svgFilterBlur = this.renderer.createElement('filter', this.SVG_LINK);
        this.renderer.setAttribute(this.svgFilterBlur, 'filterUnits', 'userSpaceOnUse');
        this.renderer.setAttribute(this.svgFilterBlur, 'id', 'blur');

        const feGaussianBlurFilter = this.renderer.createElement('feGaussianBlur', this.SVG_LINK);
        this.renderer.setAttribute(feGaussianBlurFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feGaussianBlurFilter, 'stdDeviation', '3');
        this.renderer.setAttribute(feGaussianBlurFilter, 'result', 'blur');
        this.renderer.appendChild(this.svgFilterBlur, feGaussianBlurFilter);

        const feColorMatrixFilter = this.renderer.createElement('feColorMatrix', this.SVG_LINK);
        this.renderer.appendChild(this.svgFilterBlur, feColorMatrixFilter);

        this.renderer.appendChild(svgElement, this.svgFilterBlur);
    }

    addRoundedFilter(svgElement: SVGElement = this.svgCanvas): void {
        this.svgFilterRound = this.renderer.createElement('filter', this.SVG_LINK);
        this.renderer.setAttribute(this.svgFilterRound, 'filterUnits', 'userSpaceOnUse');
        this.renderer.setAttribute(this.svgFilterRound, 'id', 'rounded');

        const feGaussianBlurFilter = this.renderer.createElement('feGaussianBlur', this.SVG_LINK);
        this.renderer.setAttribute(feGaussianBlurFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feGaussianBlurFilter, 'stdDeviation', '4');
        this.renderer.setAttribute(feGaussianBlurFilter, 'result', 'blur');
        this.renderer.appendChild(this.svgFilterRound, feGaussianBlurFilter);

        const feOffsetFilter = this.renderer.createElement('feOffset', this.SVG_LINK);
        this.renderer.setAttribute(feOffsetFilter, 'in', 'blur');
        this.renderer.setAttribute(feOffsetFilter, 'dx', '4');
        this.renderer.setAttribute(feOffsetFilter, 'dy', '4');
        this.renderer.setAttribute(feOffsetFilter, 'result', 'offsetBlur');
        this.renderer.appendChild(this.svgFilterRound, feOffsetFilter);

        const feSpecularLightingFilter = this.renderer.createElement('feSpecularLighting', this.SVG_LINK);
        this.renderer.setAttribute(feSpecularLightingFilter, 'in', 'blur');
        this.renderer.setAttribute(feSpecularLightingFilter, 'surfaceScale', '5');
        this.renderer.setAttribute(feSpecularLightingFilter, 'specularConstant', '.75');
        this.renderer.setAttribute(feSpecularLightingFilter, 'specularExponent', '20');
        this.renderer.setAttribute(feSpecularLightingFilter, 'lighting-color', '#bbbbbb');
        this.renderer.setAttribute(feSpecularLightingFilter, 'result', 'specOut');

        const fePointLightFilter = this.renderer.createElement('fePointLight', this.SVG_LINK);
        this.renderer.setAttribute(fePointLightFilter, 'x', '-5000');
        this.renderer.setAttribute(fePointLightFilter, 'y', '-10000');
        this.renderer.setAttribute(fePointLightFilter, 'z', '20000');
        this.renderer.appendChild(feSpecularLightingFilter, fePointLightFilter);

        this.renderer.appendChild(this.svgFilterRound, feSpecularLightingFilter);

        const feCompositeFirstFilter = this.renderer.createElement('feComposite', this.SVG_LINK);
        this.renderer.setAttribute(feCompositeFirstFilter, 'in', 'specOut');
        this.renderer.setAttribute(feCompositeFirstFilter, 'in2', 'SourceAlpha');
        this.renderer.setAttribute(feCompositeFirstFilter, 'operator', 'in');
        this.renderer.setAttribute(feCompositeFirstFilter, 'result', 'specOut');

        this.renderer.appendChild(this.svgFilterRound, feCompositeFirstFilter);

        const feCompositeSecondFilter = this.renderer.createElement('feComposite', this.SVG_LINK);
        this.renderer.setAttribute(feCompositeSecondFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feCompositeSecondFilter, 'in2', 'specOut');
        this.renderer.setAttribute(feCompositeSecondFilter, 'operator', 'arithmetic');
        this.renderer.setAttribute(feCompositeSecondFilter, 'k1', '0');
        this.renderer.setAttribute(feCompositeSecondFilter, 'k2', '1');
        this.renderer.setAttribute(feCompositeSecondFilter, 'k3', '1');
        this.renderer.setAttribute(feCompositeSecondFilter, 'k4', '0');
        this.renderer.setAttribute(feCompositeSecondFilter, 'result', 'litPaint');

        this.renderer.appendChild(this.svgFilterRound, feCompositeSecondFilter);

        this.renderer.appendChild(svgElement, this.svgFilterRound);
    }

    addFogFilter(svgElement: SVGElement = this.svgCanvas): void {
        this.svgFilterFog = this.renderer.createElement('filter', this.SVG_LINK);
        this.renderer.setAttribute(this.svgFilterFog, 'filterUnits', 'userSpaceOnUse');
        this.renderer.setAttribute(this.svgFilterFog, 'id', 'fog');

        const feTurbulenceFilter = this.renderer.createElement('feTurbulence', this.SVG_LINK);
        this.renderer.setAttribute(feTurbulenceFilter, 'type', 'turbulence');
        this.renderer.setAttribute(feTurbulenceFilter, 'baseFrequency', '0.01 0.4');
        this.renderer.setAttribute(feTurbulenceFilter, 'numOctaves', '2');
        this.renderer.setAttribute(feTurbulenceFilter, 'result', 'turbulence');

        this.renderer.appendChild(this.svgFilterFog, feTurbulenceFilter);

        const feDisplacementMapFilter = this.renderer.createElement('feDisplacementMap', this.SVG_LINK);
        this.renderer.setAttribute(feDisplacementMapFilter, 'in2', 'pict2');
        this.renderer.setAttribute(feDisplacementMapFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feDisplacementMapFilter, 'scale', '20');
        this.renderer.setAttribute(feDisplacementMapFilter, 'xChannelSelector', 'R');
        this.renderer.setAttribute(feDisplacementMapFilter, 'yChannelSelector', 'R');

        this.renderer.appendChild(this.svgFilterFog, feDisplacementMapFilter);

        this.renderer.appendChild(svgElement, this.svgFilterFog);
    }

    addChaosFilter(svgElement: SVGElement = this.svgCanvas): void {
        this.svgFilterChaos = this.renderer.createElement('filter', this.SVG_LINK);
        this.renderer.setAttribute(this.svgFilterChaos, 'filterUnits', 'userSpaceOnUse');
        this.renderer.setAttribute(this.svgFilterChaos, 'id', 'chaos');

        const feTurbulenceFilter = this.renderer.createElement('feTurbulence', this.SVG_LINK);
        this.renderer.setAttribute(feTurbulenceFilter, 'type', 'turbulence');
        this.renderer.setAttribute(feTurbulenceFilter, 'baseFrequency', '0.05');
        this.renderer.setAttribute(feTurbulenceFilter, 'numOctaves', '2');
        this.renderer.setAttribute(feTurbulenceFilter, 'result', 'turbulence');

        this.renderer.appendChild(this.svgFilterChaos, feTurbulenceFilter);

        const feDisplacementMapFilter = this.renderer.createElement('feDisplacementMap', this.SVG_LINK);
        this.renderer.setAttribute(feDisplacementMapFilter, 'in2', 'pict2');
        this.renderer.setAttribute(feDisplacementMapFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feDisplacementMapFilter, 'scale', '20');
        this.renderer.setAttribute(feDisplacementMapFilter, 'xChannelSelector', 'R');
        this.renderer.setAttribute(feDisplacementMapFilter, 'yChannelSelector', 'G');

        this.renderer.appendChild(this.svgFilterChaos, feDisplacementMapFilter);

        this.renderer.appendChild(svgElement, this.svgFilterChaos);
    }

    addShadowFilter(svgElement: SVGElement = this.svgCanvas): void {
        this.svgFilterShadow = this.renderer.createElement('filter', this.SVG_LINK);
        this.renderer.setAttribute(this.svgFilterShadow, 'filterUnits', 'userSpaceOnUse');
        this.renderer.setAttribute(this.svgFilterShadow, 'id', 'shadow');

        const feOffsetFilter = this.renderer.createElement('feOffset', this.SVG_LINK);
        this.renderer.setAttribute(feOffsetFilter, 'result', 'offOut');
        this.renderer.setAttribute(feOffsetFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feOffsetFilter, 'dx', '20');
        this.renderer.setAttribute(feOffsetFilter, 'dy', '20');

        this.renderer.appendChild(this.svgFilterShadow, feOffsetFilter);

        const feGaussianBlurFilter = this.renderer.createElement('feGaussianBlur', this.SVG_LINK);
        this.renderer.setAttribute(feGaussianBlurFilter, 'result', 'blurOut');
        this.renderer.setAttribute(feGaussianBlurFilter, 'in', 'blurOut');
        this.renderer.setAttribute(feGaussianBlurFilter, 'stdDeviation', '10');

        this.renderer.appendChild(this.svgFilterShadow, feGaussianBlurFilter);

        const feBlendFilter = this.renderer.createElement('feBlend', this.SVG_LINK);
        this.renderer.setAttribute(feBlendFilter, 'in', 'SourceGraphic');
        this.renderer.setAttribute(feBlendFilter, 'in2', 'blurOut');
        this.renderer.setAttribute(feBlendFilter, 'mode', 'normal');

        this.renderer.appendChild(this.svgFilterShadow, feBlendFilter);

        this.renderer.appendChild(svgElement, this.svgFilterShadow);
    }
}
