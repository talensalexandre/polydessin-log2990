// Some external source code has been used for reference
// Lukas Marx (2018) Creating a Color Picker Component with Angular [Online]
// Available : https://github.com/LukasMarx/angular-color-picker

import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, Output, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';

const WIDTH = 20;
const HEIGHT = 200;
const ONE_SIXTH = 0.17;
const ONE_THIRD = 0.33;
const ONE_HALF = 0.5;
const TWO_THIRD = 0.67;
const FIVE_SIXTH = 0.83;

@Component({
  selector: 'app-all-color-gradient',
  templateUrl: './all-color-gradient.component.html',
  styleUrls: ['./all-color-gradient.component.scss']
})
export class AllColorGradientComponent implements AfterViewInit {

    @ViewChild('allColorGradient', {static: false}) allColorGradientSelectionCanvas: ElementRef;
    allGradientContext: CanvasRenderingContext2D;
    @Output() shouldUpdateGradient: EventEmitter<string> = new EventEmitter();

    constructor(@Inject(MAT_DIALOG_DATA) public colorChoice: ColorChoice,
                private colorManager: ColorManagerService) { }

    ngAfterViewInit(): void {
        this.createAllGradiantDisplayer();
    }

    private createAllGradiantDisplayer(): void {
        this.allGradientContext = this.allColorGradientSelectionCanvas.nativeElement.getContext('2d');
        const gradientToBottom = this.allGradientContext.createLinearGradient(0, 0, 0, HEIGHT);
        gradientToBottom.addColorStop(0, 'rgba(255,0,0,1)');
        gradientToBottom.addColorStop(ONE_SIXTH, 'rgba(255,255,0,1)');
        gradientToBottom.addColorStop(ONE_THIRD, 'rgba(0,255,0,1)');
        gradientToBottom.addColorStop(ONE_HALF, 'rgba(0,255,255,1)');
        gradientToBottom.addColorStop(TWO_THIRD, 'rgba(0,0,255,1)');
        gradientToBottom.addColorStop(FIVE_SIXTH, 'rgba(255,0,255,1)');
        gradientToBottom.addColorStop(1, 'rgba(255,0,0,1)');
        this.allGradientContext.beginPath();
        this.allGradientContext.rect(0, 0, WIDTH, HEIGHT);
        this.allGradientContext.fillStyle = gradientToBottom;
        this.allGradientContext.fill();
        this.allGradientContext.closePath();
    }

    updateColorWithCoordinates(coordinates: Coordinates): void {
        const colorPixel = this.allGradientContext.getImageData(coordinates.x, coordinates.y, 1, 1).data;
        if (colorPixel) {
            this.colorManager.updateColorWithPixel(this.colorChoice, colorPixel);
        }
    }

    mouseDownFromGradient(event: MouseEvent): void {
        const coordinates: Coordinates = { x : event.offsetX, y : event.offsetY};
        this.updateColorWithCoordinates(coordinates);
        this.shouldUpdateGradient.emit(this.colorManager.colorSelected[this.colorChoice].inString);
    }
}
