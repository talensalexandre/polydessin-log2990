export enum NavigationArrows {
    up,
    down,
    left,
    right
}
