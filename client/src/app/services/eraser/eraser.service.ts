import { Injectable, Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { DrawingService } from '../drawing/drawing.service';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const MIN_SIZE = 5;
const PX_INCREMENT = 3;
const BEGIN_SLICE = 5;
const END_SLICE = -3;
const MAX_RED_CONVERTION = 180;
const MIN_RED_CONVERTION = 80;
const RED_DECREMENT = 60;
const LAST_INDEX = -1;

@Injectable({
    providedIn: 'root'
})
export class EraserService {
    private renderer: Renderer2;
    private lastSelectedElements: SVGElement[];
    private erasedElements: SVGElement[];
    private eraser: SVGElement;

    constructor(
        private svgHandler: SvgHandlerService,
        private drawingService: DrawingService,
        private undoRedo: UndoRedoService,
        private saveManager: SaveManagerService
    ) {
        this.renderer = this.svgHandler.renderer;
        this.lastSelectedElements = new Array();
        this.erasedElements = new Array();
    }

    moveEraser(coordinates: Coordinates, size: number): SVGElement {
        const startCoords: Coordinates = { x: coordinates.x - (size / 2), y: coordinates.y - (size / 2) };
        if (!this.eraser) {
            this.eraser = this.renderer.createElement('path', this.svgHandler.SVG_LINK);
            this.renderer.setAttribute(this.eraser, 'fill', 'white');
            this.renderer.setAttribute(this.eraser, 'stroke', 'black');
            this.renderer.setAttribute(this.eraser, 'stroke-width', '1');
            this.renderer.appendChild(this.svgHandler.svgCanvas, this.eraser);
        }
        const newPath = 'M' + String(startCoords.x) + ' ' + String(startCoords.y) +
            ' h ' + String(size) + ' v ' + String(size) +
            ' H ' + String(startCoords.x) + 'L ' + String(startCoords.x) + ' ' + String(startCoords.y);
        this.renderer.setAttribute(this.eraser, 'd', newPath);
        return this.eraser as SVGElement;
    }

    getSelectedElements(size: number, eraserCoordinates: Coordinates, isMouseDown: boolean): void {
        const selectedElements: SVGElement[] = new Array();
        for (let i = eraserCoordinates.x - (size / 2); i <= eraserCoordinates.x + (size / 2); i += PX_INCREMENT) {
            for (let j = eraserCoordinates.y - (size / 2); j <= eraserCoordinates.y + (size / 2); j += PX_INCREMENT) {
                let element = document.elementsFromPoint(i, j)[1] as SVGElement;
                if (element && element.parentNode && element.parentNode.nodeName === 'svg:g') {
                    element = element.parentNode as SVGElement;
                }
                if ((selectedElements.indexOf(element) === LAST_INDEX) && this.svgHandler.elements.indexOf(element) !== LAST_INDEX) {
                    selectedElements.push(element);
                }
            }
        }
        isMouseDown ? this.eraseElements(selectedElements) : this.highlightElements(selectedElements);
    }

    private eraseElements(selectedElements: SVGElement[]): void {
        this.clearHighlights();
        selectedElements.forEach((element: SVGElement) => {
            this.erasedElements.push(element);
            this.renderer.removeChild(this.svgHandler.svgCanvas, element);
            this.saveManager.removeSvgElement(element);
            this.svgHandler.elements.splice(this.svgHandler.elements.indexOf(element), 1);
        });
    }

    private highlightElements(selectedElements: SVGElement[]): void {
        selectedElements.forEach((element: SVGElement) => {
            if (this.lastSelectedElements.lastIndexOf(element) === LAST_INDEX) {
                this.lastSelectedElements.push(element);
            }
            const color = element.getAttribute('stroke');
            if (color) {
                const colorValues = color.slice(BEGIN_SLICE, END_SLICE);
                const colorArray = colorValues.split(',');
                if (Number(colorArray[0]) >= MAX_RED_CONVERTION &&
                    Number(colorArray[0]) > Number(colorArray[1]) &&
                    Number(colorArray[0]) > Number(colorArray[2]) &&
                    Number(colorArray[1]) <= MIN_RED_CONVERTION &&
                    Number(colorArray[2]) <= MIN_RED_CONVERTION) {
                    const newRed = Number(colorArray[0]) - RED_DECREMENT;
                    const newColor = 'rgba(' + String(newRed) + ', ' + colorArray[1] + ', ' + colorArray[2] + ')';
                    this.renderer.setStyle(element, 'stroke', newColor);
                } else {
                    this.renderer.setStyle(element, 'stroke', 'rgba(255, 0, 0)');
                }
            }
            if (Number(element.getAttribute('stroke-width')) <= MIN_SIZE || element.getAttribute('stroke-width') === 'none') {
                this.renderer.setStyle(element, 'stroke-width', '6');
            }
        });

        this.lastSelectedElements.forEach((element: SVGElement) => {
            if (selectedElements.lastIndexOf(element) === LAST_INDEX) {
                this.lastSelectedElements.slice(this.lastSelectedElements.lastIndexOf(element));
                this.renderer.removeStyle(element, 'stroke');
                this.renderer.removeStyle(element, 'stroke-width');
            }
        });
    }

    clearHighlights(): void {
        this.lastSelectedElements.forEach((element: SVGElement) => {
            this.renderer.removeStyle(element, 'stroke');
            this.renderer.removeStyle(element, 'stroke-width');
        });
    }

    deleteEraser(): void {
        this.drawingService.removeElement(this.eraser);
        delete this.eraser;
    }

    addToUndoRedo(): void {
        if (this.erasedElements.length === 0) { return; }
        this.undoRedo.updateCurrent(this.erasedElements, ChangeType.elementRemoved);
        this.erasedElements = new Array();
    }
}
