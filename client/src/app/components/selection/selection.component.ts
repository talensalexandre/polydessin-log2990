import { AfterViewInit, Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { NavigationArrows } from 'src/app/interfaces-enums/navigation-arrows';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PrevisualisationRectangleService } from 'src/app/services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SelectionCalculatorService } from 'src/app/services/selection-services/selection-calculator/selection-calculator.service';
import { SelectionMoveService } from 'src/app/services/selection-services/selection-move/selection-move.service';
import { SelectionRotateService } from 'src/app/services/selection-services/selection-rotate/selection-rotate.service';
import { SelectionSvgHandlerService } from 'src/app/services/selection-services/selection-svg-handler/selection-svg-handler.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { UndoRedoService } from 'src/app/services/undo-redo/undo-redo.service';
import { MinMaxCoords } from '../../interfaces-enums/min-max-coords';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const INITAL_COORDS = -1;
const LEFT_KEY_ELEMENT = 'left';
const RIGHT_KEY_ELEMENT = 'right';
const BOUNDARY_ELEMENT = 'boundary';
const TOP_ELEMENT = 'top';
const BOTTOM_ELEMENT = 'bottom';
const SELECT_ALL_KEY = 'a';

@Component({
    selector: 'app-selection',
    templateUrl: './selection.component.html',
    styleUrls: ['./selection.component.scss']
})
export class SelectionComponent extends AbstractToolComponent implements AfterViewInit {
    @ViewChild(BOUNDARY_ELEMENT, { static: false }) boundary: ElementRef;
    @ViewChild(TOP_ELEMENT, { static: false }) top: ElementRef;
    @ViewChild(BOTTOM_ELEMENT, { static: false }) bottom: ElementRef;
    @ViewChild(LEFT_KEY_ELEMENT, { static: false }) left: ElementRef;
    @ViewChild(RIGHT_KEY_ELEMENT, { static: false }) right: ElementRef;
    private previsualisationRectangleValues: MinMaxCoords;
    private mouseMoved: boolean;
    private clickedInsideSelection: boolean;
    private selectionMoved: boolean;
    private shiftDown: boolean;
    private altDown: boolean;

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        private previsualizationRectangleService: PrevisualisationRectangleService,
        private selectionSvgHandler: SelectionSvgHandlerService,
        private selectionMove: SelectionMoveService,
        private selectionCalculator: SelectionCalculatorService,
        private dialog: MatDialog,
        protected saveManager: SaveManagerService,
        private selectionRotate: SelectionRotateService,
        private undoRedo: UndoRedoService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
        this.previsualisationRectangleValues = {
            minCoords: { x: INITAL_COORDS, y: INITAL_COORDS },
            maxCoords: { x: INITAL_COORDS, y: INITAL_COORDS }
        };
        this.mouseMoved = false;
        this.selectionMoved = false;
        this.clickedInsideSelection = false;
        this.shiftDown = false;
        this.altDown = false;
    }

    ngAfterViewInit(): void {
        this.selectionSvgHandler.addNewSelectionRectangle(BOUNDARY_ELEMENT, this.boundary.nativeElement);
        this.selectionSvgHandler.addNewSelectionRectangle(TOP_ELEMENT, this.top.nativeElement);
        this.selectionSvgHandler.addNewSelectionRectangle(BOTTOM_ELEMENT, this.bottom.nativeElement);
        this.selectionSvgHandler.addNewSelectionRectangle(LEFT_KEY_ELEMENT, this.left.nativeElement);
        this.selectionSvgHandler.addNewSelectionRectangle(RIGHT_KEY_ELEMENT, this.right.nativeElement);
        this.selectionSvgHandler.listenToBoundaryRectangles();
    }

    protected initExtend(): void {
        this.eventListeners.mouseMove = ($event) => this.updateMousePosition($event);
        this.eventListeners.mouseDown = ($event) => this.onMouseDown($event);
        this.eventListeners.mouseUp = ($event) => this.updateMouseUp($event);
        this.eventListeners.contextMenu = ($event) => this.onContextMenu($event);
        this.eventListeners.mouseLeave = ($event) => this.updateMouseUp($event);
        this.eventListeners.mouseWheel = ($event) => this.mouseWheel($event);
        this.eventListeners.changedWheelPosition = true;
        this.eventListeners.changedMouseMove = true;
        this.eventListeners.changedMouseDown = true;
        this.eventListeners.changedMouseUp = true;
        this.eventListeners.changedContextMenu = true;
        this.eventListeners.changedMouseLeave = true;
        this.setDefaultSize();
        this.svgHandler.addListeners(this.eventListeners);
    }

    private onMouseDown(event: MouseEvent): void {
        const actualCoordinates = { x: event.offsetX, y: event.offsetY };
        if (this.selectionSvgHandler.clickedOnElement) {
            this.updateMouseDown(event);
            this.undoRedo.addElementBeforeTransform(this.selectionCalculator.selectedObjects);
            this.mouseHandler.isMouseDown = true;
            return;
        } else if (this.selectionSvgHandler.mouseInSelection(actualCoordinates, this.boundary.nativeElement) && event.button === 0) {
            this.updateMouseDown(event);
            this.undoRedo.addElementBeforeTransform(this.selectionCalculator.selectedObjects);
            this.clickedInsideSelection = true;
            return;
        }
        if (event.button === 0) {
            this.updateMouseDown(event);
            this.selectionSvgHandler.unselectAll();
        } else if (event.button === 2) {
            this.updateMouseDown(event, 2);
        }
    }

    private onContextMenu(event: MouseEvent): boolean {
        this.updateContextMenu(event);
        return false;
    }

    protected destroyExtend(): void {
        if (this.svgHandler.elements) {
            this.selectionSvgHandler.unselectAll();
        }
    }

    private updateContextMenu(event: MouseEvent): void {
        event.preventDefault();
    }

    protected mouseMouvementExtend(): void {
        if (!this.mouseHandler.isMouseDown) {
            return;
        } else if (this.selectionSvgHandler.clickedOnElement || this.clickedInsideSelection) {
            this.selectionMove.moveWithMouse();
            this.selectionMoved = true;
            return;
        }
        const previsualizationRectangle = this.previsualizationRectangleService.createPrevisualizationRectangle();
        this.previsualisationRectangleValues.minCoords.x = Number(previsualizationRectangle.getAttribute('x'));
        this.previsualisationRectangleValues.minCoords.y = Number(previsualizationRectangle.getAttribute('y'));
        this.previsualisationRectangleValues.maxCoords.x =
            Number(previsualizationRectangle.getAttribute('x')) + Number(previsualizationRectangle.getAttribute('width'));
        this.previsualisationRectangleValues.maxCoords.y =
            Number(previsualizationRectangle.getAttribute('y')) + Number(previsualizationRectangle.getAttribute('height'));
        if (this.buttonUsed === 0) {
            this.selectionSvgHandler.findAllObjectsInLimits(
                { x: this.previsualisationRectangleValues.minCoords.x, y: this.previsualisationRectangleValues.minCoords.y },
                { x: this.previsualisationRectangleValues.maxCoords.x, y: this.previsualisationRectangleValues.maxCoords.y }
            );
        } else if (this.buttonUsed === 2) {
            this.selectionSvgHandler.toggleObjectsInLimits(
                { x: this.previsualisationRectangleValues.minCoords.x, y: this.previsualisationRectangleValues.minCoords.y },
                { x: this.previsualisationRectangleValues.maxCoords.x, y: this.previsualisationRectangleValues.maxCoords.y }
            );
        }
        this.mouseMoved = true;
    }

    protected mouseUpExtend(coordinates: Coordinates, button: number): void {
        this.previsualizationRectangleService.deletePrevisualization();
        const isMouseInSelection = this.selectionSvgHandler.mouseInSelection(coordinates, this.boundary.nativeElement);
        if (isMouseInSelection && button === 0 && !this.mouseMoved && !this.selectionSvgHandler.clickedOnElement) {
            this.selectionSvgHandler.clickedOnBoundary = false;
            this.undoRedo.transformCurrent();
        } else if (button === 0 && this.selectionMoved && this.selectionSvgHandler.clickedOnElement) {
            this.undoRedo.transformCurrent();
        }
        this.selectionMoved = false;
        this.selectionSvgHandler.clickedOnElement = false;
        this.mouseMoved = false;
        this.clickedInsideSelection = false;
        delete this.selectionSvgHandler.nonTogableItem;
        this.selectionMove.resetMouseCoordinates();
    }

    protected mouseDownExtend(): void {
        this.selectionSvgHandler.toggledItems = new Array<SVGElement>();
    }

    @HostListener('window:keyup', ['$event']) handleKeyUp(event: KeyboardEvent): void {
        if (this.selectionCalculator.selectedObjects.length > 0) {
            switch (event.key) {
                case 'ArrowUp':
                    this.selectionMove.destroyInterval(NavigationArrows.up);
                    break;
                case 'ArrowDown':
                    this.selectionMove.destroyInterval(NavigationArrows.down);
                    break;
                case 'ArrowLeft':
                    this.selectionMove.destroyInterval(NavigationArrows.left);
                    break;
                case 'ArrowRight':
                    this.selectionMove.destroyInterval(NavigationArrows.right);
                    break;
            }
        }
        if (!event.shiftKey) {
            this.shiftDown = false;
            event.preventDefault();
        }
        if (!event.altKey) {
            this.altDown = false;
            event.preventDefault();
        }
    }

    @HostListener('window:keydown', ['$event']) handleKeyDown(event: KeyboardEvent): void {
        if (!this.dialog.openDialogs.length) {
            if (this.selectionCalculator.selectedObjects.length > 0) {
                switch (event.key) {
                    case 'ArrowUp':
                        event.preventDefault();
                        this.selectionMove.keyboardDelay(NavigationArrows.up);
                        break;
                    case 'ArrowDown':
                        event.preventDefault();
                        this.selectionMove.keyboardDelay(NavigationArrows.down);
                        break;
                    case 'ArrowLeft':
                        event.preventDefault();
                        this.selectionMove.keyboardDelay(NavigationArrows.left);
                        break;
                    case 'ArrowRight':
                        event.preventDefault();
                        this.selectionMove.keyboardDelay(NavigationArrows.right);
                        break;
                }
                if (event.shiftKey) {
                    this.shiftDown = true;
                    this.selectionCalculator.computeCenters();
                    event.preventDefault();
                }
                if (event.altKey) {
                    this.altDown = true;
                    event.preventDefault();
                }
            }
            if (event.ctrlKey && event.key === SELECT_ALL_KEY) {
                this.selectionSvgHandler.selectAll();
            }
        }
    }

    private mouseWheel(event: WheelEvent): void {
        event.preventDefault();
        this.selectionRotate.wheelMoved((event.deltaY < 0), this.shiftDown, this.altDown);
    }

    protected setDefaultSize(): void {
        return;
    }
}
