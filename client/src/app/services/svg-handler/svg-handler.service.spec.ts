import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EventListeners } from '../../interfaces-enums/event-listeners';
import { SvgHandlerService } from './svg-handler.service';

describe('SvgHandlerService', () => {
    let service: SvgHandlerService;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    const color = 'red';
    const truthEventListeners: EventListeners = {
        mouseMove: () => {return; },
        changedMouseMove: true,
        mouseDown: () => {return; },
        changedMouseDown: true,
        mouseUp: () => {return; },
        changedMouseUp: true,
        mouseLeave: () => {return; },
        changedMouseLeave: true,
        mouseClick: () => {return; },
        changedMouseClick: true,
        mouseDblClick: () => {return; },
        changedMouseDblClick: true,
        mouseWheel: () => {return; },
        changedWheelPosition: true,
        contextMenu: () => {return; },
        changedContextMenu: true

    };
    const falseEventListeners: EventListeners = {
        mouseMove: () => {return; },
        changedMouseMove: false,
        mouseDown: () => {return; },
        changedMouseDown: false,
        mouseUp: () => {return; },
        changedMouseUp: false,
        mouseLeave: () => {return; },
        changedMouseLeave: false,
        mouseClick: () => {return; },
        changedMouseClick: false,
        mouseDblClick: () => {return; },
        changedMouseDblClick: false,
        mouseWheel: () => {return; },
        changedWheelPosition: false,
        contextMenu: () => {return; },
        changedContextMenu: false
    };

    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [Renderer2]
        }).compileComponents()
    );

    beforeEach(() => {
        rendererSpy = jasmine.createSpyObj('Renderer2',
                                            ['createElement',
                                            'setStyle',
                                            'appendChild',
                                            'removeChild',
                                            'setAttribute',
                                            'listen']);
        service = new SvgHandlerService();
        service.renderer = rendererSpy;
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should create svgCanvas', () => {
        const dimensions = 200;
        service.createCanvas(dimensions, dimensions, color);
        const calledTimes = 8;
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(calledTimes);
        expect(rendererSpy.appendChild).toHaveBeenCalled();
    });

    it('should delete all elements in the canvas', () => {
        service.deleteCanvas();
        expect(rendererSpy.removeChild).toHaveBeenCalledTimes(1);
    });

    it('should add listeners', () => {
        service.addListeners(truthEventListeners);
        const calledTimes = 8;
        expect(rendererSpy.listen).toHaveBeenCalledTimes(calledTimes);
    });

    it('should not add listeners', () => {
        service.addListeners(falseEventListeners);
        expect(rendererSpy.listen).toHaveBeenCalledTimes(0);
    });

    it('should delete listeners', () => {
        rendererSpy.listen.and.returnValue(() => {return; });
        service.addListeners(truthEventListeners);
        service.deleteListeners(truthEventListeners);
        const calledTimes = 8;
        expect(rendererSpy.listen).toHaveBeenCalledTimes(calledTimes);
    });

    it('should not delete listeners', () => {
        rendererSpy.listen.and.returnValue(() => {return; });
        service.addListeners(falseEventListeners);
        service.deleteListeners(falseEventListeners);
        expect(rendererSpy.listen).toHaveBeenCalledTimes(0);
    });

    it('should update background color', () => {
        service.updateBackgroundColor('blue');
        expect(rendererSpy.setStyle).toHaveBeenCalledTimes(1);
        expect(rendererSpy.appendChild).toHaveBeenCalledTimes(1);
    });
});
