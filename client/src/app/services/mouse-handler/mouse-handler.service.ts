import { Injectable } from '@angular/core';
import { Coordinates } from '../../interfaces-enums/coordinates';

const INIT_COORDS = -1;

@Injectable({
    providedIn: 'root'
})
export class MouseHandlerService {
    startCoordinates: Coordinates;
    actualCoordinates: Coordinates;
    endCoordinates: Coordinates;
    isMouseDown: boolean;

    constructor() {
        this.startCoordinates = {x: INIT_COORDS, y: INIT_COORDS};
        this.actualCoordinates = {x: INIT_COORDS, y: INIT_COORDS};
        this.endCoordinates = {x: INIT_COORDS, y: INIT_COORDS};
        this.isMouseDown = false;
    }

    onMouseDown(coordinates: Coordinates): void {
        this.startCoordinates = coordinates;
        this.isMouseDown = true;
    }

    onMouseMovement(coordinates: Coordinates): void {
        this.actualCoordinates = coordinates;
    }

    onMouseUp(coordinates: Coordinates): void {
        this.endCoordinates = coordinates;
        this.isMouseDown = false;
    }

    onMouseLeave(): void {
        this.endCoordinates = this.actualCoordinates;
        this.isMouseDown = false;
    }

    onMouseClick(coordinates: Coordinates, counter: number): void {
        if (counter === 0) {
            this.startCoordinates = coordinates;
            this.actualCoordinates = coordinates;
        }
        this.endCoordinates = coordinates;
    }

    makeActualCoordsEndCoords(): void {
        this.actualCoordinates = this.endCoordinates;
    }

    updateEndCoords(coordinates: Coordinates): void {
        this.endCoordinates = coordinates;
    }
}
