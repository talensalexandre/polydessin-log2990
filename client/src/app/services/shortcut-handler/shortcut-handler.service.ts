import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ShortcutHandlerService {
    private shortcuts: Map<string, () => void>;
    private ctrlShortcuts: Map<string, () => void>;

    constructor() {
        this.shortcuts = new Map<string, () => void>();
        this.ctrlShortcuts = new Map<string, () => void>();
    }

    addShortcut(key: string, callback: () => void): void {
        this.shortcuts.set(key, callback);
    }

    addCtrlShortcut(key: string, callback: () => void): void {
        this.ctrlShortcuts.set(key, callback);
    }

    executeShortcut(event: KeyboardEvent): void {
        const callback = this.shortcuts.get(event.key);
        if (callback && !event.ctrlKey) {
            event.preventDefault();
            callback();
        }
    }

    executeCtrlShortcut(event: KeyboardEvent): void {
        const callback = this.ctrlShortcuts.get(event.key);
        if (callback && event.ctrlKey) {
            event.preventDefault();
            callback();
        }
    }
}
