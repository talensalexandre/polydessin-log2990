import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectionType } from 'src/app/interfaces-enums/selection-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { EllipseHandlerService } from 'src/app/services/ellipse-handler/ellipse-handler.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { PrevisualisationRectangleService } from 'src/app/services/previsualisation-rectangle/previsualisation-rectangle.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { EllipseComponent } from './ellipse.component';

// tslint:disable:no-string-literal no-magic-numbers

describe('EllipseComponent', () => {
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerServiceSpy: jasmine.SpyObj<MouseHandlerService>;
    let mouseHandler: MouseHandlerService;
    let ellipseHandlerSpy: jasmine.SpyObj<EllipseHandlerService>;
    let drawingServiceSpy: jasmine.SpyObj<DrawingService>;
    let component: EllipseComponent;
    let fixture: ComponentFixture<EllipseComponent>;
    let previsualizationRectangleSpy: jasmine.SpyObj<PrevisualisationRectangleService>;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:ellipse');

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]);
        mouseHandlerServiceSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]);
        ellipseHandlerSpy = jasmine.createSpyObj(
            'EllipseHandlerService',
            [
                'createElement',
                'calculateCircle',
                'calculateEllipse',
                'elementPrevisualization',
                'deletePrevisualization',
                'isShapeTooBigComparedToSize'
        ]);

        previsualizationRectangleSpy = jasmine.createSpyObj(
            'PrevisualizationRectangleService',
            [
                'createPrevisualizationRectangle',
                'deletePrevisualization'
            ]);
        drawingServiceSpy = jasmine.createSpyObj('DrawingService', ['removeElement']);
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
        imports:
            [
                MatFormFieldModule, MatSelectModule, BrowserAnimationsModule,
                FormsModule, MatInputModule, MatIconModule, MatDialogModule, HttpClientModule
            ],
        declarations: [ EllipseComponent, ColorComponent ],
        providers: [{provide: SvgHandlerService, useValue: svgHandlerServiceSpy},
                    {provide: MouseHandlerService, useValue: mouseHandlerServiceSpy},
                    {provide: EllipseHandlerService, useValue: ellipseHandlerSpy},
                    {provide: DrawingService, useValue: drawingServiceSpy},
                    {provide: PrevisualisationRectangleService, useValue: previsualizationRectangleSpy}]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EllipseComponent);
        mouseHandler = TestBed.get(MouseHandlerService);
        component = fixture.componentInstance;
        component['svgElement'] = fakeSvgElement;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened', () => {
        component.ngOnInit();
        expect(svgHandlerServiceSpy.addListeners).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', {key: 'Shift'});
        mouseHandler.isMouseDown = true;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeTruthy();
        expect(ellipseHandlerSpy.calculateCircle).toHaveBeenCalled();
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', {key: 'Shift'});
        mouseHandler.isMouseDown = false;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeTruthy();
        expect(ellipseHandlerSpy.calculateCircle).toHaveBeenCalledTimes(0);
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey pressed', () => {
        const shiftKeyMock = new KeyboardEvent('keydown', {key: 'alt'});
        mouseHandler.isMouseDown = false;
        component.handleKeyDown(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(ellipseHandlerSpy.calculateCircle).toHaveBeenCalledTimes(0);
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', {key: 'Shift'});
        mouseHandler.isMouseDown = true;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(ellipseHandlerSpy.calculateEllipse).toHaveBeenCalled();
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalled();
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', {key: 'alt'});
        mouseHandler.isMouseDown = false;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(ellipseHandlerSpy.calculateEllipse).toHaveBeenCalledTimes(0);
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should change shiftKeyDown and call the right function when shiftkey let go', () => {
        const shiftKeyMock = new KeyboardEvent('keyup', {key: 'Shift'});
        mouseHandler.isMouseDown = false;
        component.handleKeyUp(shiftKeyMock);
        expect(component['shiftKeyDown']).toBeFalsy();
        expect(ellipseHandlerSpy.calculateEllipse).toHaveBeenCalledTimes(0);
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
    });

    it('should call onMouseMovement when mouse moves with shiftKeyDown=true and mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        const shiftKeyDown = true;
        component.selectedType = SelectionType.outline;
        component['shiftKeyDown'] = shiftKeyDown;
        mouseHandler.isMouseDown = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(ellipseHandlerSpy.calculateCircle).toHaveBeenCalled();
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should call mouseMouvementExtend when mouse moves with shiftKeyDown=false and mouse not down', () => {
        const mockMouse = new MouseEvent('mousemove');
        mouseHandler.isMouseDown = false;
        component.selectedType = SelectionType.outline;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(ellipseHandlerSpy.calculateEllipse).toHaveBeenCalledTimes(0);
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should not call onMouseUp when mouse goes up but mouse did not move', () => {
        const mockMouseUp = new MouseEvent('mouseup');
        component['mouseMoved'] = false;
        component.selectedType = SelectionType.outline;
        component['eventListeners'].mouseUp(mockMouseUp);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(ellipseHandlerSpy.createElement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should change fill and stroke when contour', () => {
        component.selectedType = SelectionType.outline;
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
    });

    it('should change fill and stroke when plein', () => {
        component.selectedType = SelectionType.fill;
        component.layoutType();
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeFalsy();
    });

    it('should change fill and stroke when plein avec contour', () => {
        component.selectedType = SelectionType.outlineAndFill;
        component.layoutType();
        expect(component['fill']).toBeTruthy();
        expect(component.stroke).toBeTruthy();
    });

    it('should call onMouseUp when eventListeners.mouseUp is called', () => {
        ellipseHandlerSpy.createElement.and.returnValue(fakeSvgElement);
        previsualizationRectangleSpy.createPrevisualizationRectangle();
        const mockMouseUp = new MouseEvent('mouseup');
        component['mouseMoved'] = true;
        component.selectedType = SelectionType.outline;
        component['eventListeners'].mouseUp(mockMouseUp);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(ellipseHandlerSpy.createElement).toHaveBeenCalledTimes(1);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should call onMouseDown when eventListeners.mouseDown is called', () => {
        const mockMouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouseDown);
        expect(mouseHandlerServiceSpy.onMouseDown).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should call mouseMouvementExtend when mouse moves with shiftKeyDown=false and mouse down', () => {
        const mockMouse = new MouseEvent('mousemove');
        const shiftKeyDown = false;
        component.selectedType = SelectionType.outline;
        component['shiftKeyDown'] = shiftKeyDown;
        mouseHandler.isMouseDown = true;
        component['eventListeners'].mouseMove(mockMouse);
        expect(component['fill']).toBeFalsy();
        expect(component.stroke).toBeTruthy();
        expect(ellipseHandlerSpy.calculateEllipse).toHaveBeenCalled();
        expect(ellipseHandlerSpy.elementPrevisualization).toHaveBeenCalled();
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerServiceSpy.isMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should should call eventListeners.mouseLeave when mouse moves out of canvas', () => {
        component['isDrawing'] = true;
        const mouseEvent = new MouseEvent('click');
        component['mouseHandler'].isMouseDown = true;
        component['eventListeners'].mouseLeave(mouseEvent);
        expect(component['isDrawing']).toEqual(false);
        expect(component['mouseHandler'].isMouseDown).toEqual(false);
        expect(mouseHandlerServiceSpy.onMouseLeave).toHaveBeenCalled();
    });

    it('should have size to 0 because its in fill mode and size < fill min size', () => {
        component.size = -1;
        component.selectedType = SelectionType.fill;
        component.changeSize();
        expect(component.size).toBe(0);
    });

    it('should have size to 3 because its in fill mode but size > fill min size', () => {
        component.size = 3;
        component.selectedType = SelectionType.fill;
        component.changeSize();
        expect(component.size).toBe(3);
    });

    it('should have size to 200 because its in fill mode and size > fill max size', () => {
        component.size = 201;
        component.selectedType = SelectionType.fill;
        component.changeSize();
        expect(component.size).toBe(200);
    });

    it('should have size to 199 because its in fill mode and size < fill max size', () => {
        component.size = 199;
        component.selectedType = SelectionType.fill;
        component.changeSize();
        expect(component.size).toBe(199);
    });
});
