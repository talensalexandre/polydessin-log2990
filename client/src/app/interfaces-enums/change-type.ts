export enum ChangeType {
    addToCanvas,
    transform,
    elementRemoved,
    colorStroke,
    colorFill
}
