// Some external source code has been used for reference
// Lukas Marx (2018) Creating a Color Picker Component with Angular [Online]
// Available : https://github.com/LukasMarx/angular-color-picker

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';

@Component({
    selector: 'app-color-pop',
    templateUrl: './color-popup.component.html',
    styleUrls: ['./color-popup.component.scss']
})
export class ColorPopupComponent {
    colorPreviewContext: CanvasRenderingContext2D;
    colorSelection: RGBA;
    colorHistory: RGBA[];
    shouldUpdateGradient: string;
    shouldUpdateForAlpha: string;

    constructor(
        public dialogRef: MatDialogRef<ColorPopupComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ColorChoice,
        private colorManager: ColorManagerService) {
            this.colorSelection = this.colorManager.colorSelected[this.data];
            this.colorHistory = this.colorManager.colorHistory;
        }

    closeWindow(): void {
        this.dialogRef.close();
    }

    updateHex(): void {
        this.colorManager.updateColorWithHex(
            this.data, this.colorSelection.Hex.Red,
            this.colorSelection.Hex.Green,
            this.colorSelection.Hex.Blue
        );
    }

    mouseClickOnHistory(event: MouseEvent, colorElement: RGBA): void {
        event.preventDefault();
        const cloneColor: RGBA = JSON.parse(JSON.stringify(colorElement));
        if (event.buttons === 1) {
            this.colorHistory.splice(this.colorHistory.indexOf(colorElement), 1);
            this.colorManager.updateColorWithRGBA(ColorChoice.mainColor, cloneColor, false);
        } else if (event.button === 2) {
            this.colorHistory.splice(this.colorHistory.indexOf(colorElement), 1);
            this.colorManager.updateColorWithRGBA(ColorChoice.secondaryColor, cloneColor, false);
        }
    }

    contextMenu(event: MouseEvent): void {
        return event.preventDefault();
    }
}
