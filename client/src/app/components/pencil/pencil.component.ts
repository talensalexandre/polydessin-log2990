import { Component } from '@angular/core';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { DrawingService } from '../../services/drawing/drawing.service';
import { StateSelectorService } from '../../services/state-selector/state-selector.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const DEFAULT_SIZE = 3;

@Component({
    selector: 'app-pencil',
    templateUrl: './pencil.component.html',
    styleUrls: ['./pencil.component.scss']
})
export class PencilComponent extends AbstractToolComponent {

    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        protected drawingService: DrawingService,
        public stateSelector: StateSelectorService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
    }

    protected mouseMouvementExtend(coordinates: Coordinates): void {
        if (this.mouseHandler.isMouseDown) {
            this.drawingService.addCoordinatesToPolyline(this.svgElement, coordinates);
        }
    }

    protected mouseDownExtend(coordinates: Coordinates): void {
        this.svgElement = this.drawingService.createPolyLine(
            coordinates,
            String(this.size)
        );
        this.mouseDownExtra();
    }

    protected mouseUpExtend(): void {
        return;
    }

    protected mouseDownExtra(): void {
        return;
    }

    protected destroyExtend(): void {
        return;
    }
}
