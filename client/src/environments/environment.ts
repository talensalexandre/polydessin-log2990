// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { IEnvironment } from './i-environment';

export const environment: IEnvironment = {
    production: false,
    SEND_URL: 'http://localhost:3000/log2990/projet2/drawing/drawings',
    GET_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/',
    GET_ALL_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/drawings',
    DELETE_URL: 'http://localhost:3000/log2990/projet2/drawing/drawing/',
    SEND_EMAIL_URL: 'http://localhost:3000/log2990/projet2/email/send'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
