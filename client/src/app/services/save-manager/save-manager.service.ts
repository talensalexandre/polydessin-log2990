import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { DrawingStored, MetaDrawing } from '../../interfaces-enums/drawing';

const SEND_URL = environment.SEND_URL;
const GET_URL = environment.GET_URL;
const GET_ALL_URL = environment.GET_ALL_URL;
const DELETE_URL = environment.DELETE_URL;

@Injectable({
    providedIn: 'root'
})
export class SaveManagerService {
    svgContainer: string[];
    private serializer: XMLSerializer;
    private autoSavedDrawing: DrawingStored;

    constructor(private http: HttpClient) {
        this.svgContainer = new Array();
        const metaDrawing: MetaDrawing = {
            timestamp: 0,
            name: '',
            labels: [],
            width: 0,
            height: 0,
            backgroundColor: ''
        };
        this.autoSavedDrawing = { metaDrawing, svgContainer: this.svgContainer };
        this.serializer = new XMLSerializer();
    }

    addSvgElement(svgElement: SVGElement): void {
        this.svgContainer.push(this.serializer.serializeToString(svgElement));
        this.autosave();
    }

    removeSvgElement(svgElement: SVGElement): void {
        this.svgContainer = this.svgContainer.filter((svg) => svg !== this.serializer.serializeToString(svgElement));
        this.autosave();
    }

    sendDrawing(metaDrawing: MetaDrawing): Observable<number> {
        const drawingToStore: DrawingStored = {
            metaDrawing,
            svgContainer: this.svgContainer
        };
        return this.http.post<number>(SEND_URL, drawingToStore)
            .pipe(catchError((error: HttpErrorResponse) => {
                return of(error.status);
            }));
    }

    getDrawing(timestamp: string): Observable<DrawingStored | number> {
        return this.http.get<DrawingStored | number>(GET_URL + timestamp)
            .pipe(catchError((error: HttpErrorResponse) => {
                return of(error.status);
            }));
    }

    getDrawings(): Observable<DrawingStored[] | number> {
        return this.http.get<DrawingStored[] | number>(GET_ALL_URL)
            .pipe(catchError((error: HttpErrorResponse) => {
                return of(error.status);
            }));
    }

    deleteDrawing(timestamp: string): Observable<DrawingStored | number> {
        return this.http.delete<DrawingStored | number>(DELETE_URL + timestamp)
            .pipe(catchError((error: HttpErrorResponse) => {
                return of(error.status);
            }));
    }

    initialiseElements(): void {
        this.svgContainer = new Array<string>();
    }

    autosaveBackground(colorString: string, canvasWidth: number, canvasHeight: number): void {
        const metaDrawing: MetaDrawing = {
            timestamp: new Date().getTime(),
            name: '',
            labels: [],
            width: canvasWidth,
            height: canvasHeight,
            backgroundColor: colorString
        };
        this.autoSavedDrawing = { metaDrawing, svgContainer: this.svgContainer };
        this.autosave();
    }

    private autosave(): void {
        this.autoSavedDrawing.svgContainer = this.svgContainer;
        localStorage.setItem('drawing', JSON.stringify(this.autoSavedDrawing));
    }

}
