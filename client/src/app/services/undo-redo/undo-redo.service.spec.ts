import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { AppComponent } from 'src/app/components/app/app.component';
import { AttributeType } from 'src/app/interfaces-enums/attribute-type';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { TempElement } from '../../interfaces-enums/temp-element';
import { SaveManagerService } from '../save-manager/save-manager.service';
import { SelectionSvgHandlerService } from '../selection-services/selection-svg-handler/selection-svg-handler.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from './undo-redo.service';

// tslint:disable:max-file-line-count no-string-literal

describe('UndoRedoService', () => {
    let svgHandlerServiceSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let service: UndoRedoService;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    let selectionSvgHandlerSpy: jasmine.SpyObj<SelectionSvgHandlerService>;
    let fakeElementStroke: SVGElement;
    let fakeElementShape: SVGElement;
    let fakeElementToAdd: TempElement | undefined;
    let fakeElementArray: SVGElement[];

    beforeEach(() =>
        TestBed.configureTestingModule({
            declarations: [AppComponent],
        })
    );

    beforeEach(() => {
        svgHandlerServiceSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer',
                'elements'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'setAttribute',
                'listen',
                'appendChild',
                'removeChild',
                'setStyle'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'addSvgElement',
                'removeSvgElement'
            ]
        );
        selectionSvgHandlerSpy = jasmine.createSpyObj(
            'SelecionSvgHandlerSpy',
            [
                'unselectAll'
            ]
        );
        svgHandlerServiceSpy.renderer = rendererSpy;
        svgHandlerServiceSpy.elements = new Array<SVGElement>();
        service = new UndoRedoService(svgHandlerServiceSpy, saveManagerSpy, selectionSvgHandlerSpy);
        fakeElementStroke = document.createElementNS('http://www.w3.org/2000/svg', 'svg:polyline');
        fakeElementShape = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
        fakeElementArray = [fakeElementStroke, fakeElementShape];
        service.current = new Array<TempElement>();
        service['undos'] = new Array<TempElement>();
        service['redos'] = new Array<TempElement>();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('it should return undefined when call getTransform and transform is undefined', () => {
        const transform = service['getTransform'](fakeElementStroke);
        expect(transform).toBe('undefined');
    });

    it('it should return transform when call getTransform ', () => {
        fakeElementStroke.setAttribute('transform', 'translate(0,0)');
        const transform = service['getTransform'](fakeElementStroke);
        expect(transform).toBe('translate(0,0)');
    });

    it('should add to current from color applicator with stroke', () => {
        fakeElementStroke.setAttribute('stroke', 'rgb(255,0,0)');
        service.addFromColorApplicator(fakeElementStroke, AttributeType.stroke);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = 'rgb(255,0,0)';
        fakeElementToAdd = { changeType: ChangeType.colorStroke, change: changeArray, svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add to current from color applicator with fill', () => {
        fakeElementShape.style.fill = 'rgb(255,0,0)';
        service.addFromColorApplicator(fakeElementShape, AttributeType.fill);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementShape;
        changeArray[0] = 'rgb(255, 0, 0)';
        fakeElementToAdd = { changeType: ChangeType.colorFill, change: changeArray, svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add to current from color applicator with stroke but no color set', () => {
        service.addFromColorApplicator(fakeElementStroke, AttributeType.stroke);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = '';
        fakeElementToAdd = { changeType: ChangeType.colorStroke, change: changeArray, svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add to current from color applicator with fill but no color set', () => {
        service.addFromColorApplicator(fakeElementShape, AttributeType.fill);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementShape;
        changeArray[0] = '';
        fakeElementToAdd = { changeType: ChangeType.colorFill, change: changeArray, svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add to current from eraser', () => {
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        service.updateCurrent(svgElementArray, ChangeType.elementRemoved);
        fakeElementToAdd = { changeType: ChangeType.elementRemoved, change: [], svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
    });

    it('should add to current', () => {
        const canRedo = false;
        service['canRedo'] = canRedo;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add to current and empty redos', () => {
        const canRedo = true;
        service['canRedo'] = canRedo;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        expect(service.current).toContain(fakeElementToAdd);
        expect(service['redos'].length).toEqual(0);
        expect(service['canRedo']).toBeFalsy();
        expect(service.showUndo).toBeTruthy();
    });

    it('should undo with addToCanvas', () => {
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
        expect(svgHandlerServiceSpy.renderer.removeChild).toHaveBeenCalled();
        expect(service['redos'].length).toBeGreaterThan(0);
        expect(service.showUndo).toBeFalsy();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElementStroke);
    });

    it('should add array to current', () => {
        const canRedo = false;
        const changeArray = new Array<string>();
        service['canRedo'] = canRedo;
        service.updateCurrent(fakeElementArray, ChangeType.addToCanvas);
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: fakeElementArray };
        expect(service.current).toContain(fakeElementToAdd);
        expect(service.showUndo).toBeTruthy();
    });

    it('should add array to current and empty redos', () => {
        const canRedo = true;
        service['canRedo'] = canRedo;
        const changeArray = new Array<string>();
        service.updateCurrent(fakeElementArray, ChangeType.addToCanvas);
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: fakeElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        expect(service.current).toContain(fakeElementToAdd);
        expect(service['redos'].length).toEqual(0);
        expect(service['canRedo']).toBeFalsy();
        expect(service.showUndo).toBeTruthy();
    });

    it('should undo with colorStroke', () => {
        fakeElementStroke.setAttribute('stroke', 'rgb(255, 0, 0)');
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = 'rgb(255, 0, 0)';
        fakeElementToAdd = { changeType: ChangeType.colorStroke, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
        expect(svgHandlerServiceSpy.renderer.setAttribute).toHaveBeenCalledWith(fakeElementToAdd.svgElement[0],
                                                                            'stroke',
                                                                            fakeElementToAdd.change[0]);
        expect(service['redos'].length).toBeGreaterThan(0);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElementStroke);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElementStroke);
    });

    it('should undo with colorFill', () => {
        fakeElementShape.style.fill = 'rgb(255, 0, 0)';
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementShape;
        changeArray[0] = 'rgb(255, 0, 0)';
        fakeElementToAdd = { changeType: ChangeType.colorFill, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
        expect(svgHandlerServiceSpy.renderer.setStyle).toHaveBeenCalledWith(fakeElementToAdd.svgElement[0],
                                                                            'fill',
                                                                            fakeElementToAdd.change[0]);
        expect(service['redos']).toContain(fakeElementToAdd);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
    });

    it('should undo with transform', () => {
        fakeElementStroke.setAttribute('transform', 'translate(0,0)');
        const changeArray = new Array<string>();
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = service['getTransform'](fakeElementStroke);
        fakeElementToAdd = { changeType: ChangeType.transform, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
        expect(svgHandlerServiceSpy.renderer.setAttribute).toHaveBeenCalledWith(svgElementArray[0],
                                                                                'transform',
                                                                                fakeElementToAdd.change[0]);
        expect(service['redos']).toContain(fakeElementToAdd);
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
    });

    it('should undo with elementRemoved', () => {
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.elementRemoved, change: [], svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
        expect(svgHandlerServiceSpy.renderer.appendChild).toHaveBeenCalledWith(service['svgHandler'].svgCanvas, fakeElementStroke);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElementStroke);
        expect(service['redos']).toContain(fakeElementToAdd);
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(service.showRedo).toBeFalsy();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElementStroke);
        expect(svgHandlerServiceSpy.renderer.removeChild).toHaveBeenCalledWith(service['svgHandler'].svgCanvas, fakeElementStroke);
        expect(service['redos']).not.toContain(fakeElementToAdd);
    });

    it('should  not add to redos if element undefined when call undo', () => {
        service.undo();
        expect(service['canRedo']).toBeTruthy();
        expect(service['redos'].length).toEqual(0);
        expect(service.showRedo).toBeFalsy();
    });

    it('should redo with addToCanvas and can not redo', () => {
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.appendChild).toHaveBeenCalled();
        expect(service.showUndo).toBeTruthy();
        service['redos'].length = 0;
        expect(service.showRedo).toBeFalsy();
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElementStroke);
    });

    it('should redo with addToCanvas and still can redo', () => {
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        service.updateCurrent(new Array(fakeElementShape), ChangeType.addToCanvas);
        service.undo();
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.appendChild).toHaveBeenCalled();
        expect(service.current.length).toBeGreaterThan(0);
        expect(service.showUndo).toBeTruthy();
        expect(service.showRedo).toBeTruthy();
    });

    it('should  not add to current if element undefined when call redo', () => {
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(service.current.length).toEqual(0);
        expect(service.showUndo).toBeFalsy();
        expect(service.showRedo).toBeFalsy();
    });

    it('should not do anything if length of redos is 0', () => {
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(service.current.length).toEqual(0);
        expect(service.showRedo).toBeFalsy();
    });

    it('should redo with addToCanvas', () => {
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        fakeElementToAdd = { changeType: ChangeType.addToCanvas, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.updateCurrent(new Array(fakeElementStroke), ChangeType.addToCanvas);
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.appendChild).toHaveBeenCalled();
        expect(service.showUndo).toBeTruthy();
    });

    it('should redo with colorStroke', () => {
        fakeElementStroke.style.stroke = 'rgb(255, 0, 0)';
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = 'rgb(255, 0, 0)';
        fakeElementToAdd = { changeType: ChangeType.colorStroke, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.setAttribute).toHaveBeenCalledWith(fakeElementToAdd.svgElement[0],
                                                                            'stroke',
                                                                            fakeElementToAdd.change[0]);
        expect(service.showUndo).toBeTruthy();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(fakeElementStroke);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(fakeElementStroke);
    });

    it('should redo with colorFill', () => {
        fakeElementShape.style.fill = 'rgb(255, 0, 0)';
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementShape;
        changeArray[0] = 'rgb(255, 0, 0)';
        fakeElementToAdd = { changeType: ChangeType.colorFill, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.setStyle).toHaveBeenCalledWith(fakeElementToAdd.svgElement[0],
                                                                            'fill',
                                                                            fakeElementToAdd.change[0]);
        expect(service.showUndo).toBeTruthy();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
    });

    it('should redo with transform', () => {
        fakeElementStroke.setAttribute('transform', 'translate(0,0)');
        const changeArray = new Array<string>('');
        const svgElementArray = new Array<SVGElement>();
        svgElementArray[0] = fakeElementStroke;
        changeArray[0] = service['getTransform'](fakeElementStroke);
        fakeElementToAdd = { changeType: ChangeType.transform, change: changeArray, svgElement: svgElementArray };
        service['elementToAdd'] = fakeElementToAdd;
        service.current.push(fakeElementToAdd);
        service.undo();
        service.redo();
        expect(service['canRedo']).toBeFalsy();
        expect(svgHandlerServiceSpy.renderer.setAttribute).toHaveBeenCalledWith(svgElementArray[0],
                                                                                'transform',
                                                                                fakeElementToAdd.change[0]);
        expect(service.showUndo).toBeTruthy();
        expect(saveManagerSpy.removeSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
        expect(saveManagerSpy.addSvgElement).toHaveBeenCalledWith(svgElementArray[0]);
    });

    it('should clear all', () => {
        service.clearAll();
        expect(service.current.length).toEqual(0);
        expect(service['undos'].length).toEqual(0);
        expect(service['redos'].length).toEqual(0);
        expect(service.showUndo).toBeFalsy();
        expect(service.showRedo).toBeFalsy();
        expect(service['canRedo']).toBeFalsy();
    });

    it('should add element before transformation', () => {
        service.addElementBeforeTransform(fakeElementArray);
        expect(service['elementBeforeTransform'].changeType).toBe(ChangeType.transform);
        expect(service['elementBeforeTransform'].change.length).toBe(2);
        expect(service['elementBeforeTransform'].svgElement).toEqual(fakeElementArray);
    });

    it('should add element before transformation', () => {
        service.transformCurrent();
        expect(service.current.length).toBe(1);
        expect(service.showUndo).toBeTruthy();
    });
});
