import { Component } from '@angular/core';
import { GridService } from 'src/app/services/grid/grid.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent {

    constructor(public gridService: GridService,
                public stateSelector: StateSelectorService) { }

    changeSize(): void {
        this.stateSelector.isModifyingInput = false;
        this.gridService.changeGridSize();
    }
}
