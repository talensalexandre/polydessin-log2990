import { TestBed } from '@angular/core/testing';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { MouseHandlerService } from './mouse-handler.service';

// tslint:disable:no-magic-numbers

describe('MouseHandlerService', () => {
    let service: MouseHandlerService;
    let coordinates: Coordinates;
    let positionOfMouse: number;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        service = TestBed.get(MouseHandlerService);
        positionOfMouse = 200;
        coordinates = { x: positionOfMouse, y: positionOfMouse };
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should be created with all coordinates to -1', () => {
        expect(service.startCoordinates.x).toBe(-1);
        expect(service.startCoordinates.y).toBe(-1);
        expect(service.actualCoordinates.x).toBe(-1);
        expect(service.actualCoordinates.y).toBe(-1);
        expect(service.endCoordinates.x).toBe(-1);
        expect(service.endCoordinates.y).toBe(-1);
    });

    it('onMouseDown should read mouse coordinates for start position', () => {
        service.onMouseDown(coordinates);
        expect(service.startCoordinates.x).toBe(positionOfMouse);
        expect(service.startCoordinates.y).toBe(positionOfMouse);
        expect(service.isMouseDown).toBeTruthy();
    });

    it('onMouseMovement should read mouse coordinates for actualCoordinates', () => {
        service.onMouseMovement(coordinates);
        expect(service.actualCoordinates.x).toBe(positionOfMouse);
        expect(service.actualCoordinates.y).toBe(positionOfMouse);
    });

    it('onMouseUp should read mouse coordinates for endCoordinates', () => {
        service.onMouseUp(coordinates);
        expect(service.endCoordinates.x).toBe(positionOfMouse);
        expect(service.endCoordinates.y).toBe(positionOfMouse);
        expect(service.isMouseDown).toBeFalsy();
    });

    it('onMouseLeave should update mouse coordinates for endCoordinates', () => {
        service.onMouseMovement(coordinates);
        service.onMouseLeave();
        expect(service.endCoordinates.x).toBe(positionOfMouse);
        expect(service.endCoordinates.y).toBe(positionOfMouse);
        expect(service.isMouseDown).toBeFalsy();
    });

    it('onMouseClick should just update endCoordinates when counter =! 0', () => {
        const counter = 1;
        service.onMouseClick(coordinates, counter);
        expect(service.endCoordinates.x).toBe(positionOfMouse);
        expect(service.endCoordinates.y).toBe(positionOfMouse);
        expect(service.startCoordinates.x).toBe(-1);
        expect(service.startCoordinates.y).toBe(-1);
        expect(service.actualCoordinates.x).toBe(-1);
        expect(service.actualCoordinates.y).toBe(-1);
    });

    it('onMouseClick should just update endCoordinates when counter = 0', () => {
        const counter = 0;
        service.onMouseClick(coordinates, counter);
        expect(service.endCoordinates.x).toBe(positionOfMouse);
        expect(service.endCoordinates.y).toBe(positionOfMouse);
        expect(service.startCoordinates.x).toBe(positionOfMouse);
        expect(service.startCoordinates.y).toBe(positionOfMouse);
        expect(service.actualCoordinates.x).toBe(positionOfMouse);
        expect(service.actualCoordinates.y).toBe(positionOfMouse);
    });

    it('makeActualCoordsEndCoords should update actualCoordinates according to endCoordinates', () => {
        service.endCoordinates = { x: positionOfMouse, y: positionOfMouse };
        service.makeActualCoordsEndCoords();
        expect(service.actualCoordinates.x).toBe(positionOfMouse);
        expect(service.actualCoordinates.y).toBe(positionOfMouse);
    });

    it('updateEndCoords should update endCoordiantes according to given coordinates', () => {
        service.updateEndCoords(coordinates);
        expect(service.endCoordinates.x).toBe(positionOfMouse);
        expect(service.endCoordinates.y).toBe(positionOfMouse);
    });
});
