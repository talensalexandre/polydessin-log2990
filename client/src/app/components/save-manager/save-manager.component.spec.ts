import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatChipsModule, MatDialogModule, MatDialogRef,
        MatFormFieldModule, MatIconModule, MatSnackBarModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { ExportDrawingService } from 'src/app/services/export-drawing/export-drawing.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SaveManagerComponent } from './save-manager.component';

// tslint:disable: no-string-literal

const FAILED = 0;
const SUCCESS = 201;

describe('SaveManagerComponent', () => {
    let component: SaveManagerComponent;
    let fixture: ComponentFixture<SaveManagerComponent>;
    let stateServiceSpy: jasmine.SpyObj<StateSelectorService>;
    let saveManagerServiceSpy: jasmine.SpyObj<SaveManagerService>;
    let exportServiceSpy: jasmine.SpyObj<ExportDrawingService>;
    let fakeCanvas: Node;
    let fakeSvgElement: SVGElement;

    const mockDialogRef = {
        close: jasmine.createSpy('close')
    };

    beforeEach(() => {
        stateServiceSpy = jasmine.createSpyObj('StateSelectorService', ['changeState']);
        saveManagerServiceSpy = jasmine.createSpyObj('SaveManagerService', ['sendDrawing']);
        exportServiceSpy = jasmine.createSpyObj('ExportDrawingService',
            [
                'initialisePrevisualization'
            ]);
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                    [
                        MatInputModule, MatFormFieldModule, BrowserAnimationsModule,
                        MatDialogModule, FormsModule, MatChipsModule, MatIconModule,
                        HttpClientModule, MatSnackBarModule
                    ],
        declarations: [ SaveManagerComponent ],
        providers: [{ provide: MatDialogRef, useValue: mockDialogRef },
                    { provide: StateSelectorService, useValue: stateServiceSpy },
                    { provide: SaveManagerService, useValue: saveManagerServiceSpy },
                    { provide: ExportDrawingService, useValue: exportServiceSpy }]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SaveManagerComponent);
        fakeCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should add a maximum of 4 labels', () => {
        const MAX_LABEL = 4;
        const event = {
            input: document.createElement('input'),
            value: 'test'
        };
        component.add(event);
        expect(component.metaDrawing.labels.length).toBe(1);
        component.add(event);
        component.add(event);
        component.add(event);
        component.add(event);
        expect(component.metaDrawing.labels.length).toBe(MAX_LABEL);
    });

    it('should not be able to add empty label', () => {
        const event = {
            input: document.createElement('input'),
            value: ''
        };
        component.add(event);
        expect(component.metaDrawing.labels.length).toBe(0);
    });

    it('should remove label selected', () => {
        const event1 = {
            input: document.createElement('input'),
            value: 'test1'
        };
        const event2 = {
            input: document.createElement('input'),
            value: 'test2'
        };
        component.add(event1);
        component.add(event2);
        component.remove('test1');
        expect(component.metaDrawing.labels.includes('test1')).toBeFalsy();
        expect(component.metaDrawing.labels.includes('test2')).toBeTruthy();
    });

    it('should save drawing only if metaDrawing has a name', () => {
        component.saveDrawing();
        expect(component.openConfirmation).toBeFalsy();
        component.metaDrawing.name = 'test';
        component.saveDrawing();
        expect(component.openConfirmation).toBeTruthy();
    });

    it('should send drawing and return to default state', () => {
        component.metaDrawing.name = 'test';
        saveManagerServiceSpy.sendDrawing.and.returnValue(of(SUCCESS));
        component.sendToServer();
        expect(stateServiceSpy.changeState).toHaveBeenCalled();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should display a popup message in any case', () => {
        const popMessage = 'popMessage';
        // tslint:disable-next-line:no-any from cli
        spyOn<any>(component, popMessage).and.callThrough();
        saveManagerServiceSpy.sendDrawing.and.returnValue(of(FAILED));
        component.sendToServer();
        fixture.detectChanges();
        expect(component[popMessage]).toHaveBeenCalledTimes(1);
        saveManagerServiceSpy.sendDrawing.and.returnValue(of(SUCCESS));
        component.sendToServer();
        expect(component[popMessage]).toHaveBeenCalledTimes(2);
    });

    it('should initialize previsu', () => {
        fakeCanvas.appendChild(fakeSvgElement);
        component['svgHandler'].svgCanvas = fakeCanvas as SVGElement;
        component['initialisePrevisualization']();
        expect(exportServiceSpy.initialisePrevisualization).toHaveBeenCalled();
    });
});
