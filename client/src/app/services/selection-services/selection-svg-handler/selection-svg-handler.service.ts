import { Injectable } from '@angular/core';
import { Coordinates } from '../../../interfaces-enums/coordinates';
import { TempElement } from '../../../interfaces-enums/temp-element';
import { SaveManagerService } from '../../save-manager/save-manager.service';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { SelectionCalculatorService } from '../selection-calculator/selection-calculator.service';

const ITEM_NOT_IN_ARRAY = -1;

@Injectable({
    providedIn: 'root'
})
export class SelectionSvgHandlerService {
    clickedOnElement: boolean;
    clickedOnBoundary: boolean;
    selectionRectangles: Map<string, SVGElement>;
    toggledItems: SVGElement[];
    nonTogableItem: SVGElement;
    tempUndoRedo: TempElement;

    constructor(public selectionCalculator: SelectionCalculatorService,
                public svgHandler: SvgHandlerService,
                private saveManager: SaveManagerService
    ) {
        this.clickedOnElement = false;
        this.selectionRectangles = new Map<string, SVGElement>();
        this.toggledItems = new Array<SVGElement>();
        this.clickedOnBoundary = false;
    }

    addNewSelectionRectangle(key: string, element: SVGElement): void {
        this.selectionRectangles.set(key, element);
    }

    toggleSelection(svgElement: SVGElement): void {
        if (svgElement.getAttribute('isSelected') === 'true') {
            this.unselectObject(svgElement);
        } else {
            this.selectObject(svgElement);
        }
    }

    findAllObjectsInLimits(minCoordinates: Coordinates, maxCoordinates: Coordinates): void {
        this.svgHandler.elements.forEach((value) => {
            if (this.selectionCalculator.isObjectInSelection(value, minCoordinates, maxCoordinates)) {
                if (this.selectionCalculator.selectedObjects.indexOf(value) === ITEM_NOT_IN_ARRAY) {
                    this.selectObject(value);
                }
            } else if (this.selectionCalculator.selectedObjects.indexOf(value) !== ITEM_NOT_IN_ARRAY) {
                this.unselectObject(value);
            }
        });
    }

    toggleObjectsInLimits(minCoordinates: Coordinates, maxCoordinates: Coordinates): void {
        this.svgHandler.elements.forEach((value) => {
            if (value !== this.nonTogableItem) {
                if (this.selectionCalculator.isObjectInSelection(value, minCoordinates, maxCoordinates)) {
                    if (this.toggledItems.indexOf(value) === ITEM_NOT_IN_ARRAY) {
                        this.toggledItems.push(value);
                        this.toggleSelection(value);
                    }
                } else if (this.toggledItems.indexOf(value) !== ITEM_NOT_IN_ARRAY) {
                    this.toggledItems.splice(this.toggledItems.indexOf(value), 1);
                    this.toggleSelection(value);
                }
            }
        });
    }

    selectObject(svgElement: SVGElement): void {
        this.saveManager.removeSvgElement(svgElement);
        this.svgHandler.renderer.setAttribute(svgElement, 'isSelected', 'true');
        if (this.selectionCalculator.selectedObjects.indexOf(svgElement) === ITEM_NOT_IN_ARRAY) {
            this.selectionCalculator.selectedObjects.push(svgElement);
            this.createSelectionRectangles(svgElement, true);
        }
        this.selectionCalculator.computeCenters();
        this.saveManager.addSvgElement(svgElement);
    }

    unselectObject(svgElement: SVGElement): void {
        this.saveManager.removeSvgElement(svgElement);
        this.svgHandler.renderer.setAttribute(svgElement, 'isSelected', 'false');
        this.selectionCalculator.selectedObjects.splice(this.selectionCalculator.selectedObjects.indexOf(svgElement), 1);
        if (this.selectionCalculator.selectedObjects.length === 0) {
            this.resetTransformSelectionRectangles();
            this.removeSelectionRectangles();
        } else {
            this.createSelectionRectangles(svgElement, false);
            this.selectionCalculator.computeCenters();
        }
        this.saveManager.addSvgElement(svgElement);
    }

    resetTransformSelectionRectangles(): void {
        this.selectionRectangles.forEach((value) => {
            this.svgHandler.renderer.setAttribute(value, 'transform', 'matrix(1,0,0,1,0,0)');
        });
    }

    unselectAll(svgElement?: SVGElement): void {
        this.svgHandler.elements.forEach((value) => {
            if (value.getAttribute('isSelected') === 'true' && value !== svgElement) {
                this.unselectObject(value);
            }
        });
    }

    selectAll(): void {
        this.svgHandler.elements.forEach((value) => {
            this.selectObject(value);
        });
    }

    createSelectionRectangles(svgElement: SVGElement, addedObject: boolean, shouldRecalculate: boolean = false): void {
        const transformAttribute = this.selectionRectangles.get('boundary') as SVGElement;
        if (transformAttribute.getAttribute('transform') !== 'matrix(1,0,0,1,0,0)') {
            this.resetTransformSelectionRectangles();
            shouldRecalculate = true;
        }
        const rectangleDimensions = this.selectionCalculator.getRectangleDimensions(svgElement, addedObject, shouldRecalculate);
        let rectangleCoordinates: Coordinates = { x: -1, y: -1 };
        this.selectionRectangles.forEach((value, key) => {
            this.svgHandler.renderer.appendChild(this.svgHandler.svgCanvas, value);
            rectangleCoordinates = this.selectionCalculator.calculateRectangleCoordinates(key);
            this.svgHandler.renderer.setAttribute(value, 'x', String(rectangleCoordinates.x));
            this.svgHandler.renderer.setAttribute(value, 'y', String(rectangleCoordinates.y));
            this.svgHandler.renderer.setAttribute(value, 'transform', 'matrix(1,0,0,1,0,0)');
            if (key === 'boundary') {
                this.svgHandler.renderer.setAttribute(value, 'width', String(rectangleDimensions.width));
                this.svgHandler.renderer.setAttribute(value, 'height', String(rectangleDimensions.height));
            }
        });
    }

    private addBoundaryStyles(svgElement: SVGElement): void {
        this.svgHandler.renderer.listen(svgElement, 'mousedown', ($event) => this.clickOnSelectionRectangle($event));
    }

    listenToBoundaryRectangles(): void {
        this.selectionRectangles.forEach((value, key) => {
            this.addBoundaryStyles(value);
            if (key === 'boundary') {
                this.svgHandler.renderer.listen(value, 'mouseup', ($event) => this.mouseOnBoundary());
            }
        });
    }

    private mouseOnBoundary(): void {
        this.clickedOnBoundary = true;
    }

    private clickOnSelectionRectangle(event: MouseEvent): void {
        if (event.button === 0) {
            this.clickedOnElement = true;
        }
    }

    removeSelectionRectangles(): void {
        this.selectionRectangles.forEach((value) => this.svgHandler.renderer.removeChild(this.svgHandler.svgCanvas, value));
    }

    mouseInSelection(coordinates: Coordinates, boundary: SVGElement): boolean {
        return this.selectionCalculator.isObjectInSelection(boundary, coordinates, coordinates);
    }
}
