// Mathematic formula found on Stackoverflow
// ChrisF (2010) Calculate coordinates of a regular polygon's vertices [Online]
// Available : https://stackoverflow.com/questions/3436453/calculate-coordinates-of-a-regular-polygons-vertices

import { Injectable } from '@angular/core';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { AbstractShapeService } from '../abstract-shape-service/abstract-shape.service';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

const LOWER_LIMIT = 3;
const UPPER_LIMIT = 12;

@Injectable({
  providedIn: 'root'
})
export class PolygonHandlerService extends AbstractShapeService {
    private radius: number;
    private polygonCoordinates: Coordinates[];
    private polygonCoordinatesRatio: Coordinates[];
    private theta: number;
    private offset: number;
    private diff: Coordinates;
    private minLengthSelection: number;
    private maxLengthX: number;
    private maxLengthY: number;
    private deltaOrientationX: number;
    private deltaOrientationY: number;

    constructor(protected svgHandler: SvgHandlerService,
                protected colorManager: ColorManagerService,
                protected clickerHandler: ClickerHandlerService,
                protected undoRedoService: UndoRedoService) {
        super(svgHandler, colorManager, clickerHandler, undoRedoService);
    }

    addCoordinatesToPolygon(polygon: SVGElement, coordinates: Coordinates): void {
        const newPoints = polygon.getAttribute('points') + ' ' + String(coordinates.x) + ',' + String(coordinates.y);
        this.renderer.setAttribute(polygon, 'points', newPoints);
        this.svgHandler.drawingOnCanvas = true;
    }

    removeAllCoordinatesFromPolygon(polygon: SVGElement): void {
        this.renderer.setAttribute(polygon, 'points', '');
    }

    getCoordinatesForPolygon(startCoordinates: Coordinates,
                             endCoordinates: Coordinates,
                             numOfSides: number,
                             thickness: number): Coordinates[] {
        this.polygonCoordinates = new Array();
        this.polygonCoordinatesRatio = new Array();
        this.theta = (2 * Math.PI / numOfSides);
        this.offset = thickness / 2;
        this.diff = {
            x: Math.abs(endCoordinates.x - startCoordinates.x),
            y: Math.abs(endCoordinates.y - startCoordinates.y)
        };
        this.minLengthSelection = Math.min(
            Math.abs(this.diff.x / 2 - this.offset),
            Math.abs(this.diff.y / 2 - this.offset));
        if (numOfSides < LOWER_LIMIT || numOfSides > UPPER_LIMIT) {
            return this.polygonCoordinates;
        }
        this.computeRefPolygon(numOfSides);
        this.findFarthestPoints();
        this.calculateRadius(numOfSides, thickness);
        this.computeRealPolygon(numOfSides, startCoordinates);
        this.adjustPolygonPositionAccordingToMouseCoordinates(startCoordinates, endCoordinates, thickness);
        this.rotateAllPoints(this.polygonCoordinates, startCoordinates, this.deltaOrientationX, this.deltaOrientationY);
        // Add first point to close polygon
        this.polygonCoordinates.push(this.polygonCoordinates[0]);
        return this.polygonCoordinates;
    }

    protected setElementAttributesPrevisualization(element: SVGElement | undefined): void {
        this.renderer.setAttribute(element, 'points', '');
    }

    protected setElementAttributesCreation(element: SVGElement | undefined): void {
        this.renderer.setAttribute(element, 'points', '');
    }

    private computeRefPolygon(numOfSides: number): void {
        for (let i = 0; i < numOfSides; i++) {
            this.polygonCoordinatesRatio.push(
                {
                    x: Math.sin(i * this.theta),
                    y: Math.cos(i * this.theta)
                }
            );
        }
    }

    private computeRealPolygon(numOfSides: number, startCoordinates: Coordinates): void {
        for (let i = 0; i < numOfSides; i++) {
            this.polygonCoordinates.push(
                {
                    x: startCoordinates.x - this.offset + this.radius * Math.sin(i * this.theta),
                    y: startCoordinates.y - this.offset + this.radius * Math.cos(i * this.theta)
                }
            );
        }
    }

    private calculateRadius(numOfSides: number, thickness: number): void {
        if (this.diff.x > this.diff.y) {
            this.radius = (2 * 2 * this.minLengthSelection / this.maxLengthX + thickness > this.diff.x && numOfSides % 2 === 1) ?
                2 * Math.abs(this.diff.x / 2 - this.offset) / this.maxLengthX : 2 * this.minLengthSelection / this.maxLengthY;
        } else {
            this.radius = (2 * 2 * this.minLengthSelection / this.maxLengthX + thickness > this.diff.y && numOfSides % 2 === 0) ?
                2 * Math.abs(this.diff.y / 2 - this.offset) / this.maxLengthY : 2 * this.minLengthSelection / this.maxLengthX;
        }
    }

    private findFarthestPoints(): void {
        this.polygonCoordinatesRatio.sort((a, b) => a.x - b.x);
        this.maxLengthX = this.polygonCoordinatesRatio[this.polygonCoordinatesRatio.length - 1].x - this.polygonCoordinatesRatio[0].x;
        this.polygonCoordinatesRatio.sort((a, b) => a.y - b.y);
        this.maxLengthY = this.polygonCoordinatesRatio[this.polygonCoordinatesRatio.length - 1].y - this.polygonCoordinatesRatio[0].y;
    }

    private rotateAllPoints(polygonCoordinates: Coordinates[],
                            startCoordinates: Coordinates,
                            deltaOrientationX: number,
                            deltaOrientationY: number): void {
        polygonCoordinates.forEach((coords) => {
            const mathCosX = (coords.x - startCoordinates.x) * Math.cos(Math.PI);
            const mathSinY = (coords.y - startCoordinates.y) * Math.sin(Math.PI);
            const mathSinX = (coords.x - startCoordinates.x) * Math.sin(Math.PI);
            const mathCosY = (coords.y - startCoordinates.y) * Math.cos(Math.PI);
            coords.x = (mathCosX - mathSinY) + startCoordinates.x + deltaOrientationX;
            coords.y = (mathSinX + mathCosY) + startCoordinates.y + deltaOrientationY;
        });
    }

    private adjustPolygonPositionAccordingToMouseCoordinates(
            startCoordinates: Coordinates,
            endCoordinates: Coordinates,
            thickness: number): void {
        this.deltaOrientationX = (this.maxLengthX * this.radius) / 2;
        this.deltaOrientationY = this.radius;
        if (startCoordinates.x > endCoordinates.x) {
            this.deltaOrientationX = - (this.maxLengthX * this.radius) / 2 - thickness;
        }
        if (startCoordinates.y > endCoordinates.y) {
            this.deltaOrientationY = this.polygonCoordinatesRatio[0].y * this.radius - thickness;
        }
    }
}
