import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Renderer2 } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { SvgHandlerService } from '../../svg-handler/svg-handler.service';
import { SelectionTransformService } from './selection-transform.service';

const DEFAULT_MOVE = 3;

describe('SelectionTransformService', () => {
    let service: SelectionTransformService;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let rendererSpy: jasmine.SpyObj<Renderer2>;
    let fakeRegArray: RegExpExecArray;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');

    beforeEach(() => {
        fakeSvgElement.setAttribute('size', '10');
        fakeSvgElement.setAttribute('transform', 'matrix(1,0,0,1,0,0)');
        fakeRegArray = /matrix\(\s*([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)[ ,]([^\s,)]+)/
            .exec('matrix(1,0,0,1,0,0)') as RegExpExecArray;
    });

    beforeEach(() => {
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners',
                'renderer',
                'setAttribute'
            ]
        );
        rendererSpy = jasmine.createSpyObj(
            'Renderer2',
            [
                'createElement',
                'setStyle',
                'appendChild',
                'removeChild',
                'setAttribute',
                'listen',
                'getAttribute'
            ]
        );
        svgHandlerSpy.renderer = rendererSpy;
  });

    beforeEach(() => TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule,
        ],
        providers: [
          { provide: SvgHandlerService, useValue: svgHandlerSpy },
          { provide: Renderer2, useValue: rendererSpy }
        ],
    }));

    beforeEach(() => {
        service = TestBed.get(SelectionTransformService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should append transformation to element', () => {
        service.ngOnInit();
        service.appendTransformationToElements(fakeSvgElement, [1, 0, 0, 1, 0, DEFAULT_MOVE]);
        expect(rendererSpy.setAttribute).toHaveBeenCalledWith(fakeSvgElement, 'transform', 'matrix(1,0,0,1,0,3)');
    });

    it('should get old matrix', () => {
      const result = service.getOldMatrix(fakeSvgElement);
      expect(result).toEqual(fakeRegArray);
    });
});
