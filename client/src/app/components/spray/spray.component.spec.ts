import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SprayHandlerService } from 'src/app/services/spray-handler/spray-handler.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { ColorComponent } from '../color-components/color/color.component';
import { SprayComponent } from './spray.component';

// tslint:disable:no-magic-numbers no-string-literal

describe('SprayComponent', () => {
    let component: SprayComponent;
    let fixture: ComponentFixture<SprayComponent>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let mouseHandlerSpy: jasmine.SpyObj<MouseHandlerService>;
    let sprayHandlerSpy: jasmine.SpyObj<SprayHandlerService>;
    const fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:path');

    beforeEach(() => {
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas',
                'addListeners',
                'deleteListeners'
            ]
        );
        mouseHandlerSpy = jasmine.createSpyObj(
            'MouseHandlerService',
            [
                'onMouseDown',
                'onMouseMovement',
                'onMouseUp',
                'onMouseLeave',
                'isMouseDown'
            ]
        );
        sprayHandlerSpy = jasmine.createSpyObj(
            'SprayHandlerService',
            [
                'createSprayObject',
                'addRandomCircle'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                    FormsModule, MatIconModule, MatDialogModule, HttpClientModule
                ],
        declarations: [ SprayComponent, ColorComponent ],
        providers: [
                {provide: SvgHandlerService, useValue: svgHandlerSpy},
                {provide: MouseHandlerService, useValue: mouseHandlerSpy},
                {provide: SprayHandlerService, useValue: sprayHandlerSpy}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SprayComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create listeners when opened and set default sizes', () => {
        component.ngOnInit();
        expect(svgHandlerSpy.addListeners).toHaveBeenCalled();
        expect(component.size).toBe(50);
        expect(component.emissions).toBe(100);
    });

    it('should check emission too big', () => {
        component.emissions = 50000;
        component.changeEmissionSize();
        expect(component.size).toBe(50);
        expect(component.emissions).toBe(200);
    });

    it('should check size too small', () => {
        component.size = -1000;
        component.changeSize();
        expect(component.size).toBe(20);
        expect(component.emissions).toBe(100);
    });

    it('should check size float', () => {
        component.size = 50.8;
        component.changeSize();
        expect(component.size).toBe(51);
        expect(component.emissions).toBe(100);
    });

    it('should not stop interval if interval does not exist', () => {
        component['mouseLeft']();
        expect(mouseHandlerSpy.onMouseLeave).toHaveBeenCalledTimes(1);
        expect(component['isDrawing']).toBeFalsy();
    });

    it('should call eventListeners.mouseMove when mouse moves', () => {
        const mockMouse = new MouseEvent('mousemove');
        mouseHandlerSpy.isMouseDown = false;
        component['eventListeners'].mouseMove(mockMouse);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalled();
        expect(mouseHandlerSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseDown when mouse goes down', () => {
        const mockMouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mockMouseDown);
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalled();
        expect(mouseHandlerSpy.onMouseLeave).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseUp when mouse goes up', () => {
        const mouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mouseDown);
        component['svgElement'] = fakeSvgElement;
        const mockMouseUp = new MouseEvent('mouseup');
        component['eventListeners'].mouseUp(mockMouseUp);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalled();
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseLeave).toHaveBeenCalledTimes(0);
    });

    it('should call eventListeners.mouseLeave when mouse leaves', () => {
        const mouseDown = new MouseEvent('mousedown');
        component['eventListeners'].mouseDown(mouseDown);
        const mockMouseLeave = new MouseEvent('mouseleave');
        component['eventListeners'].mouseLeave(mockMouseLeave);
        expect(mouseHandlerSpy.onMouseLeave).toHaveBeenCalled();
        expect(mouseHandlerSpy.onMouseDown).toHaveBeenCalledTimes(1);
        expect(mouseHandlerSpy.onMouseMovement).toHaveBeenCalledTimes(0);
        expect(mouseHandlerSpy.onMouseUp).toHaveBeenCalledTimes(0);
    });

    it('should increase emissions', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeEmissionSize').and.callThrough();
        component.increaseEmission();
        expect(component.changeEmissionSize).toHaveBeenCalledTimes(1);
    });

    it('should decrease emissions', () => {
        // tslint:disable-next-line: no-any
        spyOn<any>(component, 'changeEmissionSize').and.callThrough();
        component.decreaseEmission();
        expect(component.changeEmissionSize).toHaveBeenCalledTimes(1);
    });
});
