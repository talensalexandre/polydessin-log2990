import { TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { SendByEmailManagerService } from './send-by-email-manager.service';

const SUCCESS = 201;
const FAILED = 400;

describe('SendByEmailManagerService', () => {
    let service: SendByEmailManagerService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, HttpClientTestingModule],
        providers: [SendByEmailManagerService],
    }));

    beforeEach(() => {
        service = TestBed.get(SendByEmailManagerService);
        httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should send email successfully', () => {
        const to = 'test@polydessin.ca';
        const payload = 'data:image/jpeg;base64,XXX';
        const name = 'Drawing name';
        const type = 'jpeg';
        const mockResponsePost = SUCCESS;
        service.sendDrawingbyEmail(to, payload, name, type).subscribe((response) => {
            expect(response).toBe(SUCCESS);
        });
        const request = httpTestingController.expectOne(environment.SEND_EMAIL_URL);
        expect(request.request.method).toEqual('POST');
        request.flush(mockResponsePost);
        httpTestingController.verify();
    });

    it('should handle errors', () => {
        service.sendDrawingbyEmail('', '', '', '').subscribe((response) => {
            expect(response).toBe(FAILED);
        });
        httpTestingController.expectOne(environment.SEND_EMAIL_URL)
            .error(new ErrorEvent('error'), { status: 400, statusText: 'Bad Request' });
        httpTestingController.verify();
    });

});
