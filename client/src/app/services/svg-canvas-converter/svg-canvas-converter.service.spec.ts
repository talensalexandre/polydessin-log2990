import { ElementRef } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { SvgCanvasConverterService } from './svg-canvas-converter.service';

describe('SvgCanvasConverterService', () => {
    let service: SvgCanvasConverterService;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;
    let htmlCanvas: ElementRef;
    let contextSpy: jasmine.SpyObj<CanvasRenderingContext2D>;
    const fakeSvgCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    let fakeCanvas: jasmine.SpyObj<HTMLCanvasElement>;

    beforeEach(() => TestBed.configureTestingModule({}));

    beforeEach(() => {
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'createCanvas'
            ]
        );
        fakeCanvas = jasmine.createSpyObj(
            'HTMLCanvasElement',
            [
                'setAttribute',
                'getContext'
            ]
        );
        contextSpy = jasmine.createSpyObj(
            'CanvasRenderingContext2D',
            [
                'drawImage'
            ]
        );
        fakeCanvas.getContext.and.returnValue(contextSpy);
        svgHandlerSpy.svgCanvas = fakeSvgCanvas;
        htmlCanvas = new ElementRef(fakeCanvas);
        htmlCanvas.nativeElement = fakeCanvas;
        service = new SvgCanvasConverterService(svgHandlerSpy);
    });

    it('should be created', () => {
        service.convertSvgToCanvas(htmlCanvas);
        expect(service).toBeTruthy();
    });
});
