import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AllColorGradientComponent } from '../all-color-gradient/all-color-gradient.component';
import { AlphaColorGradientComponent } from '../alpha-color-gradient/alpha-color-gradient.component';
import { ColorPopupComponent } from '../color-popup/color-popup.component';
import { MainColorGradientComponent } from '../main-color-gradient/main-color-gradient.component';
import { ColorComponent } from './color.component';

describe('ColorComponent', () => {
    let component: ColorComponent;
    let fixture: ComponentFixture<ColorComponent>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;
    let svgHandlerSpy: jasmine.SpyObj<SvgHandlerService>;

    const mainDefaultColor: RGBA = {
        Dec: {
            Red: 255,
            Green: 255,
            Blue: 255,
            Alpha: 1
        },
        Hex: {
            Red: 'ff',
            Green: 'ff',
            Blue: 'ff'
        },
        inString: 'rgba(255, 255, 255, 1)'
    };
    const secondaryDefaultColor: RGBA = {
        Dec: {
            Red: 255,
            Green: 255,
            Blue: 255,
            Alpha: 1
        },
        Hex: {
            Red: 'ff',
            Green: 'ff',
            Blue: 'ff'
        },
        inString: 'rgba(255, 255, 255, 1)'
    };

    const dialogRefSpy = {
        open: jasmine.createSpy('open')
    };

    beforeEach(() => {
        svgHandlerSpy = jasmine.createSpyObj(
            'SvgHandlerService',
            [
                'updateBackgroundColor'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'autosaveBackground'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
                [
                    MatIconModule, MatDialogModule, BrowserAnimationsModule,
                    MatInputModule, MatFormFieldModule, FormsModule
                ],
            declarations: [ColorComponent, ColorPopupComponent,
                MainColorGradientComponent, AlphaColorGradientComponent,
                AllColorGradientComponent],
            providers: [
                { provide: MatDialogRef, useValue: dialogRefSpy },
                { provide: SvgHandlerService, useValue: svgHandlerSpy },
                { provide: SaveManagerService, useValue: saveManagerSpy }
            ]
        })
            .overrideModule(
                BrowserDynamicTestingModule,
                {
                    set: { entryComponents: [ColorPopupComponent] }
                });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should open color picker', () => {
        component.openColorPicker(ColorChoice.secondaryColor);
        expect(ColorPopupComponent).toBeTruthy();
    });

    it('should update background color', () => {
        component.subscribeFunction();
        expect(svgHandlerSpy.updateBackgroundColor).toHaveBeenCalledTimes(1);
    });

    it('should switch color', () => {
        const colorManagerSpy = jasmine.createSpyObj('ColorManagerService', ['colorSelected']);
        colorManagerSpy.colorSelected[ColorChoice.mainColor] = mainDefaultColor;
        colorManagerSpy.colorSelected[ColorChoice.secondaryColor] = secondaryDefaultColor;
        component.switchColor();
        expect(colorManagerSpy.colorSelected[ColorChoice.mainColor]).toEqual(secondaryDefaultColor);
        expect(colorManagerSpy.colorSelected[ColorChoice.secondaryColor]).toEqual(mainDefaultColor);
    });
});
