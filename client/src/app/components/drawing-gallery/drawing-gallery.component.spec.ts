import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import {
    MatCardModule, MatChipInputEvent, MatChipsModule, MatDialog, MatDialogModule,
    MatDialogRef, MatFormFieldModule, MatIconModule, MatProgressSpinnerModule, MatSnackBarModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Observable, of } from 'rxjs';
import { DrawingStored, MetaDrawing } from 'src/app/interfaces-enums/drawing';
import { DrawingGalleryService } from 'src/app/services/drawing-gallery/drawing-gallery.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { DrawingGalleryComponent } from './drawing-gallery.component';

// tslint:disable: no-string-literal no-any

export class MatDialogMock {
    static confirm: boolean;
    // tslint:disable-next-line: typedef
    open() {
        return {
            afterClosed: () => of(MatDialogMock.confirm)
        };
    }
}

describe('DrawingGalleryComponent', () => {
    let component: DrawingGalleryComponent;
    let fixture: ComponentFixture<DrawingGalleryComponent>;
    let drawingGalleryServiceSpy: jasmine.SpyObj<DrawingGalleryService>;
    let saveDeleteSpy: jasmine.Spy<(timestamp: string) => Observable<number | DrawingStored>>;
    let debugElement: DebugElement;
    let saveManagerService: SaveManagerService;
    let svgHandlerService: SvgHandlerService;
    let drawingStore: DrawingStored;
    let fakeCanvas: SVGElement;
    let fakeSvgElement: SVGElement;

    const mockDialogRef = {
        close: jasmine.createSpy('close'),
        open: jasmine.createSpy('open')
    };

    beforeEach((() => {
        drawingGalleryServiceSpy = jasmine.createSpyObj('DrawingGalleryService',
            [
                'createElementFromString',
                'createDrawing',
                'displayElement'
            ]);
        TestBed.configureTestingModule({
            imports:
                [
                    MatChipsModule, MatFormFieldModule, FormsModule, MatDialogModule,
                    BrowserAnimationsModule, MatDialogModule, MatIconModule,
                    MatCardModule, MatProgressSpinnerModule, MatChipsModule, MatSnackBarModule,
                    HttpClientModule
                ],
            declarations: [DrawingGalleryComponent],
            providers:
                [
                    { provide: MatDialogRef, useValue: mockDialogRef },
                    { provide: MatDialog, useClass: MatDialogMock },
                    { provide: DrawingGalleryService, useValue: drawingGalleryServiceSpy },
                    SaveManagerService, SvgHandlerService]
        })
            .compileComponents();
        fixture = TestBed.createComponent(DrawingGalleryComponent);
        component = fixture.componentInstance;
        debugElement = fixture.debugElement;
        saveManagerService = debugElement.injector.get(SaveManagerService);
        svgHandlerService = debugElement.injector.get(SvgHandlerService);
        saveDeleteSpy = spyOn(saveManagerService, 'deleteDrawing').and.callThrough();
        const labels = ['label', 'oui', 'logiciel'];
        const metaDrawing: MetaDrawing = {
            timestamp: 1, name: 'testDessin', labels, width: 1292, height: 923,
            backgroundColor: 'rgb(255, 0, 255)'
        };
        const fakeElementString = ['<svg:polyline xmlns:svg="http://www.w3.org/2000/svg" _ngcontent-hnw-c0="" stroke-width="12"' +
            ' points="193,246 193,246" stroke="rgba(255,0,0,1)" typeElement="strokeElement" size="12" isSelected="false"' +
            ' transform="translate(0,0)" style="stroke-linecap: round; stroke-linejoin: round; fill: none;"></svg:polyline>'];
        drawingStore = { metaDrawing, svgContainer: fakeElementString };
        fakeCanvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        fakeSvgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg:rect');
        fakeCanvas.appendChild(fakeSvgElement);
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should close dialog', () => {
        component.closeDialog();
        expect(mockDialogRef.close).toHaveBeenCalled();
    });

    it('should add label and display drawingStore', () => {
        component.labels = [];
        const htmlInput: HTMLInputElement = document.createElement('input');
        const mockEvent: MatChipInputEvent = ({ input: htmlInput, value: 'test' });
        drawingStore.metaDrawing.labels.push('test');
        const drawingStoredArray: DrawingStored[] = [drawingStore];
        component.drawings = drawingStoredArray;
        component.addLabel(mockEvent);
        expect(component.drawingsToDisplay).toContain(drawingStore);
        expect(component.labels).toContain(mockEvent.value);
    });

    it('should add label and not display drawingStore', () => {
        component.labels = [];
        const htmlInput: HTMLInputElement = document.createElement('input');
        const mockEvent: MatChipInputEvent = ({ input: htmlInput, value: 'test' });
        drawingStore.metaDrawing.labels.push('test2');
        const drawingStoredArray: DrawingStored[] = [drawingStore];
        component.drawings = drawingStoredArray;
        component.addLabel(mockEvent);
        expect(component.drawingsToDisplay).not.toContain(drawingStore);
        expect(component.labels).toContain(mockEvent.value);
    });

    it('should add label and only display drawingStore once', () => {
        component.labels = [];
        const htmlInput: HTMLInputElement = document.createElement('input');
        const mockEvent: MatChipInputEvent = ({ input: htmlInput, value: 'test' });
        const secondMockEvent: MatChipInputEvent = ({ input: htmlInput, value: 'test2' });
        drawingStore.metaDrawing.labels.push('test');
        drawingStore.metaDrawing.labels.push('test2');
        const drawingStoredArray: DrawingStored[] = [drawingStore];
        component.drawings = drawingStoredArray;
        component.addLabel(mockEvent);
        component.addLabel(secondMockEvent);
        expect(component.labels).toContain(mockEvent.value);
    });

    it('should remove label', () => {
        component.labels = ['test'];
        component.removeLabel(component.labels[0]);
        expect(component.labels).not.toContain(component.labels[0]);
    });

    it('should only remove one label', () => {
        component.labels = ['test', 'test2'];
        component.removeLabel(component.labels[0]);
        expect(component.labels).toContain(component.labels[0]);
    });

    it('should create drawing', () => {
        spyOn<any>(component, 'openSnackBar');
        component['createDrawing'](drawingStore);
        expect(drawingGalleryServiceSpy.createDrawing).toHaveBeenCalled();
        expect(component['openSnackBar']).toHaveBeenCalled();
    });

    it('should not removeDrawing', () => {
        spyOn<any>(component, 'openSnackBar');
        MatDialogMock.confirm = false;
        component.removeDrawing(drawingStore);
        expect(saveDeleteSpy).not.toHaveBeenCalled();
        expect(component['openSnackBar']).not.toHaveBeenCalled();
    });

    it('should removeDrawing', () => {
        spyOn<any>(component, 'createDrawing');
        MatDialogMock.confirm = true;
        saveDeleteSpy.and.returnValue(of(drawingStore));
        component.removeDrawing(drawingStore);
        fixture.detectChanges();
        expect(saveDeleteSpy).toHaveBeenCalled();
    });

    it('should not removeDrawing either', () => {
        spyOn<any>(component, 'openSnackBar');
        MatDialogMock.confirm = true;
        saveDeleteSpy.and.returnValue(of(0));
        component.removeDrawing(drawingStore);
        fixture.detectChanges();
        expect(saveDeleteSpy).toHaveBeenCalled();
        expect(component['openSnackBar']).toHaveBeenCalled();
    });

    it('should openDrawing', () => {
        svgHandlerService.svgCanvas = fakeCanvas;
        spyOn<any>(component, 'createDrawing');
        MatDialogMock.confirm = true;
        component.openDrawing(drawingStore);
        expect(component['createDrawing']).toHaveBeenCalled();
    });

    it('should not openDrawing', () => {
        spyOn<any>(component, 'createDrawing');
        component.openDrawing(drawingStore);
        expect(component['createDrawing']).toHaveBeenCalled();
    });

    it('should not openDrawing either', () => {
        svgHandlerService.svgCanvas = fakeCanvas;
        MatDialogMock.confirm = false;
        spyOn<any>(component, 'createDrawing');
        component.openDrawing(drawingStore);
        expect(component['createDrawing']).not.toHaveBeenCalled();
    });

    it('should display all drawings', () => {
        component.drawingsToDisplay.push(drawingStore);
        component['displayDrawings'](component.drawingsToDisplay);
        expect(drawingGalleryServiceSpy.createElementFromString).toHaveBeenCalled();
    });

    it('should get all drawings', () => {
        spyOn<any>(component, 'displayDrawings');
        const drawingStoredArray: DrawingStored[] = [drawingStore];
        const getAllSpy = spyOn(saveManagerService, 'getDrawings').and.callThrough().and.returnValue(of(drawingStoredArray));
        component['getAllDrawings']();
        fixture.detectChanges();
        expect(getAllSpy).toHaveBeenCalled();
        expect(component['displayDrawings']).toHaveBeenCalled();
    });

    it('should not get all drawings', () => {
        spyOn<any>(component, 'displayDrawings');
        const getAllSpy = spyOn(saveManagerService, 'getDrawings').and.callThrough().and.returnValue(of(0));
        component['getAllDrawings']();
        fixture.detectChanges();
        expect(getAllSpy).toHaveBeenCalled();
        expect(component['displayDrawings']).not.toHaveBeenCalled();
    });

    it('should not add label', () => {
        component.labels = [];
        const htmlInput: HTMLInputElement = document.createElement('input');
        const mockEvent: MatChipInputEvent = ({ input: htmlInput, value: '' });
        component.addLabel(mockEvent);
        expect(component.labels).not.toContain(mockEvent.value);
    });

    it('should have no label remove label', () => {
        component.labels = [];
        component.removeLabel('test');
        expect(component.labels.length).toEqual(0);
    });

    it('should not detect changes if view not active', () => {
        const spy = spyOn<any>(component['cdRef'], 'detectChanges');
        component.drawingsToDisplay.push(drawingStore);
        component.viewActive = false;
        component['displayDrawings'](component.drawingsToDisplay);
        expect(spy).not.toHaveBeenCalled();
    });
});
