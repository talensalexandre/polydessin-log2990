import { Component } from '@angular/core';
import { ElementType } from 'src/app/interfaces-enums/element-type';
import { DrawingService } from 'src/app/services/drawing/drawing.service';
import { EllipseHandlerService } from 'src/app/services/ellipse-handler/ellipse-handler.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { StateSelectorService } from '../../services/state-selector/state-selector.service';
import { AbstractShapeComponent } from '../abstract-tool/abstract-shape.component';

@Component({
    selector: 'app-ellipse',
    templateUrl: './ellipse.component.html',
    styleUrls: ['./ellipse.component.scss'],
})
export class EllipseComponent extends AbstractShapeComponent {

    constructor(
        protected mouseHandler: MouseHandlerService,
        protected svgHandler: SvgHandlerService,
        public stateSelector: StateSelectorService,
        private ellipseHandler: EllipseHandlerService,
        protected drawingService: DrawingService,
        protected saveManager: SaveManagerService
    ) {
        super(mouseHandler, svgHandler, stateSelector, drawingService, saveManager);
    }

    protected handleKeyDownExtend(): void {
        this.ellipseHandler.calculateCircle(this.size);
        this.previsualisationElement = this.ellipseHandler.elementPrevisualization(
            Boolean(this.fill),
            Boolean(this.stroke),
            this.size,
            ElementType.ellipse,
            this.previsualisationElement
        );
    }

    protected handleKeyUpExtend(): void {
        this.ellipseHandler.calculateEllipse(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.previsualisationElement = this.ellipseHandler.elementPrevisualization(
            Boolean(this.fill),
            Boolean(this.stroke),
            this.size,
            ElementType.ellipse,
            this.previsualisationElement
        );
    }

    protected destroyExtend(): void {
        this.drawingService.removeElement(this.previsualisationElement);
    }

    protected mouseMouvementCalculateShiftShape(): void {
        this.ellipseHandler.calculateCircle(this.size);
        this.isSelectionTooSmall = this.ellipseHandler.isShapeTooBigComparedToSize(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
    }

    protected mouseMouvementCalculateNormalShape(): void {
        this.ellipseHandler.calculateEllipse(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
        this.isSelectionTooSmall = this.ellipseHandler.isShapeTooBigComparedToSize(
            this.mouseHandler.startCoordinates,
            this.mouseHandler.actualCoordinates,
            this.size
        );
    }

    protected mouseMouvementPrevisualization(): void {
        this.previsualisationElement = this.ellipseHandler.elementPrevisualization(
            this.fill,
            this.stroke,
            this.size,
            ElementType.ellipse,
            this.previsualisationElement
        );
    }

    protected mouseUpCreateShape(): void {
        this.svgElement = this.ellipseHandler.createElement(
            this.fill,
            this.stroke,
            this.size,
            ElementType.ellipse
        );
    }

    protected mouseLeftExtend(): void {
        this.ellipseHandler.deletePrevisualization();
    }

    protected mouseUpExtendExtra(): void {
        this.ellipseHandler.deletePrevisualization();
    }
}
