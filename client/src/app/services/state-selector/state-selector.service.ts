import { Injectable } from '@angular/core';
import { States } from '../../interfaces-enums/states';

@Injectable({
  providedIn: 'root'
})
export class StateSelectorService {
    stateSelected: States;
    isModifyingInput: boolean;

    constructor() {
        this.stateSelected = States.defaultState;
    }

    changeState(stateWanted: States, sidenavOpen: boolean): boolean {
        if (sidenavOpen && this.stateSelected === stateWanted) {
            sidenavOpen = false;
        } else if (!sidenavOpen) {
            sidenavOpen = true;
        }
        this.stateSelected = stateWanted;
        const shouldCloseSidenav =
            stateWanted === States.documentation || stateWanted === States.save ||
            stateWanted === States.newDrawing || stateWanted === States.selection ||
            stateWanted === States.drawingExport || stateWanted === States.drawingGallery ||
            stateWanted === States.colorPicker;
        if (shouldCloseSidenav) {
            sidenavOpen = false;
        }
        return sidenavOpen;
    }
}
