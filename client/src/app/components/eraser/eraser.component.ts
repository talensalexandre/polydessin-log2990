import { Component } from '@angular/core';
import { Coordinates } from 'src/app/interfaces-enums/coordinates';
import { EraserService } from 'src/app/services/eraser/eraser.service';
import { MouseHandlerService } from 'src/app/services/mouse-handler/mouse-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { StateSelectorService } from 'src/app/services/state-selector/state-selector.service';
import { SvgHandlerService } from 'src/app/services/svg-handler/svg-handler.service';
import { AbstractToolComponent } from '../abstract-tool/abstract-tool.component';

const DEFAULT_SIZE = 3;
const MAXIMUM_SIZE = 40;

@Component({
    selector: 'app-eraser',
    templateUrl: './eraser.component.html',
    styleUrls: ['./eraser.component.scss']
})
export class EraserComponent extends AbstractToolComponent {
    constructor(
        protected svgHandler: SvgHandlerService,
        protected mouseHandler: MouseHandlerService,
        public stateSelector: StateSelectorService,
        private eraserService: EraserService,
        protected saveManager: SaveManagerService
    ) {
        super(svgHandler, mouseHandler, stateSelector, saveManager);
    }

    protected setDefaultSize(): void {
        this.size = DEFAULT_SIZE;
    }

    protected destroyExtend(): void {
        this.eraserService.clearHighlights();
        this.mouseLeft();
    }

    changeSize(): void {
        this.size = (this.size < DEFAULT_SIZE) ? DEFAULT_SIZE : this.size;
        this.size = (this.size > MAXIMUM_SIZE) ? MAXIMUM_SIZE : this.size;
        this.stateSelector.isModifyingInput = false;
    }

    protected updateMouseDown(event: MouseEvent): void {
        const eraserCoordinates: Coordinates = { x: event.clientX, y: event.clientY };
        this.mouseHandler.onMouseDown(eraserCoordinates);
        this.isDrawing = true;
        this.buttonUsed = event.button;
        this.eraserService.getSelectedElements(this.size, eraserCoordinates, this.mouseHandler.isMouseDown);
        this.mouseDownExtend();
    }

    protected mouseUpExtend(): void {
        this.eraserService.addToUndoRedo();
    }

    protected updateMousePosition(event: MouseEvent): void {
        const coordinates: Coordinates = { x: event.offsetX, y: event.offsetY };
        const eraserCoordinates: Coordinates = { x: event.clientX, y: event.clientY };
        this.eraserService.moveEraser(coordinates, this.size);
        this.eraserService.getSelectedElements(this.size, eraserCoordinates, this.mouseHandler.isMouseDown);
        this.mouseMouvementExtend();
    }

    protected mouseLeft(): void {
        this.isDrawing = false;
        this.eraserService.clearHighlights();
        this.eraserService.deleteEraser();
    }

    protected mouseMouvementExtend(): void { return; }
    protected mouseDownExtend(): void { return; }
}
