import { Injectable } from '@angular/core';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { AbstractShapeService } from '../abstract-shape-service/abstract-shape.service';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';
@Injectable({
    providedIn: 'root'
})
export class RectangleHandlerService extends AbstractShapeService {
    constructor(
        protected svgHandler: SvgHandlerService,
        protected colorManager: ColorManagerService,
        protected clickerHandler: ClickerHandlerService,
        protected undoRedoService: UndoRedoService
    ) {
        super(svgHandler, colorManager, clickerHandler, undoRedoService);
    }

    calculateRectangle(startCoordinates: Coordinates,
                       endCoordinates: Coordinates,
                       size: number): void {
        this.dimensions.coordinates.x = Math.min(startCoordinates.x + size / 2, endCoordinates.x + size / 2);
        this.dimensions.coordinates.y = Math.min(startCoordinates.y + size / 2, endCoordinates.y + size / 2);
        this.dimensions.width = Math.abs(endCoordinates.x - startCoordinates.x - size / 2);
        this.dimensions.height = Math.abs(endCoordinates.y - startCoordinates.y - size / 2);
        if (startCoordinates.x > endCoordinates.x) {
            this.dimensions.coordinates.x = startCoordinates.x - this.dimensions.width - size / 2;
        }
        if (startCoordinates.y > endCoordinates.y) {
            this.dimensions.coordinates.y = startCoordinates.y - this.dimensions.height - size / 2;
        }
    }

    calculateSquare(startCoordinates: Coordinates,
                    endCoordinates: Coordinates,
                    size: number): void {
        this.dimensions.coordinates.x = Math.min(startCoordinates.x + size / 2, endCoordinates.x + size / 2);
        this.dimensions.coordinates.y = Math.min(startCoordinates.y + size / 2, endCoordinates.y + size / 2);
        this.dimensions.width = Math.abs(endCoordinates.x - startCoordinates.x - size / 2);
        this.dimensions.height = Math.abs(endCoordinates.y - startCoordinates.y - size / 2);
        this.dimensions.width = Math.min(this.dimensions.width, this.dimensions.height);
        this.dimensions.height = this.dimensions.width;
        if (!(startCoordinates.x <= endCoordinates.x)) {
            this.dimensions.coordinates.x = startCoordinates.x - this.dimensions.width - size / 2;
        }
        if (!(startCoordinates.y <= endCoordinates.y)) {
            this.dimensions.coordinates.y = startCoordinates.y - this.dimensions.width - size / 2;
        }
    }

    protected setElementAttributesPrevisualization(element: SVGElement | undefined): void {
        this.renderer.setAttribute(element, 'x', String(this.dimensions.coordinates.x));
        this.renderer.setAttribute( element, 'y', String(this.dimensions.coordinates.y));
        this.renderer.setAttribute(element, 'width', String(this.dimensions.width));
        this.renderer.setAttribute(element, 'height', String(this.dimensions.height));
    }

    protected setElementAttributesCreation(element: SVGElement | undefined): void {
        this.setElementAttributesPrevisualization(element);
    }
}
