import { expect } from 'chai';
import * as supertest from 'supertest';
import { Message } from '../../../common/communication/message';
import { Stubbed, testingContainer } from '../../test/test-utils';
import { Application } from '../app';
import { DatabaseService } from '../services/database.service';
import Types from '../types';

// tslint:disable:no-any
const HTTP_STATUS_OK = 200;
const HTTP_STATUS_CREATED = 201;
const HTTP_NOT_FOUND = 404;
const HTTP_BAD_REQUEST = 400;

describe('DatabaseController', () => {
    const baseMessage = { title: 'Home page', body: 'If this message appears, your request is invalid' } as Message;
    let databaseService: Stubbed<DatabaseService>;
    let app: Express.Application;

    beforeEach(async () => {
        const [container, sandbox] = await testingContainer();
        container.rebind(Types.DatabaseService).toConstantValue({
            homePage: sandbox.stub().resolves(baseMessage),
            getAllDrawings: sandbox.stub(),
            getDrawingWithTimestamp: sandbox.stub(),
            addDrawing: sandbox.stub(),
            deleteDrawing: sandbox.stub(),
            modifyDrawing: sandbox.stub()
        });
        databaseService = container.get(Types.DatabaseService);
        app = container.get<Application>(Types.Application).app;
    });

    it('should return message from database service on valid get request to root', async () => {
        return supertest(app)
            .get('/log2990/projet2/drawing')
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(baseMessage);
            });
    });

    it('should return message from database service on valid get request to all drawings', async () => {
        const aboutMessage = { title: 'all drawings' };
        databaseService.getAllDrawings.resolves(aboutMessage);
        return supertest(app)
            .get('/log2990/projet2/drawing/drawing/drawings')
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(aboutMessage);
            });
    });

    it('should catch error when rejected for all drawings', async () => {
        databaseService.getAllDrawings.rejects(new Error('error'));
        return supertest(app)
            .get('/log2990/projet2/drawing/drawing/drawings')
            .expect(HTTP_NOT_FOUND);
    });

    it('should return message from database service on valid post request to add one drawing', async () => {
        const aboutMessage = {};
        databaseService.addDrawing.resolves(aboutMessage);
        return supertest(app)
            .post('/log2990/projet2/drawing/drawings')
            .expect(HTTP_STATUS_CREATED)
            .then((response: any) => {
                expect(response.body).to.deep.equal(aboutMessage);
            });
    });

    it('should catch error when rejected when creating drawing', async () => {
        databaseService.addDrawing.rejects(new Error('error'));
        return supertest(app)
            .post('/log2990/projet2/drawing/drawings')
            .expect(HTTP_BAD_REQUEST);
    });

    it('should return message from database service on valid get request to one drawing', async () => {
        const aboutMessage = {};
        databaseService.getDrawingWithTimestamp.resolves(aboutMessage);
        return supertest(app)
            .get('/log2990/projet2/drawing/drawing/12')
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(aboutMessage);
            });
    });

    it('should catch error when rejected for one specific drawing', async () => {
        databaseService.getDrawingWithTimestamp.rejects(new Error('error'));
        return supertest(app)
            .get('/log2990/projet2/drawing/drawing/5465454345')
            .expect(HTTP_NOT_FOUND);
    });

    it('should return message from database service on valid delete request to delete one drawing', async () => {
        const aboutMessage = {};
        databaseService.deleteDrawing.resolves(aboutMessage);
        return supertest(app)
            .delete('/log2990/projet2/drawing/drawing/test')
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(aboutMessage);
            });
    });

    it('should catch error when rejected for delete of one drawing', async () => {
        databaseService.deleteDrawing.rejects(new Error('error'));
        return supertest(app)
            .delete('/log2990/projet2/drawing/drawing/test')
            .expect(HTTP_BAD_REQUEST);
    });

    it('should return message from database service on valid patch request to update one drawing', async () => {
        const aboutMessage = {};
        databaseService.modifyDrawing.resolves(aboutMessage);
        return supertest(app)
            .patch('/log2990/projet2/drawing/drawing/test')
            .expect(HTTP_STATUS_OK)
            .then((response: any) => {
                expect(response.body).to.deep.equal(aboutMessage);
            });
    });

    it('should catch error when rejected for update of one drawing', async () => {
        databaseService.modifyDrawing.rejects(new Error('error'));
        return supertest(app)
            .patch('/log2990/projet2/drawing/drawing/test')
            .expect(HTTP_NOT_FOUND);
    });
});
