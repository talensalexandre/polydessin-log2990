import { Injectable, Renderer2 } from '@angular/core';
import { ChangeType } from 'src/app/interfaces-enums/change-type';
import { ColorChoice } from '../../interfaces-enums/color-choice';
import { Coordinates } from '../../interfaces-enums/coordinates';
import { ClickerHandlerService } from '../clicker-handler/clicker-handler.service';
import { ColorManagerService } from '../color-manager/color-manager.service';
import { SvgHandlerService } from '../svg-handler/svg-handler.service';
import { UndoRedoService } from '../undo-redo/undo-redo.service';

@Injectable({
    providedIn: 'root'
})

export class DrawingService {
    private renderer: Renderer2;

    constructor(
        private svgHandler: SvgHandlerService,
        private colorManager: ColorManagerService,
        private clickerHandler: ClickerHandlerService,
        private undoRedoService: UndoRedoService
    ) {
        this.renderer = this.svgHandler.renderer;
    }

    createPolyLine(coordinates: Coordinates, size: string, shouldAddSelection: boolean = true): SVGElement {
        const polyline = this.renderer.createElement('svg:polyline', this.svgHandler.SVG_LINK);
        this.renderer.setAttribute(polyline, 'stroke-width', size);
        this.renderer.setStyle(polyline, 'stroke-linecap', 'round');
        this.renderer.setStyle(polyline, 'stroke-linejoin', 'round');
        this.renderer.setStyle(polyline, 'fill', 'none');
        this.renderer.setAttribute(polyline, 'points',
        String(coordinates.x) + ',' + String(coordinates.y));
        this.addCoordinatesToPolyline(polyline, coordinates);
        this.renderer.appendChild(this.svgHandler.svgCanvas, polyline);
        this.svgHandler.drawingOnCanvas = true;
        if (shouldAddSelection) {
            this.undoRedoService.updateCurrent(new Array(polyline), ChangeType.addToCanvas);
            this.renderer.setAttribute(polyline, 'stroke', this.colorManager.colorSelected[ColorChoice.mainColor].inString);
            this.renderer.setAttribute(polyline, 'typeElement', 'strokeElement');
            this.renderer.setAttribute(polyline, 'size', String(size));
            this.clickerHandler.addSelectionStyles(polyline);
        }
        return polyline;
    }

    addCoordinatesToPolyline(polyline: SVGElement, coordinates: Coordinates): void {
        const newPoints = polyline.getAttribute('points') + ' ' + String(coordinates.x) + ',' + String(coordinates.y);
        this.renderer.setAttribute(polyline, 'points', newPoints);
        this.svgHandler.drawingOnCanvas = true;
    }

    deleteLastSegmentFromPolyline(polyline: SVGElement): void {
        let points = polyline.getAttribute('points');
        if (points !== null) {
            points = points.substr(0, points.lastIndexOf(' '));
            this.renderer.setAttribute(polyline, 'points', points);
        }
    }

    getLastCoordinatesOfPolyline(polyline: SVGElement): Coordinates {
        let points = polyline.getAttribute('points');
        if (points !== null) {
            points = points.substr(points.lastIndexOf(' ') + 1);
            const xCoord = points.substr(0, points.indexOf(','));
            const yCoord = points.substr(points.indexOf(',') + 1);
            return {x: Number(xCoord), y: Number(yCoord)};
        }
        return {x: 0, y: 0};
    }

    applyFilterToPolyLine(polyline: SVGElement, filter: string): void {
        this.renderer.setAttribute(polyline, 'filter', 'url(#' + filter + ')');
    }

    removeElement(element?: SVGElement): void {
        if (element !== undefined) {
            this.renderer.removeChild(this.svgHandler.svgCanvas, element);
        }
    }
}
