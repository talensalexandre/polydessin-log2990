import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule, MatFormFieldModule, MatIconModule, MatInputModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickerHandlerService } from 'src/app/services/clicker-handler/clicker-handler.service';
import { SaveManagerService } from 'src/app/services/save-manager/save-manager.service';
import { ColorComponent } from '../color/color.component';
import { ColorApplicatorComponent } from './color-applicator.component';

describe('ColorApplicatorComponent', () => {
    let component: ColorApplicatorComponent;
    let fixture: ComponentFixture<ColorApplicatorComponent>;
    let clickerHandlerSpy: jasmine.SpyObj<ClickerHandlerService>;
    let saveManagerSpy: jasmine.SpyObj<SaveManagerService>;

    beforeEach(() => {
        clickerHandlerSpy = jasmine.createSpyObj(
            'ClickerHandlerService',
            [
                'inApplicator'
            ]
        );
        saveManagerSpy = jasmine.createSpyObj(
            'SaveManagerService',
            [
                'inApplicator'
            ]
        );
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:
            [
                MatFormFieldModule, MatInputModule, BrowserAnimationsModule,
                FormsModule, MatSelectModule, MatIconModule, MatDialogModule
            ],
            declarations: [ ColorApplicatorComponent, ColorComponent ],
            providers:
            [
                {provide: ClickerHandlerService, useValue: clickerHandlerSpy},
                {provide: SaveManagerService, useValue: saveManagerSpy}
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColorApplicatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
