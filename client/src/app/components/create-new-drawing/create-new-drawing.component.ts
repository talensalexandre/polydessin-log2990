import { Component, EventEmitter, HostListener, OnInit} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ColorChoice } from 'src/app/interfaces-enums/color-choice';
import { RGBA } from 'src/app/interfaces-enums/rgba';
import { ColorManagerService } from 'src/app/services/color-manager/color-manager.service';
import { CreateNewDrawingService } from 'src/app/services/create-new-drawing/create-new-drawing.service';
import { ColorPopupComponent } from '../color-components/color-popup/color-popup.component';

const COLOR_DIALOG_WIDTH = '500px';

@Component({
    selector: 'app-create-new-drawing',
    templateUrl: './create-new-drawing.component.html',
    styleUrls: ['./create-new-drawing.component.scss']
})
export class CreateNewDrawingComponent implements OnInit {
    width: number;
    height: number;
    isFromEntryPoint: boolean;
    private innerWidth: number;
    private innerHeight: number;
    backgroundColor: RGBA;
    readonly openEntryPoint: EventEmitter<undefined> = new EventEmitter();
    readonly backgroundDefaultColor: RGBA = {
        Dec: {
            Red: 255,
            Green: 255,
            Blue: 255,
            Alpha: 1
        },
        Hex : {
            Red: 'ff',
            Green: 'ff',
            Blue: 'ff'
        },
        inString: 'rgba(255, 255, 255, 1)'
    };

    @HostListener('window:resize') onResize(): void {
        const newInnerDimensions = this.newDrawingService.findInnerDimensions(window.innerWidth,
                                                                              window.innerHeight);
        this.innerWidth = newInnerDimensions.x;
        this.innerHeight = newInnerDimensions.y;
        const newDimensions = this.newDrawingService.updateDimensions({x: this.width, y: this.height},
                                                                      {x: this.innerWidth, y: this.innerHeight});
        this.width = newDimensions.x;
        this.height = newDimensions.y;
    }

    constructor(public dialogRef: MatDialogRef<CreateNewDrawingComponent>,
                public newDrawingService: CreateNewDrawingService,
                public dialog: MatDialog,
                private colorManager: ColorManagerService) {
        this.newDrawingService.dimensionsChanged = false;
        this.newDrawingService.openConfirmation = false;
        this.isFromEntryPoint = true;
        this.backgroundColor = this.colorManager.colorSelected[ColorChoice.backgroundColor];
    }

    ngOnInit(): void {
        const newDimensions = this.newDrawingService.findInnerDimensions(window.innerWidth,
                                                                         window.innerHeight);
        this.width = newDimensions.x;
        this.height = newDimensions.y;
        this.colorManager.colorSelected[ColorChoice.backgroundColor] = this.backgroundDefaultColor;
        this.backgroundColor = this.backgroundDefaultColor;
    }

    closeDialog(): void {
        this.dialogRef.close();
        this.openEntryPoint.emit(undefined);
    }

    createNewDrawing(): void {
        const shouldClose = this.newDrawingService.createNewDrawing(
            this.width,
            this.height,
            this.colorManager.colorSelected[ColorChoice.backgroundColor].inString,
            this.dialogRef
        );
        if (shouldClose) {
            this.dialogRef.close();
        }
    }

    confirmDrawing(): void {
        this.dialogRef.close();
        this.newDrawingService.createCanvas();
    }

    verifyDimensions(): void {
        const dimensions = this.newDrawingService.verifyDimensions({x: Math.round(this.width),
                                                                    y: Math.round(this.height)});
        this.width = dimensions.x;
        this.height = dimensions.y;
    }

    openColorPicker(): void {
        this.dialog.open(ColorPopupComponent, {
            width: COLOR_DIALOG_WIDTH,
            data: ColorChoice.backgroundColor
        });
    }

    increaseHeight(): void {
        this.height++;
        this.verifyDimensions();
    }

    decreaseHeight(): void {
        this.height--;
        this.verifyDimensions();
    }

    increaseWidth(): void {
        this.width++;
        this.verifyDimensions();
    }

    decreaseWidth(): void {
        this.width--;
        this.verifyDimensions();
    }
}
